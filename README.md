<img src="https://gitlab.com/hoppr/hoppr/-/raw/dev/media/hoppr-repo-banner.png" />
<br />
<br />

# What is Hoppr?

Hoppr is a Python plugin-based framework for collecting, processing, and bundling your software supply chain.
Feed Hoppr your [CycloneDX-spec SBOMs](https://cyclonedx.org/specification/overview/) (Software Bill of Materials) and receive enhanced SBOMs and component bundles in return!  Those are just the basics, learn more at our:

- **Documentation**: <https://hoppr.dev>
- **Source Code**: <https://gitlab.com/hoppr/hoppr>

# Getting Started

## Basic Install [from PyPI](https://pypi.org/project/hoppr/)
```
pip install hoppr
```

## Quickstart Tutorial

Once you have Hoppr installed, we recommend the ["Your First Bundle" tutorial](https://hoppr.dev/docs/using-hoppr/tutorials/your-first-bundle) to understand the basics.

# Join Us!

We're completely open source, [MIT licensed](LICENSE), and community friendly. Built with a plugin architecture, Hoppr enables users to extend its SBOM-processing capabilities through their own plugins and algorithms.  
[Learn more about how to contribute](https://hoppr.dev/docs/category/contribute-to-hoppr), it's easy and we've got
a welcoming community!