## [1.8.2](https://gitlab.com/hoppr/hoppr/compare/v1.8.1...v1.8.2) (2023-04-18)


### Bug Fixes

* Apply MR fixes ([c88c7ec](https://gitlab.com/hoppr/hoppr/commit/c88c7ec717101c802fc1d9ed7b3f1dc96085fc47))
* Correct coverage badge ([6e040be](https://gitlab.com/hoppr/hoppr/commit/6e040be1f3856bf975c0a172cdf9f526bef089af))
* Correct coverage regex ([d1d748e](https://gitlab.com/hoppr/hoppr/commit/d1d748e1f31fb8a35565dfd8a6c3509eaf5eaca2))
* invalid or missing URL scheme ([1b8313e](https://gitlab.com/hoppr/hoppr/commit/1b8313e70aff649525c64f90dc065c1e48696359))
* Merge main into dev ([083c046](https://gitlab.com/hoppr/hoppr/commit/083c046dfed4af8bd9d7669b4e07d8656fa68d07))
* Only release if release version is set ([356b4f6](https://gitlab.com/hoppr/hoppr/commit/356b4f673350b52926c5568032f3e06e951e49ee))
* Only sign and push to dockerhub if RELEASE_VERSION populated ([a4e32a0](https://gitlab.com/hoppr/hoppr/commit/a4e32a08d961aa12ba6c720b0ee81d61827a24be))
* URL strings for commands ([4e4f1c1](https://gitlab.com/hoppr/hoppr/commit/4e4f1c12a23ae8a8189ecb119d2ba36305fcbcc7))
* URL trailing slash handling ([8a0b7ec](https://gitlab.com/hoppr/hoppr/commit/8a0b7ec609d716457de9439bef2d586493bf8b4b))
* v1.8.2 Release Branch ([6088524](https://gitlab.com/hoppr/hoppr/commit/60885244aa8f85949194415611af72d7d1da1c45))

## [1.8.2-dev.4](https://gitlab.com/hoppr/hoppr/compare/v1.8.2-dev.3...v1.8.2-dev.4) (2023-04-18)


### Bug Fixes

* Merge main into dev ([083c046](https://gitlab.com/hoppr/hoppr/commit/083c046dfed4af8bd9d7669b4e07d8656fa68d07))
* v1.8.2 Release Branch ([6088524](https://gitlab.com/hoppr/hoppr/commit/60885244aa8f85949194415611af72d7d1da1c45))

## [1.8.2-dev.3](https://gitlab.com/hoppr/hoppr/compare/v1.8.2-dev.2...v1.8.2-dev.3) (2023-04-18)


### Bug Fixes

* Only release if release version is set ([356b4f6](https://gitlab.com/hoppr/hoppr/commit/356b4f673350b52926c5568032f3e06e951e49ee))
* Apply MR fixes ([c88c7ec](https://gitlab.com/hoppr/hoppr/commit/c88c7ec717101c802fc1d9ed7b3f1dc96085fc47))
* Correct coverage regex ([d1d748e](https://gitlab.com/hoppr/hoppr/commit/d1d748e1f31fb8a35565dfd8a6c3509eaf5eaca2))
### Bug Fixes

* invalid or missing URL scheme ([1b8313e](https://gitlab.com/hoppr/hoppr/commit/1b8313e70aff649525c64f90dc065c1e48696359))
* URL strings for commands ([4e4f1c1](https://gitlab.com/hoppr/hoppr/commit/4e4f1c12a23ae8a8189ecb119d2ba36305fcbcc7))
* URL trailing slash handling ([8a0b7ec](https://gitlab.com/hoppr/hoppr/commit/8a0b7ec609d716457de9439bef2d586493bf8b4b))

## [1.8.2-dev.1](https://gitlab.com/hoppr/hoppr/compare/v1.8.1...v1.8.2-dev.1) (2023-04-17)


### Bug Fixes

* Apply MR fixes ([c88c7ec](https://gitlab.com/hoppr/hoppr/commit/c88c7ec717101c802fc1d9ed7b3f1dc96085fc47))
* Correct coverage badge ([6e040be](https://gitlab.com/hoppr/hoppr/commit/6e040be1f3856bf975c0a172cdf9f526bef089af))
* Correct coverage regex ([d1d748e](https://gitlab.com/hoppr/hoppr/commit/d1d748e1f31fb8a35565dfd8a6c3509eaf5eaca2))
* Only sign and push to dockerhub if RELEASE_VERSION populated ([a4e32a0](https://gitlab.com/hoppr/hoppr/commit/a4e32a08d961aa12ba6c720b0ee81d61827a24be))

## [1.8.1-dev.7](https://gitlab.com/hoppr/hoppr/compare/v1.8.1-dev.6...v1.8.1-dev.7) (2023-04-17)


### Bug Fixes

* Apply MR fixes ([c88c7ec](https://gitlab.com/hoppr/hoppr/commit/c88c7ec717101c802fc1d9ed7b3f1dc96085fc47))
* Correct coverage badge ([6e040be](https://gitlab.com/hoppr/hoppr/commit/6e040be1f3856bf975c0a172cdf9f526bef089af))
* Correct coverage regex ([d1d748e](https://gitlab.com/hoppr/hoppr/commit/d1d748e1f31fb8a35565dfd8a6c3509eaf5eaca2))
* Correct sed command in build ([c4bdd01](https://gitlab.com/hoppr/hoppr/commit/c4bdd0147769495804367204ec2ff1412ba0d7ab))
* Correct trailing newline ([5a48644](https://gitlab.com/hoppr/hoppr/commit/5a486443b3523be29313e86ff8ad0bb36fbecf41))
* instances of SecretStr treated as string ([5b5266d](https://gitlab.com/hoppr/hoppr/commit/5b5266d9677e1153f0d5dc1796f59a7f45a25405))
### Bug Fixes

* Correct trailing newline ([5a48644](https://gitlab.com/hoppr/hoppr/commit/5a486443b3523be29313e86ff8ad0bb36fbecf41))
* Merge main into dev ([d098528](https://gitlab.com/hoppr/hoppr/commit/d098528c2d0e7d77bfee8c8e60ab28b2304de0cf))

## [1.8.0](https://gitlab.com/hoppr/hoppr/compare/v1.7.2...v1.8.0) (2023-04-11)


### Features

* --no-strict CLI flag ([05cf05c](https://gitlab.com/hoppr/hoppr/commit/05cf05caea72c11823c635e391a8dd10eb6bd0d4))
* --no-strict CLI flag ([12bc0d3](https://gitlab.com/hoppr/hoppr/commit/12bc0d3200a5f19cf5c04b0cf8eb8423f9c00c4c))
* add composite collector ([47037b8](https://gitlab.com/hoppr/hoppr/commit/47037b89e38e8694d803309eed486245a461a4b2))
* add delta_sbom capability ([c73e081](https://gitlab.com/hoppr/hoppr/commit/c73e0816444cefa4547917da54dc650e7777d7da))
* add delta_sbom capability ([a7d265c](https://gitlab.com/hoppr/hoppr/commit/a7d265ccbe6254a78f5cf5e1b9808c7a3eb570a5))
* add delta_sbom capability ([5bb5a59](https://gitlab.com/hoppr/hoppr/commit/5bb5a599d0c111399101f5e676f20e19c1954022))
* add delta_sbom capability ([9a39d88](https://gitlab.com/hoppr/hoppr/commit/9a39d88b53f1f277aecfa1c6d2edefe82098e801))
* add nexus search collector ([7ccb05e](https://gitlab.com/hoppr/hoppr/commit/7ccb05e01300cb9dde702185d28bc2941c83c3fc))
* allow docker collector to use docker.io when --no-strict option is set ([1163405](https://gitlab.com/hoppr/hoppr/commit/116340559d89fa8f6618dc48b524ff6b0a42c66c))
* creation of in-toto attestations ([b58a973](https://gitlab.com/hoppr/hoppr/commit/b58a973e9b7bd5a3d52e5f23d4cdd97155b5143e))
* Report Generation Plugin ([126a0e5](https://gitlab.com/hoppr/hoppr/commit/126a0e5430be13824e67037225b617f24965f79c))
* skip collecting components with a scope of excluded ([e2fd680](https://gitlab.com/hoppr/hoppr/commit/e2fd680389100d216089a0a1aeedcbf4121b635b))


### Bug Fixes

* Add additional configuration capability ([dc14c57](https://gitlab.com/hoppr/hoppr/commit/dc14c5728f5869f9f47f0f2e18cc1b01812e6201))
* Add additional fixes per MR feedback ([598b9f1](https://gitlab.com/hoppr/hoppr/commit/598b9f1c65bf1dcee2cf31dd84553143be73b25f))
* Add Bot label to renovate MRs ([f43cd65](https://gitlab.com/hoppr/hoppr/commit/f43cd65250b65e93eb1cfa3debdf74a3bad58807))
* Add build arg to dockerfile ([ae46f93](https://gitlab.com/hoppr/hoppr/commit/ae46f93d360ab0639d706036721b48769b4c2feb))
* add collection metadata to sbom for apt components ([b8284f5](https://gitlab.com/hoppr/hoppr/commit/b8284f54fc8d0d193bad0916dc7ce83d7ff97e83))
* add collections params for docker ([228d327](https://gitlab.com/hoppr/hoppr/commit/228d327a31b2a7e043af38a016b98d20825f5348))
* add command line option to override previous collection location in delta_sbom plugin ([b3eb0bd](https://gitlab.com/hoppr/hoppr/commit/b3eb0bd1b1b40c936d34c7075fcf525ddf2e76c7))
* add command line option to override previous collection location in delta_sbom plugin ([3485048](https://gitlab.com/hoppr/hoppr/commit/34850483f44788e7e7cd44965984bba6b6502922))
* add command line option to override previous collection location in delta_sbom plugin ([d818730](https://gitlab.com/hoppr/hoppr/commit/d818730a0b6c4d930cdb220f31c2555a21aff42a))
* add command line option to override previous collection location in delta_sbom plugin ([2331668](https://gitlab.com/hoppr/hoppr/commit/2331668aa793c290764f6107dcfbe5f787dbba04))
* Add curl for oras test ([804f8b0](https://gitlab.com/hoppr/hoppr/commit/804f8b0b3d665d40e5299d21ec84f847a2a6e7d1))
* add delivered_sbom to context, work from that variable ([07b6ba9](https://gitlab.com/hoppr/hoppr/commit/07b6ba93a7d5b5b939ad53e4ad0d408276cd337a))
* Add entrypoint to hoppr ([073fdeb](https://gitlab.com/hoppr/hoppr/commit/073fdebdae77d9db6774ab1b84c2a336df68ad52))
* add expected-tar-toc for remaining integration tests ([7bbcac9](https://gitlab.com/hoppr/hoppr/commit/7bbcac9c821b6528edf9f7b247886b11ee18db45))
* add expected-tar-toc for remaining integration tests ([a071522](https://gitlab.com/hoppr/hoppr/commit/a07152294d7430bd552255dd911a9730ae526061))
* add expected-tar-toc to delta integration test ([40fdb97](https://gitlab.com/hoppr/hoppr/commit/40fdb9778b2b20c0b07aa475e5de931a6e4cf227))
* add expected-tar-toc to delta integration test ([33ed419](https://gitlab.com/hoppr/hoppr/commit/33ed41925bedc0258c2bb3a537ccb6a0b4311b39))
* Add hopctl docker image ([1363cec](https://gitlab.com/hoppr/hoppr/commit/1363cec9d1214acfed27c03b3c785fdcf29e7be2))
* Add hopctl docker image ([c4d00fc](https://gitlab.com/hoppr/hoppr/commit/c4d00fc04074112c5fddad91005cb6361ddaf8fc))
* Add hopctl docker image ([5ad0556](https://gitlab.com/hoppr/hoppr/commit/5ad0556d41e665fe20706d6e34833ab835ead8e7))
* add initial checks for BOM access ([d89fed2](https://gitlab.com/hoppr/hoppr/commit/d89fed21d1c807f17d1089e748d38bcb97fc9dde))
* add integration test for deltas ([e0c7e47](https://gitlab.com/hoppr/hoppr/commit/e0c7e47619424601f867040064a5fc6df402e29b))
* add integration test for deltas ([2ca6ed0](https://gitlab.com/hoppr/hoppr/commit/2ca6ed0019f9b21613193a5dd525b470a12a1d56))
* add integration test for deltas ([6789f9c](https://gitlab.com/hoppr/hoppr/commit/6789f9c240a110f10e717dc42c3cff40ba4352e6))
* add integration test for deltas ([c56d066](https://gitlab.com/hoppr/hoppr/commit/c56d0664ddeb49fe6bc4b4cf997ebd782a761163))
* add missing import ([603f219](https://gitlab.com/hoppr/hoppr/commit/603f21910e7d2cbf3f98813ba45a22454330b0ff))
* add missing strict_repos typer argument ([ad9bb37](https://gitlab.com/hoppr/hoppr/commit/ad9bb3736d57cea04ff902aa2241c88ba426d394))
* add mock for os.path.exist ([5463b80](https://gitlab.com/hoppr/hoppr/commit/5463b801539bbf1436e41b0aa0827cb2a54f2b9a))
* Add oci artifacts for reference ([f6661be](https://gitlab.com/hoppr/hoppr/commit/f6661beba1f1fff54a779ea973a5869a72c6616d))
* Add oras integration test ([cbd9376](https://gitlab.com/hoppr/hoppr/commit/cbd937615039f3f99451bf600ae40948e69f0049))
* add purl-type/repo-type mappings to nexus_search collector ([1b3d06a](https://gitlab.com/hoppr/hoppr/commit/1b3d06a04e67dcf0acc4bd2dd55eca07aa282329))
* add pytest-cov package ([a6666a8](https://gitlab.com/hoppr/hoppr/commit/a6666a84c85d0e7939519506226fef23dd05c221))
* add repo to purl type list ([01fa2f2](https://gitlab.com/hoppr/hoppr/commit/01fa2f254d56a06481e4b79e29ecd3f184ce7e04))
* add repository/directory properties to bom for all collectors ([bad3136](https://gitlab.com/hoppr/hoppr/commit/bad3136b6f7dd1fc7579afd5230cd28c5aa26b1b))
* add shared logfile lock ([e350189](https://gitlab.com/hoppr/hoppr/commit/e350189654b9461759e1483458489e42db510f1e))
* add shared logfile lock ([3c3c238](https://gitlab.com/hoppr/hoppr/commit/3c3c23803a56eaad75eaa41090b5ed258393ea0d))
* Add test verification for oras bundle ([77f04b7](https://gitlab.com/hoppr/hoppr/commit/77f04b775b030db593ade6d4437a8e6ff0309be6))
* add version to bom plutin property ([166538c](https://gitlab.com/hoppr/hoppr/commit/166538cf487149e3932a8a243742f8af016db123))
* Added user_env support to find_credentials ([ee58f2d](https://gitlab.com/hoppr/hoppr/commit/ee58f2d48766562152be513083038b226ed9ef98))
* address issue where in-toto was looking maven files ([98fc06a](https://gitlab.com/hoppr/hoppr/commit/98fc06aaeea92bd2eacb459f1d4ca9fa97cf34f0))
* all source distros from manifest repos ([52c8c80](https://gitlab.com/hoppr/hoppr/commit/52c8c8073eabedb81aa93e325c7ee03b465750c1))
* allow more full repository specification for collect_nexus_search ([e2cdf02](https://gitlab.com/hoppr/hoppr/commit/e2cdf027754a2f1568035fe5303a8e62024a00fa))
* allow more full repository specification for collect_nexus_search ([6ebcceb](https://gitlab.com/hoppr/hoppr/commit/6ebccebab92c792fc335ba1251908faf69e77dab))
* allow no scheme for repo URLs as last resort ([866382f](https://gitlab.com/hoppr/hoppr/commit/866382f298a701eadb13cd8b609b74b5e13da88a))
* allow no scheme for repo URLs as last resort ([fd1feec](https://gitlab.com/hoppr/hoppr/commit/fd1feecf559855b4eb3b8127d848f1daf2c72a19))
* allow spaces in stage name ([d7af57f](https://gitlab.com/hoppr/hoppr/commit/d7af57f3892e34fd62b5bfe1dae9f4c5d3704657))
* allow spaces in stage name ([923c043](https://gitlab.com/hoppr/hoppr/commit/923c043b1aa7b686e4777844372272aafb9a7e77))
* append dev instead of current branch name ([d6abddc](https://gitlab.com/hoppr/hoppr/commit/d6abddc66d3b832feb1923d1eb31d0557b03c892))
* Apply correction to git repository collector ([dc522b5](https://gitlab.com/hoppr/hoppr/commit/dc522b555ad2965ba7965f4630e0e1f7d1eb2849))
* apply in-toto suggestions ([3e92177](https://gitlab.com/hoppr/hoppr/commit/3e92177f671c4781907c0a6df279293e543ebb2f))
* apply in-toto suggestions ([401757e](https://gitlab.com/hoppr/hoppr/commit/401757eae524fe76d7eda6f39ab38ff20dd0e01c))
* applying MR suggestion ([c3adeee](https://gitlab.com/hoppr/hoppr/commit/c3adeee1813eba8dd308fec358d27af9f25c1f1d))
* applying MR suggestion ([4e5ca7f](https://gitlab.com/hoppr/hoppr/commit/4e5ca7fb7258c667a431653ab01bb8745a87308f))
* applying MR suggestion ([4e61f84](https://gitlab.com/hoppr/hoppr/commit/4e61f84272b14180e16f6a4baa424f5d60eb2752))
* applying MR suggestion ([b034b72](https://gitlab.com/hoppr/hoppr/commit/b034b7273179a7fb8bd3bda411050d9815b58ce7))
* apt collector _get_download_url_path ([aa988d6](https://gitlab.com/hoppr/hoppr/commit/aa988d680fe350e5716f30abbed0437e51ae9797))
* apt collector _get_download_url_path ([1d36fd4](https://gitlab.com/hoppr/hoppr/commit/1d36fd457fc60d9d9638611e3b91940a2a523194))
* bom helm chart version ([009bdf0](https://gitlab.com/hoppr/hoppr/commit/009bdf00412fe6964b81c15b86aec52a761890df))
* Branch isolation testing ([fcde124](https://gitlab.com/hoppr/hoppr/commit/fcde12425dd610fe3da0dbbd56de6af677a8688e))
* bug with with attestations created from GitLab CI Runners ([f8ca8e7](https://gitlab.com/hoppr/hoppr/commit/f8ca8e79144b6377250d38ccc37bb2f7689d2033))
* casing for "kind" field ([26b0927](https://gitlab.com/hoppr/hoppr/commit/26b0927a143763f688215b0fc3d49e27a56f0b71))
* casing for "kind" field ([533f6a7](https://gitlab.com/hoppr/hoppr/commit/533f6a770bc8738aa39b22c2faa6d262c4b7264d))
* catch exception from _get_required_coverage, check for empty/missing boms ([d905e35](https://gitlab.com/hoppr/hoppr/commit/d905e3557ce6def86b923db7961f3631808f3445))
* check for exception not thrown in pypi success ([0ba41f6](https://gitlab.com/hoppr/hoppr/commit/0ba41f6a9962ede186765b25e7939623fbb15095))
* check tar toc on integration tests ([aafc6bb](https://gitlab.com/hoppr/hoppr/commit/aafc6bb6eb9215e0c158a1de007fc67a8509059e))
* check tar toc on integration tests ([105c87a](https://gitlab.com/hoppr/hoppr/commit/105c87a4c81809c1a586713d42ff1e857e0c260e))
* Clean up artifact name ([a59aaac](https://gitlab.com/hoppr/hoppr/commit/a59aaacc2bb23988b8e240d0f6c237b547e86f39))
* Clean up other variables in ci ([d289184](https://gitlab.com/hoppr/hoppr/commit/d28918482ceb951461725095e72f7ca9ebcb23ce))
* clean up repository_url handling ([c867a3b](https://gitlab.com/hoppr/hoppr/commit/c867a3bda17c70e59e1d50672543077b7daae8cb))
* clean up repository_url handling ([6fb544b](https://gitlab.com/hoppr/hoppr/commit/6fb544b1ceb0846d096cf790efa8f52a7160960c))
* clean up repository_url handling ([95a0288](https://gitlab.com/hoppr/hoppr/commit/95a0288eeecd7c0baf03af5b67dd0d50fb549708))
* clean up repository_url handling ([011e53b](https://gitlab.com/hoppr/hoppr/commit/011e53b8b96760f03863b5a9582f91ad30f98733))
* Clean up rules and workflow ([69e9b73](https://gitlab.com/hoppr/hoppr/commit/69e9b73ebab517658ad0e5238038e01979f6e98a))
* Cleaned up maven command ([2b4af6e](https://gitlab.com/hoppr/hoppr/commit/2b4af6e57658f5f96a930cf11542f802f783476d))
* Cleanup notes in config ([2632bfa](https://gitlab.com/hoppr/hoppr/commit/2632bfabe9794542033ce8b4dd8d6a5cb4e96da8))
* Cleanup rules ([ea79632](https://gitlab.com/hoppr/hoppr/commit/ea7963280418793154b6cd113c926ff160ce011e))
* Cleanup rules ([76d65e5](https://gitlab.com/hoppr/hoppr/commit/76d65e5cf60559f4672f63dd33a62c45f3009eab))
* clear loaded manifests ([39e7341](https://gitlab.com/hoppr/hoppr/commit/39e73413a9e446d1f5b98118ff8b3ec2799260e5))
* code review comments ([c9fa7e2](https://gitlab.com/hoppr/hoppr/commit/c9fa7e2b37422a04c4a2281b81b425d46bcedb12))
* Code review comments ([a204676](https://gitlab.com/hoppr/hoppr/commit/a204676aa26173864856e458a30f119caeb3ae9a))
* complete unit test coverage for collect_nexus_search ([24827e4](https://gitlab.com/hoppr/hoppr/commit/24827e4cab02da2808deb99fd372c6a6b3935ed7))
* component search sequence ([ff89f56](https://gitlab.com/hoppr/hoppr/commit/ff89f5633b386b6b006ff7f76b6558ad894f2bb2))
* component search sequence ([397d3bf](https://gitlab.com/hoppr/hoppr/commit/397d3bf639ed8e729fb4e209078cd0877ae6b07f))
* Correct build artifacts ([40ca47f](https://gitlab.com/hoppr/hoppr/commit/40ca47f2642bd82be5aec18f75a3f468c1d880e7))
* Correct deployment teir ([d4282c8](https://gitlab.com/hoppr/hoppr/commit/d4282c8c1e54fe188acfde63a9307c33474c9f8b))
* Correct deployment teir ([983932d](https://gitlab.com/hoppr/hoppr/commit/983932d32f998ca41536002671b324734d711716))
* Correct dockerfile ([30f438a](https://gitlab.com/hoppr/hoppr/commit/30f438a16501280c0dd97633d3e2447e2c80998e))
* Correct git bom, had incorrect purl, name, and version ([e6a1265](https://gitlab.com/hoppr/hoppr/commit/e6a1265e9862aa66996e10a7c956a1315bcfc89b))
* Correct integration tests ([b065953](https://gitlab.com/hoppr/hoppr/commit/b0659532c14ddea25afcb7357d14104427cddefe))
* Correct media types for registry ([d3ea0df](https://gitlab.com/hoppr/hoppr/commit/d3ea0df0cd2eb276eb86d1cb82dfceeb39604e71))
* Correct release yaml file ([a29c26c](https://gitlab.com/hoppr/hoppr/commit/a29c26c86220443c22e94954bd6db5d3e7a59147))
* Correct releaserc file ([8868fab](https://gitlab.com/hoppr/hoppr/commit/8868fab85c0ed587f8d9679af6c8d793ed78c50b))
* Correct releaserc.yml ([4b55f7f](https://gitlab.com/hoppr/hoppr/commit/4b55f7f25895b1251ab7e9499280848d86732cbb))
* Correct requirements ([7598beb](https://gitlab.com/hoppr/hoppr/commit/7598beb540c9c26860501b42ba92fb807f79fd33))
* Correct simlinks in ci docker ([deb0728](https://gitlab.com/hoppr/hoppr/commit/deb07284c27603325678ae3332f0a2023ed42f80))
* Correct syntax in ci docker ([f96c938](https://gitlab.com/hoppr/hoppr/commit/f96c938502b98e0bdbe4096ed6a35f830abaf378))
* Correct tests so they pass ([eeebaef](https://gitlab.com/hoppr/hoppr/commit/eeebaef2b781cde5e668d0d75e65977185d56d45))
* Correct the oras binary arch type ([71bc21e](https://gitlab.com/hoppr/hoppr/commit/71bc21ec0b5308c4299906ab04b270b678480551))
* Correct the wheel name ([4847b4a](https://gitlab.com/hoppr/hoppr/commit/4847b4a8548c563ad4298616f8c16288ca925aaa))
* Correct trivy timeout ([5a9246a](https://gitlab.com/hoppr/hoppr/commit/5a9246ae86bb2e517cb39c3c7b5d240a9fdb8bec))
* Corrected Maven-Dependency-Plugin arguments so that maven artifacts would be bundled ([9fe9eaa](https://gitlab.com/hoppr/hoppr/commit/9fe9eaa13c68ed8d36437a6662f735c347018ed4))
* credential.find method no longer needs exact match.  added lines to _run_data_ metadata file. ([202b0ae](https://gitlab.com/hoppr/hoppr/commit/202b0ae90ba70cf8d45990064e4b3d2ce6b9fb4a))
* Cut release from next branch ([17d0d03](https://gitlab.com/hoppr/hoppr/commit/17d0d03ac243fea51d9ea98738d0e8293c60803d))
* Cut release from next branch ([e1c56d2](https://gitlab.com/hoppr/hoppr/commit/e1c56d28394803045b974195e6e7d9cc796c967b))
* **deps:** update dependency hoppr-cyclonedx-models to v0.2.10 ([ee37064](https://gitlab.com/hoppr/hoppr/commit/ee370642914f1b507ed4793e11cbc575d1c51720))
* **deps:** update dependency typer to ^0.7.0 ([a35bd85](https://gitlab.com/hoppr/hoppr/commit/a35bd8519c8486b68a9228b660a2d2f44bad7f2c))
* dev branch test; update README ([f8ed5ee](https://gitlab.com/hoppr/hoppr/commit/f8ed5eec1a2b8d68730b88d6d378d132fa67c1c7))
* DNF download directly from found URL ([bf84cec](https://gitlab.com/hoppr/hoppr/commit/bf84cec779df983642116cf99cf42403675f17e9))
* DNF download directly from found URL ([11d5e5e](https://gitlab.com/hoppr/hoppr/commit/11d5e5ec6280b60d821880833d5ee7f0d3683432))
* do not re-generate consolidated/delivered sboms ([ee1928b](https://gitlab.com/hoppr/hoppr/commit/ee1928b6198e0fb57c3eb56af0dc3b5c057166d2))
* do not re-generate consolidated/delivered sboms ([ce59dcc](https://gitlab.com/hoppr/hoppr/commit/ce59dcc46fbbef3f3941c0f33c3d0cc0c0abacf7))
* Don't run pipelines on merge event ([a0b8c53](https://gitlab.com/hoppr/hoppr/commit/a0b8c53add628e08a0c02f30beea57a399b9f109))
* Ensure python is on the path ([a1c03eb](https://gitlab.com/hoppr/hoppr/commit/a1c03eb6e2e607267886890831c2675572525d6b))
* enum base type ([4e3577c](https://gitlab.com/hoppr/hoppr/commit/4e3577c8b96bd5c107649c3d6adb6ef1dd34e4d1))
* exclude nulls from output sboms ([e41bd52](https://gitlab.com/hoppr/hoppr/commit/e41bd52190ec8acba3fdd419650674e7739224f7))
* expected apt SBOM ([c6b01e9](https://gitlab.com/hoppr/hoppr/commit/c6b01e935d7aadf1f2fe3410edf901d95c9f8fb3))
* expected apt SBOM ([8e7d3da](https://gitlab.com/hoppr/hoppr/commit/8e7d3daaab31eccc1686963d30bbbd72c70f6bf0))
* expected metadata source location ([53f74d7](https://gitlab.com/hoppr/hoppr/commit/53f74d7f154df37e45d91e782bfbee5f30570fd9))
* expected metadata source location ([f918d63](https://gitlab.com/hoppr/hoppr/commit/f918d63976bfb5ac57405ea48b30c9b34e8fe0a6))
* expected-tar-toc sort order changed ([b89f853](https://gitlab.com/hoppr/hoppr/commit/b89f853e5547aac8e0a69b7245f8f3abddc5d70f))
* fail on no change, improved status messages ([801dea2](https://gitlab.com/hoppr/hoppr/commit/801dea20d82167b303ebbd0a339cb8e408c9b873))
* fail on no change, improved status messages ([1a4c87c](https://gitlab.com/hoppr/hoppr/commit/1a4c87ceeceaabbe3ecf13e905fdd9a08866e647))
* fail on no change, improved status messages ([fa610d0](https://gitlab.com/hoppr/hoppr/commit/fa610d09a73bf6736e9792096614e08a84a6c8aa))
* fail on no change, improved status messages ([69d15b1](https://gitlab.com/hoppr/hoppr/commit/69d15b1c4e30684fe59020b96b002a7f1d27c30c))
* fast forward branch ([1428ecb](https://gitlab.com/hoppr/hoppr/commit/1428ecb2ff108f25ca0c424f53984771a16e8e8e))
* Fixed Docker repo:tag information being lost in collection ([5111f7f](https://gitlab.com/hoppr/hoppr/commit/5111f7fe86bd2c833ac4c0bb0392932b599f6549))
* Fixed Docker repo:tag information being lost in collection ([3319a36](https://gitlab.com/hoppr/hoppr/commit/3319a36bd90ca7798141ee0e7fe92bb6c361559d))
* fixed issue with bundle options for functionary_key ([d798606](https://gitlab.com/hoppr/hoppr/commit/d7986062051b5b0be426dda0842acb1bab601ec2))
* Fixed the releaseing issue ([ce8b2f5](https://gitlab.com/hoppr/hoppr/commit/ce8b2f57703bfaf0984e2b750005f866a8ad8b35))
* Fixed type-check findings ([7bb21df](https://gitlab.com/hoppr/hoppr/commit/7bb21df1b19f3f2ed8f275db995c722db9e78ed0))
* Fixed unit tests ([4e80266](https://gitlab.com/hoppr/hoppr/commit/4e8026652458dc5c35bdc3dfc5a983727e5bf44f))
* Fixes [#150](https://gitlab.com/hoppr/hoppr/issues/150) and corrected when consolidated and deliveried SBOMs are written. It also address additional unit testing ([5adf219](https://gitlab.com/hoppr/hoppr/commit/5adf2193b30af0b6aba0c901dd369df324d53e19))
* Fixing Merge Conflicts ([d24cff2](https://gitlab.com/hoppr/hoppr/commit/d24cff258f10542bdc9b1af462c3a61bc1af2722))
* generate dev version if not on main/dev ([3aa6ec5](https://gitlab.com/hoppr/hoppr/commit/3aa6ec50f186ee520eb1279160c8b1a0715711b2))
* Get better results ([688e176](https://gitlab.com/hoppr/hoppr/commit/688e1766cffb34cc155e0ea0c3f1d7a608c58947))
* gitattributes ([5d3bde3](https://gitlab.com/hoppr/hoppr/commit/5d3bde370513376a36b9a0cc7279b9464fd8a09a))
* Grab versioning properly ([cc052cf](https://gitlab.com/hoppr/hoppr/commit/cc052cfcedbd149b295e090c493adb650b47f1ce))
* have nexus collector respect user-specified purl types ([9116a03](https://gitlab.com/hoppr/hoppr/commit/9116a034125ccbe2eb47d06e0172176e3eae2d86))
* helm collector append purl name ([5574739](https://gitlab.com/hoppr/hoppr/commit/5574739d2e34ba0a538ee8295939e2ad473144f5))
* hoppr group ([c21bc29](https://gitlab.com/hoppr/hoppr/commit/c21bc295fb9ab40756e107b5047e2cb3d36d8a06))
* image tag reset after rebase ([45a850a](https://gitlab.com/hoppr/hoppr/commit/45a850a4c43f29c7253c4c97620c1fc6cce32fb5))
* import source type error ([464211c](https://gitlab.com/hoppr/hoppr/commit/464211cddc9f7ea6c7a5182449155e4c7467289c))
* Improve config error logic in git collector ([cb3681a](https://gitlab.com/hoppr/hoppr/commit/cb3681a6f95c8cc031ca69276f84015925659296))
* Improve matching pattern and logging ([12a68eb](https://gitlab.com/hoppr/hoppr/commit/12a68eb3ab2e049260bba97ddf8f7c0fc585a45d))
* include resources folder in poetry build ([5ae0ffb](https://gitlab.com/hoppr/hoppr/commit/5ae0ffb5d761acd41c7fafd6a0cfa43f40d7eafd))
* included manifest repo merge, add tests ([359364c](https://gitlab.com/hoppr/hoppr/commit/359364c453f72cc35cafd752b0b21a7aac79e0c3))
* included manifest repo merge, add tests ([1c22c6e](https://gitlab.com/hoppr/hoppr/commit/1c22c6eec5ed7c3c1dc01e3d70e6241f0da7f7ee))
* increase minimum unit test coverage to 100% ([9c697e3](https://gitlab.com/hoppr/hoppr/commit/9c697e35712ee8e8138848f506de71ff7054d71b))
* lint error ([990ef3f](https://gitlab.com/hoppr/hoppr/commit/990ef3ff6d79359c45481f0deeb640a4ec629ccb))
* lint error ([f9f2cc0](https://gitlab.com/hoppr/hoppr/commit/f9f2cc0095060a84642717a0d77e28d26d97467b))
* linting union for type hints ([bbaf8f0](https://gitlab.com/hoppr/hoppr/commit/bbaf8f0f688c30d206bc1842cd19b616bda0b949))
* main module exits using sys instead of typer ([356112e](https://gitlab.com/hoppr/hoppr/commit/356112e622d329d53a0902532476957f92790491))
* main process logfile lock ([4519f53](https://gitlab.com/hoppr/hoppr/commit/4519f53808064a9829f46bfec19f06156966a59c))
* Make a quicklink script for linking python in dockerfile ([ae3dace](https://gitlab.com/hoppr/hoppr/commit/ae3dace2b4101bc9c73381dcf097baf505965e25))
* Make semantic release pass all jobs ([ddc3c49](https://gitlab.com/hoppr/hoppr/commit/ddc3c495048e8be5302bf9c7ba7a4ad5418944a7))
* manifest helm repo URL ([438fae5](https://gitlab.com/hoppr/hoppr/commit/438fae572062f9726c451031a88e403a67390c99))
* manual version revs because lock file conflicts spam ([e8a473f](https://gitlab.com/hoppr/hoppr/commit/e8a473ffd01938734a7a84180e78d075927240c4))
* merge components, add verification tests ([f0ecf1e](https://gitlab.com/hoppr/hoppr/commit/f0ecf1e7f7471b17dbbf42e6aa80e8dedc46f76b))
* merge components, add verification tests ([ce6e4d1](https://gitlab.com/hoppr/hoppr/commit/ce6e4d17cd8ce4d1127956d1f72b2f346ee7402a))
* Merge dev into next ([765d25a](https://gitlab.com/hoppr/hoppr/commit/765d25a29cc9d3fed6794a01d25892118408e177))
* Merge main into branch ([e2fd972](https://gitlab.com/hoppr/hoppr/commit/e2fd97246baadff00a84b1fecb0b8a79412e70ee))
* **Minor:** Image build cleanup ([ba4e6c3](https://gitlab.com/hoppr/hoppr/commit/ba4e6c3bc68fe469b3ad68ed636f43b55c84f425))
* missing `rev` command in skopeo image ([cb38a3e](https://gitlab.com/hoppr/hoppr/commit/cb38a3e8ae5dc2afcac4bdbb87f110d6af58997e))
* missing collector plugin CLI tools ([b1284ba](https://gitlab.com/hoppr/hoppr/commit/b1284ba6c1df27487e36279c5af5478b27450322))
* move insertion of required stages to Transfer object creation ([fa0d31b](https://gitlab.com/hoppr/hoppr/commit/fa0d31b99fcc55411cc2fd32bcfb7f050b14fba9))
* move insertion of required stages to Transfer object creation ([a7da1d5](https://gitlab.com/hoppr/hoppr/commit/a7da1d53253de835a764f3d2ab68f052faa03aa7))
* move insertion of required stages to Transfer object creation ([57604d5](https://gitlab.com/hoppr/hoppr/commit/57604d5e724fadcd71694b466a82b19a9696d624))
* move insertion of required stages to Transfer object creation ([53243ab](https://gitlab.com/hoppr/hoppr/commit/53243abcb44c5c031af5c1cc625b8cf81e2b3954))
* moved test_main.py under test/unit/ directory ([90df7ad](https://gitlab.com/hoppr/hoppr/commit/90df7adb7fd293b7b3da96ac70ca159fe0d8f480))
* mypy errors ([b08426c](https://gitlab.com/hoppr/hoppr/commit/b08426ca5b7af32c8d192d2309381b19d03d136d))
* mypy errors ([d94d090](https://gitlab.com/hoppr/hoppr/commit/d94d090f231edaa21de806bc8ada8743326c57c7))
* mypy errors ([d612474](https://gitlab.com/hoppr/hoppr/commit/d612474d5f71b79683280781ca3cda18ee05d240))
* New branch old issue ([a906ca3](https://gitlab.com/hoppr/hoppr/commit/a906ca3cc6afc01f97483e136f9b3370312bb5bb))
* Only attempt PyPI source collect if whl not collected ([31de8ae](https://gitlab.com/hoppr/hoppr/commit/31de8ae07d414c09295db59662442ee7a15f92cc))
* only load purl-type-specific plugins when components of that type are being processed (Issue [#77](https://gitlab.com/hoppr/hoppr/issues/77)) ([1789f21](https://gitlab.com/hoppr/hoppr/commit/1789f21df784738c58b8ea3dac19e50240a2b13b))
* Only publish if there's a new release from semver dryrun ([da88e9b](https://gitlab.com/hoppr/hoppr/commit/da88e9b4a2e6449ea5550f8adcffceb975756af4))
* only rename maven file on successful collection ([3455e74](https://gitlab.com/hoppr/hoppr/commit/3455e743e92fa74ed1d8f82e66921dd41c837c63))
* Only run semantic release publish on develop and main ([0ecc904](https://gitlab.com/hoppr/hoppr/commit/0ecc904fb1583a9f530d4a469a331391007e70d1))
* parameter name typo ([9d7c3b1](https://gitlab.com/hoppr/hoppr/commit/9d7c3b1c0751816f5c108f3fb83eaec10f47a490))
* pip arguments ([0cc47f1](https://gitlab.com/hoppr/hoppr/commit/0cc47f17f3eefcd4a650d213d7d8701c59485c8b))
* platform check ([de541db](https://gitlab.com/hoppr/hoppr/commit/de541db0bf4a810a158ca175b1c38a573a1ab9e7))
* **plugin:** add type hint to auth ([0bc2e29](https://gitlab.com/hoppr/hoppr/commit/0bc2e29c0e36d0b596cd1d5f493a14566e9bd9dc))
* **plugin:** properly authenticate nexus requests ([be82ed4](https://gitlab.com/hoppr/hoppr/commit/be82ed43660ca25ed2f105a43dbd1603deb4df38))
* prepend stage if needed for deltas ([a0dbfcb](https://gitlab.com/hoppr/hoppr/commit/a0dbfcbb40649182f1fef9ce8cdb1ae5bf151c73))
* prepend stage if needed for deltas ([7bbb2b5](https://gitlab.com/hoppr/hoppr/commit/7bbb2b5d76d6245430e5e587e8e15b64f448c2d5))
* prepend stage if needed for deltas ([e93d3a0](https://gitlab.com/hoppr/hoppr/commit/e93d3a0ff654fe4aae50a96003682dcd6a7d4f74))
* prepend stage if needed for deltas ([2467c6b](https://gitlab.com/hoppr/hoppr/commit/2467c6b693832775d4739d0de41d7fc2059b5296))
* prevent loading plugins that aren't needed ([17722f3](https://gitlab.com/hoppr/hoppr/commit/17722f3238b9c5436549e3c1fbd9e7fb84f49dc3))
* prevent loading plugins that aren't needed ([b00db2b](https://gitlab.com/hoppr/hoppr/commit/b00db2b5925915fa950e14ea565e4496c4bf4aa9))
* processor relative file handling ([59636aa](https://gitlab.com/hoppr/hoppr/commit/59636aaeb06ff4bddd167fdc302b914ca3342cd5))
* processor relative file handling ([5c3cdb1](https://gitlab.com/hoppr/hoppr/commit/5c3cdb18abede20e59e52afdcaeec1de69a8298c))
* proper attestations for nexus-search- and composite- collectors ([a1a9651](https://gitlab.com/hoppr/hoppr/commit/a1a96517eebc432d83153cf4808cc17d40f17f81))
* proper attestations for nexus-search- and composite- collectors ([7ae7d7a](https://gitlab.com/hoppr/hoppr/commit/7ae7d7ae82558a5586740a4dd50a700a6a6e82b6))
* Protect keys ([9001a21](https://gitlab.com/hoppr/hoppr/commit/9001a219a93817d064a5cce207c267347b112549))
* provide better error message on empty config file content ([e87fff8](https://gitlab.com/hoppr/hoppr/commit/e87fff885842320adf85e7c86abb319e8dfa4f2d))
* pylint error ([6982109](https://gitlab.com/hoppr/hoppr/commit/69821095b2319075c65433a1f76e5d4e47fd361b))
* pylint error ([e36b242](https://gitlab.com/hoppr/hoppr/commit/e36b2424739e0dc4814855fb304dca1edd24e448))
* pylint issues fix ([7c7810c](https://gitlab.com/hoppr/hoppr/commit/7c7810c9ac63268472c2faf50e43694bdcd2cccb))
* raw collector stripping purl namespace ([1aca256](https://gitlab.com/hoppr/hoppr/commit/1aca256351c854eaa1c7f02e209961a20ece99aa))
* raw collector stripping purl namespace ([ee20ebe](https://gitlab.com/hoppr/hoppr/commit/ee20ebe02b6c1c7f1b1be9a44b82087565a5a5af))
* raw collector stripping purl namespace ([bba65cb](https://gitlab.com/hoppr/hoppr/commit/bba65cb7805b78a329ee59df1c10b9ad54959f8d))
* raw collector stripping purl namespace ([2c9309d](https://gitlab.com/hoppr/hoppr/commit/2c9309d09150511489185b455cf38d631611820b))
* README feedback; broken link fixes; try PyPI banner fix ([932121d](https://gitlab.com/hoppr/hoppr/commit/932121d087bbd2019007adb576b6c9122d4881fa))
* Rebase dev ([9399451](https://gitlab.com/hoppr/hoppr/commit/9399451c5f23b599d6d54c02538ebe4017a95620))
* redeclare Component attrs with hashable types ([a2034fa](https://gitlab.com/hoppr/hoppr/commit/a2034fa8ffd64f58b832199d0964d43893401962))
* redeclare Component attrs with hashable types ([26bbfc0](https://gitlab.com/hoppr/hoppr/commit/26bbfc064c252ce8b7e65205fe7d0c98987a5a41))
* Reference main so that develop can become default branch ([76f707b](https://gitlab.com/hoppr/hoppr/commit/76f707b6c3435bdc68d73b3717c451c6a6c95369))
* Release dev channel ([42ee862](https://gitlab.com/hoppr/hoppr/commit/42ee86236810072b2d303ea255f2d2556fd53749))
* Release dev channel ([15b4ae7](https://gitlab.com/hoppr/hoppr/commit/15b4ae76944accd1b4f0e6ae379b02c5efabb923))
* Remove blank lines ([196218c](https://gitlab.com/hoppr/hoppr/commit/196218c39d7dbb50c74722922190f61905f46053))
* remove construct method call ([2cf4963](https://gitlab.com/hoppr/hoppr/commit/2cf49637902c87dd786f37485521e8c55617685f))
* Remove gitlab semantic release comment on MRs ([4af82e6](https://gitlab.com/hoppr/hoppr/commit/4af82e6023f842a8587ec5183c5b3a212dc9e5f5))
* Remove licensing scanning and replace with policy ([e852316](https://gitlab.com/hoppr/hoppr/commit/e85231642c2f75a022d3872049b26e349821cc13))
* remove need for boolean from fail-open logic ([23bfa03](https://gitlab.com/hoppr/hoppr/commit/23bfa0397d12d39fcbca526eaadd651805e97c57))
* Remove node engine reference from package.json ([3c26b4e](https://gitlab.com/hoppr/hoppr/commit/3c26b4e768f23e9746db9f4b654a4799cb95e00b))
* Remove pack, and idx from check ([6e24462](https://gitlab.com/hoppr/hoppr/commit/6e244629bf39228cee030550da47863b11c6aec5))
* Remove package.json ([e13de8d](https://gitlab.com/hoppr/hoppr/commit/e13de8d885045b1e750fceeace1e60d4d9d032b6))
* remove prerelease ([1e038d0](https://gitlab.com/hoppr/hoppr/commit/1e038d0b73fe23da781bdea4cb02b9241e19aa8e))
* remove prerelease ([6818939](https://gitlab.com/hoppr/hoppr/commit/6818939d8237876f7dfabdce72a0dab1270db9b3))
* remove problematic git files from tar toc comparison ([b883b23](https://gitlab.com/hoppr/hoppr/commit/b883b23fbf230857a6244b7b8207144e3cc3a920))
* remove problematic git files from tar toc comparison ([89f1621](https://gitlab.com/hoppr/hoppr/commit/89f16211e602d92820cad9719257a57d2bfbcb39))
* Remove quotations around build ([3848f46](https://gitlab.com/hoppr/hoppr/commit/3848f463bb8640e4acd524bfdd04838291e9222d))
* Remove quotations from parallel matrix ([28ee2b7](https://gitlab.com/hoppr/hoppr/commit/28ee2b707fa0f36992f131e7bbb548f164b23e14))
* Remove quotes ([7323a8b](https://gitlab.com/hoppr/hoppr/commit/7323a8b16abc50d284a7d67bf7a346fe0190369c))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([0ac8adc](https://gitlab.com/hoppr/hoppr/commit/0ac8adc4c70bf79e5068daf8f8d5501c045af29a))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([87ae120](https://gitlab.com/hoppr/hoppr/commit/87ae1209adced6c7004143d33e314df719740da1))
* remove references to deleted job ([6d9890e](https://gitlab.com/hoppr/hoppr/commit/6d9890e99f43b439f20505142e2706c1556d5abf))
* remove references to deleted job ([7d1ef14](https://gitlab.com/hoppr/hoppr/commit/7d1ef14d1bb86f478ad35b5c08038b08b1fc916c))
* Remove skip ci and attempt to let workflow rules handle pipelines ([0e325a5](https://gitlab.com/hoppr/hoppr/commit/0e325a5e7a2caea831856e0ba6566f0a5ecaf2c7))
* restore Dockerfile FROM directive ([e7e0622](https://gitlab.com/hoppr/hoppr/commit/e7e06222a9768bfeaab9f10e13a9b213ccd45be7))
* Result constructor param typo ([09290d2](https://gitlab.com/hoppr/hoppr/commit/09290d25bd2be5f07635c718c7691dad6e00e652))
* Revert Changes ([f6797ea](https://gitlab.com/hoppr/hoppr/commit/f6797ea323e79a9bcd8b67b00c4939004e0dafcc))
* revert to older version of npm-check-updates ([9fb3ebf](https://gitlab.com/hoppr/hoppr/commit/9fb3ebf011be353fe119759514868151e0ddf3c2))
* Roll back chore rules ([e40fbfb](https://gitlab.com/hoppr/hoppr/commit/e40fbfbcb463ccc704c351b1acad04f3ea1fb0ea))
* Roll base image back from 9.0 to 8.6 ([31f8166](https://gitlab.com/hoppr/hoppr/commit/31f81666e3bb135ad7ad7b05d28cbc96c77859bc))
* Run if commit message doesn't start with chore ([daa5d11](https://gitlab.com/hoppr/hoppr/commit/daa5d119686ca58488cc04c62d48b73bb9c49319))
* Run pre-commit ([7b740ec](https://gitlab.com/hoppr/hoppr/commit/7b740eca0a6f3bd92d99e7c8f29c3fd435f0df4d))
* Run the entire dockerfile as one large image ([47ec5a5](https://gitlab.com/hoppr/hoppr/commit/47ec5a57dece0be112dca8c068a2807edc811bf8))
* sbom metadata ([eb5225f](https://gitlab.com/hoppr/hoppr/commit/eb5225f8f0729e44701d6b75ce7f74870f7eecbb))
* sbom metadata ([f97e7a7](https://gitlab.com/hoppr/hoppr/commit/f97e7a7acb00e9d361887dfad55c7bc2f03b10f0))
* SBOM spec selection logic ([d12bd8b](https://gitlab.com/hoppr/hoppr/commit/d12bd8ba19c0b5b8ec879d1f8190b8afb999d80b))
* SBOM spec selection logic ([2cd7286](https://gitlab.com/hoppr/hoppr/commit/2cd72866b1ae7a1fda1b580a4f7dbc2942a0da49))
* semantic-release version bump ([d45efb7](https://gitlab.com/hoppr/hoppr/commit/d45efb73d70625bda73ca4c4dfec33698c62957f))
* set file name correctly for collect_maven_plugin ([07e4eb5](https://gitlab.com/hoppr/hoppr/commit/07e4eb55388b25f46fdd2ed7a0af04fa86a8e427))
* set prerelease back to true ([49b130e](https://gitlab.com/hoppr/hoppr/commit/49b130e916dc7b3f9ec9fdaead06ce29e78f32c8))
* Set the correct version of oras ([d8357a4](https://gitlab.com/hoppr/hoppr/commit/d8357a431e33ec0fb45e0fc51537eae603e7cb6a))
* set timeout for run_command to reduce hanging ([9be8afe](https://gitlab.com/hoppr/hoppr/commit/9be8afefc62f1868d908cf8ecb8e9bd211662c37))
* set timetag to utc timezone ([12aa368](https://gitlab.com/hoppr/hoppr/commit/12aa368689a4225fc9662f59624ee90049fb8c4f))
* Setup codequality analytics and build attestation ([c5c4514](https://gitlab.com/hoppr/hoppr/commit/c5c4514e153becdf4a39713259b289c4469a24eb))
* Setup dependencies on test image ([6fabada](https://gitlab.com/hoppr/hoppr/commit/6fabada652d546cd89f1cd16eb32bae2cd0cb08a))
* shared RLock for Docker unit tests ([b2f3c9c](https://gitlab.com/hoppr/hoppr/commit/b2f3c9c5eb31bd31e46940fdf453ff56ae0f54f5))
* Simlink python ([0009483](https://gitlab.com/hoppr/hoppr/commit/0009483d56965e8770ae67383ecc69d4e7a53697))
* Simplify some of the configuration options ([a52594a](https://gitlab.com/hoppr/hoppr/commit/a52594a9e0873392934d6bfdb442ee3633f58a00))
* speed up unit tests ([32f8715](https://gitlab.com/hoppr/hoppr/commit/32f87159f4fc125a338e61fadefb80f5cb306ccb))
* Split out ci config per !184#note_1170995317 ([f42e582](https://gitlab.com/hoppr/hoppr/commit/f42e5825d7a3ebeab60ae107b10624d5567c7173)), closes [184#note_1170995317](https://gitlab.com/hoppr/184/issues/note_1170995317)
* stage name regex field type ([3d4c133](https://gitlab.com/hoppr/hoppr/commit/3d4c13381a43ddbdcbe4629a22ef0f75b7019035))
* strip quotes to compare found URL ([396c31a](https://gitlab.com/hoppr/hoppr/commit/396c31a90846673f991867a2c2057959457222f0))
* syntax ([88b8961](https://gitlab.com/hoppr/hoppr/commit/88b8961dcc41c6a21e02da417a3c059cf0036641))
* syntax ([4e8fc18](https://gitlab.com/hoppr/hoppr/commit/4e8fc18f3bc21f00d60e9a190e94fc7a4a9d9078))
* syntax ([84c2c38](https://gitlab.com/hoppr/hoppr/commit/84c2c3888485f188af9202b9fc8c1d20fc243331))
* syntax ([762a702](https://gitlab.com/hoppr/hoppr/commit/762a702417b358c3fd0b568c631bbd0c9a5a5aef))
* temporary peg of securesystemslib to address in-toto bug ([8261f41](https://gitlab.com/hoppr/hoppr/commit/8261f414b9951ede1e5a7efc44dfa429d6cf5790))
* test image tags ([0180488](https://gitlab.com/hoppr/hoppr/commit/01804886f7a709e105e432d53d96c7ca19c67a80))
* test pattern matching ([c6e7c51](https://gitlab.com/hoppr/hoppr/commit/c6e7c5197847e13f54a14cf112ca58d5cad86602))
* **test:** credentials unit test coverage ([74b981f](https://gitlab.com/hoppr/hoppr/commit/74b981f51e58adbd5581e815f239f97e54003ed0))
* transfer file default value ([ac1bf1a](https://gitlab.com/hoppr/hoppr/commit/ac1bf1a775d534bd89217443c1711bebd49e957b))
* turn off allow-failure on semantic-release:dry-run job to avoid breaking changes from renovate ([3a32a65](https://gitlab.com/hoppr/hoppr/commit/3a32a656abdb3c2f7cb006547a353f3fdae615b5))
* type check errors ([02b3d27](https://gitlab.com/hoppr/hoppr/commit/02b3d2713ccd8169643121abfb5ec206d01bbe4b))
* type error ([25aea9c](https://gitlab.com/hoppr/hoppr/commit/25aea9cb031b72a620c04029fce89872e0a9f3cd))
* type errors ([30861b6](https://gitlab.com/hoppr/hoppr/commit/30861b6abc6810f234c85a9de1d6db64c826430b))
* unit test context missing delivered sbom ([0c3d27a](https://gitlab.com/hoppr/hoppr/commit/0c3d27ae52d28492b3080249430099a6cd3cd93f))
* unit test message ([81415ed](https://gitlab.com/hoppr/hoppr/commit/81415edb08a9a6f925a911939095a5c001107531))
* unit test message ([dc95a02](https://gitlab.com/hoppr/hoppr/commit/dc95a025e8d295c4b0572ba105ede3b69f8a4738))
* unit test message ([69a164f](https://gitlab.com/hoppr/hoppr/commit/69a164f500097c97b29cf408a9ca695d6a3505e3))
* unit test message ([5846f61](https://gitlab.com/hoppr/hoppr/commit/5846f6109f3e5adf55ec6bfa4596e95f002e1ead))
* Update artifact name ([4f64b03](https://gitlab.com/hoppr/hoppr/commit/4f64b03b8a36590887e8430aa5caf3a48e25cd7a))
* update assert ([eba7ec9](https://gitlab.com/hoppr/hoppr/commit/eba7ec90d1927369be2bb91115388ec3fab0ec58))
* update CODEOWNERS for repo move ([6541b0c](https://gitlab.com/hoppr/hoppr/commit/6541b0c6ce79ed1dc0e4156d2e39a2620ee4b0c4))
* update delivered_sbom with process return objects ([46da6f3](https://gitlab.com/hoppr/hoppr/commit/46da6f324eca0ff4bda4077f385ff2ad20e87177))
* Update expected toc (since it's changed), and correct the regression boms ([fe79087](https://gitlab.com/hoppr/hoppr/commit/fe79087aeef00eab3792de8c5d3ea343ab9ce4d3))
* update expected toc's with delta stage intermediate delivered bom ([f016526](https://gitlab.com/hoppr/hoppr/commit/f0165265077c35e578b62d252c384594732e0d3c))
* update expected toc's with delta stage intermediate delivered bom ([3291773](https://gitlab.com/hoppr/hoppr/commit/32917735d1e84afa74aa584c83f899206bb3cfea))
* update expected_tar_toc files for changed maven file names ([b1ef013](https://gitlab.com/hoppr/hoppr/commit/b1ef01389841407c0723a2ced6bc397709c2a24d))
* Update git collector depth defaults per MR suggestion ([43a3dbb](https://gitlab.com/hoppr/hoppr/commit/43a3dbb296db4a43f2a70f9eb87a6d56ad3ff510))
* Update maven write to disk ([82d42b6](https://gitlab.com/hoppr/hoppr/commit/82d42b6dbce5eec0868199da0c1f4c32c9a99d61))
* update Result object to include optional return object.  Check that return object matches plugin Bom Access value ([b346872](https://gitlab.com/hoppr/hoppr/commit/b3468720b276af24d95c0e748ecb3701a4fd0764))
* Update variables to contain artifact name ([0400e28](https://gitlab.com/hoppr/hoppr/commit/0400e280e42b9eec5282bb7d59fab3ae100a1872))
* updated expected tar for pypi ([7356d6b](https://gitlab.com/hoppr/hoppr/commit/7356d6b6c82ee1d54dcd22158451664dd18c7779))
* Updated unit tests for docker collection ([7dbd2af](https://gitlab.com/hoppr/hoppr/commit/7dbd2af98c871d8cfdc20b48f0164fc8cb861ff0))
* updates to ensure pipeline works for forks. ([5fa6fe9](https://gitlab.com/hoppr/hoppr/commit/5fa6fe98921248797dc75375d500be9ab2f37381))
* urljoin stripping path components ([4c6ad9f](https://gitlab.com/hoppr/hoppr/commit/4c6ad9f3de6d2351b4eb274b884062d1b0c6e841))
* use enums for constants, per code review ([40cc853](https://gitlab.com/hoppr/hoppr/commit/40cc8538d5921846202167e7f0089a18d1719137))
* Use list of options and check for troublesome values ([fb39372](https://gitlab.com/hoppr/hoppr/commit/fb39372b37ee1bc5eec7ac49e618a521d38b5be3))
* Use long switches for readability. ([fdbfec2](https://gitlab.com/hoppr/hoppr/commit/fdbfec26f6f528ce1c79bff518b4f61f2708db5d))
* use more stable package for apt testing ([f365df7](https://gitlab.com/hoppr/hoppr/commit/f365df765c2bd162a9cf6c8120c720a6897fbc23))
* use more stable package for apt testing ([1aca9d7](https://gitlab.com/hoppr/hoppr/commit/1aca9d7c5d751785f8aba52bbd2cb63319fd8583))
* Use python3 for virtual environment creation in CI Dockerfile ([e39ea91](https://gitlab.com/hoppr/hoppr/commit/e39ea91f9456b7f006ef6dea5f57579376829776))
* Use semantic release to publish package ([27e8eb0](https://gitlab.com/hoppr/hoppr/commit/27e8eb02b8bd3405cec90b5d3112662dc2675c17))
* Using copy from Maven-Dependency-Plugin instead of get ([7ab2fe1](https://gitlab.com/hoppr/hoppr/commit/7ab2fe15234b1bda27f91bb0ea6b025cde2c22d2))
* windows ANSI processing ([eba2cb4](https://gitlab.com/hoppr/hoppr/commit/eba2cb4a18d7bb375e9984c838772506d6c40bb7))
* wrapper for run cmd ([9d5844b](https://gitlab.com/hoppr/hoppr/commit/9d5844b8f0661b3be8ef63d120bf5d96023fe844))


### Reverts

* files not relevant to this branch ([fe61dea](https://gitlab.com/hoppr/hoppr/commit/fe61deaf45ff0fd0e0e5252ac783f751ee6ddedf))
* files not relevant to this branch ([c66595a](https://gitlab.com/hoppr/hoppr/commit/c66595a47a8d089b98566940eb4ae565d094c5da))
* generic types ([96721e4](https://gitlab.com/hoppr/hoppr/commit/96721e42511934c36bbb9014da20d24a1b2b9546))
* generic types ([9915c1a](https://gitlab.com/hoppr/hoppr/commit/9915c1aa2bb39bbccdc959b2584f56954698d84c))
* method_name condition logic ([6d9a16b](https://gitlab.com/hoppr/hoppr/commit/6d9a16bc87de3e8cce5a835093076ea76242b627))
* method_name condition logic ([6445f18](https://gitlab.com/hoppr/hoppr/commit/6445f18cb62e61bb2b8738bd46dbd75df766e961))
* model integration changes ([e27eb91](https://gitlab.com/hoppr/hoppr/commit/e27eb91fcc8a8b5cc2e0c6f110c6de1e37010f82))
* shared memory manager changes ([09a6ee3](https://gitlab.com/hoppr/hoppr/commit/09a6ee348e8820fc91d5ac5ad2016f8e96aa041e))
* shared memory manager changes ([4dbd8c0](https://gitlab.com/hoppr/hoppr/commit/4dbd8c0fc99b7e2d52b717bc555b326962a27467))

## [1.8.0](https://gitlab.com/hoppr/hoppr/compare/v1.7.2...v1.8.0) (2023-04-04)


### Bug Fixes

* instances of SecretStr treated as string ([5b5266d](https://gitlab.com/hoppr/hoppr/commit/5b5266d9677e1153f0d5dc1796f59a7f45a25405))
* missed instance of SecretStr ([a63ce34](https://gitlab.com/hoppr/hoppr/commit/a63ce34495d2dad61619ddb5600af109dcd1d64d))

## [1.8.1-dev.2](https://gitlab.com/hoppr/hoppr/compare/v1.8.1-dev.1...v1.8.1-dev.2) (2023-04-11)


### Features

* --no-strict CLI flag ([05cf05c](https://gitlab.com/hoppr/hoppr/commit/05cf05caea72c11823c635e391a8dd10eb6bd0d4))
* --no-strict CLI flag ([12bc0d3](https://gitlab.com/hoppr/hoppr/commit/12bc0d3200a5f19cf5c04b0cf8eb8423f9c00c4c))
* add composite collector ([47037b8](https://gitlab.com/hoppr/hoppr/commit/47037b89e38e8694d803309eed486245a461a4b2))
* add delta_sbom capability ([c73e081](https://gitlab.com/hoppr/hoppr/commit/c73e0816444cefa4547917da54dc650e7777d7da))
* add delta_sbom capability ([a7d265c](https://gitlab.com/hoppr/hoppr/commit/a7d265ccbe6254a78f5cf5e1b9808c7a3eb570a5))
* add delta_sbom capability ([5bb5a59](https://gitlab.com/hoppr/hoppr/commit/5bb5a599d0c111399101f5e676f20e19c1954022))
* add delta_sbom capability ([9a39d88](https://gitlab.com/hoppr/hoppr/commit/9a39d88b53f1f277aecfa1c6d2edefe82098e801))
* add nexus search collector ([7ccb05e](https://gitlab.com/hoppr/hoppr/commit/7ccb05e01300cb9dde702185d28bc2941c83c3fc))
* allow docker collector to use docker.io when --no-strict option is set ([1163405](https://gitlab.com/hoppr/hoppr/commit/116340559d89fa8f6618dc48b524ff6b0a42c66c))
* creation of in-toto attestations ([b58a973](https://gitlab.com/hoppr/hoppr/commit/b58a973e9b7bd5a3d52e5f23d4cdd97155b5143e))
* Report Generation Plugin ([126a0e5](https://gitlab.com/hoppr/hoppr/commit/126a0e5430be13824e67037225b617f24965f79c))
* skip collecting components with a scope of excluded ([e2fd680](https://gitlab.com/hoppr/hoppr/commit/e2fd680389100d216089a0a1aeedcbf4121b635b))

### Bug Fixes

* Add additional configuration capability ([dc14c57](https://gitlab.com/hoppr/hoppr/commit/dc14c5728f5869f9f47f0f2e18cc1b01812e6201))
* Add additional fixes per MR feedback ([598b9f1](https://gitlab.com/hoppr/hoppr/commit/598b9f1c65bf1dcee2cf31dd84553143be73b25f))
* Add Bot label to renovate MRs ([f43cd65](https://gitlab.com/hoppr/hoppr/commit/f43cd65250b65e93eb1cfa3debdf74a3bad58807))
* add collection metadata to sbom for apt components ([b8284f5](https://gitlab.com/hoppr/hoppr/commit/b8284f54fc8d0d193bad0916dc7ce83d7ff97e83))
* add collections params for docker ([228d327](https://gitlab.com/hoppr/hoppr/commit/228d327a31b2a7e043af38a016b98d20825f5348))
* add command line option to override previous collection location in delta_sbom plugin ([b3eb0bd](https://gitlab.com/hoppr/hoppr/commit/b3eb0bd1b1b40c936d34c7075fcf525ddf2e76c7))
* add command line option to override previous collection location in delta_sbom plugin ([3485048](https://gitlab.com/hoppr/hoppr/commit/34850483f44788e7e7cd44965984bba6b6502922))
* add command line option to override previous collection location in delta_sbom plugin ([d818730](https://gitlab.com/hoppr/hoppr/commit/d818730a0b6c4d930cdb220f31c2555a21aff42a))
* add command line option to override previous collection location in delta_sbom plugin ([2331668](https://gitlab.com/hoppr/hoppr/commit/2331668aa793c290764f6107dcfbe5f787dbba04))
* Add curl for oras test ([804f8b0](https://gitlab.com/hoppr/hoppr/commit/804f8b0b3d665d40e5299d21ec84f847a2a6e7d1))
* add delivered_sbom to context, work from that variable ([07b6ba9](https://gitlab.com/hoppr/hoppr/commit/07b6ba93a7d5b5b939ad53e4ad0d408276cd337a))
* Add entrypoint to hoppr ([073fdeb](https://gitlab.com/hoppr/hoppr/commit/073fdebdae77d9db6774ab1b84c2a336df68ad52))
* add expected-tar-toc for remaining integration tests ([7bbcac9](https://gitlab.com/hoppr/hoppr/commit/7bbcac9c821b6528edf9f7b247886b11ee18db45))
* add expected-tar-toc for remaining integration tests ([a071522](https://gitlab.com/hoppr/hoppr/commit/a07152294d7430bd552255dd911a9730ae526061))
* add expected-tar-toc to delta integration test ([40fdb97](https://gitlab.com/hoppr/hoppr/commit/40fdb9778b2b20c0b07aa475e5de931a6e4cf227))
* add expected-tar-toc to delta integration test ([33ed419](https://gitlab.com/hoppr/hoppr/commit/33ed41925bedc0258c2bb3a537ccb6a0b4311b39))
* Add hopctl docker image ([1363cec](https://gitlab.com/hoppr/hoppr/commit/1363cec9d1214acfed27c03b3c785fdcf29e7be2))
* Add hopctl docker image ([c4d00fc](https://gitlab.com/hoppr/hoppr/commit/c4d00fc04074112c5fddad91005cb6361ddaf8fc))
* Add hopctl docker image ([5ad0556](https://gitlab.com/hoppr/hoppr/commit/5ad0556d41e665fe20706d6e34833ab835ead8e7))
* add initial checks for BOM access ([d89fed2](https://gitlab.com/hoppr/hoppr/commit/d89fed21d1c807f17d1089e748d38bcb97fc9dde))
* add integration test for deltas ([e0c7e47](https://gitlab.com/hoppr/hoppr/commit/e0c7e47619424601f867040064a5fc6df402e29b))
* add integration test for deltas ([2ca6ed0](https://gitlab.com/hoppr/hoppr/commit/2ca6ed0019f9b21613193a5dd525b470a12a1d56))
* add integration test for deltas ([6789f9c](https://gitlab.com/hoppr/hoppr/commit/6789f9c240a110f10e717dc42c3cff40ba4352e6))
* add integration test for deltas ([c56d066](https://gitlab.com/hoppr/hoppr/commit/c56d0664ddeb49fe6bc4b4cf997ebd782a761163))
* add missing import ([603f219](https://gitlab.com/hoppr/hoppr/commit/603f21910e7d2cbf3f98813ba45a22454330b0ff))
* add missing strict_repos typer argument ([ad9bb37](https://gitlab.com/hoppr/hoppr/commit/ad9bb3736d57cea04ff902aa2241c88ba426d394))
* add mock for os.path.exist ([5463b80](https://gitlab.com/hoppr/hoppr/commit/5463b801539bbf1436e41b0aa0827cb2a54f2b9a))
* Add oci artifacts for reference ([f6661be](https://gitlab.com/hoppr/hoppr/commit/f6661beba1f1fff54a779ea973a5869a72c6616d))
* Add oras integration test ([cbd9376](https://gitlab.com/hoppr/hoppr/commit/cbd937615039f3f99451bf600ae40948e69f0049))
* add purl-type/repo-type mappings to nexus_search collector ([1b3d06a](https://gitlab.com/hoppr/hoppr/commit/1b3d06a04e67dcf0acc4bd2dd55eca07aa282329))
* add pytest-cov package ([a6666a8](https://gitlab.com/hoppr/hoppr/commit/a6666a84c85d0e7939519506226fef23dd05c221))
* add repo to purl type list ([01fa2f2](https://gitlab.com/hoppr/hoppr/commit/01fa2f254d56a06481e4b79e29ecd3f184ce7e04))
* add repository/directory properties to bom for all collectors ([bad3136](https://gitlab.com/hoppr/hoppr/commit/bad3136b6f7dd1fc7579afd5230cd28c5aa26b1b))
* add shared logfile lock ([e350189](https://gitlab.com/hoppr/hoppr/commit/e350189654b9461759e1483458489e42db510f1e))
* add shared logfile lock ([3c3c238](https://gitlab.com/hoppr/hoppr/commit/3c3c23803a56eaad75eaa41090b5ed258393ea0d))
* Add test verification for oras bundle ([77f04b7](https://gitlab.com/hoppr/hoppr/commit/77f04b775b030db593ade6d4437a8e6ff0309be6))
* add version to bom plutin property ([166538c](https://gitlab.com/hoppr/hoppr/commit/166538cf487149e3932a8a243742f8af016db123))
* Added user_env support to find_credentials ([ee58f2d](https://gitlab.com/hoppr/hoppr/commit/ee58f2d48766562152be513083038b226ed9ef98))
* address issue where in-toto was looking maven files ([98fc06a](https://gitlab.com/hoppr/hoppr/commit/98fc06aaeea92bd2eacb459f1d4ca9fa97cf34f0))
* all source distros from manifest repos ([52c8c80](https://gitlab.com/hoppr/hoppr/commit/52c8c8073eabedb81aa93e325c7ee03b465750c1))
* allow more full repository specification for collect_nexus_search ([e2cdf02](https://gitlab.com/hoppr/hoppr/commit/e2cdf027754a2f1568035fe5303a8e62024a00fa))
* allow more full repository specification for collect_nexus_search ([6ebcceb](https://gitlab.com/hoppr/hoppr/commit/6ebccebab92c792fc335ba1251908faf69e77dab))
* allow no scheme for repo URLs as last resort ([866382f](https://gitlab.com/hoppr/hoppr/commit/866382f298a701eadb13cd8b609b74b5e13da88a))
* allow no scheme for repo URLs as last resort ([fd1feec](https://gitlab.com/hoppr/hoppr/commit/fd1feecf559855b4eb3b8127d848f1daf2c72a19))
* allow spaces in stage name ([d7af57f](https://gitlab.com/hoppr/hoppr/commit/d7af57f3892e34fd62b5bfe1dae9f4c5d3704657))
* allow spaces in stage name ([923c043](https://gitlab.com/hoppr/hoppr/commit/923c043b1aa7b686e4777844372272aafb9a7e77))
* append dev instead of current branch name ([d6abddc](https://gitlab.com/hoppr/hoppr/commit/d6abddc66d3b832feb1923d1eb31d0557b03c892))
* Apply correction to git repository collector ([dc522b5](https://gitlab.com/hoppr/hoppr/commit/dc522b555ad2965ba7965f4630e0e1f7d1eb2849))
* apply in-toto suggestions ([3e92177](https://gitlab.com/hoppr/hoppr/commit/3e92177f671c4781907c0a6df279293e543ebb2f))
* apply in-toto suggestions ([401757e](https://gitlab.com/hoppr/hoppr/commit/401757eae524fe76d7eda6f39ab38ff20dd0e01c))
* applying MR suggestion ([c3adeee](https://gitlab.com/hoppr/hoppr/commit/c3adeee1813eba8dd308fec358d27af9f25c1f1d))
* applying MR suggestion ([4e5ca7f](https://gitlab.com/hoppr/hoppr/commit/4e5ca7fb7258c667a431653ab01bb8745a87308f))
* applying MR suggestion ([4e61f84](https://gitlab.com/hoppr/hoppr/commit/4e61f84272b14180e16f6a4baa424f5d60eb2752))
* applying MR suggestion ([b034b72](https://gitlab.com/hoppr/hoppr/commit/b034b7273179a7fb8bd3bda411050d9815b58ce7))
* apt collector _get_download_url_path ([aa988d6](https://gitlab.com/hoppr/hoppr/commit/aa988d680fe350e5716f30abbed0437e51ae9797))
* apt collector _get_download_url_path ([1d36fd4](https://gitlab.com/hoppr/hoppr/commit/1d36fd457fc60d9d9638611e3b91940a2a523194))
* bom helm chart version ([009bdf0](https://gitlab.com/hoppr/hoppr/commit/009bdf00412fe6964b81c15b86aec52a761890df))
* Branch isolation testing ([fcde124](https://gitlab.com/hoppr/hoppr/commit/fcde12425dd610fe3da0dbbd56de6af677a8688e))
* bug with with attestations created from GitLab CI Runners ([f8ca8e7](https://gitlab.com/hoppr/hoppr/commit/f8ca8e79144b6377250d38ccc37bb2f7689d2033))
* casing for "kind" field ([26b0927](https://gitlab.com/hoppr/hoppr/commit/26b0927a143763f688215b0fc3d49e27a56f0b71))
* casing for "kind" field ([533f6a7](https://gitlab.com/hoppr/hoppr/commit/533f6a770bc8738aa39b22c2faa6d262c4b7264d))
* catch exception from _get_required_coverage, check for empty/missing boms ([d905e35](https://gitlab.com/hoppr/hoppr/commit/d905e3557ce6def86b923db7961f3631808f3445))
* check for exception not thrown in pypi success ([0ba41f6](https://gitlab.com/hoppr/hoppr/commit/0ba41f6a9962ede186765b25e7939623fbb15095))
* check tar toc on integration tests ([aafc6bb](https://gitlab.com/hoppr/hoppr/commit/aafc6bb6eb9215e0c158a1de007fc67a8509059e))
* check tar toc on integration tests ([105c87a](https://gitlab.com/hoppr/hoppr/commit/105c87a4c81809c1a586713d42ff1e857e0c260e))
* Clean up artifact name ([a59aaac](https://gitlab.com/hoppr/hoppr/commit/a59aaacc2bb23988b8e240d0f6c237b547e86f39))
* Clean up other variables in ci ([d289184](https://gitlab.com/hoppr/hoppr/commit/d28918482ceb951461725095e72f7ca9ebcb23ce))
* clean up repository_url handling ([c867a3b](https://gitlab.com/hoppr/hoppr/commit/c867a3bda17c70e59e1d50672543077b7daae8cb))
* clean up repository_url handling ([6fb544b](https://gitlab.com/hoppr/hoppr/commit/6fb544b1ceb0846d096cf790efa8f52a7160960c))
* clean up repository_url handling ([95a0288](https://gitlab.com/hoppr/hoppr/commit/95a0288eeecd7c0baf03af5b67dd0d50fb549708))
* clean up repository_url handling ([011e53b](https://gitlab.com/hoppr/hoppr/commit/011e53b8b96760f03863b5a9582f91ad30f98733))
* Clean up rules and workflow ([69e9b73](https://gitlab.com/hoppr/hoppr/commit/69e9b73ebab517658ad0e5238038e01979f6e98a))
* Cleaned up maven command ([2b4af6e](https://gitlab.com/hoppr/hoppr/commit/2b4af6e57658f5f96a930cf11542f802f783476d))
* Cleanup notes in config ([2632bfa](https://gitlab.com/hoppr/hoppr/commit/2632bfabe9794542033ce8b4dd8d6a5cb4e96da8))
* Cleanup rules ([ea79632](https://gitlab.com/hoppr/hoppr/commit/ea7963280418793154b6cd113c926ff160ce011e))
* Cleanup rules ([76d65e5](https://gitlab.com/hoppr/hoppr/commit/76d65e5cf60559f4672f63dd33a62c45f3009eab))
* clear loaded manifests ([39e7341](https://gitlab.com/hoppr/hoppr/commit/39e73413a9e446d1f5b98118ff8b3ec2799260e5))
* code review comments ([c9fa7e2](https://gitlab.com/hoppr/hoppr/commit/c9fa7e2b37422a04c4a2281b81b425d46bcedb12))
* Code review comments ([a204676](https://gitlab.com/hoppr/hoppr/commit/a204676aa26173864856e458a30f119caeb3ae9a))
* complete unit test coverage for collect_nexus_search ([24827e4](https://gitlab.com/hoppr/hoppr/commit/24827e4cab02da2808deb99fd372c6a6b3935ed7))
* component search sequence ([ff89f56](https://gitlab.com/hoppr/hoppr/commit/ff89f5633b386b6b006ff7f76b6558ad894f2bb2))
* component search sequence ([397d3bf](https://gitlab.com/hoppr/hoppr/commit/397d3bf639ed8e729fb4e209078cd0877ae6b07f))
* Correct build artifacts ([40ca47f](https://gitlab.com/hoppr/hoppr/commit/40ca47f2642bd82be5aec18f75a3f468c1d880e7))
* Correct deployment teir ([d4282c8](https://gitlab.com/hoppr/hoppr/commit/d4282c8c1e54fe188acfde63a9307c33474c9f8b))
* Correct deployment teir ([983932d](https://gitlab.com/hoppr/hoppr/commit/983932d32f998ca41536002671b324734d711716))
* Correct dockerfile ([30f438a](https://gitlab.com/hoppr/hoppr/commit/30f438a16501280c0dd97633d3e2447e2c80998e))
* Correct git bom, had incorrect purl, name, and version ([e6a1265](https://gitlab.com/hoppr/hoppr/commit/e6a1265e9862aa66996e10a7c956a1315bcfc89b))
* Correct integration tests ([b065953](https://gitlab.com/hoppr/hoppr/commit/b0659532c14ddea25afcb7357d14104427cddefe))
* Correct media types for registry ([d3ea0df](https://gitlab.com/hoppr/hoppr/commit/d3ea0df0cd2eb276eb86d1cb82dfceeb39604e71))
* Correct release yaml file ([a29c26c](https://gitlab.com/hoppr/hoppr/commit/a29c26c86220443c22e94954bd6db5d3e7a59147))
* Correct releaserc file ([8868fab](https://gitlab.com/hoppr/hoppr/commit/8868fab85c0ed587f8d9679af6c8d793ed78c50b))
* Correct releaserc.yml ([4b55f7f](https://gitlab.com/hoppr/hoppr/commit/4b55f7f25895b1251ab7e9499280848d86732cbb))
* Correct requirements ([7598beb](https://gitlab.com/hoppr/hoppr/commit/7598beb540c9c26860501b42ba92fb807f79fd33))
* Correct simlinks in ci docker ([deb0728](https://gitlab.com/hoppr/hoppr/commit/deb07284c27603325678ae3332f0a2023ed42f80))
* Correct syntax in ci docker ([f96c938](https://gitlab.com/hoppr/hoppr/commit/f96c938502b98e0bdbe4096ed6a35f830abaf378))
* Correct tests so they pass ([eeebaef](https://gitlab.com/hoppr/hoppr/commit/eeebaef2b781cde5e668d0d75e65977185d56d45))
* Correct the oras binary arch type ([71bc21e](https://gitlab.com/hoppr/hoppr/commit/71bc21ec0b5308c4299906ab04b270b678480551))
* Correct the wheel name ([4847b4a](https://gitlab.com/hoppr/hoppr/commit/4847b4a8548c563ad4298616f8c16288ca925aaa))
* Correct trivy timeout ([5a9246a](https://gitlab.com/hoppr/hoppr/commit/5a9246ae86bb2e517cb39c3c7b5d240a9fdb8bec))
* Corrected Maven-Dependency-Plugin arguments so that maven artifacts would be bundled ([9fe9eaa](https://gitlab.com/hoppr/hoppr/commit/9fe9eaa13c68ed8d36437a6662f735c347018ed4))
* credential.find method no longer needs exact match.  added lines to _run_data_ metadata file. ([202b0ae](https://gitlab.com/hoppr/hoppr/commit/202b0ae90ba70cf8d45990064e4b3d2ce6b9fb4a))
* Cut release from next branch ([17d0d03](https://gitlab.com/hoppr/hoppr/commit/17d0d03ac243fea51d9ea98738d0e8293c60803d))
* Cut release from next branch ([e1c56d2](https://gitlab.com/hoppr/hoppr/commit/e1c56d28394803045b974195e6e7d9cc796c967b))
* **deps:** update dependency hoppr-cyclonedx-models to v0.2.10 ([ee37064](https://gitlab.com/hoppr/hoppr/commit/ee370642914f1b507ed4793e11cbc575d1c51720))
* **deps:** update dependency typer to ^0.7.0 ([a35bd85](https://gitlab.com/hoppr/hoppr/commit/a35bd8519c8486b68a9228b660a2d2f44bad7f2c))
* dev branch test; update README ([f8ed5ee](https://gitlab.com/hoppr/hoppr/commit/f8ed5eec1a2b8d68730b88d6d378d132fa67c1c7))
* DNF download directly from found URL ([bf84cec](https://gitlab.com/hoppr/hoppr/commit/bf84cec779df983642116cf99cf42403675f17e9))
* DNF download directly from found URL ([11d5e5e](https://gitlab.com/hoppr/hoppr/commit/11d5e5ec6280b60d821880833d5ee7f0d3683432))
* do not re-generate consolidated/delivered sboms ([ee1928b](https://gitlab.com/hoppr/hoppr/commit/ee1928b6198e0fb57c3eb56af0dc3b5c057166d2))
* do not re-generate consolidated/delivered sboms ([ce59dcc](https://gitlab.com/hoppr/hoppr/commit/ce59dcc46fbbef3f3941c0f33c3d0cc0c0abacf7))
* Don't run pipelines on merge event ([a0b8c53](https://gitlab.com/hoppr/hoppr/commit/a0b8c53add628e08a0c02f30beea57a399b9f109))
* Ensure python is on the path ([a1c03eb](https://gitlab.com/hoppr/hoppr/commit/a1c03eb6e2e607267886890831c2675572525d6b))
* enum base type ([4e3577c](https://gitlab.com/hoppr/hoppr/commit/4e3577c8b96bd5c107649c3d6adb6ef1dd34e4d1))
* exclude nulls from output sboms ([e41bd52](https://gitlab.com/hoppr/hoppr/commit/e41bd52190ec8acba3fdd419650674e7739224f7))
* expected apt SBOM ([c6b01e9](https://gitlab.com/hoppr/hoppr/commit/c6b01e935d7aadf1f2fe3410edf901d95c9f8fb3))
* expected apt SBOM ([8e7d3da](https://gitlab.com/hoppr/hoppr/commit/8e7d3daaab31eccc1686963d30bbbd72c70f6bf0))
* expected metadata source location ([53f74d7](https://gitlab.com/hoppr/hoppr/commit/53f74d7f154df37e45d91e782bfbee5f30570fd9))
* expected metadata source location ([f918d63](https://gitlab.com/hoppr/hoppr/commit/f918d63976bfb5ac57405ea48b30c9b34e8fe0a6))
* expected-tar-toc sort order changed ([b89f853](https://gitlab.com/hoppr/hoppr/commit/b89f853e5547aac8e0a69b7245f8f3abddc5d70f))
* fail on no change, improved status messages ([801dea2](https://gitlab.com/hoppr/hoppr/commit/801dea20d82167b303ebbd0a339cb8e408c9b873))
* fail on no change, improved status messages ([1a4c87c](https://gitlab.com/hoppr/hoppr/commit/1a4c87ceeceaabbe3ecf13e905fdd9a08866e647))
* fail on no change, improved status messages ([fa610d0](https://gitlab.com/hoppr/hoppr/commit/fa610d09a73bf6736e9792096614e08a84a6c8aa))
* fail on no change, improved status messages ([69d15b1](https://gitlab.com/hoppr/hoppr/commit/69d15b1c4e30684fe59020b96b002a7f1d27c30c))
* fast forward branch ([1428ecb](https://gitlab.com/hoppr/hoppr/commit/1428ecb2ff108f25ca0c424f53984771a16e8e8e))
* Fixed Docker repo:tag information being lost in collection ([5111f7f](https://gitlab.com/hoppr/hoppr/commit/5111f7fe86bd2c833ac4c0bb0392932b599f6549))
* Fixed Docker repo:tag information being lost in collection ([3319a36](https://gitlab.com/hoppr/hoppr/commit/3319a36bd90ca7798141ee0e7fe92bb6c361559d))
* fixed issue with bundle options for functionary_key ([d798606](https://gitlab.com/hoppr/hoppr/commit/d7986062051b5b0be426dda0842acb1bab601ec2))
* Fixed the releaseing issue ([ce8b2f5](https://gitlab.com/hoppr/hoppr/commit/ce8b2f57703bfaf0984e2b750005f866a8ad8b35))
* Fixed type-check findings ([7bb21df](https://gitlab.com/hoppr/hoppr/commit/7bb21df1b19f3f2ed8f275db995c722db9e78ed0))
* Fixed unit tests ([4e80266](https://gitlab.com/hoppr/hoppr/commit/4e8026652458dc5c35bdc3dfc5a983727e5bf44f))
* Fixes [#150](https://gitlab.com/hoppr/hoppr/issues/150) and corrected when consolidated and deliveried SBOMs are written. It also address additional unit testing ([5adf219](https://gitlab.com/hoppr/hoppr/commit/5adf2193b30af0b6aba0c901dd369df324d53e19))
* Fixing Merge Conflicts ([d24cff2](https://gitlab.com/hoppr/hoppr/commit/d24cff258f10542bdc9b1af462c3a61bc1af2722))
* generate dev version if not on main/dev ([3aa6ec5](https://gitlab.com/hoppr/hoppr/commit/3aa6ec50f186ee520eb1279160c8b1a0715711b2))
* Get better results ([688e176](https://gitlab.com/hoppr/hoppr/commit/688e1766cffb34cc155e0ea0c3f1d7a608c58947))
* gitattributes ([5d3bde3](https://gitlab.com/hoppr/hoppr/commit/5d3bde370513376a36b9a0cc7279b9464fd8a09a))
* Grab versioning properly ([cc052cf](https://gitlab.com/hoppr/hoppr/commit/cc052cfcedbd149b295e090c493adb650b47f1ce))
* have nexus collector respect user-specified purl types ([9116a03](https://gitlab.com/hoppr/hoppr/commit/9116a034125ccbe2eb47d06e0172176e3eae2d86))
* helm collector append purl name ([5574739](https://gitlab.com/hoppr/hoppr/commit/5574739d2e34ba0a538ee8295939e2ad473144f5))
* hoppr group ([c21bc29](https://gitlab.com/hoppr/hoppr/commit/c21bc295fb9ab40756e107b5047e2cb3d36d8a06))
* image tag reset after rebase ([45a850a](https://gitlab.com/hoppr/hoppr/commit/45a850a4c43f29c7253c4c97620c1fc6cce32fb5))
* import source type error ([464211c](https://gitlab.com/hoppr/hoppr/commit/464211cddc9f7ea6c7a5182449155e4c7467289c))
* Improve config error logic in git collector ([cb3681a](https://gitlab.com/hoppr/hoppr/commit/cb3681a6f95c8cc031ca69276f84015925659296))
* Improve matching pattern and logging ([12a68eb](https://gitlab.com/hoppr/hoppr/commit/12a68eb3ab2e049260bba97ddf8f7c0fc585a45d))
* include resources folder in poetry build ([5ae0ffb](https://gitlab.com/hoppr/hoppr/commit/5ae0ffb5d761acd41c7fafd6a0cfa43f40d7eafd))
* included manifest repo merge, add tests ([359364c](https://gitlab.com/hoppr/hoppr/commit/359364c453f72cc35cafd752b0b21a7aac79e0c3))
* included manifest repo merge, add tests ([1c22c6e](https://gitlab.com/hoppr/hoppr/commit/1c22c6eec5ed7c3c1dc01e3d70e6241f0da7f7ee))
* increase minimum unit test coverage to 100% ([9c697e3](https://gitlab.com/hoppr/hoppr/commit/9c697e35712ee8e8138848f506de71ff7054d71b))
* lint error ([990ef3f](https://gitlab.com/hoppr/hoppr/commit/990ef3ff6d79359c45481f0deeb640a4ec629ccb))
* lint error ([f9f2cc0](https://gitlab.com/hoppr/hoppr/commit/f9f2cc0095060a84642717a0d77e28d26d97467b))
* linting union for type hints ([bbaf8f0](https://gitlab.com/hoppr/hoppr/commit/bbaf8f0f688c30d206bc1842cd19b616bda0b949))
* main module exits using sys instead of typer ([356112e](https://gitlab.com/hoppr/hoppr/commit/356112e622d329d53a0902532476957f92790491))
* main process logfile lock ([4519f53](https://gitlab.com/hoppr/hoppr/commit/4519f53808064a9829f46bfec19f06156966a59c))
* Make a quicklink script for linking python in dockerfile ([ae3dace](https://gitlab.com/hoppr/hoppr/commit/ae3dace2b4101bc9c73381dcf097baf505965e25))
* Make semantic release pass all jobs ([ddc3c49](https://gitlab.com/hoppr/hoppr/commit/ddc3c495048e8be5302bf9c7ba7a4ad5418944a7))
* manifest helm repo URL ([438fae5](https://gitlab.com/hoppr/hoppr/commit/438fae572062f9726c451031a88e403a67390c99))
* manual version revs because lock file conflicts spam ([e8a473f](https://gitlab.com/hoppr/hoppr/commit/e8a473ffd01938734a7a84180e78d075927240c4))
* merge components, add verification tests ([f0ecf1e](https://gitlab.com/hoppr/hoppr/commit/f0ecf1e7f7471b17dbbf42e6aa80e8dedc46f76b))
* merge components, add verification tests ([ce6e4d1](https://gitlab.com/hoppr/hoppr/commit/ce6e4d17cd8ce4d1127956d1f72b2f346ee7402a))
* Merge dev into next ([765d25a](https://gitlab.com/hoppr/hoppr/commit/765d25a29cc9d3fed6794a01d25892118408e177))
* Merge main into branch ([e2fd972](https://gitlab.com/hoppr/hoppr/commit/e2fd97246baadff00a84b1fecb0b8a79412e70ee))
* **Minor:** Image build cleanup ([ba4e6c3](https://gitlab.com/hoppr/hoppr/commit/ba4e6c3bc68fe469b3ad68ed636f43b55c84f425))
* missing `rev` command in skopeo image ([cb38a3e](https://gitlab.com/hoppr/hoppr/commit/cb38a3e8ae5dc2afcac4bdbb87f110d6af58997e))
* missing collector plugin CLI tools ([b1284ba](https://gitlab.com/hoppr/hoppr/commit/b1284ba6c1df27487e36279c5af5478b27450322))
* move insertion of required stages to Transfer object creation ([fa0d31b](https://gitlab.com/hoppr/hoppr/commit/fa0d31b99fcc55411cc2fd32bcfb7f050b14fba9))
* move insertion of required stages to Transfer object creation ([a7da1d5](https://gitlab.com/hoppr/hoppr/commit/a7da1d53253de835a764f3d2ab68f052faa03aa7))
* move insertion of required stages to Transfer object creation ([57604d5](https://gitlab.com/hoppr/hoppr/commit/57604d5e724fadcd71694b466a82b19a9696d624))
* move insertion of required stages to Transfer object creation ([53243ab](https://gitlab.com/hoppr/hoppr/commit/53243abcb44c5c031af5c1cc625b8cf81e2b3954))
* moved test_main.py under test/unit/ directory ([90df7ad](https://gitlab.com/hoppr/hoppr/commit/90df7adb7fd293b7b3da96ac70ca159fe0d8f480))
* mypy errors ([b08426c](https://gitlab.com/hoppr/hoppr/commit/b08426ca5b7af32c8d192d2309381b19d03d136d))
* mypy errors ([d94d090](https://gitlab.com/hoppr/hoppr/commit/d94d090f231edaa21de806bc8ada8743326c57c7))
* mypy errors ([d612474](https://gitlab.com/hoppr/hoppr/commit/d612474d5f71b79683280781ca3cda18ee05d240))
* New branch old issue ([a906ca3](https://gitlab.com/hoppr/hoppr/commit/a906ca3cc6afc01f97483e136f9b3370312bb5bb))
* Only attempt PyPI source collect if whl not collected ([31de8ae](https://gitlab.com/hoppr/hoppr/commit/31de8ae07d414c09295db59662442ee7a15f92cc))
* only load purl-type-specific plugins when components of that type are being processed (Issue [#77](https://gitlab.com/hoppr/hoppr/issues/77)) ([1789f21](https://gitlab.com/hoppr/hoppr/commit/1789f21df784738c58b8ea3dac19e50240a2b13b))
* Only publish if there's a new release from semver dryrun ([da88e9b](https://gitlab.com/hoppr/hoppr/commit/da88e9b4a2e6449ea5550f8adcffceb975756af4))
* only rename maven file on successful collection ([3455e74](https://gitlab.com/hoppr/hoppr/commit/3455e743e92fa74ed1d8f82e66921dd41c837c63))
* Only run semantic release publish on develop and main ([0ecc904](https://gitlab.com/hoppr/hoppr/commit/0ecc904fb1583a9f530d4a469a331391007e70d1))
* parameter name typo ([9d7c3b1](https://gitlab.com/hoppr/hoppr/commit/9d7c3b1c0751816f5c108f3fb83eaec10f47a490))
* pip arguments ([0cc47f1](https://gitlab.com/hoppr/hoppr/commit/0cc47f17f3eefcd4a650d213d7d8701c59485c8b))
* platform check ([de541db](https://gitlab.com/hoppr/hoppr/commit/de541db0bf4a810a158ca175b1c38a573a1ab9e7))
* **plugin:** add type hint to auth ([0bc2e29](https://gitlab.com/hoppr/hoppr/commit/0bc2e29c0e36d0b596cd1d5f493a14566e9bd9dc))
* **plugin:** properly authenticate nexus requests ([be82ed4](https://gitlab.com/hoppr/hoppr/commit/be82ed43660ca25ed2f105a43dbd1603deb4df38))
* prepend stage if needed for deltas ([a0dbfcb](https://gitlab.com/hoppr/hoppr/commit/a0dbfcbb40649182f1fef9ce8cdb1ae5bf151c73))
* prepend stage if needed for deltas ([7bbb2b5](https://gitlab.com/hoppr/hoppr/commit/7bbb2b5d76d6245430e5e587e8e15b64f448c2d5))
* prepend stage if needed for deltas ([e93d3a0](https://gitlab.com/hoppr/hoppr/commit/e93d3a0ff654fe4aae50a96003682dcd6a7d4f74))
* prepend stage if needed for deltas ([2467c6b](https://gitlab.com/hoppr/hoppr/commit/2467c6b693832775d4739d0de41d7fc2059b5296))
* prevent loading plugins that aren't needed ([17722f3](https://gitlab.com/hoppr/hoppr/commit/17722f3238b9c5436549e3c1fbd9e7fb84f49dc3))
* prevent loading plugins that aren't needed ([b00db2b](https://gitlab.com/hoppr/hoppr/commit/b00db2b5925915fa950e14ea565e4496c4bf4aa9))
* processor relative file handling ([59636aa](https://gitlab.com/hoppr/hoppr/commit/59636aaeb06ff4bddd167fdc302b914ca3342cd5))
* processor relative file handling ([5c3cdb1](https://gitlab.com/hoppr/hoppr/commit/5c3cdb18abede20e59e52afdcaeec1de69a8298c))
* proper attestations for nexus-search- and composite- collectors ([a1a9651](https://gitlab.com/hoppr/hoppr/commit/a1a96517eebc432d83153cf4808cc17d40f17f81))
* proper attestations for nexus-search- and composite- collectors ([7ae7d7a](https://gitlab.com/hoppr/hoppr/commit/7ae7d7ae82558a5586740a4dd50a700a6a6e82b6))
* Protect keys ([9001a21](https://gitlab.com/hoppr/hoppr/commit/9001a219a93817d064a5cce207c267347b112549))
* provide better error message on empty config file content ([e87fff8](https://gitlab.com/hoppr/hoppr/commit/e87fff885842320adf85e7c86abb319e8dfa4f2d))
* pylint error ([6982109](https://gitlab.com/hoppr/hoppr/commit/69821095b2319075c65433a1f76e5d4e47fd361b))
* pylint error ([e36b242](https://gitlab.com/hoppr/hoppr/commit/e36b2424739e0dc4814855fb304dca1edd24e448))
* pylint issues fix ([7c7810c](https://gitlab.com/hoppr/hoppr/commit/7c7810c9ac63268472c2faf50e43694bdcd2cccb))
* raw collector stripping purl namespace ([1aca256](https://gitlab.com/hoppr/hoppr/commit/1aca256351c854eaa1c7f02e209961a20ece99aa))
* raw collector stripping purl namespace ([ee20ebe](https://gitlab.com/hoppr/hoppr/commit/ee20ebe02b6c1c7f1b1be9a44b82087565a5a5af))
* raw collector stripping purl namespace ([bba65cb](https://gitlab.com/hoppr/hoppr/commit/bba65cb7805b78a329ee59df1c10b9ad54959f8d))
* raw collector stripping purl namespace ([2c9309d](https://gitlab.com/hoppr/hoppr/commit/2c9309d09150511489185b455cf38d631611820b))
* README feedback; broken link fixes; try PyPI banner fix ([932121d](https://gitlab.com/hoppr/hoppr/commit/932121d087bbd2019007adb576b6c9122d4881fa))
* Rebase dev ([9399451](https://gitlab.com/hoppr/hoppr/commit/9399451c5f23b599d6d54c02538ebe4017a95620))
* redeclare Component attrs with hashable types ([a2034fa](https://gitlab.com/hoppr/hoppr/commit/a2034fa8ffd64f58b832199d0964d43893401962))
* redeclare Component attrs with hashable types ([26bbfc0](https://gitlab.com/hoppr/hoppr/commit/26bbfc064c252ce8b7e65205fe7d0c98987a5a41))
* Reference main so that develop can become default branch ([76f707b](https://gitlab.com/hoppr/hoppr/commit/76f707b6c3435bdc68d73b3717c451c6a6c95369))
* Release dev channel ([42ee862](https://gitlab.com/hoppr/hoppr/commit/42ee86236810072b2d303ea255f2d2556fd53749))
* Release dev channel ([15b4ae7](https://gitlab.com/hoppr/hoppr/commit/15b4ae76944accd1b4f0e6ae379b02c5efabb923))
* Remove blank lines ([196218c](https://gitlab.com/hoppr/hoppr/commit/196218c39d7dbb50c74722922190f61905f46053))
* remove construct method call ([2cf4963](https://gitlab.com/hoppr/hoppr/commit/2cf49637902c87dd786f37485521e8c55617685f))
* Remove gitlab semantic release comment on MRs ([4af82e6](https://gitlab.com/hoppr/hoppr/commit/4af82e6023f842a8587ec5183c5b3a212dc9e5f5))
* Remove licensing scanning and replace with policy ([e852316](https://gitlab.com/hoppr/hoppr/commit/e85231642c2f75a022d3872049b26e349821cc13))
* remove need for boolean from fail-open logic ([23bfa03](https://gitlab.com/hoppr/hoppr/commit/23bfa0397d12d39fcbca526eaadd651805e97c57))
* Remove node engine reference from package.json ([3c26b4e](https://gitlab.com/hoppr/hoppr/commit/3c26b4e768f23e9746db9f4b654a4799cb95e00b))
* Remove pack, and idx from check ([6e24462](https://gitlab.com/hoppr/hoppr/commit/6e244629bf39228cee030550da47863b11c6aec5))
* Remove package.json ([e13de8d](https://gitlab.com/hoppr/hoppr/commit/e13de8d885045b1e750fceeace1e60d4d9d032b6))
* remove prerelease ([1e038d0](https://gitlab.com/hoppr/hoppr/commit/1e038d0b73fe23da781bdea4cb02b9241e19aa8e))
* remove prerelease ([6818939](https://gitlab.com/hoppr/hoppr/commit/6818939d8237876f7dfabdce72a0dab1270db9b3))
* remove problematic git files from tar toc comparison ([b883b23](https://gitlab.com/hoppr/hoppr/commit/b883b23fbf230857a6244b7b8207144e3cc3a920))
* remove problematic git files from tar toc comparison ([89f1621](https://gitlab.com/hoppr/hoppr/commit/89f16211e602d92820cad9719257a57d2bfbcb39))
* Remove quotations around build ([3848f46](https://gitlab.com/hoppr/hoppr/commit/3848f463bb8640e4acd524bfdd04838291e9222d))
* Remove quotations from parallel matrix ([28ee2b7](https://gitlab.com/hoppr/hoppr/commit/28ee2b707fa0f36992f131e7bbb548f164b23e14))
* Remove quotes ([7323a8b](https://gitlab.com/hoppr/hoppr/commit/7323a8b16abc50d284a7d67bf7a346fe0190369c))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([0ac8adc](https://gitlab.com/hoppr/hoppr/commit/0ac8adc4c70bf79e5068daf8f8d5501c045af29a))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([87ae120](https://gitlab.com/hoppr/hoppr/commit/87ae1209adced6c7004143d33e314df719740da1))
* remove references to deleted job ([6d9890e](https://gitlab.com/hoppr/hoppr/commit/6d9890e99f43b439f20505142e2706c1556d5abf))
* remove references to deleted job ([7d1ef14](https://gitlab.com/hoppr/hoppr/commit/7d1ef14d1bb86f478ad35b5c08038b08b1fc916c))
* Remove skip ci and attempt to let workflow rules handle pipelines ([0e325a5](https://gitlab.com/hoppr/hoppr/commit/0e325a5e7a2caea831856e0ba6566f0a5ecaf2c7))
* restore Dockerfile FROM directive ([e7e0622](https://gitlab.com/hoppr/hoppr/commit/e7e06222a9768bfeaab9f10e13a9b213ccd45be7))
* Result constructor param typo ([09290d2](https://gitlab.com/hoppr/hoppr/commit/09290d25bd2be5f07635c718c7691dad6e00e652))
* Revert Changes ([f6797ea](https://gitlab.com/hoppr/hoppr/commit/f6797ea323e79a9bcd8b67b00c4939004e0dafcc))
* revert to older version of npm-check-updates ([9fb3ebf](https://gitlab.com/hoppr/hoppr/commit/9fb3ebf011be353fe119759514868151e0ddf3c2))
* Roll back chore rules ([e40fbfb](https://gitlab.com/hoppr/hoppr/commit/e40fbfbcb463ccc704c351b1acad04f3ea1fb0ea))
* Roll base image back from 9.0 to 8.6 ([31f8166](https://gitlab.com/hoppr/hoppr/commit/31f81666e3bb135ad7ad7b05d28cbc96c77859bc))
* Run if commit message doesn't start with chore ([daa5d11](https://gitlab.com/hoppr/hoppr/commit/daa5d119686ca58488cc04c62d48b73bb9c49319))
* Run pre-commit ([7b740ec](https://gitlab.com/hoppr/hoppr/commit/7b740eca0a6f3bd92d99e7c8f29c3fd435f0df4d))
* Run the entire dockerfile as one large image ([47ec5a5](https://gitlab.com/hoppr/hoppr/commit/47ec5a57dece0be112dca8c068a2807edc811bf8))
* sbom metadata ([eb5225f](https://gitlab.com/hoppr/hoppr/commit/eb5225f8f0729e44701d6b75ce7f74870f7eecbb))
* sbom metadata ([f97e7a7](https://gitlab.com/hoppr/hoppr/commit/f97e7a7acb00e9d361887dfad55c7bc2f03b10f0))
* SBOM spec selection logic ([d12bd8b](https://gitlab.com/hoppr/hoppr/commit/d12bd8ba19c0b5b8ec879d1f8190b8afb999d80b))
* SBOM spec selection logic ([2cd7286](https://gitlab.com/hoppr/hoppr/commit/2cd72866b1ae7a1fda1b580a4f7dbc2942a0da49))
* semantic-release version bump ([d45efb7](https://gitlab.com/hoppr/hoppr/commit/d45efb73d70625bda73ca4c4dfec33698c62957f))
* set file name correctly for collect_maven_plugin ([07e4eb5](https://gitlab.com/hoppr/hoppr/commit/07e4eb55388b25f46fdd2ed7a0af04fa86a8e427))
* set prerelease back to true ([49b130e](https://gitlab.com/hoppr/hoppr/commit/49b130e916dc7b3f9ec9fdaead06ce29e78f32c8))
* Set the correct version of oras ([d8357a4](https://gitlab.com/hoppr/hoppr/commit/d8357a431e33ec0fb45e0fc51537eae603e7cb6a))
* set timeout for run_command to reduce hanging ([9be8afe](https://gitlab.com/hoppr/hoppr/commit/9be8afefc62f1868d908cf8ecb8e9bd211662c37))
* set timetag to utc timezone ([12aa368](https://gitlab.com/hoppr/hoppr/commit/12aa368689a4225fc9662f59624ee90049fb8c4f))
* Setup codequality analytics and build attestation ([c5c4514](https://gitlab.com/hoppr/hoppr/commit/c5c4514e153becdf4a39713259b289c4469a24eb))
* Setup dependencies on test image ([6fabada](https://gitlab.com/hoppr/hoppr/commit/6fabada652d546cd89f1cd16eb32bae2cd0cb08a))
* shared RLock for Docker unit tests ([b2f3c9c](https://gitlab.com/hoppr/hoppr/commit/b2f3c9c5eb31bd31e46940fdf453ff56ae0f54f5))
* Simlink python ([0009483](https://gitlab.com/hoppr/hoppr/commit/0009483d56965e8770ae67383ecc69d4e7a53697))
* Simplify some of the configuration options ([a52594a](https://gitlab.com/hoppr/hoppr/commit/a52594a9e0873392934d6bfdb442ee3633f58a00))
* speed up unit tests ([32f8715](https://gitlab.com/hoppr/hoppr/commit/32f87159f4fc125a338e61fadefb80f5cb306ccb))
* Split out ci config per !184#note_1170995317 ([f42e582](https://gitlab.com/hoppr/hoppr/commit/f42e5825d7a3ebeab60ae107b10624d5567c7173)), closes [184#note_1170995317](https://gitlab.com/hoppr/184/issues/note_1170995317)
* stage name regex field type ([3d4c133](https://gitlab.com/hoppr/hoppr/commit/3d4c13381a43ddbdcbe4629a22ef0f75b7019035))
* strip quotes to compare found URL ([396c31a](https://gitlab.com/hoppr/hoppr/commit/396c31a90846673f991867a2c2057959457222f0))
* syntax ([88b8961](https://gitlab.com/hoppr/hoppr/commit/88b8961dcc41c6a21e02da417a3c059cf0036641))
* syntax ([4e8fc18](https://gitlab.com/hoppr/hoppr/commit/4e8fc18f3bc21f00d60e9a190e94fc7a4a9d9078))
* syntax ([84c2c38](https://gitlab.com/hoppr/hoppr/commit/84c2c3888485f188af9202b9fc8c1d20fc243331))
* syntax ([762a702](https://gitlab.com/hoppr/hoppr/commit/762a702417b358c3fd0b568c631bbd0c9a5a5aef))
* temporary peg of securesystemslib to address in-toto bug ([8261f41](https://gitlab.com/hoppr/hoppr/commit/8261f414b9951ede1e5a7efc44dfa429d6cf5790))
* test image tags ([0180488](https://gitlab.com/hoppr/hoppr/commit/01804886f7a709e105e432d53d96c7ca19c67a80))
* test pattern matching ([c6e7c51](https://gitlab.com/hoppr/hoppr/commit/c6e7c5197847e13f54a14cf112ca58d5cad86602))
* **test:** credentials unit test coverage ([74b981f](https://gitlab.com/hoppr/hoppr/commit/74b981f51e58adbd5581e815f239f97e54003ed0))
* transfer file default value ([ac1bf1a](https://gitlab.com/hoppr/hoppr/commit/ac1bf1a775d534bd89217443c1711bebd49e957b))
* turn off allow-failure on semantic-release:dry-run job to avoid breaking changes from renovate ([3a32a65](https://gitlab.com/hoppr/hoppr/commit/3a32a656abdb3c2f7cb006547a353f3fdae615b5))
* type check errors ([02b3d27](https://gitlab.com/hoppr/hoppr/commit/02b3d2713ccd8169643121abfb5ec206d01bbe4b))
* type error ([25aea9c](https://gitlab.com/hoppr/hoppr/commit/25aea9cb031b72a620c04029fce89872e0a9f3cd))
* type errors ([30861b6](https://gitlab.com/hoppr/hoppr/commit/30861b6abc6810f234c85a9de1d6db64c826430b))
* unit test context missing delivered sbom ([0c3d27a](https://gitlab.com/hoppr/hoppr/commit/0c3d27ae52d28492b3080249430099a6cd3cd93f))
* unit test message ([81415ed](https://gitlab.com/hoppr/hoppr/commit/81415edb08a9a6f925a911939095a5c001107531))
* unit test message ([dc95a02](https://gitlab.com/hoppr/hoppr/commit/dc95a025e8d295c4b0572ba105ede3b69f8a4738))
* unit test message ([69a164f](https://gitlab.com/hoppr/hoppr/commit/69a164f500097c97b29cf408a9ca695d6a3505e3))
* unit test message ([5846f61](https://gitlab.com/hoppr/hoppr/commit/5846f6109f3e5adf55ec6bfa4596e95f002e1ead))
* Update artifact name ([4f64b03](https://gitlab.com/hoppr/hoppr/commit/4f64b03b8a36590887e8430aa5caf3a48e25cd7a))
* update assert ([eba7ec9](https://gitlab.com/hoppr/hoppr/commit/eba7ec90d1927369be2bb91115388ec3fab0ec58))
* update CODEOWNERS for repo move ([6541b0c](https://gitlab.com/hoppr/hoppr/commit/6541b0c6ce79ed1dc0e4156d2e39a2620ee4b0c4))
* update delivered_sbom with process return objects ([46da6f3](https://gitlab.com/hoppr/hoppr/commit/46da6f324eca0ff4bda4077f385ff2ad20e87177))
* Update expected toc (since it's changed), and correct the regression boms ([fe79087](https://gitlab.com/hoppr/hoppr/commit/fe79087aeef00eab3792de8c5d3ea343ab9ce4d3))
* update expected toc's with delta stage intermediate delivered bom ([f016526](https://gitlab.com/hoppr/hoppr/commit/f0165265077c35e578b62d252c384594732e0d3c))
* update expected toc's with delta stage intermediate delivered bom ([3291773](https://gitlab.com/hoppr/hoppr/commit/32917735d1e84afa74aa584c83f899206bb3cfea))
* update expected_tar_toc files for changed maven file names ([b1ef013](https://gitlab.com/hoppr/hoppr/commit/b1ef01389841407c0723a2ced6bc397709c2a24d))
* Update git collector depth defaults per MR suggestion ([43a3dbb](https://gitlab.com/hoppr/hoppr/commit/43a3dbb296db4a43f2a70f9eb87a6d56ad3ff510))
* Update maven write to disk ([82d42b6](https://gitlab.com/hoppr/hoppr/commit/82d42b6dbce5eec0868199da0c1f4c32c9a99d61))
* update Result object to include optional return object.  Check that return object matches plugin Bom Access value ([b346872](https://gitlab.com/hoppr/hoppr/commit/b3468720b276af24d95c0e748ecb3701a4fd0764))
* Update variables to contain artifact name ([0400e28](https://gitlab.com/hoppr/hoppr/commit/0400e280e42b9eec5282bb7d59fab3ae100a1872))
* updated expected tar for pypi ([7356d6b](https://gitlab.com/hoppr/hoppr/commit/7356d6b6c82ee1d54dcd22158451664dd18c7779))
* Updated unit tests for docker collection ([7dbd2af](https://gitlab.com/hoppr/hoppr/commit/7dbd2af98c871d8cfdc20b48f0164fc8cb861ff0))
* updates to ensure pipeline works for forks. ([5fa6fe9](https://gitlab.com/hoppr/hoppr/commit/5fa6fe98921248797dc75375d500be9ab2f37381))
* urljoin stripping path components ([4c6ad9f](https://gitlab.com/hoppr/hoppr/commit/4c6ad9f3de6d2351b4eb274b884062d1b0c6e841))
* use enums for constants, per code review ([40cc853](https://gitlab.com/hoppr/hoppr/commit/40cc8538d5921846202167e7f0089a18d1719137))
* Use list of options and check for troublesome values ([fb39372](https://gitlab.com/hoppr/hoppr/commit/fb39372b37ee1bc5eec7ac49e618a521d38b5be3))
* Use long switches for readability. ([fdbfec2](https://gitlab.com/hoppr/hoppr/commit/fdbfec26f6f528ce1c79bff518b4f61f2708db5d))
* use more stable package for apt testing ([f365df7](https://gitlab.com/hoppr/hoppr/commit/f365df765c2bd162a9cf6c8120c720a6897fbc23))
* use more stable package for apt testing ([1aca9d7](https://gitlab.com/hoppr/hoppr/commit/1aca9d7c5d751785f8aba52bbd2cb63319fd8583))
* Use python3 for virtual environment creation in CI Dockerfile ([e39ea91](https://gitlab.com/hoppr/hoppr/commit/e39ea91f9456b7f006ef6dea5f57579376829776))
* Use semantic release to publish package ([27e8eb0](https://gitlab.com/hoppr/hoppr/commit/27e8eb02b8bd3405cec90b5d3112662dc2675c17))
* Using copy from Maven-Dependency-Plugin instead of get ([7ab2fe1](https://gitlab.com/hoppr/hoppr/commit/7ab2fe15234b1bda27f91bb0ea6b025cde2c22d2))
* windows ANSI processing ([eba2cb4](https://gitlab.com/hoppr/hoppr/commit/eba2cb4a18d7bb375e9984c838772506d6c40bb7))
* wrapper for run cmd ([9d5844b](https://gitlab.com/hoppr/hoppr/commit/9d5844b8f0661b3be8ef63d120bf5d96023fe844))


### Reverts

* files not relevant to this branch ([fe61dea](https://gitlab.com/hoppr/hoppr/commit/fe61deaf45ff0fd0e0e5252ac783f751ee6ddedf))
* files not relevant to this branch ([c66595a](https://gitlab.com/hoppr/hoppr/commit/c66595a47a8d089b98566940eb4ae565d094c5da))
* generic types ([96721e4](https://gitlab.com/hoppr/hoppr/commit/96721e42511934c36bbb9014da20d24a1b2b9546))
* generic types ([9915c1a](https://gitlab.com/hoppr/hoppr/commit/9915c1aa2bb39bbccdc959b2584f56954698d84c))
* method_name condition logic ([6d9a16b](https://gitlab.com/hoppr/hoppr/commit/6d9a16bc87de3e8cce5a835093076ea76242b627))
* method_name condition logic ([6445f18](https://gitlab.com/hoppr/hoppr/commit/6445f18cb62e61bb2b8738bd46dbd75df766e961))
* model integration changes ([e27eb91](https://gitlab.com/hoppr/hoppr/commit/e27eb91fcc8a8b5cc2e0c6f110c6de1e37010f82))
* shared memory manager changes ([09a6ee3](https://gitlab.com/hoppr/hoppr/commit/09a6ee348e8820fc91d5ac5ad2016f8e96aa041e))
* shared memory manager changes ([4dbd8c0](https://gitlab.com/hoppr/hoppr/commit/4dbd8c0fc99b7e2d52b717bc555b326962a27467))

### Features

* --no-strict CLI flag ([05cf05c](https://gitlab.com/hoppr/hoppr/commit/05cf05caea72c11823c635e391a8dd10eb6bd0d4))
* --no-strict CLI flag ([12bc0d3](https://gitlab.com/hoppr/hoppr/commit/12bc0d3200a5f19cf5c04b0cf8eb8423f9c00c4c))
* add composite collector ([47037b8](https://gitlab.com/hoppr/hoppr/commit/47037b89e38e8694d803309eed486245a461a4b2))
* add delta_sbom capability ([c73e081](https://gitlab.com/hoppr/hoppr/commit/c73e0816444cefa4547917da54dc650e7777d7da))
* add delta_sbom capability ([a7d265c](https://gitlab.com/hoppr/hoppr/commit/a7d265ccbe6254a78f5cf5e1b9808c7a3eb570a5))
* add delta_sbom capability ([5bb5a59](https://gitlab.com/hoppr/hoppr/commit/5bb5a599d0c111399101f5e676f20e19c1954022))
* add delta_sbom capability ([9a39d88](https://gitlab.com/hoppr/hoppr/commit/9a39d88b53f1f277aecfa1c6d2edefe82098e801))
* add nexus search collector ([7ccb05e](https://gitlab.com/hoppr/hoppr/commit/7ccb05e01300cb9dde702185d28bc2941c83c3fc))
* allow docker collector to use docker.io when --no-strict option is set ([1163405](https://gitlab.com/hoppr/hoppr/commit/116340559d89fa8f6618dc48b524ff6b0a42c66c))
* creation of in-toto attestations ([b58a973](https://gitlab.com/hoppr/hoppr/commit/b58a973e9b7bd5a3d52e5f23d4cdd97155b5143e))
* Report Generation Plugin ([126a0e5](https://gitlab.com/hoppr/hoppr/commit/126a0e5430be13824e67037225b617f24965f79c))
* skip collecting components with a scope of excluded ([e2fd680](https://gitlab.com/hoppr/hoppr/commit/e2fd680389100d216089a0a1aeedcbf4121b635b))


### Bug Fixes

* Add additional configuration capability ([dc14c57](https://gitlab.com/hoppr/hoppr/commit/dc14c5728f5869f9f47f0f2e18cc1b01812e6201))
* Add additional fixes per MR feedback ([598b9f1](https://gitlab.com/hoppr/hoppr/commit/598b9f1c65bf1dcee2cf31dd84553143be73b25f))
* Add Bot label to renovate MRs ([f43cd65](https://gitlab.com/hoppr/hoppr/commit/f43cd65250b65e93eb1cfa3debdf74a3bad58807))
* Add build arg to dockerfile ([ae46f93](https://gitlab.com/hoppr/hoppr/commit/ae46f93d360ab0639d706036721b48769b4c2feb))
* add collection metadata to sbom for apt components ([b8284f5](https://gitlab.com/hoppr/hoppr/commit/b8284f54fc8d0d193bad0916dc7ce83d7ff97e83))
* add collections params for docker ([228d327](https://gitlab.com/hoppr/hoppr/commit/228d327a31b2a7e043af38a016b98d20825f5348))
* add command line option to override previous collection location in delta_sbom plugin ([b3eb0bd](https://gitlab.com/hoppr/hoppr/commit/b3eb0bd1b1b40c936d34c7075fcf525ddf2e76c7))
* add command line option to override previous collection location in delta_sbom plugin ([3485048](https://gitlab.com/hoppr/hoppr/commit/34850483f44788e7e7cd44965984bba6b6502922))
* add command line option to override previous collection location in delta_sbom plugin ([d818730](https://gitlab.com/hoppr/hoppr/commit/d818730a0b6c4d930cdb220f31c2555a21aff42a))
* add command line option to override previous collection location in delta_sbom plugin ([2331668](https://gitlab.com/hoppr/hoppr/commit/2331668aa793c290764f6107dcfbe5f787dbba04))
* Add curl for oras test ([804f8b0](https://gitlab.com/hoppr/hoppr/commit/804f8b0b3d665d40e5299d21ec84f847a2a6e7d1))
* add delivered_sbom to context, work from that variable ([07b6ba9](https://gitlab.com/hoppr/hoppr/commit/07b6ba93a7d5b5b939ad53e4ad0d408276cd337a))
* Add entrypoint to hoppr ([073fdeb](https://gitlab.com/hoppr/hoppr/commit/073fdebdae77d9db6774ab1b84c2a336df68ad52))
* add expected-tar-toc for remaining integration tests ([7bbcac9](https://gitlab.com/hoppr/hoppr/commit/7bbcac9c821b6528edf9f7b247886b11ee18db45))
* add expected-tar-toc for remaining integration tests ([a071522](https://gitlab.com/hoppr/hoppr/commit/a07152294d7430bd552255dd911a9730ae526061))
* add expected-tar-toc to delta integration test ([40fdb97](https://gitlab.com/hoppr/hoppr/commit/40fdb9778b2b20c0b07aa475e5de931a6e4cf227))
* add expected-tar-toc to delta integration test ([33ed419](https://gitlab.com/hoppr/hoppr/commit/33ed41925bedc0258c2bb3a537ccb6a0b4311b39))
* Add hopctl docker image ([1363cec](https://gitlab.com/hoppr/hoppr/commit/1363cec9d1214acfed27c03b3c785fdcf29e7be2))
* Add hopctl docker image ([c4d00fc](https://gitlab.com/hoppr/hoppr/commit/c4d00fc04074112c5fddad91005cb6361ddaf8fc))
* Add hopctl docker image ([5ad0556](https://gitlab.com/hoppr/hoppr/commit/5ad0556d41e665fe20706d6e34833ab835ead8e7))
* add initial checks for BOM access ([d89fed2](https://gitlab.com/hoppr/hoppr/commit/d89fed21d1c807f17d1089e748d38bcb97fc9dde))
* add integration test for deltas ([e0c7e47](https://gitlab.com/hoppr/hoppr/commit/e0c7e47619424601f867040064a5fc6df402e29b))
* add integration test for deltas ([2ca6ed0](https://gitlab.com/hoppr/hoppr/commit/2ca6ed0019f9b21613193a5dd525b470a12a1d56))
* add integration test for deltas ([6789f9c](https://gitlab.com/hoppr/hoppr/commit/6789f9c240a110f10e717dc42c3cff40ba4352e6))
* add integration test for deltas ([c56d066](https://gitlab.com/hoppr/hoppr/commit/c56d0664ddeb49fe6bc4b4cf997ebd782a761163))
* add missing import ([603f219](https://gitlab.com/hoppr/hoppr/commit/603f21910e7d2cbf3f98813ba45a22454330b0ff))
* add missing strict_repos typer argument ([ad9bb37](https://gitlab.com/hoppr/hoppr/commit/ad9bb3736d57cea04ff902aa2241c88ba426d394))
* add mock for os.path.exist ([5463b80](https://gitlab.com/hoppr/hoppr/commit/5463b801539bbf1436e41b0aa0827cb2a54f2b9a))
* Add oci artifacts for reference ([f6661be](https://gitlab.com/hoppr/hoppr/commit/f6661beba1f1fff54a779ea973a5869a72c6616d))
* Add oras integration test ([cbd9376](https://gitlab.com/hoppr/hoppr/commit/cbd937615039f3f99451bf600ae40948e69f0049))
* add purl-type/repo-type mappings to nexus_search collector ([1b3d06a](https://gitlab.com/hoppr/hoppr/commit/1b3d06a04e67dcf0acc4bd2dd55eca07aa282329))
* add pytest-cov package ([a6666a8](https://gitlab.com/hoppr/hoppr/commit/a6666a84c85d0e7939519506226fef23dd05c221))
* add repo to purl type list ([01fa2f2](https://gitlab.com/hoppr/hoppr/commit/01fa2f254d56a06481e4b79e29ecd3f184ce7e04))
* add repository/directory properties to bom for all collectors ([bad3136](https://gitlab.com/hoppr/hoppr/commit/bad3136b6f7dd1fc7579afd5230cd28c5aa26b1b))
* add shared logfile lock ([e350189](https://gitlab.com/hoppr/hoppr/commit/e350189654b9461759e1483458489e42db510f1e))
* add shared logfile lock ([3c3c238](https://gitlab.com/hoppr/hoppr/commit/3c3c23803a56eaad75eaa41090b5ed258393ea0d))
* Add test verification for oras bundle ([77f04b7](https://gitlab.com/hoppr/hoppr/commit/77f04b775b030db593ade6d4437a8e6ff0309be6))
* add version to bom plutin property ([166538c](https://gitlab.com/hoppr/hoppr/commit/166538cf487149e3932a8a243742f8af016db123))
* Added user_env support to find_credentials ([ee58f2d](https://gitlab.com/hoppr/hoppr/commit/ee58f2d48766562152be513083038b226ed9ef98))
* address issue where in-toto was looking maven files ([98fc06a](https://gitlab.com/hoppr/hoppr/commit/98fc06aaeea92bd2eacb459f1d4ca9fa97cf34f0))
* all source distros from manifest repos ([52c8c80](https://gitlab.com/hoppr/hoppr/commit/52c8c8073eabedb81aa93e325c7ee03b465750c1))
* allow more full repository specification for collect_nexus_search ([e2cdf02](https://gitlab.com/hoppr/hoppr/commit/e2cdf027754a2f1568035fe5303a8e62024a00fa))
* allow more full repository specification for collect_nexus_search ([6ebcceb](https://gitlab.com/hoppr/hoppr/commit/6ebccebab92c792fc335ba1251908faf69e77dab))
* allow no scheme for repo URLs as last resort ([866382f](https://gitlab.com/hoppr/hoppr/commit/866382f298a701eadb13cd8b609b74b5e13da88a))
* allow no scheme for repo URLs as last resort ([fd1feec](https://gitlab.com/hoppr/hoppr/commit/fd1feecf559855b4eb3b8127d848f1daf2c72a19))
* allow spaces in stage name ([d7af57f](https://gitlab.com/hoppr/hoppr/commit/d7af57f3892e34fd62b5bfe1dae9f4c5d3704657))
* allow spaces in stage name ([923c043](https://gitlab.com/hoppr/hoppr/commit/923c043b1aa7b686e4777844372272aafb9a7e77))
* append dev instead of current branch name ([d6abddc](https://gitlab.com/hoppr/hoppr/commit/d6abddc66d3b832feb1923d1eb31d0557b03c892))
* Apply correction to git repository collector ([dc522b5](https://gitlab.com/hoppr/hoppr/commit/dc522b555ad2965ba7965f4630e0e1f7d1eb2849))
* apply in-toto suggestions ([3e92177](https://gitlab.com/hoppr/hoppr/commit/3e92177f671c4781907c0a6df279293e543ebb2f))
* apply in-toto suggestions ([401757e](https://gitlab.com/hoppr/hoppr/commit/401757eae524fe76d7eda6f39ab38ff20dd0e01c))
* applying MR suggestion ([c3adeee](https://gitlab.com/hoppr/hoppr/commit/c3adeee1813eba8dd308fec358d27af9f25c1f1d))
* applying MR suggestion ([4e5ca7f](https://gitlab.com/hoppr/hoppr/commit/4e5ca7fb7258c667a431653ab01bb8745a87308f))
* applying MR suggestion ([4e61f84](https://gitlab.com/hoppr/hoppr/commit/4e61f84272b14180e16f6a4baa424f5d60eb2752))
* applying MR suggestion ([b034b72](https://gitlab.com/hoppr/hoppr/commit/b034b7273179a7fb8bd3bda411050d9815b58ce7))
* apt collector _get_download_url_path ([aa988d6](https://gitlab.com/hoppr/hoppr/commit/aa988d680fe350e5716f30abbed0437e51ae9797))
* apt collector _get_download_url_path ([1d36fd4](https://gitlab.com/hoppr/hoppr/commit/1d36fd457fc60d9d9638611e3b91940a2a523194))
* bom helm chart version ([009bdf0](https://gitlab.com/hoppr/hoppr/commit/009bdf00412fe6964b81c15b86aec52a761890df))
* Branch isolation testing ([fcde124](https://gitlab.com/hoppr/hoppr/commit/fcde12425dd610fe3da0dbbd56de6af677a8688e))
* bug with with attestations created from GitLab CI Runners ([f8ca8e7](https://gitlab.com/hoppr/hoppr/commit/f8ca8e79144b6377250d38ccc37bb2f7689d2033))
* casing for "kind" field ([26b0927](https://gitlab.com/hoppr/hoppr/commit/26b0927a143763f688215b0fc3d49e27a56f0b71))
* casing for "kind" field ([533f6a7](https://gitlab.com/hoppr/hoppr/commit/533f6a770bc8738aa39b22c2faa6d262c4b7264d))
* catch exception from _get_required_coverage, check for empty/missing boms ([d905e35](https://gitlab.com/hoppr/hoppr/commit/d905e3557ce6def86b923db7961f3631808f3445))
* check for exception not thrown in pypi success ([0ba41f6](https://gitlab.com/hoppr/hoppr/commit/0ba41f6a9962ede186765b25e7939623fbb15095))
* check tar toc on integration tests ([aafc6bb](https://gitlab.com/hoppr/hoppr/commit/aafc6bb6eb9215e0c158a1de007fc67a8509059e))
* check tar toc on integration tests ([105c87a](https://gitlab.com/hoppr/hoppr/commit/105c87a4c81809c1a586713d42ff1e857e0c260e))
* Clean up artifact name ([a59aaac](https://gitlab.com/hoppr/hoppr/commit/a59aaacc2bb23988b8e240d0f6c237b547e86f39))
* Clean up other variables in ci ([d289184](https://gitlab.com/hoppr/hoppr/commit/d28918482ceb951461725095e72f7ca9ebcb23ce))
* clean up repository_url handling ([c867a3b](https://gitlab.com/hoppr/hoppr/commit/c867a3bda17c70e59e1d50672543077b7daae8cb))
* clean up repository_url handling ([6fb544b](https://gitlab.com/hoppr/hoppr/commit/6fb544b1ceb0846d096cf790efa8f52a7160960c))
* clean up repository_url handling ([95a0288](https://gitlab.com/hoppr/hoppr/commit/95a0288eeecd7c0baf03af5b67dd0d50fb549708))
* clean up repository_url handling ([011e53b](https://gitlab.com/hoppr/hoppr/commit/011e53b8b96760f03863b5a9582f91ad30f98733))
* Clean up rules and workflow ([69e9b73](https://gitlab.com/hoppr/hoppr/commit/69e9b73ebab517658ad0e5238038e01979f6e98a))
* Cleaned up maven command ([2b4af6e](https://gitlab.com/hoppr/hoppr/commit/2b4af6e57658f5f96a930cf11542f802f783476d))
* Cleanup notes in config ([2632bfa](https://gitlab.com/hoppr/hoppr/commit/2632bfabe9794542033ce8b4dd8d6a5cb4e96da8))
* Cleanup rules ([ea79632](https://gitlab.com/hoppr/hoppr/commit/ea7963280418793154b6cd113c926ff160ce011e))
* Cleanup rules ([76d65e5](https://gitlab.com/hoppr/hoppr/commit/76d65e5cf60559f4672f63dd33a62c45f3009eab))
* clear loaded manifests ([39e7341](https://gitlab.com/hoppr/hoppr/commit/39e73413a9e446d1f5b98118ff8b3ec2799260e5))
* code review comments ([c9fa7e2](https://gitlab.com/hoppr/hoppr/commit/c9fa7e2b37422a04c4a2281b81b425d46bcedb12))
* Code review comments ([a204676](https://gitlab.com/hoppr/hoppr/commit/a204676aa26173864856e458a30f119caeb3ae9a))
* complete unit test coverage for collect_nexus_search ([24827e4](https://gitlab.com/hoppr/hoppr/commit/24827e4cab02da2808deb99fd372c6a6b3935ed7))
* component search sequence ([ff89f56](https://gitlab.com/hoppr/hoppr/commit/ff89f5633b386b6b006ff7f76b6558ad894f2bb2))
* component search sequence ([397d3bf](https://gitlab.com/hoppr/hoppr/commit/397d3bf639ed8e729fb4e209078cd0877ae6b07f))
* Correct build artifacts ([40ca47f](https://gitlab.com/hoppr/hoppr/commit/40ca47f2642bd82be5aec18f75a3f468c1d880e7))
* Correct deployment teir ([d4282c8](https://gitlab.com/hoppr/hoppr/commit/d4282c8c1e54fe188acfde63a9307c33474c9f8b))
* Correct deployment teir ([983932d](https://gitlab.com/hoppr/hoppr/commit/983932d32f998ca41536002671b324734d711716))
* Correct dockerfile ([30f438a](https://gitlab.com/hoppr/hoppr/commit/30f438a16501280c0dd97633d3e2447e2c80998e))
* Correct git bom, had incorrect purl, name, and version ([e6a1265](https://gitlab.com/hoppr/hoppr/commit/e6a1265e9862aa66996e10a7c956a1315bcfc89b))
* Correct integration tests ([b065953](https://gitlab.com/hoppr/hoppr/commit/b0659532c14ddea25afcb7357d14104427cddefe))
* Correct media types for registry ([d3ea0df](https://gitlab.com/hoppr/hoppr/commit/d3ea0df0cd2eb276eb86d1cb82dfceeb39604e71))
* Correct release yaml file ([a29c26c](https://gitlab.com/hoppr/hoppr/commit/a29c26c86220443c22e94954bd6db5d3e7a59147))
* Correct releaserc file ([8868fab](https://gitlab.com/hoppr/hoppr/commit/8868fab85c0ed587f8d9679af6c8d793ed78c50b))
* Correct releaserc.yml ([4b55f7f](https://gitlab.com/hoppr/hoppr/commit/4b55f7f25895b1251ab7e9499280848d86732cbb))
* Correct requirements ([7598beb](https://gitlab.com/hoppr/hoppr/commit/7598beb540c9c26860501b42ba92fb807f79fd33))
* Correct simlinks in ci docker ([deb0728](https://gitlab.com/hoppr/hoppr/commit/deb07284c27603325678ae3332f0a2023ed42f80))
* Correct syntax in ci docker ([f96c938](https://gitlab.com/hoppr/hoppr/commit/f96c938502b98e0bdbe4096ed6a35f830abaf378))
* Correct tests so they pass ([eeebaef](https://gitlab.com/hoppr/hoppr/commit/eeebaef2b781cde5e668d0d75e65977185d56d45))
* Correct the oras binary arch type ([71bc21e](https://gitlab.com/hoppr/hoppr/commit/71bc21ec0b5308c4299906ab04b270b678480551))
* Correct the wheel name ([4847b4a](https://gitlab.com/hoppr/hoppr/commit/4847b4a8548c563ad4298616f8c16288ca925aaa))
* Correct trivy timeout ([5a9246a](https://gitlab.com/hoppr/hoppr/commit/5a9246ae86bb2e517cb39c3c7b5d240a9fdb8bec))
* Corrected Maven-Dependency-Plugin arguments so that maven artifacts would be bundled ([9fe9eaa](https://gitlab.com/hoppr/hoppr/commit/9fe9eaa13c68ed8d36437a6662f735c347018ed4))
* credential.find method no longer needs exact match.  added lines to _run_data_ metadata file. ([202b0ae](https://gitlab.com/hoppr/hoppr/commit/202b0ae90ba70cf8d45990064e4b3d2ce6b9fb4a))
* Cut release from next branch ([17d0d03](https://gitlab.com/hoppr/hoppr/commit/17d0d03ac243fea51d9ea98738d0e8293c60803d))
* Cut release from next branch ([e1c56d2](https://gitlab.com/hoppr/hoppr/commit/e1c56d28394803045b974195e6e7d9cc796c967b))
* **deps:** update dependency hoppr-cyclonedx-models to v0.2.10 ([ee37064](https://gitlab.com/hoppr/hoppr/commit/ee370642914f1b507ed4793e11cbc575d1c51720))
* **deps:** update dependency typer to ^0.7.0 ([a35bd85](https://gitlab.com/hoppr/hoppr/commit/a35bd8519c8486b68a9228b660a2d2f44bad7f2c))
* dev branch test; update README ([f8ed5ee](https://gitlab.com/hoppr/hoppr/commit/f8ed5eec1a2b8d68730b88d6d378d132fa67c1c7))
* DNF download directly from found URL ([bf84cec](https://gitlab.com/hoppr/hoppr/commit/bf84cec779df983642116cf99cf42403675f17e9))
* DNF download directly from found URL ([11d5e5e](https://gitlab.com/hoppr/hoppr/commit/11d5e5ec6280b60d821880833d5ee7f0d3683432))
* do not re-generate consolidated/delivered sboms ([ee1928b](https://gitlab.com/hoppr/hoppr/commit/ee1928b6198e0fb57c3eb56af0dc3b5c057166d2))
* do not re-generate consolidated/delivered sboms ([ce59dcc](https://gitlab.com/hoppr/hoppr/commit/ce59dcc46fbbef3f3941c0f33c3d0cc0c0abacf7))
* Don't run pipelines on merge event ([a0b8c53](https://gitlab.com/hoppr/hoppr/commit/a0b8c53add628e08a0c02f30beea57a399b9f109))
* Ensure python is on the path ([a1c03eb](https://gitlab.com/hoppr/hoppr/commit/a1c03eb6e2e607267886890831c2675572525d6b))
* enum base type ([4e3577c](https://gitlab.com/hoppr/hoppr/commit/4e3577c8b96bd5c107649c3d6adb6ef1dd34e4d1))
* exclude nulls from output sboms ([e41bd52](https://gitlab.com/hoppr/hoppr/commit/e41bd52190ec8acba3fdd419650674e7739224f7))
* expected apt SBOM ([c6b01e9](https://gitlab.com/hoppr/hoppr/commit/c6b01e935d7aadf1f2fe3410edf901d95c9f8fb3))
* expected apt SBOM ([8e7d3da](https://gitlab.com/hoppr/hoppr/commit/8e7d3daaab31eccc1686963d30bbbd72c70f6bf0))
* expected metadata source location ([53f74d7](https://gitlab.com/hoppr/hoppr/commit/53f74d7f154df37e45d91e782bfbee5f30570fd9))
* expected metadata source location ([f918d63](https://gitlab.com/hoppr/hoppr/commit/f918d63976bfb5ac57405ea48b30c9b34e8fe0a6))
* expected-tar-toc sort order changed ([b89f853](https://gitlab.com/hoppr/hoppr/commit/b89f853e5547aac8e0a69b7245f8f3abddc5d70f))
* fail on no change, improved status messages ([801dea2](https://gitlab.com/hoppr/hoppr/commit/801dea20d82167b303ebbd0a339cb8e408c9b873))
* fail on no change, improved status messages ([1a4c87c](https://gitlab.com/hoppr/hoppr/commit/1a4c87ceeceaabbe3ecf13e905fdd9a08866e647))
* fail on no change, improved status messages ([fa610d0](https://gitlab.com/hoppr/hoppr/commit/fa610d09a73bf6736e9792096614e08a84a6c8aa))
* fail on no change, improved status messages ([69d15b1](https://gitlab.com/hoppr/hoppr/commit/69d15b1c4e30684fe59020b96b002a7f1d27c30c))
* fast forward branch ([1428ecb](https://gitlab.com/hoppr/hoppr/commit/1428ecb2ff108f25ca0c424f53984771a16e8e8e))
* Fixed Docker repo:tag information being lost in collection ([5111f7f](https://gitlab.com/hoppr/hoppr/commit/5111f7fe86bd2c833ac4c0bb0392932b599f6549))
* Fixed Docker repo:tag information being lost in collection ([3319a36](https://gitlab.com/hoppr/hoppr/commit/3319a36bd90ca7798141ee0e7fe92bb6c361559d))
* fixed issue with bundle options for functionary_key ([d798606](https://gitlab.com/hoppr/hoppr/commit/d7986062051b5b0be426dda0842acb1bab601ec2))
* Fixed the releaseing issue ([ce8b2f5](https://gitlab.com/hoppr/hoppr/commit/ce8b2f57703bfaf0984e2b750005f866a8ad8b35))
* Fixed type-check findings ([7bb21df](https://gitlab.com/hoppr/hoppr/commit/7bb21df1b19f3f2ed8f275db995c722db9e78ed0))
* Fixed unit tests ([4e80266](https://gitlab.com/hoppr/hoppr/commit/4e8026652458dc5c35bdc3dfc5a983727e5bf44f))
* Fixes [#150](https://gitlab.com/hoppr/hoppr/issues/150) and corrected when consolidated and deliveried SBOMs are written. It also address additional unit testing ([5adf219](https://gitlab.com/hoppr/hoppr/commit/5adf2193b30af0b6aba0c901dd369df324d53e19))
* Fixing Merge Conflicts ([d24cff2](https://gitlab.com/hoppr/hoppr/commit/d24cff258f10542bdc9b1af462c3a61bc1af2722))
* generate dev version if not on main/dev ([3aa6ec5](https://gitlab.com/hoppr/hoppr/commit/3aa6ec50f186ee520eb1279160c8b1a0715711b2))
* Get better results ([688e176](https://gitlab.com/hoppr/hoppr/commit/688e1766cffb34cc155e0ea0c3f1d7a608c58947))
* gitattributes ([5d3bde3](https://gitlab.com/hoppr/hoppr/commit/5d3bde370513376a36b9a0cc7279b9464fd8a09a))
* Grab versioning properly ([cc052cf](https://gitlab.com/hoppr/hoppr/commit/cc052cfcedbd149b295e090c493adb650b47f1ce))
* have nexus collector respect user-specified purl types ([9116a03](https://gitlab.com/hoppr/hoppr/commit/9116a034125ccbe2eb47d06e0172176e3eae2d86))
* helm collector append purl name ([5574739](https://gitlab.com/hoppr/hoppr/commit/5574739d2e34ba0a538ee8295939e2ad473144f5))
* hoppr group ([c21bc29](https://gitlab.com/hoppr/hoppr/commit/c21bc295fb9ab40756e107b5047e2cb3d36d8a06))
* image tag reset after rebase ([45a850a](https://gitlab.com/hoppr/hoppr/commit/45a850a4c43f29c7253c4c97620c1fc6cce32fb5))
* import source type error ([464211c](https://gitlab.com/hoppr/hoppr/commit/464211cddc9f7ea6c7a5182449155e4c7467289c))
* Improve config error logic in git collector ([cb3681a](https://gitlab.com/hoppr/hoppr/commit/cb3681a6f95c8cc031ca69276f84015925659296))
* Improve matching pattern and logging ([12a68eb](https://gitlab.com/hoppr/hoppr/commit/12a68eb3ab2e049260bba97ddf8f7c0fc585a45d))
* include resources folder in poetry build ([5ae0ffb](https://gitlab.com/hoppr/hoppr/commit/5ae0ffb5d761acd41c7fafd6a0cfa43f40d7eafd))
* included manifest repo merge, add tests ([359364c](https://gitlab.com/hoppr/hoppr/commit/359364c453f72cc35cafd752b0b21a7aac79e0c3))
* included manifest repo merge, add tests ([1c22c6e](https://gitlab.com/hoppr/hoppr/commit/1c22c6eec5ed7c3c1dc01e3d70e6241f0da7f7ee))
* increase minimum unit test coverage to 100% ([9c697e3](https://gitlab.com/hoppr/hoppr/commit/9c697e35712ee8e8138848f506de71ff7054d71b))
* lint error ([990ef3f](https://gitlab.com/hoppr/hoppr/commit/990ef3ff6d79359c45481f0deeb640a4ec629ccb))
* lint error ([f9f2cc0](https://gitlab.com/hoppr/hoppr/commit/f9f2cc0095060a84642717a0d77e28d26d97467b))
* linting union for type hints ([bbaf8f0](https://gitlab.com/hoppr/hoppr/commit/bbaf8f0f688c30d206bc1842cd19b616bda0b949))
* main module exits using sys instead of typer ([356112e](https://gitlab.com/hoppr/hoppr/commit/356112e622d329d53a0902532476957f92790491))
* main process logfile lock ([4519f53](https://gitlab.com/hoppr/hoppr/commit/4519f53808064a9829f46bfec19f06156966a59c))
* Make a quicklink script for linking python in dockerfile ([ae3dace](https://gitlab.com/hoppr/hoppr/commit/ae3dace2b4101bc9c73381dcf097baf505965e25))
* Make semantic release pass all jobs ([ddc3c49](https://gitlab.com/hoppr/hoppr/commit/ddc3c495048e8be5302bf9c7ba7a4ad5418944a7))
* manifest helm repo URL ([438fae5](https://gitlab.com/hoppr/hoppr/commit/438fae572062f9726c451031a88e403a67390c99))
* manual version revs because lock file conflicts spam ([e8a473f](https://gitlab.com/hoppr/hoppr/commit/e8a473ffd01938734a7a84180e78d075927240c4))
* merge components, add verification tests ([f0ecf1e](https://gitlab.com/hoppr/hoppr/commit/f0ecf1e7f7471b17dbbf42e6aa80e8dedc46f76b))
* merge components, add verification tests ([ce6e4d1](https://gitlab.com/hoppr/hoppr/commit/ce6e4d17cd8ce4d1127956d1f72b2f346ee7402a))
* Merge dev into next ([765d25a](https://gitlab.com/hoppr/hoppr/commit/765d25a29cc9d3fed6794a01d25892118408e177))
* Merge main into branch ([e2fd972](https://gitlab.com/hoppr/hoppr/commit/e2fd97246baadff00a84b1fecb0b8a79412e70ee))
* **Minor:** Image build cleanup ([ba4e6c3](https://gitlab.com/hoppr/hoppr/commit/ba4e6c3bc68fe469b3ad68ed636f43b55c84f425))
* missing `rev` command in skopeo image ([cb38a3e](https://gitlab.com/hoppr/hoppr/commit/cb38a3e8ae5dc2afcac4bdbb87f110d6af58997e))
* missing collector plugin CLI tools ([b1284ba](https://gitlab.com/hoppr/hoppr/commit/b1284ba6c1df27487e36279c5af5478b27450322))
* move insertion of required stages to Transfer object creation ([fa0d31b](https://gitlab.com/hoppr/hoppr/commit/fa0d31b99fcc55411cc2fd32bcfb7f050b14fba9))
* move insertion of required stages to Transfer object creation ([a7da1d5](https://gitlab.com/hoppr/hoppr/commit/a7da1d53253de835a764f3d2ab68f052faa03aa7))
* move insertion of required stages to Transfer object creation ([57604d5](https://gitlab.com/hoppr/hoppr/commit/57604d5e724fadcd71694b466a82b19a9696d624))
* move insertion of required stages to Transfer object creation ([53243ab](https://gitlab.com/hoppr/hoppr/commit/53243abcb44c5c031af5c1cc625b8cf81e2b3954))
* moved test_main.py under test/unit/ directory ([90df7ad](https://gitlab.com/hoppr/hoppr/commit/90df7adb7fd293b7b3da96ac70ca159fe0d8f480))
* mypy errors ([b08426c](https://gitlab.com/hoppr/hoppr/commit/b08426ca5b7af32c8d192d2309381b19d03d136d))
* mypy errors ([d94d090](https://gitlab.com/hoppr/hoppr/commit/d94d090f231edaa21de806bc8ada8743326c57c7))
* mypy errors ([d612474](https://gitlab.com/hoppr/hoppr/commit/d612474d5f71b79683280781ca3cda18ee05d240))
* New branch old issue ([a906ca3](https://gitlab.com/hoppr/hoppr/commit/a906ca3cc6afc01f97483e136f9b3370312bb5bb))
* Only attempt PyPI source collect if whl not collected ([31de8ae](https://gitlab.com/hoppr/hoppr/commit/31de8ae07d414c09295db59662442ee7a15f92cc))
* only load purl-type-specific plugins when components of that type are being processed (Issue [#77](https://gitlab.com/hoppr/hoppr/issues/77)) ([1789f21](https://gitlab.com/hoppr/hoppr/commit/1789f21df784738c58b8ea3dac19e50240a2b13b))
* Only publish if there's a new release from semver dryrun ([da88e9b](https://gitlab.com/hoppr/hoppr/commit/da88e9b4a2e6449ea5550f8adcffceb975756af4))
* only rename maven file on successful collection ([3455e74](https://gitlab.com/hoppr/hoppr/commit/3455e743e92fa74ed1d8f82e66921dd41c837c63))
* Only run semantic release publish on develop and main ([0ecc904](https://gitlab.com/hoppr/hoppr/commit/0ecc904fb1583a9f530d4a469a331391007e70d1))
* parameter name typo ([9d7c3b1](https://gitlab.com/hoppr/hoppr/commit/9d7c3b1c0751816f5c108f3fb83eaec10f47a490))
* pip arguments ([0cc47f1](https://gitlab.com/hoppr/hoppr/commit/0cc47f17f3eefcd4a650d213d7d8701c59485c8b))
* platform check ([de541db](https://gitlab.com/hoppr/hoppr/commit/de541db0bf4a810a158ca175b1c38a573a1ab9e7))
* **plugin:** add type hint to auth ([0bc2e29](https://gitlab.com/hoppr/hoppr/commit/0bc2e29c0e36d0b596cd1d5f493a14566e9bd9dc))
* **plugin:** properly authenticate nexus requests ([be82ed4](https://gitlab.com/hoppr/hoppr/commit/be82ed43660ca25ed2f105a43dbd1603deb4df38))
* prepend stage if needed for deltas ([a0dbfcb](https://gitlab.com/hoppr/hoppr/commit/a0dbfcbb40649182f1fef9ce8cdb1ae5bf151c73))
* prepend stage if needed for deltas ([7bbb2b5](https://gitlab.com/hoppr/hoppr/commit/7bbb2b5d76d6245430e5e587e8e15b64f448c2d5))
* prepend stage if needed for deltas ([e93d3a0](https://gitlab.com/hoppr/hoppr/commit/e93d3a0ff654fe4aae50a96003682dcd6a7d4f74))
* prepend stage if needed for deltas ([2467c6b](https://gitlab.com/hoppr/hoppr/commit/2467c6b693832775d4739d0de41d7fc2059b5296))
* prevent loading plugins that aren't needed ([17722f3](https://gitlab.com/hoppr/hoppr/commit/17722f3238b9c5436549e3c1fbd9e7fb84f49dc3))
* prevent loading plugins that aren't needed ([b00db2b](https://gitlab.com/hoppr/hoppr/commit/b00db2b5925915fa950e14ea565e4496c4bf4aa9))
* processor relative file handling ([59636aa](https://gitlab.com/hoppr/hoppr/commit/59636aaeb06ff4bddd167fdc302b914ca3342cd5))
* processor relative file handling ([5c3cdb1](https://gitlab.com/hoppr/hoppr/commit/5c3cdb18abede20e59e52afdcaeec1de69a8298c))
* proper attestations for nexus-search- and composite- collectors ([a1a9651](https://gitlab.com/hoppr/hoppr/commit/a1a96517eebc432d83153cf4808cc17d40f17f81))
* proper attestations for nexus-search- and composite- collectors ([7ae7d7a](https://gitlab.com/hoppr/hoppr/commit/7ae7d7ae82558a5586740a4dd50a700a6a6e82b6))
* Protect keys ([9001a21](https://gitlab.com/hoppr/hoppr/commit/9001a219a93817d064a5cce207c267347b112549))
* provide better error message on empty config file content ([e87fff8](https://gitlab.com/hoppr/hoppr/commit/e87fff885842320adf85e7c86abb319e8dfa4f2d))
* pylint error ([6982109](https://gitlab.com/hoppr/hoppr/commit/69821095b2319075c65433a1f76e5d4e47fd361b))
* pylint error ([e36b242](https://gitlab.com/hoppr/hoppr/commit/e36b2424739e0dc4814855fb304dca1edd24e448))
* pylint issues fix ([7c7810c](https://gitlab.com/hoppr/hoppr/commit/7c7810c9ac63268472c2faf50e43694bdcd2cccb))
* raw collector stripping purl namespace ([1aca256](https://gitlab.com/hoppr/hoppr/commit/1aca256351c854eaa1c7f02e209961a20ece99aa))
* raw collector stripping purl namespace ([ee20ebe](https://gitlab.com/hoppr/hoppr/commit/ee20ebe02b6c1c7f1b1be9a44b82087565a5a5af))
* raw collector stripping purl namespace ([bba65cb](https://gitlab.com/hoppr/hoppr/commit/bba65cb7805b78a329ee59df1c10b9ad54959f8d))
* raw collector stripping purl namespace ([2c9309d](https://gitlab.com/hoppr/hoppr/commit/2c9309d09150511489185b455cf38d631611820b))
* README feedback; broken link fixes; try PyPI banner fix ([932121d](https://gitlab.com/hoppr/hoppr/commit/932121d087bbd2019007adb576b6c9122d4881fa))
* Rebase dev ([9399451](https://gitlab.com/hoppr/hoppr/commit/9399451c5f23b599d6d54c02538ebe4017a95620))
* redeclare Component attrs with hashable types ([a2034fa](https://gitlab.com/hoppr/hoppr/commit/a2034fa8ffd64f58b832199d0964d43893401962))
* redeclare Component attrs with hashable types ([26bbfc0](https://gitlab.com/hoppr/hoppr/commit/26bbfc064c252ce8b7e65205fe7d0c98987a5a41))
* Reference main so that develop can become default branch ([76f707b](https://gitlab.com/hoppr/hoppr/commit/76f707b6c3435bdc68d73b3717c451c6a6c95369))
* Release dev channel ([42ee862](https://gitlab.com/hoppr/hoppr/commit/42ee86236810072b2d303ea255f2d2556fd53749))
* Release dev channel ([15b4ae7](https://gitlab.com/hoppr/hoppr/commit/15b4ae76944accd1b4f0e6ae379b02c5efabb923))
* Remove blank lines ([196218c](https://gitlab.com/hoppr/hoppr/commit/196218c39d7dbb50c74722922190f61905f46053))
* remove construct method call ([2cf4963](https://gitlab.com/hoppr/hoppr/commit/2cf49637902c87dd786f37485521e8c55617685f))
* Remove gitlab semantic release comment on MRs ([4af82e6](https://gitlab.com/hoppr/hoppr/commit/4af82e6023f842a8587ec5183c5b3a212dc9e5f5))
* Remove licensing scanning and replace with policy ([e852316](https://gitlab.com/hoppr/hoppr/commit/e85231642c2f75a022d3872049b26e349821cc13))
* remove need for boolean from fail-open logic ([23bfa03](https://gitlab.com/hoppr/hoppr/commit/23bfa0397d12d39fcbca526eaadd651805e97c57))
* Remove node engine reference from package.json ([3c26b4e](https://gitlab.com/hoppr/hoppr/commit/3c26b4e768f23e9746db9f4b654a4799cb95e00b))
* Remove pack, and idx from check ([6e24462](https://gitlab.com/hoppr/hoppr/commit/6e244629bf39228cee030550da47863b11c6aec5))
* Remove package.json ([e13de8d](https://gitlab.com/hoppr/hoppr/commit/e13de8d885045b1e750fceeace1e60d4d9d032b6))
* remove prerelease ([1e038d0](https://gitlab.com/hoppr/hoppr/commit/1e038d0b73fe23da781bdea4cb02b9241e19aa8e))
* remove prerelease ([6818939](https://gitlab.com/hoppr/hoppr/commit/6818939d8237876f7dfabdce72a0dab1270db9b3))
* remove problematic git files from tar toc comparison ([b883b23](https://gitlab.com/hoppr/hoppr/commit/b883b23fbf230857a6244b7b8207144e3cc3a920))
* remove problematic git files from tar toc comparison ([89f1621](https://gitlab.com/hoppr/hoppr/commit/89f16211e602d92820cad9719257a57d2bfbcb39))
* Remove quotations around build ([3848f46](https://gitlab.com/hoppr/hoppr/commit/3848f463bb8640e4acd524bfdd04838291e9222d))
* Remove quotations from parallel matrix ([28ee2b7](https://gitlab.com/hoppr/hoppr/commit/28ee2b707fa0f36992f131e7bbb548f164b23e14))
* Remove quotes ([7323a8b](https://gitlab.com/hoppr/hoppr/commit/7323a8b16abc50d284a7d67bf7a346fe0190369c))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([0ac8adc](https://gitlab.com/hoppr/hoppr/commit/0ac8adc4c70bf79e5068daf8f8d5501c045af29a))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([87ae120](https://gitlab.com/hoppr/hoppr/commit/87ae1209adced6c7004143d33e314df719740da1))
* remove references to deleted job ([6d9890e](https://gitlab.com/hoppr/hoppr/commit/6d9890e99f43b439f20505142e2706c1556d5abf))
* remove references to deleted job ([7d1ef14](https://gitlab.com/hoppr/hoppr/commit/7d1ef14d1bb86f478ad35b5c08038b08b1fc916c))
* Remove skip ci and attempt to let workflow rules handle pipelines ([0e325a5](https://gitlab.com/hoppr/hoppr/commit/0e325a5e7a2caea831856e0ba6566f0a5ecaf2c7))
* restore Dockerfile FROM directive ([e7e0622](https://gitlab.com/hoppr/hoppr/commit/e7e06222a9768bfeaab9f10e13a9b213ccd45be7))
* Result constructor param typo ([09290d2](https://gitlab.com/hoppr/hoppr/commit/09290d25bd2be5f07635c718c7691dad6e00e652))
* Revert Changes ([f6797ea](https://gitlab.com/hoppr/hoppr/commit/f6797ea323e79a9bcd8b67b00c4939004e0dafcc))
* revert to older version of npm-check-updates ([9fb3ebf](https://gitlab.com/hoppr/hoppr/commit/9fb3ebf011be353fe119759514868151e0ddf3c2))
* Roll back chore rules ([e40fbfb](https://gitlab.com/hoppr/hoppr/commit/e40fbfbcb463ccc704c351b1acad04f3ea1fb0ea))
* Roll base image back from 9.0 to 8.6 ([31f8166](https://gitlab.com/hoppr/hoppr/commit/31f81666e3bb135ad7ad7b05d28cbc96c77859bc))
* Run if commit message doesn't start with chore ([daa5d11](https://gitlab.com/hoppr/hoppr/commit/daa5d119686ca58488cc04c62d48b73bb9c49319))
* Run pre-commit ([7b740ec](https://gitlab.com/hoppr/hoppr/commit/7b740eca0a6f3bd92d99e7c8f29c3fd435f0df4d))
* Run the entire dockerfile as one large image ([47ec5a5](https://gitlab.com/hoppr/hoppr/commit/47ec5a57dece0be112dca8c068a2807edc811bf8))
* sbom metadata ([eb5225f](https://gitlab.com/hoppr/hoppr/commit/eb5225f8f0729e44701d6b75ce7f74870f7eecbb))
* sbom metadata ([f97e7a7](https://gitlab.com/hoppr/hoppr/commit/f97e7a7acb00e9d361887dfad55c7bc2f03b10f0))
* SBOM spec selection logic ([d12bd8b](https://gitlab.com/hoppr/hoppr/commit/d12bd8ba19c0b5b8ec879d1f8190b8afb999d80b))
* SBOM spec selection logic ([2cd7286](https://gitlab.com/hoppr/hoppr/commit/2cd72866b1ae7a1fda1b580a4f7dbc2942a0da49))
* semantic-release version bump ([d45efb7](https://gitlab.com/hoppr/hoppr/commit/d45efb73d70625bda73ca4c4dfec33698c62957f))
* set file name correctly for collect_maven_plugin ([07e4eb5](https://gitlab.com/hoppr/hoppr/commit/07e4eb55388b25f46fdd2ed7a0af04fa86a8e427))
* set prerelease back to true ([49b130e](https://gitlab.com/hoppr/hoppr/commit/49b130e916dc7b3f9ec9fdaead06ce29e78f32c8))
* Set the correct version of oras ([d8357a4](https://gitlab.com/hoppr/hoppr/commit/d8357a431e33ec0fb45e0fc51537eae603e7cb6a))
* set timeout for run_command to reduce hanging ([9be8afe](https://gitlab.com/hoppr/hoppr/commit/9be8afefc62f1868d908cf8ecb8e9bd211662c37))
* set timetag to utc timezone ([12aa368](https://gitlab.com/hoppr/hoppr/commit/12aa368689a4225fc9662f59624ee90049fb8c4f))
* Setup codequality analytics and build attestation ([c5c4514](https://gitlab.com/hoppr/hoppr/commit/c5c4514e153becdf4a39713259b289c4469a24eb))
* Setup dependencies on test image ([6fabada](https://gitlab.com/hoppr/hoppr/commit/6fabada652d546cd89f1cd16eb32bae2cd0cb08a))
* shared RLock for Docker unit tests ([b2f3c9c](https://gitlab.com/hoppr/hoppr/commit/b2f3c9c5eb31bd31e46940fdf453ff56ae0f54f5))
* Simlink python ([0009483](https://gitlab.com/hoppr/hoppr/commit/0009483d56965e8770ae67383ecc69d4e7a53697))
* Simplify some of the configuration options ([a52594a](https://gitlab.com/hoppr/hoppr/commit/a52594a9e0873392934d6bfdb442ee3633f58a00))
* speed up unit tests ([32f8715](https://gitlab.com/hoppr/hoppr/commit/32f87159f4fc125a338e61fadefb80f5cb306ccb))
* Split out ci config per !184#note_1170995317 ([f42e582](https://gitlab.com/hoppr/hoppr/commit/f42e5825d7a3ebeab60ae107b10624d5567c7173)), closes [184#note_1170995317](https://gitlab.com/hoppr/184/issues/note_1170995317)
* stage name regex field type ([3d4c133](https://gitlab.com/hoppr/hoppr/commit/3d4c13381a43ddbdcbe4629a22ef0f75b7019035))
* strip quotes to compare found URL ([396c31a](https://gitlab.com/hoppr/hoppr/commit/396c31a90846673f991867a2c2057959457222f0))
* syntax ([88b8961](https://gitlab.com/hoppr/hoppr/commit/88b8961dcc41c6a21e02da417a3c059cf0036641))
* syntax ([4e8fc18](https://gitlab.com/hoppr/hoppr/commit/4e8fc18f3bc21f00d60e9a190e94fc7a4a9d9078))
* syntax ([84c2c38](https://gitlab.com/hoppr/hoppr/commit/84c2c3888485f188af9202b9fc8c1d20fc243331))
* syntax ([762a702](https://gitlab.com/hoppr/hoppr/commit/762a702417b358c3fd0b568c631bbd0c9a5a5aef))
* temporary peg of securesystemslib to address in-toto bug ([8261f41](https://gitlab.com/hoppr/hoppr/commit/8261f414b9951ede1e5a7efc44dfa429d6cf5790))
* test image tags ([0180488](https://gitlab.com/hoppr/hoppr/commit/01804886f7a709e105e432d53d96c7ca19c67a80))
* test pattern matching ([c6e7c51](https://gitlab.com/hoppr/hoppr/commit/c6e7c5197847e13f54a14cf112ca58d5cad86602))
* **test:** credentials unit test coverage ([74b981f](https://gitlab.com/hoppr/hoppr/commit/74b981f51e58adbd5581e815f239f97e54003ed0))
* transfer file default value ([ac1bf1a](https://gitlab.com/hoppr/hoppr/commit/ac1bf1a775d534bd89217443c1711bebd49e957b))
* turn off allow-failure on semantic-release:dry-run job to avoid breaking changes from renovate ([3a32a65](https://gitlab.com/hoppr/hoppr/commit/3a32a656abdb3c2f7cb006547a353f3fdae615b5))
* type check errors ([02b3d27](https://gitlab.com/hoppr/hoppr/commit/02b3d2713ccd8169643121abfb5ec206d01bbe4b))
* type error ([25aea9c](https://gitlab.com/hoppr/hoppr/commit/25aea9cb031b72a620c04029fce89872e0a9f3cd))
* type errors ([30861b6](https://gitlab.com/hoppr/hoppr/commit/30861b6abc6810f234c85a9de1d6db64c826430b))
* unit test context missing delivered sbom ([0c3d27a](https://gitlab.com/hoppr/hoppr/commit/0c3d27ae52d28492b3080249430099a6cd3cd93f))
* unit test message ([81415ed](https://gitlab.com/hoppr/hoppr/commit/81415edb08a9a6f925a911939095a5c001107531))
* unit test message ([dc95a02](https://gitlab.com/hoppr/hoppr/commit/dc95a025e8d295c4b0572ba105ede3b69f8a4738))
* unit test message ([69a164f](https://gitlab.com/hoppr/hoppr/commit/69a164f500097c97b29cf408a9ca695d6a3505e3))
* unit test message ([5846f61](https://gitlab.com/hoppr/hoppr/commit/5846f6109f3e5adf55ec6bfa4596e95f002e1ead))
* Update artifact name ([4f64b03](https://gitlab.com/hoppr/hoppr/commit/4f64b03b8a36590887e8430aa5caf3a48e25cd7a))
* update assert ([eba7ec9](https://gitlab.com/hoppr/hoppr/commit/eba7ec90d1927369be2bb91115388ec3fab0ec58))
* update CODEOWNERS for repo move ([6541b0c](https://gitlab.com/hoppr/hoppr/commit/6541b0c6ce79ed1dc0e4156d2e39a2620ee4b0c4))
* update delivered_sbom with process return objects ([46da6f3](https://gitlab.com/hoppr/hoppr/commit/46da6f324eca0ff4bda4077f385ff2ad20e87177))
* Update expected toc (since it's changed), and correct the regression boms ([fe79087](https://gitlab.com/hoppr/hoppr/commit/fe79087aeef00eab3792de8c5d3ea343ab9ce4d3))
* update expected toc's with delta stage intermediate delivered bom ([f016526](https://gitlab.com/hoppr/hoppr/commit/f0165265077c35e578b62d252c384594732e0d3c))
* update expected toc's with delta stage intermediate delivered bom ([3291773](https://gitlab.com/hoppr/hoppr/commit/32917735d1e84afa74aa584c83f899206bb3cfea))
* update expected_tar_toc files for changed maven file names ([b1ef013](https://gitlab.com/hoppr/hoppr/commit/b1ef01389841407c0723a2ced6bc397709c2a24d))
* Update git collector depth defaults per MR suggestion ([43a3dbb](https://gitlab.com/hoppr/hoppr/commit/43a3dbb296db4a43f2a70f9eb87a6d56ad3ff510))
* Update maven write to disk ([82d42b6](https://gitlab.com/hoppr/hoppr/commit/82d42b6dbce5eec0868199da0c1f4c32c9a99d61))
* update Result object to include optional return object.  Check that return object matches plugin Bom Access value ([b346872](https://gitlab.com/hoppr/hoppr/commit/b3468720b276af24d95c0e748ecb3701a4fd0764))
* Update variables to contain artifact name ([0400e28](https://gitlab.com/hoppr/hoppr/commit/0400e280e42b9eec5282bb7d59fab3ae100a1872))
* updated expected tar for pypi ([7356d6b](https://gitlab.com/hoppr/hoppr/commit/7356d6b6c82ee1d54dcd22158451664dd18c7779))
* Updated unit tests for docker collection ([7dbd2af](https://gitlab.com/hoppr/hoppr/commit/7dbd2af98c871d8cfdc20b48f0164fc8cb861ff0))
* updates to ensure pipeline works for forks. ([5fa6fe9](https://gitlab.com/hoppr/hoppr/commit/5fa6fe98921248797dc75375d500be9ab2f37381))
* urljoin stripping path components ([4c6ad9f](https://gitlab.com/hoppr/hoppr/commit/4c6ad9f3de6d2351b4eb274b884062d1b0c6e841))
* use enums for constants, per code review ([40cc853](https://gitlab.com/hoppr/hoppr/commit/40cc8538d5921846202167e7f0089a18d1719137))
* Use list of options and check for troublesome values ([fb39372](https://gitlab.com/hoppr/hoppr/commit/fb39372b37ee1bc5eec7ac49e618a521d38b5be3))
* Use long switches for readability. ([fdbfec2](https://gitlab.com/hoppr/hoppr/commit/fdbfec26f6f528ce1c79bff518b4f61f2708db5d))
* use more stable package for apt testing ([f365df7](https://gitlab.com/hoppr/hoppr/commit/f365df765c2bd162a9cf6c8120c720a6897fbc23))
* use more stable package for apt testing ([1aca9d7](https://gitlab.com/hoppr/hoppr/commit/1aca9d7c5d751785f8aba52bbd2cb63319fd8583))
* Use python3 for virtual environment creation in CI Dockerfile ([e39ea91](https://gitlab.com/hoppr/hoppr/commit/e39ea91f9456b7f006ef6dea5f57579376829776))
* Use semantic release to publish package ([27e8eb0](https://gitlab.com/hoppr/hoppr/commit/27e8eb02b8bd3405cec90b5d3112662dc2675c17))
* Using copy from Maven-Dependency-Plugin instead of get ([7ab2fe1](https://gitlab.com/hoppr/hoppr/commit/7ab2fe15234b1bda27f91bb0ea6b025cde2c22d2))
* windows ANSI processing ([eba2cb4](https://gitlab.com/hoppr/hoppr/commit/eba2cb4a18d7bb375e9984c838772506d6c40bb7))
* wrapper for run cmd ([9d5844b](https://gitlab.com/hoppr/hoppr/commit/9d5844b8f0661b3be8ef63d120bf5d96023fe844))


### Reverts

* files not relevant to this branch ([fe61dea](https://gitlab.com/hoppr/hoppr/commit/fe61deaf45ff0fd0e0e5252ac783f751ee6ddedf))
* files not relevant to this branch ([c66595a](https://gitlab.com/hoppr/hoppr/commit/c66595a47a8d089b98566940eb4ae565d094c5da))
* generic types ([96721e4](https://gitlab.com/hoppr/hoppr/commit/96721e42511934c36bbb9014da20d24a1b2b9546))
* generic types ([9915c1a](https://gitlab.com/hoppr/hoppr/commit/9915c1aa2bb39bbccdc959b2584f56954698d84c))
* method_name condition logic ([6d9a16b](https://gitlab.com/hoppr/hoppr/commit/6d9a16bc87de3e8cce5a835093076ea76242b627))
* method_name condition logic ([6445f18](https://gitlab.com/hoppr/hoppr/commit/6445f18cb62e61bb2b8738bd46dbd75df766e961))
* model integration changes ([e27eb91](https://gitlab.com/hoppr/hoppr/commit/e27eb91fcc8a8b5cc2e0c6f110c6de1e37010f82))
* shared memory manager changes ([09a6ee3](https://gitlab.com/hoppr/hoppr/commit/09a6ee348e8820fc91d5ac5ad2016f8e96aa041e))
* shared memory manager changes ([4dbd8c0](https://gitlab.com/hoppr/hoppr/commit/4dbd8c0fc99b7e2d52b717bc555b326962a27467))

## [1.8.0](https://gitlab.com/hoppr/hoppr/compare/v1.7.2...v1.8.0) (2023-04-04)


### Features

* --no-strict CLI flag ([05cf05c](https://gitlab.com/hoppr/hoppr/commit/05cf05caea72c11823c635e391a8dd10eb6bd0d4))
* --no-strict CLI flag ([12bc0d3](https://gitlab.com/hoppr/hoppr/commit/12bc0d3200a5f19cf5c04b0cf8eb8423f9c00c4c))
* add composite collector ([47037b8](https://gitlab.com/hoppr/hoppr/commit/47037b89e38e8694d803309eed486245a461a4b2))
* add delta_sbom capability ([c73e081](https://gitlab.com/hoppr/hoppr/commit/c73e0816444cefa4547917da54dc650e7777d7da))
* add delta_sbom capability ([a7d265c](https://gitlab.com/hoppr/hoppr/commit/a7d265ccbe6254a78f5cf5e1b9808c7a3eb570a5))
* add delta_sbom capability ([5bb5a59](https://gitlab.com/hoppr/hoppr/commit/5bb5a599d0c111399101f5e676f20e19c1954022))
* add delta_sbom capability ([9a39d88](https://gitlab.com/hoppr/hoppr/commit/9a39d88b53f1f277aecfa1c6d2edefe82098e801))
* add nexus search collector ([7ccb05e](https://gitlab.com/hoppr/hoppr/commit/7ccb05e01300cb9dde702185d28bc2941c83c3fc))
* allow docker collector to use docker.io when --no-strict option is set ([1163405](https://gitlab.com/hoppr/hoppr/commit/116340559d89fa8f6618dc48b524ff6b0a42c66c))
* creation of in-toto attestations ([b58a973](https://gitlab.com/hoppr/hoppr/commit/b58a973e9b7bd5a3d52e5f23d4cdd97155b5143e))
* Report Generation Plugin ([126a0e5](https://gitlab.com/hoppr/hoppr/commit/126a0e5430be13824e67037225b617f24965f79c))
* skip collecting components with a scope of excluded ([e2fd680](https://gitlab.com/hoppr/hoppr/commit/e2fd680389100d216089a0a1aeedcbf4121b635b))


### Bug Fixes

* Add additional configuration capability ([dc14c57](https://gitlab.com/hoppr/hoppr/commit/dc14c5728f5869f9f47f0f2e18cc1b01812e6201))
* Add additional fixes per MR feedback ([598b9f1](https://gitlab.com/hoppr/hoppr/commit/598b9f1c65bf1dcee2cf31dd84553143be73b25f))
* Add Bot label to renovate MRs ([f43cd65](https://gitlab.com/hoppr/hoppr/commit/f43cd65250b65e93eb1cfa3debdf74a3bad58807))
* Add build arg to dockerfile ([ae46f93](https://gitlab.com/hoppr/hoppr/commit/ae46f93d360ab0639d706036721b48769b4c2feb))
* add collection metadata to sbom for apt components ([b8284f5](https://gitlab.com/hoppr/hoppr/commit/b8284f54fc8d0d193bad0916dc7ce83d7ff97e83))
* add collections params for docker ([228d327](https://gitlab.com/hoppr/hoppr/commit/228d327a31b2a7e043af38a016b98d20825f5348))
* add command line option to override previous collection location in delta_sbom plugin ([b3eb0bd](https://gitlab.com/hoppr/hoppr/commit/b3eb0bd1b1b40c936d34c7075fcf525ddf2e76c7))
* add command line option to override previous collection location in delta_sbom plugin ([3485048](https://gitlab.com/hoppr/hoppr/commit/34850483f44788e7e7cd44965984bba6b6502922))
* add command line option to override previous collection location in delta_sbom plugin ([d818730](https://gitlab.com/hoppr/hoppr/commit/d818730a0b6c4d930cdb220f31c2555a21aff42a))
* add command line option to override previous collection location in delta_sbom plugin ([2331668](https://gitlab.com/hoppr/hoppr/commit/2331668aa793c290764f6107dcfbe5f787dbba04))
* Add curl for oras test ([804f8b0](https://gitlab.com/hoppr/hoppr/commit/804f8b0b3d665d40e5299d21ec84f847a2a6e7d1))
* add delivered_sbom to context, work from that variable ([07b6ba9](https://gitlab.com/hoppr/hoppr/commit/07b6ba93a7d5b5b939ad53e4ad0d408276cd337a))
* Add entrypoint to hoppr ([073fdeb](https://gitlab.com/hoppr/hoppr/commit/073fdebdae77d9db6774ab1b84c2a336df68ad52))
* add expected-tar-toc for remaining integration tests ([7bbcac9](https://gitlab.com/hoppr/hoppr/commit/7bbcac9c821b6528edf9f7b247886b11ee18db45))
* add expected-tar-toc for remaining integration tests ([a071522](https://gitlab.com/hoppr/hoppr/commit/a07152294d7430bd552255dd911a9730ae526061))
* add expected-tar-toc to delta integration test ([40fdb97](https://gitlab.com/hoppr/hoppr/commit/40fdb9778b2b20c0b07aa475e5de931a6e4cf227))
* add expected-tar-toc to delta integration test ([33ed419](https://gitlab.com/hoppr/hoppr/commit/33ed41925bedc0258c2bb3a537ccb6a0b4311b39))
* Add hopctl docker image ([1363cec](https://gitlab.com/hoppr/hoppr/commit/1363cec9d1214acfed27c03b3c785fdcf29e7be2))
* Add hopctl docker image ([c4d00fc](https://gitlab.com/hoppr/hoppr/commit/c4d00fc04074112c5fddad91005cb6361ddaf8fc))
* Add hopctl docker image ([5ad0556](https://gitlab.com/hoppr/hoppr/commit/5ad0556d41e665fe20706d6e34833ab835ead8e7))
* add initial checks for BOM access ([d89fed2](https://gitlab.com/hoppr/hoppr/commit/d89fed21d1c807f17d1089e748d38bcb97fc9dde))
* add integration test for deltas ([e0c7e47](https://gitlab.com/hoppr/hoppr/commit/e0c7e47619424601f867040064a5fc6df402e29b))
* add integration test for deltas ([2ca6ed0](https://gitlab.com/hoppr/hoppr/commit/2ca6ed0019f9b21613193a5dd525b470a12a1d56))
* add integration test for deltas ([6789f9c](https://gitlab.com/hoppr/hoppr/commit/6789f9c240a110f10e717dc42c3cff40ba4352e6))
* add integration test for deltas ([c56d066](https://gitlab.com/hoppr/hoppr/commit/c56d0664ddeb49fe6bc4b4cf997ebd782a761163))
* add missing import ([603f219](https://gitlab.com/hoppr/hoppr/commit/603f21910e7d2cbf3f98813ba45a22454330b0ff))
* add missing strict_repos typer argument ([ad9bb37](https://gitlab.com/hoppr/hoppr/commit/ad9bb3736d57cea04ff902aa2241c88ba426d394))
* add mock for os.path.exist ([5463b80](https://gitlab.com/hoppr/hoppr/commit/5463b801539bbf1436e41b0aa0827cb2a54f2b9a))
* Add oci artifacts for reference ([f6661be](https://gitlab.com/hoppr/hoppr/commit/f6661beba1f1fff54a779ea973a5869a72c6616d))
* Add oras integration test ([cbd9376](https://gitlab.com/hoppr/hoppr/commit/cbd937615039f3f99451bf600ae40948e69f0049))
* add purl-type/repo-type mappings to nexus_search collector ([1b3d06a](https://gitlab.com/hoppr/hoppr/commit/1b3d06a04e67dcf0acc4bd2dd55eca07aa282329))
* add pytest-cov package ([a6666a8](https://gitlab.com/hoppr/hoppr/commit/a6666a84c85d0e7939519506226fef23dd05c221))
* add repo to purl type list ([01fa2f2](https://gitlab.com/hoppr/hoppr/commit/01fa2f254d56a06481e4b79e29ecd3f184ce7e04))
* add repository/directory properties to bom for all collectors ([bad3136](https://gitlab.com/hoppr/hoppr/commit/bad3136b6f7dd1fc7579afd5230cd28c5aa26b1b))
* add shared logfile lock ([e350189](https://gitlab.com/hoppr/hoppr/commit/e350189654b9461759e1483458489e42db510f1e))
* add shared logfile lock ([3c3c238](https://gitlab.com/hoppr/hoppr/commit/3c3c23803a56eaad75eaa41090b5ed258393ea0d))
* Add test verification for oras bundle ([77f04b7](https://gitlab.com/hoppr/hoppr/commit/77f04b775b030db593ade6d4437a8e6ff0309be6))
* add version to bom plutin property ([166538c](https://gitlab.com/hoppr/hoppr/commit/166538cf487149e3932a8a243742f8af016db123))
* Added user_env support to find_credentials ([ee58f2d](https://gitlab.com/hoppr/hoppr/commit/ee58f2d48766562152be513083038b226ed9ef98))
* address issue where in-toto was looking maven files ([98fc06a](https://gitlab.com/hoppr/hoppr/commit/98fc06aaeea92bd2eacb459f1d4ca9fa97cf34f0))
* all source distros from manifest repos ([52c8c80](https://gitlab.com/hoppr/hoppr/commit/52c8c8073eabedb81aa93e325c7ee03b465750c1))
* allow more full repository specification for collect_nexus_search ([e2cdf02](https://gitlab.com/hoppr/hoppr/commit/e2cdf027754a2f1568035fe5303a8e62024a00fa))
* allow more full repository specification for collect_nexus_search ([6ebcceb](https://gitlab.com/hoppr/hoppr/commit/6ebccebab92c792fc335ba1251908faf69e77dab))
* allow no scheme for repo URLs as last resort ([866382f](https://gitlab.com/hoppr/hoppr/commit/866382f298a701eadb13cd8b609b74b5e13da88a))
* allow no scheme for repo URLs as last resort ([fd1feec](https://gitlab.com/hoppr/hoppr/commit/fd1feecf559855b4eb3b8127d848f1daf2c72a19))
* allow spaces in stage name ([d7af57f](https://gitlab.com/hoppr/hoppr/commit/d7af57f3892e34fd62b5bfe1dae9f4c5d3704657))
* allow spaces in stage name ([923c043](https://gitlab.com/hoppr/hoppr/commit/923c043b1aa7b686e4777844372272aafb9a7e77))
* append dev instead of current branch name ([d6abddc](https://gitlab.com/hoppr/hoppr/commit/d6abddc66d3b832feb1923d1eb31d0557b03c892))
* Apply correction to git repository collector ([dc522b5](https://gitlab.com/hoppr/hoppr/commit/dc522b555ad2965ba7965f4630e0e1f7d1eb2849))
* apply in-toto suggestions ([3e92177](https://gitlab.com/hoppr/hoppr/commit/3e92177f671c4781907c0a6df279293e543ebb2f))
* apply in-toto suggestions ([401757e](https://gitlab.com/hoppr/hoppr/commit/401757eae524fe76d7eda6f39ab38ff20dd0e01c))
* applying MR suggestion ([c3adeee](https://gitlab.com/hoppr/hoppr/commit/c3adeee1813eba8dd308fec358d27af9f25c1f1d))
* applying MR suggestion ([4e5ca7f](https://gitlab.com/hoppr/hoppr/commit/4e5ca7fb7258c667a431653ab01bb8745a87308f))
* applying MR suggestion ([4e61f84](https://gitlab.com/hoppr/hoppr/commit/4e61f84272b14180e16f6a4baa424f5d60eb2752))
* applying MR suggestion ([b034b72](https://gitlab.com/hoppr/hoppr/commit/b034b7273179a7fb8bd3bda411050d9815b58ce7))
* apt collector _get_download_url_path ([aa988d6](https://gitlab.com/hoppr/hoppr/commit/aa988d680fe350e5716f30abbed0437e51ae9797))
* apt collector _get_download_url_path ([1d36fd4](https://gitlab.com/hoppr/hoppr/commit/1d36fd457fc60d9d9638611e3b91940a2a523194))
* bom helm chart version ([009bdf0](https://gitlab.com/hoppr/hoppr/commit/009bdf00412fe6964b81c15b86aec52a761890df))
* Branch isolation testing ([fcde124](https://gitlab.com/hoppr/hoppr/commit/fcde12425dd610fe3da0dbbd56de6af677a8688e))
* bug with with attestations created from GitLab CI Runners ([f8ca8e7](https://gitlab.com/hoppr/hoppr/commit/f8ca8e79144b6377250d38ccc37bb2f7689d2033))
* casing for "kind" field ([26b0927](https://gitlab.com/hoppr/hoppr/commit/26b0927a143763f688215b0fc3d49e27a56f0b71))
* casing for "kind" field ([533f6a7](https://gitlab.com/hoppr/hoppr/commit/533f6a770bc8738aa39b22c2faa6d262c4b7264d))
* catch exception from _get_required_coverage, check for empty/missing boms ([d905e35](https://gitlab.com/hoppr/hoppr/commit/d905e3557ce6def86b923db7961f3631808f3445))
* check for exception not thrown in pypi success ([0ba41f6](https://gitlab.com/hoppr/hoppr/commit/0ba41f6a9962ede186765b25e7939623fbb15095))
* check tar toc on integration tests ([aafc6bb](https://gitlab.com/hoppr/hoppr/commit/aafc6bb6eb9215e0c158a1de007fc67a8509059e))
* check tar toc on integration tests ([105c87a](https://gitlab.com/hoppr/hoppr/commit/105c87a4c81809c1a586713d42ff1e857e0c260e))
* Clean up artifact name ([a59aaac](https://gitlab.com/hoppr/hoppr/commit/a59aaacc2bb23988b8e240d0f6c237b547e86f39))
* Clean up other variables in ci ([d289184](https://gitlab.com/hoppr/hoppr/commit/d28918482ceb951461725095e72f7ca9ebcb23ce))
* clean up repository_url handling ([c867a3b](https://gitlab.com/hoppr/hoppr/commit/c867a3bda17c70e59e1d50672543077b7daae8cb))
* clean up repository_url handling ([6fb544b](https://gitlab.com/hoppr/hoppr/commit/6fb544b1ceb0846d096cf790efa8f52a7160960c))
* clean up repository_url handling ([95a0288](https://gitlab.com/hoppr/hoppr/commit/95a0288eeecd7c0baf03af5b67dd0d50fb549708))
* clean up repository_url handling ([011e53b](https://gitlab.com/hoppr/hoppr/commit/011e53b8b96760f03863b5a9582f91ad30f98733))
* Clean up rules and workflow ([69e9b73](https://gitlab.com/hoppr/hoppr/commit/69e9b73ebab517658ad0e5238038e01979f6e98a))
* Cleaned up maven command ([2b4af6e](https://gitlab.com/hoppr/hoppr/commit/2b4af6e57658f5f96a930cf11542f802f783476d))
* Cleanup notes in config ([2632bfa](https://gitlab.com/hoppr/hoppr/commit/2632bfabe9794542033ce8b4dd8d6a5cb4e96da8))
* Cleanup rules ([ea79632](https://gitlab.com/hoppr/hoppr/commit/ea7963280418793154b6cd113c926ff160ce011e))
* Cleanup rules ([76d65e5](https://gitlab.com/hoppr/hoppr/commit/76d65e5cf60559f4672f63dd33a62c45f3009eab))
* clear loaded manifests ([39e7341](https://gitlab.com/hoppr/hoppr/commit/39e73413a9e446d1f5b98118ff8b3ec2799260e5))
* code review comments ([c9fa7e2](https://gitlab.com/hoppr/hoppr/commit/c9fa7e2b37422a04c4a2281b81b425d46bcedb12))
* Code review comments ([a204676](https://gitlab.com/hoppr/hoppr/commit/a204676aa26173864856e458a30f119caeb3ae9a))
* complete unit test coverage for collect_nexus_search ([24827e4](https://gitlab.com/hoppr/hoppr/commit/24827e4cab02da2808deb99fd372c6a6b3935ed7))
* component search sequence ([ff89f56](https://gitlab.com/hoppr/hoppr/commit/ff89f5633b386b6b006ff7f76b6558ad894f2bb2))
* component search sequence ([397d3bf](https://gitlab.com/hoppr/hoppr/commit/397d3bf639ed8e729fb4e209078cd0877ae6b07f))
* Correct build artifacts ([40ca47f](https://gitlab.com/hoppr/hoppr/commit/40ca47f2642bd82be5aec18f75a3f468c1d880e7))
* Correct deployment teir ([d4282c8](https://gitlab.com/hoppr/hoppr/commit/d4282c8c1e54fe188acfde63a9307c33474c9f8b))
* Correct deployment teir ([983932d](https://gitlab.com/hoppr/hoppr/commit/983932d32f998ca41536002671b324734d711716))
* Correct dockerfile ([30f438a](https://gitlab.com/hoppr/hoppr/commit/30f438a16501280c0dd97633d3e2447e2c80998e))
* Correct git bom, had incorrect purl, name, and version ([e6a1265](https://gitlab.com/hoppr/hoppr/commit/e6a1265e9862aa66996e10a7c956a1315bcfc89b))
* Correct integration tests ([b065953](https://gitlab.com/hoppr/hoppr/commit/b0659532c14ddea25afcb7357d14104427cddefe))
* Correct media types for registry ([d3ea0df](https://gitlab.com/hoppr/hoppr/commit/d3ea0df0cd2eb276eb86d1cb82dfceeb39604e71))
* Correct release yaml file ([a29c26c](https://gitlab.com/hoppr/hoppr/commit/a29c26c86220443c22e94954bd6db5d3e7a59147))
* Correct releaserc file ([8868fab](https://gitlab.com/hoppr/hoppr/commit/8868fab85c0ed587f8d9679af6c8d793ed78c50b))
* Correct releaserc.yml ([4b55f7f](https://gitlab.com/hoppr/hoppr/commit/4b55f7f25895b1251ab7e9499280848d86732cbb))
* Correct requirements ([7598beb](https://gitlab.com/hoppr/hoppr/commit/7598beb540c9c26860501b42ba92fb807f79fd33))
* Correct simlinks in ci docker ([deb0728](https://gitlab.com/hoppr/hoppr/commit/deb07284c27603325678ae3332f0a2023ed42f80))
* Correct syntax in ci docker ([f96c938](https://gitlab.com/hoppr/hoppr/commit/f96c938502b98e0bdbe4096ed6a35f830abaf378))
* Correct tests so they pass ([eeebaef](https://gitlab.com/hoppr/hoppr/commit/eeebaef2b781cde5e668d0d75e65977185d56d45))
* Correct the oras binary arch type ([71bc21e](https://gitlab.com/hoppr/hoppr/commit/71bc21ec0b5308c4299906ab04b270b678480551))
* Correct the wheel name ([4847b4a](https://gitlab.com/hoppr/hoppr/commit/4847b4a8548c563ad4298616f8c16288ca925aaa))
* Corrected Maven-Dependency-Plugin arguments so that maven artifacts would be bundled ([9fe9eaa](https://gitlab.com/hoppr/hoppr/commit/9fe9eaa13c68ed8d36437a6662f735c347018ed4))
* credential.find method no longer needs exact match.  added lines to _run_data_ metadata file. ([202b0ae](https://gitlab.com/hoppr/hoppr/commit/202b0ae90ba70cf8d45990064e4b3d2ce6b9fb4a))
* Cut release from next branch ([17d0d03](https://gitlab.com/hoppr/hoppr/commit/17d0d03ac243fea51d9ea98738d0e8293c60803d))
* Cut release from next branch ([e1c56d2](https://gitlab.com/hoppr/hoppr/commit/e1c56d28394803045b974195e6e7d9cc796c967b))
* **deps:** update dependency hoppr-cyclonedx-models to v0.2.10 ([ee37064](https://gitlab.com/hoppr/hoppr/commit/ee370642914f1b507ed4793e11cbc575d1c51720))
* **deps:** update dependency typer to ^0.7.0 ([a35bd85](https://gitlab.com/hoppr/hoppr/commit/a35bd8519c8486b68a9228b660a2d2f44bad7f2c))
* dev branch test; update README ([f8ed5ee](https://gitlab.com/hoppr/hoppr/commit/f8ed5eec1a2b8d68730b88d6d378d132fa67c1c7))
* DNF download directly from found URL ([bf84cec](https://gitlab.com/hoppr/hoppr/commit/bf84cec779df983642116cf99cf42403675f17e9))
* DNF download directly from found URL ([11d5e5e](https://gitlab.com/hoppr/hoppr/commit/11d5e5ec6280b60d821880833d5ee7f0d3683432))
* do not re-generate consolidated/delivered sboms ([ee1928b](https://gitlab.com/hoppr/hoppr/commit/ee1928b6198e0fb57c3eb56af0dc3b5c057166d2))
* do not re-generate consolidated/delivered sboms ([ce59dcc](https://gitlab.com/hoppr/hoppr/commit/ce59dcc46fbbef3f3941c0f33c3d0cc0c0abacf7))
* Don't run pipelines on merge event ([a0b8c53](https://gitlab.com/hoppr/hoppr/commit/a0b8c53add628e08a0c02f30beea57a399b9f109))
* Ensure python is on the path ([a1c03eb](https://gitlab.com/hoppr/hoppr/commit/a1c03eb6e2e607267886890831c2675572525d6b))
* enum base type ([4e3577c](https://gitlab.com/hoppr/hoppr/commit/4e3577c8b96bd5c107649c3d6adb6ef1dd34e4d1))
* exclude nulls from output sboms ([e41bd52](https://gitlab.com/hoppr/hoppr/commit/e41bd52190ec8acba3fdd419650674e7739224f7))
* expected apt SBOM ([c6b01e9](https://gitlab.com/hoppr/hoppr/commit/c6b01e935d7aadf1f2fe3410edf901d95c9f8fb3))
* expected apt SBOM ([8e7d3da](https://gitlab.com/hoppr/hoppr/commit/8e7d3daaab31eccc1686963d30bbbd72c70f6bf0))
* expected metadata source location ([53f74d7](https://gitlab.com/hoppr/hoppr/commit/53f74d7f154df37e45d91e782bfbee5f30570fd9))
* expected metadata source location ([f918d63](https://gitlab.com/hoppr/hoppr/commit/f918d63976bfb5ac57405ea48b30c9b34e8fe0a6))
* expected-tar-toc sort order changed ([b89f853](https://gitlab.com/hoppr/hoppr/commit/b89f853e5547aac8e0a69b7245f8f3abddc5d70f))
* fail on no change, improved status messages ([801dea2](https://gitlab.com/hoppr/hoppr/commit/801dea20d82167b303ebbd0a339cb8e408c9b873))
* fail on no change, improved status messages ([1a4c87c](https://gitlab.com/hoppr/hoppr/commit/1a4c87ceeceaabbe3ecf13e905fdd9a08866e647))
* fail on no change, improved status messages ([fa610d0](https://gitlab.com/hoppr/hoppr/commit/fa610d09a73bf6736e9792096614e08a84a6c8aa))
* fail on no change, improved status messages ([69d15b1](https://gitlab.com/hoppr/hoppr/commit/69d15b1c4e30684fe59020b96b002a7f1d27c30c))
* fast forward branch ([1428ecb](https://gitlab.com/hoppr/hoppr/commit/1428ecb2ff108f25ca0c424f53984771a16e8e8e))
* Fixed Docker repo:tag information being lost in collection ([5111f7f](https://gitlab.com/hoppr/hoppr/commit/5111f7fe86bd2c833ac4c0bb0392932b599f6549))
* Fixed Docker repo:tag information being lost in collection ([3319a36](https://gitlab.com/hoppr/hoppr/commit/3319a36bd90ca7798141ee0e7fe92bb6c361559d))
* fixed issue with bundle options for functionary_key ([d798606](https://gitlab.com/hoppr/hoppr/commit/d7986062051b5b0be426dda0842acb1bab601ec2))
* Fixed the releaseing issue ([ce8b2f5](https://gitlab.com/hoppr/hoppr/commit/ce8b2f57703bfaf0984e2b750005f866a8ad8b35))
* Fixed type-check findings ([7bb21df](https://gitlab.com/hoppr/hoppr/commit/7bb21df1b19f3f2ed8f275db995c722db9e78ed0))
* Fixed unit tests ([4e80266](https://gitlab.com/hoppr/hoppr/commit/4e8026652458dc5c35bdc3dfc5a983727e5bf44f))
* Fixes [#150](https://gitlab.com/hoppr/hoppr/issues/150) and corrected when consolidated and deliveried SBOMs are written. It also address additional unit testing ([5adf219](https://gitlab.com/hoppr/hoppr/commit/5adf2193b30af0b6aba0c901dd369df324d53e19))
* Fixing Merge Conflicts ([d24cff2](https://gitlab.com/hoppr/hoppr/commit/d24cff258f10542bdc9b1af462c3a61bc1af2722))
* generate dev version if not on main/dev ([3aa6ec5](https://gitlab.com/hoppr/hoppr/commit/3aa6ec50f186ee520eb1279160c8b1a0715711b2))
* Get better results ([688e176](https://gitlab.com/hoppr/hoppr/commit/688e1766cffb34cc155e0ea0c3f1d7a608c58947))
* gitattributes ([5d3bde3](https://gitlab.com/hoppr/hoppr/commit/5d3bde370513376a36b9a0cc7279b9464fd8a09a))
* Grab versioning properly ([cc052cf](https://gitlab.com/hoppr/hoppr/commit/cc052cfcedbd149b295e090c493adb650b47f1ce))
* have nexus collector respect user-specified purl types ([9116a03](https://gitlab.com/hoppr/hoppr/commit/9116a034125ccbe2eb47d06e0172176e3eae2d86))
* helm collector append purl name ([5574739](https://gitlab.com/hoppr/hoppr/commit/5574739d2e34ba0a538ee8295939e2ad473144f5))
* hoppr group ([c21bc29](https://gitlab.com/hoppr/hoppr/commit/c21bc295fb9ab40756e107b5047e2cb3d36d8a06))
* image tag reset after rebase ([45a850a](https://gitlab.com/hoppr/hoppr/commit/45a850a4c43f29c7253c4c97620c1fc6cce32fb5))
* import source type error ([464211c](https://gitlab.com/hoppr/hoppr/commit/464211cddc9f7ea6c7a5182449155e4c7467289c))
* Improve config error logic in git collector ([cb3681a](https://gitlab.com/hoppr/hoppr/commit/cb3681a6f95c8cc031ca69276f84015925659296))
* Improve matching pattern and logging ([12a68eb](https://gitlab.com/hoppr/hoppr/commit/12a68eb3ab2e049260bba97ddf8f7c0fc585a45d))
* include resources folder in poetry build ([5ae0ffb](https://gitlab.com/hoppr/hoppr/commit/5ae0ffb5d761acd41c7fafd6a0cfa43f40d7eafd))
* included manifest repo merge, add tests ([359364c](https://gitlab.com/hoppr/hoppr/commit/359364c453f72cc35cafd752b0b21a7aac79e0c3))
* included manifest repo merge, add tests ([1c22c6e](https://gitlab.com/hoppr/hoppr/commit/1c22c6eec5ed7c3c1dc01e3d70e6241f0da7f7ee))
* increase minimum unit test coverage to 100% ([9c697e3](https://gitlab.com/hoppr/hoppr/commit/9c697e35712ee8e8138848f506de71ff7054d71b))
* lint error ([990ef3f](https://gitlab.com/hoppr/hoppr/commit/990ef3ff6d79359c45481f0deeb640a4ec629ccb))
* lint error ([f9f2cc0](https://gitlab.com/hoppr/hoppr/commit/f9f2cc0095060a84642717a0d77e28d26d97467b))
* linting union for type hints ([bbaf8f0](https://gitlab.com/hoppr/hoppr/commit/bbaf8f0f688c30d206bc1842cd19b616bda0b949))
* main module exits using sys instead of typer ([356112e](https://gitlab.com/hoppr/hoppr/commit/356112e622d329d53a0902532476957f92790491))
* main process logfile lock ([4519f53](https://gitlab.com/hoppr/hoppr/commit/4519f53808064a9829f46bfec19f06156966a59c))
* Make a quicklink script for linking python in dockerfile ([ae3dace](https://gitlab.com/hoppr/hoppr/commit/ae3dace2b4101bc9c73381dcf097baf505965e25))
* Make semantic release pass all jobs ([ddc3c49](https://gitlab.com/hoppr/hoppr/commit/ddc3c495048e8be5302bf9c7ba7a4ad5418944a7))
* manifest helm repo URL ([438fae5](https://gitlab.com/hoppr/hoppr/commit/438fae572062f9726c451031a88e403a67390c99))
* manual version revs because lock file conflicts spam ([e8a473f](https://gitlab.com/hoppr/hoppr/commit/e8a473ffd01938734a7a84180e78d075927240c4))
* merge components, add verification tests ([f0ecf1e](https://gitlab.com/hoppr/hoppr/commit/f0ecf1e7f7471b17dbbf42e6aa80e8dedc46f76b))
* merge components, add verification tests ([ce6e4d1](https://gitlab.com/hoppr/hoppr/commit/ce6e4d17cd8ce4d1127956d1f72b2f346ee7402a))
* Merge dev into next ([765d25a](https://gitlab.com/hoppr/hoppr/commit/765d25a29cc9d3fed6794a01d25892118408e177))
* Merge main into branch ([e2fd972](https://gitlab.com/hoppr/hoppr/commit/e2fd97246baadff00a84b1fecb0b8a79412e70ee))
* **Minor:** Image build cleanup ([ba4e6c3](https://gitlab.com/hoppr/hoppr/commit/ba4e6c3bc68fe469b3ad68ed636f43b55c84f425))
* missing `rev` command in skopeo image ([cb38a3e](https://gitlab.com/hoppr/hoppr/commit/cb38a3e8ae5dc2afcac4bdbb87f110d6af58997e))
* missing collector plugin CLI tools ([b1284ba](https://gitlab.com/hoppr/hoppr/commit/b1284ba6c1df27487e36279c5af5478b27450322))
* move insertion of required stages to Transfer object creation ([fa0d31b](https://gitlab.com/hoppr/hoppr/commit/fa0d31b99fcc55411cc2fd32bcfb7f050b14fba9))
* move insertion of required stages to Transfer object creation ([a7da1d5](https://gitlab.com/hoppr/hoppr/commit/a7da1d53253de835a764f3d2ab68f052faa03aa7))
* move insertion of required stages to Transfer object creation ([57604d5](https://gitlab.com/hoppr/hoppr/commit/57604d5e724fadcd71694b466a82b19a9696d624))
* move insertion of required stages to Transfer object creation ([53243ab](https://gitlab.com/hoppr/hoppr/commit/53243abcb44c5c031af5c1cc625b8cf81e2b3954))
* moved test_main.py under test/unit/ directory ([90df7ad](https://gitlab.com/hoppr/hoppr/commit/90df7adb7fd293b7b3da96ac70ca159fe0d8f480))
* mypy errors ([b08426c](https://gitlab.com/hoppr/hoppr/commit/b08426ca5b7af32c8d192d2309381b19d03d136d))
* mypy errors ([d94d090](https://gitlab.com/hoppr/hoppr/commit/d94d090f231edaa21de806bc8ada8743326c57c7))
* mypy errors ([d612474](https://gitlab.com/hoppr/hoppr/commit/d612474d5f71b79683280781ca3cda18ee05d240))
* New branch old issue ([a906ca3](https://gitlab.com/hoppr/hoppr/commit/a906ca3cc6afc01f97483e136f9b3370312bb5bb))
* Only attempt PyPI source collect if whl not collected ([31de8ae](https://gitlab.com/hoppr/hoppr/commit/31de8ae07d414c09295db59662442ee7a15f92cc))
* only load purl-type-specific plugins when components of that type are being processed (Issue [#77](https://gitlab.com/hoppr/hoppr/issues/77)) ([1789f21](https://gitlab.com/hoppr/hoppr/commit/1789f21df784738c58b8ea3dac19e50240a2b13b))
* Only publish if there's a new release from semver dryrun ([da88e9b](https://gitlab.com/hoppr/hoppr/commit/da88e9b4a2e6449ea5550f8adcffceb975756af4))
* only rename maven file on successful collection ([3455e74](https://gitlab.com/hoppr/hoppr/commit/3455e743e92fa74ed1d8f82e66921dd41c837c63))
* Only run semantic release publish on develop and main ([0ecc904](https://gitlab.com/hoppr/hoppr/commit/0ecc904fb1583a9f530d4a469a331391007e70d1))
* parameter name typo ([9d7c3b1](https://gitlab.com/hoppr/hoppr/commit/9d7c3b1c0751816f5c108f3fb83eaec10f47a490))
* pip arguments ([0cc47f1](https://gitlab.com/hoppr/hoppr/commit/0cc47f17f3eefcd4a650d213d7d8701c59485c8b))
* platform check ([de541db](https://gitlab.com/hoppr/hoppr/commit/de541db0bf4a810a158ca175b1c38a573a1ab9e7))
* **plugin:** add type hint to auth ([0bc2e29](https://gitlab.com/hoppr/hoppr/commit/0bc2e29c0e36d0b596cd1d5f493a14566e9bd9dc))
* **plugin:** properly authenticate nexus requests ([be82ed4](https://gitlab.com/hoppr/hoppr/commit/be82ed43660ca25ed2f105a43dbd1603deb4df38))
* prepend stage if needed for deltas ([a0dbfcb](https://gitlab.com/hoppr/hoppr/commit/a0dbfcbb40649182f1fef9ce8cdb1ae5bf151c73))
* prepend stage if needed for deltas ([7bbb2b5](https://gitlab.com/hoppr/hoppr/commit/7bbb2b5d76d6245430e5e587e8e15b64f448c2d5))
* prepend stage if needed for deltas ([e93d3a0](https://gitlab.com/hoppr/hoppr/commit/e93d3a0ff654fe4aae50a96003682dcd6a7d4f74))
* prepend stage if needed for deltas ([2467c6b](https://gitlab.com/hoppr/hoppr/commit/2467c6b693832775d4739d0de41d7fc2059b5296))
* prevent loading plugins that aren't needed ([17722f3](https://gitlab.com/hoppr/hoppr/commit/17722f3238b9c5436549e3c1fbd9e7fb84f49dc3))
* prevent loading plugins that aren't needed ([b00db2b](https://gitlab.com/hoppr/hoppr/commit/b00db2b5925915fa950e14ea565e4496c4bf4aa9))
* processor relative file handling ([59636aa](https://gitlab.com/hoppr/hoppr/commit/59636aaeb06ff4bddd167fdc302b914ca3342cd5))
* processor relative file handling ([5c3cdb1](https://gitlab.com/hoppr/hoppr/commit/5c3cdb18abede20e59e52afdcaeec1de69a8298c))
* proper attestations for nexus-search- and composite- collectors ([a1a9651](https://gitlab.com/hoppr/hoppr/commit/a1a96517eebc432d83153cf4808cc17d40f17f81))
* proper attestations for nexus-search- and composite- collectors ([7ae7d7a](https://gitlab.com/hoppr/hoppr/commit/7ae7d7ae82558a5586740a4dd50a700a6a6e82b6))
* Protect keys ([9001a21](https://gitlab.com/hoppr/hoppr/commit/9001a219a93817d064a5cce207c267347b112549))
* provide better error message on empty config file content ([e87fff8](https://gitlab.com/hoppr/hoppr/commit/e87fff885842320adf85e7c86abb319e8dfa4f2d))
* pylint error ([6982109](https://gitlab.com/hoppr/hoppr/commit/69821095b2319075c65433a1f76e5d4e47fd361b))
* pylint error ([e36b242](https://gitlab.com/hoppr/hoppr/commit/e36b2424739e0dc4814855fb304dca1edd24e448))
* pylint issues fix ([7c7810c](https://gitlab.com/hoppr/hoppr/commit/7c7810c9ac63268472c2faf50e43694bdcd2cccb))
* raw collector stripping purl namespace ([1aca256](https://gitlab.com/hoppr/hoppr/commit/1aca256351c854eaa1c7f02e209961a20ece99aa))
* raw collector stripping purl namespace ([ee20ebe](https://gitlab.com/hoppr/hoppr/commit/ee20ebe02b6c1c7f1b1be9a44b82087565a5a5af))
* raw collector stripping purl namespace ([bba65cb](https://gitlab.com/hoppr/hoppr/commit/bba65cb7805b78a329ee59df1c10b9ad54959f8d))
* raw collector stripping purl namespace ([2c9309d](https://gitlab.com/hoppr/hoppr/commit/2c9309d09150511489185b455cf38d631611820b))
* README feedback; broken link fixes; try PyPI banner fix ([932121d](https://gitlab.com/hoppr/hoppr/commit/932121d087bbd2019007adb576b6c9122d4881fa))
* Rebase dev ([9399451](https://gitlab.com/hoppr/hoppr/commit/9399451c5f23b599d6d54c02538ebe4017a95620))
* redeclare Component attrs with hashable types ([a2034fa](https://gitlab.com/hoppr/hoppr/commit/a2034fa8ffd64f58b832199d0964d43893401962))
* redeclare Component attrs with hashable types ([26bbfc0](https://gitlab.com/hoppr/hoppr/commit/26bbfc064c252ce8b7e65205fe7d0c98987a5a41))
* Reference main so that develop can become default branch ([76f707b](https://gitlab.com/hoppr/hoppr/commit/76f707b6c3435bdc68d73b3717c451c6a6c95369))
* Release dev channel ([42ee862](https://gitlab.com/hoppr/hoppr/commit/42ee86236810072b2d303ea255f2d2556fd53749))
* Release dev channel ([15b4ae7](https://gitlab.com/hoppr/hoppr/commit/15b4ae76944accd1b4f0e6ae379b02c5efabb923))
* Remove blank lines ([196218c](https://gitlab.com/hoppr/hoppr/commit/196218c39d7dbb50c74722922190f61905f46053))
* remove construct method call ([2cf4963](https://gitlab.com/hoppr/hoppr/commit/2cf49637902c87dd786f37485521e8c55617685f))
* Remove gitlab semantic release comment on MRs ([4af82e6](https://gitlab.com/hoppr/hoppr/commit/4af82e6023f842a8587ec5183c5b3a212dc9e5f5))
* Remove licensing scanning and replace with policy ([e852316](https://gitlab.com/hoppr/hoppr/commit/e85231642c2f75a022d3872049b26e349821cc13))
* remove need for boolean from fail-open logic ([23bfa03](https://gitlab.com/hoppr/hoppr/commit/23bfa0397d12d39fcbca526eaadd651805e97c57))
* Remove node engine reference from package.json ([3c26b4e](https://gitlab.com/hoppr/hoppr/commit/3c26b4e768f23e9746db9f4b654a4799cb95e00b))
* Remove pack, and idx from check ([6e24462](https://gitlab.com/hoppr/hoppr/commit/6e244629bf39228cee030550da47863b11c6aec5))
* Remove package.json ([e13de8d](https://gitlab.com/hoppr/hoppr/commit/e13de8d885045b1e750fceeace1e60d4d9d032b6))
* remove prerelease ([1e038d0](https://gitlab.com/hoppr/hoppr/commit/1e038d0b73fe23da781bdea4cb02b9241e19aa8e))
* remove prerelease ([6818939](https://gitlab.com/hoppr/hoppr/commit/6818939d8237876f7dfabdce72a0dab1270db9b3))
* remove problematic git files from tar toc comparison ([b883b23](https://gitlab.com/hoppr/hoppr/commit/b883b23fbf230857a6244b7b8207144e3cc3a920))
* remove problematic git files from tar toc comparison ([89f1621](https://gitlab.com/hoppr/hoppr/commit/89f16211e602d92820cad9719257a57d2bfbcb39))
* Remove quotations around build ([3848f46](https://gitlab.com/hoppr/hoppr/commit/3848f463bb8640e4acd524bfdd04838291e9222d))
* Remove quotations from parallel matrix ([28ee2b7](https://gitlab.com/hoppr/hoppr/commit/28ee2b707fa0f36992f131e7bbb548f164b23e14))
* Remove quotes ([7323a8b](https://gitlab.com/hoppr/hoppr/commit/7323a8b16abc50d284a7d67bf7a346fe0190369c))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([0ac8adc](https://gitlab.com/hoppr/hoppr/commit/0ac8adc4c70bf79e5068daf8f8d5501c045af29a))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([87ae120](https://gitlab.com/hoppr/hoppr/commit/87ae1209adced6c7004143d33e314df719740da1))
* remove references to deleted job ([6d9890e](https://gitlab.com/hoppr/hoppr/commit/6d9890e99f43b439f20505142e2706c1556d5abf))
* remove references to deleted job ([7d1ef14](https://gitlab.com/hoppr/hoppr/commit/7d1ef14d1bb86f478ad35b5c08038b08b1fc916c))
* Remove skip ci and attempt to let workflow rules handle pipelines ([0e325a5](https://gitlab.com/hoppr/hoppr/commit/0e325a5e7a2caea831856e0ba6566f0a5ecaf2c7))
* restore Dockerfile FROM directive ([e7e0622](https://gitlab.com/hoppr/hoppr/commit/e7e06222a9768bfeaab9f10e13a9b213ccd45be7))
* Result constructor param typo ([09290d2](https://gitlab.com/hoppr/hoppr/commit/09290d25bd2be5f07635c718c7691dad6e00e652))
* Revert Changes ([f6797ea](https://gitlab.com/hoppr/hoppr/commit/f6797ea323e79a9bcd8b67b00c4939004e0dafcc))
* revert to older version of npm-check-updates ([9fb3ebf](https://gitlab.com/hoppr/hoppr/commit/9fb3ebf011be353fe119759514868151e0ddf3c2))
* Roll back chore rules ([e40fbfb](https://gitlab.com/hoppr/hoppr/commit/e40fbfbcb463ccc704c351b1acad04f3ea1fb0ea))
* Roll base image back from 9.0 to 8.6 ([31f8166](https://gitlab.com/hoppr/hoppr/commit/31f81666e3bb135ad7ad7b05d28cbc96c77859bc))
* Run if commit message doesn't start with chore ([daa5d11](https://gitlab.com/hoppr/hoppr/commit/daa5d119686ca58488cc04c62d48b73bb9c49319))
* Run pre-commit ([7b740ec](https://gitlab.com/hoppr/hoppr/commit/7b740eca0a6f3bd92d99e7c8f29c3fd435f0df4d))
* Run the entire dockerfile as one large image ([47ec5a5](https://gitlab.com/hoppr/hoppr/commit/47ec5a57dece0be112dca8c068a2807edc811bf8))
* sbom metadata ([eb5225f](https://gitlab.com/hoppr/hoppr/commit/eb5225f8f0729e44701d6b75ce7f74870f7eecbb))
* sbom metadata ([f97e7a7](https://gitlab.com/hoppr/hoppr/commit/f97e7a7acb00e9d361887dfad55c7bc2f03b10f0))
* SBOM spec selection logic ([d12bd8b](https://gitlab.com/hoppr/hoppr/commit/d12bd8ba19c0b5b8ec879d1f8190b8afb999d80b))
* SBOM spec selection logic ([2cd7286](https://gitlab.com/hoppr/hoppr/commit/2cd72866b1ae7a1fda1b580a4f7dbc2942a0da49))
* semantic-release version bump ([d45efb7](https://gitlab.com/hoppr/hoppr/commit/d45efb73d70625bda73ca4c4dfec33698c62957f))
* set file name correctly for collect_maven_plugin ([07e4eb5](https://gitlab.com/hoppr/hoppr/commit/07e4eb55388b25f46fdd2ed7a0af04fa86a8e427))
* set prerelease back to true ([49b130e](https://gitlab.com/hoppr/hoppr/commit/49b130e916dc7b3f9ec9fdaead06ce29e78f32c8))
* Set the correct version of oras ([d8357a4](https://gitlab.com/hoppr/hoppr/commit/d8357a431e33ec0fb45e0fc51537eae603e7cb6a))
* set timeout for run_command to reduce hanging ([9be8afe](https://gitlab.com/hoppr/hoppr/commit/9be8afefc62f1868d908cf8ecb8e9bd211662c37))
* set timetag to utc timezone ([12aa368](https://gitlab.com/hoppr/hoppr/commit/12aa368689a4225fc9662f59624ee90049fb8c4f))
* Setup codequality analytics and build attestation ([c5c4514](https://gitlab.com/hoppr/hoppr/commit/c5c4514e153becdf4a39713259b289c4469a24eb))
* Setup dependencies on test image ([6fabada](https://gitlab.com/hoppr/hoppr/commit/6fabada652d546cd89f1cd16eb32bae2cd0cb08a))
* shared RLock for Docker unit tests ([b2f3c9c](https://gitlab.com/hoppr/hoppr/commit/b2f3c9c5eb31bd31e46940fdf453ff56ae0f54f5))
* Simlink python ([0009483](https://gitlab.com/hoppr/hoppr/commit/0009483d56965e8770ae67383ecc69d4e7a53697))
* Simplify some of the configuration options ([a52594a](https://gitlab.com/hoppr/hoppr/commit/a52594a9e0873392934d6bfdb442ee3633f58a00))
* speed up unit tests ([32f8715](https://gitlab.com/hoppr/hoppr/commit/32f87159f4fc125a338e61fadefb80f5cb306ccb))
* Split out ci config per !184#note_1170995317 ([f42e582](https://gitlab.com/hoppr/hoppr/commit/f42e5825d7a3ebeab60ae107b10624d5567c7173)), closes [184#note_1170995317](https://gitlab.com/hoppr/184/issues/note_1170995317)
* stage name regex field type ([3d4c133](https://gitlab.com/hoppr/hoppr/commit/3d4c13381a43ddbdcbe4629a22ef0f75b7019035))
* strip quotes to compare found URL ([396c31a](https://gitlab.com/hoppr/hoppr/commit/396c31a90846673f991867a2c2057959457222f0))
* syntax ([88b8961](https://gitlab.com/hoppr/hoppr/commit/88b8961dcc41c6a21e02da417a3c059cf0036641))
* syntax ([4e8fc18](https://gitlab.com/hoppr/hoppr/commit/4e8fc18f3bc21f00d60e9a190e94fc7a4a9d9078))
* syntax ([84c2c38](https://gitlab.com/hoppr/hoppr/commit/84c2c3888485f188af9202b9fc8c1d20fc243331))
* syntax ([762a702](https://gitlab.com/hoppr/hoppr/commit/762a702417b358c3fd0b568c631bbd0c9a5a5aef))
* temporary peg of securesystemslib to address in-toto bug ([8261f41](https://gitlab.com/hoppr/hoppr/commit/8261f414b9951ede1e5a7efc44dfa429d6cf5790))
* test image tags ([0180488](https://gitlab.com/hoppr/hoppr/commit/01804886f7a709e105e432d53d96c7ca19c67a80))
* test pattern matching ([c6e7c51](https://gitlab.com/hoppr/hoppr/commit/c6e7c5197847e13f54a14cf112ca58d5cad86602))
* **test:** credentials unit test coverage ([74b981f](https://gitlab.com/hoppr/hoppr/commit/74b981f51e58adbd5581e815f239f97e54003ed0))
* transfer file default value ([ac1bf1a](https://gitlab.com/hoppr/hoppr/commit/ac1bf1a775d534bd89217443c1711bebd49e957b))
* turn off allow-failure on semantic-release:dry-run job to avoid breaking changes from renovate ([3a32a65](https://gitlab.com/hoppr/hoppr/commit/3a32a656abdb3c2f7cb006547a353f3fdae615b5))
* type check errors ([02b3d27](https://gitlab.com/hoppr/hoppr/commit/02b3d2713ccd8169643121abfb5ec206d01bbe4b))
* type error ([25aea9c](https://gitlab.com/hoppr/hoppr/commit/25aea9cb031b72a620c04029fce89872e0a9f3cd))
* type errors ([30861b6](https://gitlab.com/hoppr/hoppr/commit/30861b6abc6810f234c85a9de1d6db64c826430b))
* unit test context missing delivered sbom ([0c3d27a](https://gitlab.com/hoppr/hoppr/commit/0c3d27ae52d28492b3080249430099a6cd3cd93f))
* unit test message ([81415ed](https://gitlab.com/hoppr/hoppr/commit/81415edb08a9a6f925a911939095a5c001107531))
* unit test message ([dc95a02](https://gitlab.com/hoppr/hoppr/commit/dc95a025e8d295c4b0572ba105ede3b69f8a4738))
* unit test message ([69a164f](https://gitlab.com/hoppr/hoppr/commit/69a164f500097c97b29cf408a9ca695d6a3505e3))
* unit test message ([5846f61](https://gitlab.com/hoppr/hoppr/commit/5846f6109f3e5adf55ec6bfa4596e95f002e1ead))
* Update artifact name ([4f64b03](https://gitlab.com/hoppr/hoppr/commit/4f64b03b8a36590887e8430aa5caf3a48e25cd7a))
* update assert ([eba7ec9](https://gitlab.com/hoppr/hoppr/commit/eba7ec90d1927369be2bb91115388ec3fab0ec58))
* update CODEOWNERS for repo move ([6541b0c](https://gitlab.com/hoppr/hoppr/commit/6541b0c6ce79ed1dc0e4156d2e39a2620ee4b0c4))
* update delivered_sbom with process return objects ([46da6f3](https://gitlab.com/hoppr/hoppr/commit/46da6f324eca0ff4bda4077f385ff2ad20e87177))
* Update expected toc (since it's changed), and correct the regression boms ([fe79087](https://gitlab.com/hoppr/hoppr/commit/fe79087aeef00eab3792de8c5d3ea343ab9ce4d3))
* update expected toc's with delta stage intermediate delivered bom ([f016526](https://gitlab.com/hoppr/hoppr/commit/f0165265077c35e578b62d252c384594732e0d3c))
* update expected toc's with delta stage intermediate delivered bom ([3291773](https://gitlab.com/hoppr/hoppr/commit/32917735d1e84afa74aa584c83f899206bb3cfea))
* update expected_tar_toc files for changed maven file names ([b1ef013](https://gitlab.com/hoppr/hoppr/commit/b1ef01389841407c0723a2ced6bc397709c2a24d))
* Update git collector depth defaults per MR suggestion ([43a3dbb](https://gitlab.com/hoppr/hoppr/commit/43a3dbb296db4a43f2a70f9eb87a6d56ad3ff510))
* Update maven write to disk ([82d42b6](https://gitlab.com/hoppr/hoppr/commit/82d42b6dbce5eec0868199da0c1f4c32c9a99d61))
* update Result object to include optional return object.  Check that return object matches plugin Bom Access value ([b346872](https://gitlab.com/hoppr/hoppr/commit/b3468720b276af24d95c0e748ecb3701a4fd0764))
* Update variables to contain artifact name ([0400e28](https://gitlab.com/hoppr/hoppr/commit/0400e280e42b9eec5282bb7d59fab3ae100a1872))
* updated expected tar for pypi ([7356d6b](https://gitlab.com/hoppr/hoppr/commit/7356d6b6c82ee1d54dcd22158451664dd18c7779))
* Updated unit tests for docker collection ([7dbd2af](https://gitlab.com/hoppr/hoppr/commit/7dbd2af98c871d8cfdc20b48f0164fc8cb861ff0))
* updates to ensure pipeline works for forks. ([5fa6fe9](https://gitlab.com/hoppr/hoppr/commit/5fa6fe98921248797dc75375d500be9ab2f37381))
* urljoin stripping path components ([4c6ad9f](https://gitlab.com/hoppr/hoppr/commit/4c6ad9f3de6d2351b4eb274b884062d1b0c6e841))
* use enums for constants, per code review ([40cc853](https://gitlab.com/hoppr/hoppr/commit/40cc8538d5921846202167e7f0089a18d1719137))
* Use list of options and check for troublesome values ([fb39372](https://gitlab.com/hoppr/hoppr/commit/fb39372b37ee1bc5eec7ac49e618a521d38b5be3))
* Use long switches for readability. ([fdbfec2](https://gitlab.com/hoppr/hoppr/commit/fdbfec26f6f528ce1c79bff518b4f61f2708db5d))
* use more stable package for apt testing ([f365df7](https://gitlab.com/hoppr/hoppr/commit/f365df765c2bd162a9cf6c8120c720a6897fbc23))
* use more stable package for apt testing ([1aca9d7](https://gitlab.com/hoppr/hoppr/commit/1aca9d7c5d751785f8aba52bbd2cb63319fd8583))
* Use python3 for virtual environment creation in CI Dockerfile ([e39ea91](https://gitlab.com/hoppr/hoppr/commit/e39ea91f9456b7f006ef6dea5f57579376829776))
* Use semantic release to publish package ([27e8eb0](https://gitlab.com/hoppr/hoppr/commit/27e8eb02b8bd3405cec90b5d3112662dc2675c17))
* Using copy from Maven-Dependency-Plugin instead of get ([7ab2fe1](https://gitlab.com/hoppr/hoppr/commit/7ab2fe15234b1bda27f91bb0ea6b025cde2c22d2))
* windows ANSI processing ([eba2cb4](https://gitlab.com/hoppr/hoppr/commit/eba2cb4a18d7bb375e9984c838772506d6c40bb7))
* wrapper for run cmd ([9d5844b](https://gitlab.com/hoppr/hoppr/commit/9d5844b8f0661b3be8ef63d120bf5d96023fe844))


### Reverts

* files not relevant to this branch ([fe61dea](https://gitlab.com/hoppr/hoppr/commit/fe61deaf45ff0fd0e0e5252ac783f751ee6ddedf))
* files not relevant to this branch ([c66595a](https://gitlab.com/hoppr/hoppr/commit/c66595a47a8d089b98566940eb4ae565d094c5da))
* generic types ([96721e4](https://gitlab.com/hoppr/hoppr/commit/96721e42511934c36bbb9014da20d24a1b2b9546))
* generic types ([9915c1a](https://gitlab.com/hoppr/hoppr/commit/9915c1aa2bb39bbccdc959b2584f56954698d84c))
* method_name condition logic ([6d9a16b](https://gitlab.com/hoppr/hoppr/commit/6d9a16bc87de3e8cce5a835093076ea76242b627))
* method_name condition logic ([6445f18](https://gitlab.com/hoppr/hoppr/commit/6445f18cb62e61bb2b8738bd46dbd75df766e961))
* model integration changes ([e27eb91](https://gitlab.com/hoppr/hoppr/commit/e27eb91fcc8a8b5cc2e0c6f110c6de1e37010f82))
* shared memory manager changes ([09a6ee3](https://gitlab.com/hoppr/hoppr/commit/09a6ee348e8820fc91d5ac5ad2016f8e96aa041e))
* shared memory manager changes ([4dbd8c0](https://gitlab.com/hoppr/hoppr/commit/4dbd8c0fc99b7e2d52b717bc555b326962a27467))

## [1.8.0-dev.1](https://gitlab.com/hoppr/hoppr/compare/v1.7.2...v1.8.0-dev.1) (2023-04-04)


### Features

* --no-strict CLI flag ([05cf05c](https://gitlab.com/hoppr/hoppr/commit/05cf05caea72c11823c635e391a8dd10eb6bd0d4))
* --no-strict CLI flag ([12bc0d3](https://gitlab.com/hoppr/hoppr/commit/12bc0d3200a5f19cf5c04b0cf8eb8423f9c00c4c))
* add composite collector ([47037b8](https://gitlab.com/hoppr/hoppr/commit/47037b89e38e8694d803309eed486245a461a4b2))
* add delta_sbom capability ([c73e081](https://gitlab.com/hoppr/hoppr/commit/c73e0816444cefa4547917da54dc650e7777d7da))
* add delta_sbom capability ([a7d265c](https://gitlab.com/hoppr/hoppr/commit/a7d265ccbe6254a78f5cf5e1b9808c7a3eb570a5))
* add delta_sbom capability ([5bb5a59](https://gitlab.com/hoppr/hoppr/commit/5bb5a599d0c111399101f5e676f20e19c1954022))
* add delta_sbom capability ([9a39d88](https://gitlab.com/hoppr/hoppr/commit/9a39d88b53f1f277aecfa1c6d2edefe82098e801))
* add nexus search collector ([7ccb05e](https://gitlab.com/hoppr/hoppr/commit/7ccb05e01300cb9dde702185d28bc2941c83c3fc))
* allow docker collector to use docker.io when --no-strict option is set ([1163405](https://gitlab.com/hoppr/hoppr/commit/116340559d89fa8f6618dc48b524ff6b0a42c66c))
* creation of in-toto attestations ([b58a973](https://gitlab.com/hoppr/hoppr/commit/b58a973e9b7bd5a3d52e5f23d4cdd97155b5143e))
* Report Generation Plugin ([126a0e5](https://gitlab.com/hoppr/hoppr/commit/126a0e5430be13824e67037225b617f24965f79c))
* skip collecting components with a scope of excluded ([e2fd680](https://gitlab.com/hoppr/hoppr/commit/e2fd680389100d216089a0a1aeedcbf4121b635b))


### Bug Fixes

* Add additional configuration capability ([dc14c57](https://gitlab.com/hoppr/hoppr/commit/dc14c5728f5869f9f47f0f2e18cc1b01812e6201))
* Add additional fixes per MR feedback ([598b9f1](https://gitlab.com/hoppr/hoppr/commit/598b9f1c65bf1dcee2cf31dd84553143be73b25f))
* Add Bot label to renovate MRs ([f43cd65](https://gitlab.com/hoppr/hoppr/commit/f43cd65250b65e93eb1cfa3debdf74a3bad58807))
* Add build arg to dockerfile ([ae46f93](https://gitlab.com/hoppr/hoppr/commit/ae46f93d360ab0639d706036721b48769b4c2feb))
* add collection metadata to sbom for apt components ([b8284f5](https://gitlab.com/hoppr/hoppr/commit/b8284f54fc8d0d193bad0916dc7ce83d7ff97e83))
* add collections params for docker ([228d327](https://gitlab.com/hoppr/hoppr/commit/228d327a31b2a7e043af38a016b98d20825f5348))
* add command line option to override previous collection location in delta_sbom plugin ([b3eb0bd](https://gitlab.com/hoppr/hoppr/commit/b3eb0bd1b1b40c936d34c7075fcf525ddf2e76c7))
* add command line option to override previous collection location in delta_sbom plugin ([3485048](https://gitlab.com/hoppr/hoppr/commit/34850483f44788e7e7cd44965984bba6b6502922))
* add command line option to override previous collection location in delta_sbom plugin ([d818730](https://gitlab.com/hoppr/hoppr/commit/d818730a0b6c4d930cdb220f31c2555a21aff42a))
* add command line option to override previous collection location in delta_sbom plugin ([2331668](https://gitlab.com/hoppr/hoppr/commit/2331668aa793c290764f6107dcfbe5f787dbba04))
* Add curl for oras test ([804f8b0](https://gitlab.com/hoppr/hoppr/commit/804f8b0b3d665d40e5299d21ec84f847a2a6e7d1))
* add delivered_sbom to context, work from that variable ([07b6ba9](https://gitlab.com/hoppr/hoppr/commit/07b6ba93a7d5b5b939ad53e4ad0d408276cd337a))
* Add entrypoint to hoppr ([073fdeb](https://gitlab.com/hoppr/hoppr/commit/073fdebdae77d9db6774ab1b84c2a336df68ad52))
* add expected-tar-toc for remaining integration tests ([7bbcac9](https://gitlab.com/hoppr/hoppr/commit/7bbcac9c821b6528edf9f7b247886b11ee18db45))
* add expected-tar-toc for remaining integration tests ([a071522](https://gitlab.com/hoppr/hoppr/commit/a07152294d7430bd552255dd911a9730ae526061))
* add expected-tar-toc to delta integration test ([40fdb97](https://gitlab.com/hoppr/hoppr/commit/40fdb9778b2b20c0b07aa475e5de931a6e4cf227))
* add expected-tar-toc to delta integration test ([33ed419](https://gitlab.com/hoppr/hoppr/commit/33ed41925bedc0258c2bb3a537ccb6a0b4311b39))
* Add hopctl docker image ([1363cec](https://gitlab.com/hoppr/hoppr/commit/1363cec9d1214acfed27c03b3c785fdcf29e7be2))
* Add hopctl docker image ([c4d00fc](https://gitlab.com/hoppr/hoppr/commit/c4d00fc04074112c5fddad91005cb6361ddaf8fc))
* Add hopctl docker image ([5ad0556](https://gitlab.com/hoppr/hoppr/commit/5ad0556d41e665fe20706d6e34833ab835ead8e7))
* add initial checks for BOM access ([d89fed2](https://gitlab.com/hoppr/hoppr/commit/d89fed21d1c807f17d1089e748d38bcb97fc9dde))
* add integration test for deltas ([e0c7e47](https://gitlab.com/hoppr/hoppr/commit/e0c7e47619424601f867040064a5fc6df402e29b))
* add integration test for deltas ([2ca6ed0](https://gitlab.com/hoppr/hoppr/commit/2ca6ed0019f9b21613193a5dd525b470a12a1d56))
* add integration test for deltas ([6789f9c](https://gitlab.com/hoppr/hoppr/commit/6789f9c240a110f10e717dc42c3cff40ba4352e6))
* add integration test for deltas ([c56d066](https://gitlab.com/hoppr/hoppr/commit/c56d0664ddeb49fe6bc4b4cf997ebd782a761163))
* add missing import ([603f219](https://gitlab.com/hoppr/hoppr/commit/603f21910e7d2cbf3f98813ba45a22454330b0ff))
* add missing strict_repos typer argument ([ad9bb37](https://gitlab.com/hoppr/hoppr/commit/ad9bb3736d57cea04ff902aa2241c88ba426d394))
* add mock for os.path.exist ([5463b80](https://gitlab.com/hoppr/hoppr/commit/5463b801539bbf1436e41b0aa0827cb2a54f2b9a))
* Add oci artifacts for reference ([f6661be](https://gitlab.com/hoppr/hoppr/commit/f6661beba1f1fff54a779ea973a5869a72c6616d))
* Add oras integration test ([cbd9376](https://gitlab.com/hoppr/hoppr/commit/cbd937615039f3f99451bf600ae40948e69f0049))
* add purl-type/repo-type mappings to nexus_search collector ([1b3d06a](https://gitlab.com/hoppr/hoppr/commit/1b3d06a04e67dcf0acc4bd2dd55eca07aa282329))
* add pytest-cov package ([a6666a8](https://gitlab.com/hoppr/hoppr/commit/a6666a84c85d0e7939519506226fef23dd05c221))
* add repo to purl type list ([01fa2f2](https://gitlab.com/hoppr/hoppr/commit/01fa2f254d56a06481e4b79e29ecd3f184ce7e04))
* add repository/directory properties to bom for all collectors ([bad3136](https://gitlab.com/hoppr/hoppr/commit/bad3136b6f7dd1fc7579afd5230cd28c5aa26b1b))
* add shared logfile lock ([e350189](https://gitlab.com/hoppr/hoppr/commit/e350189654b9461759e1483458489e42db510f1e))
* add shared logfile lock ([3c3c238](https://gitlab.com/hoppr/hoppr/commit/3c3c23803a56eaad75eaa41090b5ed258393ea0d))
* Add test verification for oras bundle ([77f04b7](https://gitlab.com/hoppr/hoppr/commit/77f04b775b030db593ade6d4437a8e6ff0309be6))
* add version to bom plutin property ([166538c](https://gitlab.com/hoppr/hoppr/commit/166538cf487149e3932a8a243742f8af016db123))
* Added user_env support to find_credentials ([ee58f2d](https://gitlab.com/hoppr/hoppr/commit/ee58f2d48766562152be513083038b226ed9ef98))
* address issue where in-toto was looking maven files ([98fc06a](https://gitlab.com/hoppr/hoppr/commit/98fc06aaeea92bd2eacb459f1d4ca9fa97cf34f0))
* all source distros from manifest repos ([52c8c80](https://gitlab.com/hoppr/hoppr/commit/52c8c8073eabedb81aa93e325c7ee03b465750c1))
* allow more full repository specification for collect_nexus_search ([e2cdf02](https://gitlab.com/hoppr/hoppr/commit/e2cdf027754a2f1568035fe5303a8e62024a00fa))
* allow more full repository specification for collect_nexus_search ([6ebcceb](https://gitlab.com/hoppr/hoppr/commit/6ebccebab92c792fc335ba1251908faf69e77dab))
* allow no scheme for repo URLs as last resort ([866382f](https://gitlab.com/hoppr/hoppr/commit/866382f298a701eadb13cd8b609b74b5e13da88a))
* allow no scheme for repo URLs as last resort ([fd1feec](https://gitlab.com/hoppr/hoppr/commit/fd1feecf559855b4eb3b8127d848f1daf2c72a19))
* allow spaces in stage name ([d7af57f](https://gitlab.com/hoppr/hoppr/commit/d7af57f3892e34fd62b5bfe1dae9f4c5d3704657))
* allow spaces in stage name ([923c043](https://gitlab.com/hoppr/hoppr/commit/923c043b1aa7b686e4777844372272aafb9a7e77))
* append dev instead of current branch name ([d6abddc](https://gitlab.com/hoppr/hoppr/commit/d6abddc66d3b832feb1923d1eb31d0557b03c892))
* Apply correction to git repository collector ([dc522b5](https://gitlab.com/hoppr/hoppr/commit/dc522b555ad2965ba7965f4630e0e1f7d1eb2849))
* apply in-toto suggestions ([3e92177](https://gitlab.com/hoppr/hoppr/commit/3e92177f671c4781907c0a6df279293e543ebb2f))
* apply in-toto suggestions ([401757e](https://gitlab.com/hoppr/hoppr/commit/401757eae524fe76d7eda6f39ab38ff20dd0e01c))
* applying MR suggestion ([c3adeee](https://gitlab.com/hoppr/hoppr/commit/c3adeee1813eba8dd308fec358d27af9f25c1f1d))
* applying MR suggestion ([4e5ca7f](https://gitlab.com/hoppr/hoppr/commit/4e5ca7fb7258c667a431653ab01bb8745a87308f))
* applying MR suggestion ([4e61f84](https://gitlab.com/hoppr/hoppr/commit/4e61f84272b14180e16f6a4baa424f5d60eb2752))
* applying MR suggestion ([b034b72](https://gitlab.com/hoppr/hoppr/commit/b034b7273179a7fb8bd3bda411050d9815b58ce7))
* apt collector _get_download_url_path ([aa988d6](https://gitlab.com/hoppr/hoppr/commit/aa988d680fe350e5716f30abbed0437e51ae9797))
* apt collector _get_download_url_path ([1d36fd4](https://gitlab.com/hoppr/hoppr/commit/1d36fd457fc60d9d9638611e3b91940a2a523194))
* bom helm chart version ([009bdf0](https://gitlab.com/hoppr/hoppr/commit/009bdf00412fe6964b81c15b86aec52a761890df))
* Branch isolation testing ([fcde124](https://gitlab.com/hoppr/hoppr/commit/fcde12425dd610fe3da0dbbd56de6af677a8688e))
* bug with with attestations created from GitLab CI Runners ([f8ca8e7](https://gitlab.com/hoppr/hoppr/commit/f8ca8e79144b6377250d38ccc37bb2f7689d2033))
* casing for "kind" field ([26b0927](https://gitlab.com/hoppr/hoppr/commit/26b0927a143763f688215b0fc3d49e27a56f0b71))
* casing for "kind" field ([533f6a7](https://gitlab.com/hoppr/hoppr/commit/533f6a770bc8738aa39b22c2faa6d262c4b7264d))
* catch exception from _get_required_coverage, check for empty/missing boms ([d905e35](https://gitlab.com/hoppr/hoppr/commit/d905e3557ce6def86b923db7961f3631808f3445))
* check for exception not thrown in pypi success ([0ba41f6](https://gitlab.com/hoppr/hoppr/commit/0ba41f6a9962ede186765b25e7939623fbb15095))
* check tar toc on integration tests ([aafc6bb](https://gitlab.com/hoppr/hoppr/commit/aafc6bb6eb9215e0c158a1de007fc67a8509059e))
* check tar toc on integration tests ([105c87a](https://gitlab.com/hoppr/hoppr/commit/105c87a4c81809c1a586713d42ff1e857e0c260e))
* Clean up artifact name ([a59aaac](https://gitlab.com/hoppr/hoppr/commit/a59aaacc2bb23988b8e240d0f6c237b547e86f39))
* Clean up other variables in ci ([d289184](https://gitlab.com/hoppr/hoppr/commit/d28918482ceb951461725095e72f7ca9ebcb23ce))
* clean up repository_url handling ([c867a3b](https://gitlab.com/hoppr/hoppr/commit/c867a3bda17c70e59e1d50672543077b7daae8cb))
* clean up repository_url handling ([6fb544b](https://gitlab.com/hoppr/hoppr/commit/6fb544b1ceb0846d096cf790efa8f52a7160960c))
* clean up repository_url handling ([95a0288](https://gitlab.com/hoppr/hoppr/commit/95a0288eeecd7c0baf03af5b67dd0d50fb549708))
* clean up repository_url handling ([011e53b](https://gitlab.com/hoppr/hoppr/commit/011e53b8b96760f03863b5a9582f91ad30f98733))
* Clean up rules and workflow ([69e9b73](https://gitlab.com/hoppr/hoppr/commit/69e9b73ebab517658ad0e5238038e01979f6e98a))
* Cleaned up maven command ([2b4af6e](https://gitlab.com/hoppr/hoppr/commit/2b4af6e57658f5f96a930cf11542f802f783476d))
* Cleanup notes in config ([2632bfa](https://gitlab.com/hoppr/hoppr/commit/2632bfabe9794542033ce8b4dd8d6a5cb4e96da8))
* Cleanup rules ([ea79632](https://gitlab.com/hoppr/hoppr/commit/ea7963280418793154b6cd113c926ff160ce011e))
* Cleanup rules ([76d65e5](https://gitlab.com/hoppr/hoppr/commit/76d65e5cf60559f4672f63dd33a62c45f3009eab))
* clear loaded manifests ([39e7341](https://gitlab.com/hoppr/hoppr/commit/39e73413a9e446d1f5b98118ff8b3ec2799260e5))
* code review comments ([c9fa7e2](https://gitlab.com/hoppr/hoppr/commit/c9fa7e2b37422a04c4a2281b81b425d46bcedb12))
* Code review comments ([a204676](https://gitlab.com/hoppr/hoppr/commit/a204676aa26173864856e458a30f119caeb3ae9a))
* complete unit test coverage for collect_nexus_search ([24827e4](https://gitlab.com/hoppr/hoppr/commit/24827e4cab02da2808deb99fd372c6a6b3935ed7))
* component search sequence ([ff89f56](https://gitlab.com/hoppr/hoppr/commit/ff89f5633b386b6b006ff7f76b6558ad894f2bb2))
* component search sequence ([397d3bf](https://gitlab.com/hoppr/hoppr/commit/397d3bf639ed8e729fb4e209078cd0877ae6b07f))
* Correct build artifacts ([40ca47f](https://gitlab.com/hoppr/hoppr/commit/40ca47f2642bd82be5aec18f75a3f468c1d880e7))
* Correct deployment teir ([d4282c8](https://gitlab.com/hoppr/hoppr/commit/d4282c8c1e54fe188acfde63a9307c33474c9f8b))
* Correct deployment teir ([983932d](https://gitlab.com/hoppr/hoppr/commit/983932d32f998ca41536002671b324734d711716))
* Correct dockerfile ([30f438a](https://gitlab.com/hoppr/hoppr/commit/30f438a16501280c0dd97633d3e2447e2c80998e))
* Correct git bom, had incorrect purl, name, and version ([e6a1265](https://gitlab.com/hoppr/hoppr/commit/e6a1265e9862aa66996e10a7c956a1315bcfc89b))
* Correct integration tests ([b065953](https://gitlab.com/hoppr/hoppr/commit/b0659532c14ddea25afcb7357d14104427cddefe))
* Correct media types for registry ([d3ea0df](https://gitlab.com/hoppr/hoppr/commit/d3ea0df0cd2eb276eb86d1cb82dfceeb39604e71))
* Correct release yaml file ([a29c26c](https://gitlab.com/hoppr/hoppr/commit/a29c26c86220443c22e94954bd6db5d3e7a59147))
* Correct releaserc file ([8868fab](https://gitlab.com/hoppr/hoppr/commit/8868fab85c0ed587f8d9679af6c8d793ed78c50b))
* Correct releaserc.yml ([4b55f7f](https://gitlab.com/hoppr/hoppr/commit/4b55f7f25895b1251ab7e9499280848d86732cbb))
* Correct requirements ([7598beb](https://gitlab.com/hoppr/hoppr/commit/7598beb540c9c26860501b42ba92fb807f79fd33))
* Correct simlinks in ci docker ([deb0728](https://gitlab.com/hoppr/hoppr/commit/deb07284c27603325678ae3332f0a2023ed42f80))
* Correct syntax in ci docker ([f96c938](https://gitlab.com/hoppr/hoppr/commit/f96c938502b98e0bdbe4096ed6a35f830abaf378))
* Correct tests so they pass ([eeebaef](https://gitlab.com/hoppr/hoppr/commit/eeebaef2b781cde5e668d0d75e65977185d56d45))
* Correct the oras binary arch type ([71bc21e](https://gitlab.com/hoppr/hoppr/commit/71bc21ec0b5308c4299906ab04b270b678480551))
* Correct the wheel name ([4847b4a](https://gitlab.com/hoppr/hoppr/commit/4847b4a8548c563ad4298616f8c16288ca925aaa))
* Corrected Maven-Dependency-Plugin arguments so that maven artifacts would be bundled ([9fe9eaa](https://gitlab.com/hoppr/hoppr/commit/9fe9eaa13c68ed8d36437a6662f735c347018ed4))
* credential.find method no longer needs exact match.  added lines to _run_data_ metadata file. ([202b0ae](https://gitlab.com/hoppr/hoppr/commit/202b0ae90ba70cf8d45990064e4b3d2ce6b9fb4a))
* Cut release from next branch ([17d0d03](https://gitlab.com/hoppr/hoppr/commit/17d0d03ac243fea51d9ea98738d0e8293c60803d))
* Cut release from next branch ([e1c56d2](https://gitlab.com/hoppr/hoppr/commit/e1c56d28394803045b974195e6e7d9cc796c967b))
* **deps:** update dependency hoppr-cyclonedx-models to v0.2.10 ([ee37064](https://gitlab.com/hoppr/hoppr/commit/ee370642914f1b507ed4793e11cbc575d1c51720))
* **deps:** update dependency typer to ^0.7.0 ([a35bd85](https://gitlab.com/hoppr/hoppr/commit/a35bd8519c8486b68a9228b660a2d2f44bad7f2c))
* dev branch test; update README ([f8ed5ee](https://gitlab.com/hoppr/hoppr/commit/f8ed5eec1a2b8d68730b88d6d378d132fa67c1c7))
* DNF download directly from found URL ([bf84cec](https://gitlab.com/hoppr/hoppr/commit/bf84cec779df983642116cf99cf42403675f17e9))
* DNF download directly from found URL ([11d5e5e](https://gitlab.com/hoppr/hoppr/commit/11d5e5ec6280b60d821880833d5ee7f0d3683432))
* do not re-generate consolidated/delivered sboms ([ee1928b](https://gitlab.com/hoppr/hoppr/commit/ee1928b6198e0fb57c3eb56af0dc3b5c057166d2))
* do not re-generate consolidated/delivered sboms ([ce59dcc](https://gitlab.com/hoppr/hoppr/commit/ce59dcc46fbbef3f3941c0f33c3d0cc0c0abacf7))
* Don't run pipelines on merge event ([a0b8c53](https://gitlab.com/hoppr/hoppr/commit/a0b8c53add628e08a0c02f30beea57a399b9f109))
* Ensure python is on the path ([a1c03eb](https://gitlab.com/hoppr/hoppr/commit/a1c03eb6e2e607267886890831c2675572525d6b))
* enum base type ([4e3577c](https://gitlab.com/hoppr/hoppr/commit/4e3577c8b96bd5c107649c3d6adb6ef1dd34e4d1))
* exclude nulls from output sboms ([e41bd52](https://gitlab.com/hoppr/hoppr/commit/e41bd52190ec8acba3fdd419650674e7739224f7))
* expected apt SBOM ([c6b01e9](https://gitlab.com/hoppr/hoppr/commit/c6b01e935d7aadf1f2fe3410edf901d95c9f8fb3))
* expected apt SBOM ([8e7d3da](https://gitlab.com/hoppr/hoppr/commit/8e7d3daaab31eccc1686963d30bbbd72c70f6bf0))
* expected metadata source location ([53f74d7](https://gitlab.com/hoppr/hoppr/commit/53f74d7f154df37e45d91e782bfbee5f30570fd9))
* expected metadata source location ([f918d63](https://gitlab.com/hoppr/hoppr/commit/f918d63976bfb5ac57405ea48b30c9b34e8fe0a6))
* expected-tar-toc sort order changed ([b89f853](https://gitlab.com/hoppr/hoppr/commit/b89f853e5547aac8e0a69b7245f8f3abddc5d70f))
* fail on no change, improved status messages ([801dea2](https://gitlab.com/hoppr/hoppr/commit/801dea20d82167b303ebbd0a339cb8e408c9b873))
* fail on no change, improved status messages ([1a4c87c](https://gitlab.com/hoppr/hoppr/commit/1a4c87ceeceaabbe3ecf13e905fdd9a08866e647))
* fail on no change, improved status messages ([fa610d0](https://gitlab.com/hoppr/hoppr/commit/fa610d09a73bf6736e9792096614e08a84a6c8aa))
* fail on no change, improved status messages ([69d15b1](https://gitlab.com/hoppr/hoppr/commit/69d15b1c4e30684fe59020b96b002a7f1d27c30c))
* fast forward branch ([1428ecb](https://gitlab.com/hoppr/hoppr/commit/1428ecb2ff108f25ca0c424f53984771a16e8e8e))
* Fixed Docker repo:tag information being lost in collection ([5111f7f](https://gitlab.com/hoppr/hoppr/commit/5111f7fe86bd2c833ac4c0bb0392932b599f6549))
* Fixed Docker repo:tag information being lost in collection ([3319a36](https://gitlab.com/hoppr/hoppr/commit/3319a36bd90ca7798141ee0e7fe92bb6c361559d))
* fixed issue with bundle options for functionary_key ([d798606](https://gitlab.com/hoppr/hoppr/commit/d7986062051b5b0be426dda0842acb1bab601ec2))
* Fixed the releaseing issue ([ce8b2f5](https://gitlab.com/hoppr/hoppr/commit/ce8b2f57703bfaf0984e2b750005f866a8ad8b35))
* Fixed type-check findings ([7bb21df](https://gitlab.com/hoppr/hoppr/commit/7bb21df1b19f3f2ed8f275db995c722db9e78ed0))
* Fixed unit tests ([4e80266](https://gitlab.com/hoppr/hoppr/commit/4e8026652458dc5c35bdc3dfc5a983727e5bf44f))
* Fixes [#150](https://gitlab.com/hoppr/hoppr/issues/150) and corrected when consolidated and deliveried SBOMs are written. It also address additional unit testing ([5adf219](https://gitlab.com/hoppr/hoppr/commit/5adf2193b30af0b6aba0c901dd369df324d53e19))
* Fixing Merge Conflicts ([d24cff2](https://gitlab.com/hoppr/hoppr/commit/d24cff258f10542bdc9b1af462c3a61bc1af2722))
* generate dev version if not on main/dev ([3aa6ec5](https://gitlab.com/hoppr/hoppr/commit/3aa6ec50f186ee520eb1279160c8b1a0715711b2))
* Get better results ([688e176](https://gitlab.com/hoppr/hoppr/commit/688e1766cffb34cc155e0ea0c3f1d7a608c58947))
* gitattributes ([5d3bde3](https://gitlab.com/hoppr/hoppr/commit/5d3bde370513376a36b9a0cc7279b9464fd8a09a))
* Grab versioning properly ([cc052cf](https://gitlab.com/hoppr/hoppr/commit/cc052cfcedbd149b295e090c493adb650b47f1ce))
* have nexus collector respect user-specified purl types ([9116a03](https://gitlab.com/hoppr/hoppr/commit/9116a034125ccbe2eb47d06e0172176e3eae2d86))
* helm collector append purl name ([5574739](https://gitlab.com/hoppr/hoppr/commit/5574739d2e34ba0a538ee8295939e2ad473144f5))
* hoppr group ([c21bc29](https://gitlab.com/hoppr/hoppr/commit/c21bc295fb9ab40756e107b5047e2cb3d36d8a06))
* image tag reset after rebase ([45a850a](https://gitlab.com/hoppr/hoppr/commit/45a850a4c43f29c7253c4c97620c1fc6cce32fb5))
* import source type error ([464211c](https://gitlab.com/hoppr/hoppr/commit/464211cddc9f7ea6c7a5182449155e4c7467289c))
* Improve config error logic in git collector ([cb3681a](https://gitlab.com/hoppr/hoppr/commit/cb3681a6f95c8cc031ca69276f84015925659296))
* Improve matching pattern and logging ([12a68eb](https://gitlab.com/hoppr/hoppr/commit/12a68eb3ab2e049260bba97ddf8f7c0fc585a45d))
* include resources folder in poetry build ([5ae0ffb](https://gitlab.com/hoppr/hoppr/commit/5ae0ffb5d761acd41c7fafd6a0cfa43f40d7eafd))
* included manifest repo merge, add tests ([359364c](https://gitlab.com/hoppr/hoppr/commit/359364c453f72cc35cafd752b0b21a7aac79e0c3))
* included manifest repo merge, add tests ([1c22c6e](https://gitlab.com/hoppr/hoppr/commit/1c22c6eec5ed7c3c1dc01e3d70e6241f0da7f7ee))
* increase minimum unit test coverage to 100% ([9c697e3](https://gitlab.com/hoppr/hoppr/commit/9c697e35712ee8e8138848f506de71ff7054d71b))
* lint error ([990ef3f](https://gitlab.com/hoppr/hoppr/commit/990ef3ff6d79359c45481f0deeb640a4ec629ccb))
* lint error ([f9f2cc0](https://gitlab.com/hoppr/hoppr/commit/f9f2cc0095060a84642717a0d77e28d26d97467b))
* linting union for type hints ([bbaf8f0](https://gitlab.com/hoppr/hoppr/commit/bbaf8f0f688c30d206bc1842cd19b616bda0b949))
* main module exits using sys instead of typer ([356112e](https://gitlab.com/hoppr/hoppr/commit/356112e622d329d53a0902532476957f92790491))
* main process logfile lock ([4519f53](https://gitlab.com/hoppr/hoppr/commit/4519f53808064a9829f46bfec19f06156966a59c))
* Make a quicklink script for linking python in dockerfile ([ae3dace](https://gitlab.com/hoppr/hoppr/commit/ae3dace2b4101bc9c73381dcf097baf505965e25))
* Make semantic release pass all jobs ([ddc3c49](https://gitlab.com/hoppr/hoppr/commit/ddc3c495048e8be5302bf9c7ba7a4ad5418944a7))
* manifest helm repo URL ([438fae5](https://gitlab.com/hoppr/hoppr/commit/438fae572062f9726c451031a88e403a67390c99))
* manual version revs because lock file conflicts spam ([e8a473f](https://gitlab.com/hoppr/hoppr/commit/e8a473ffd01938734a7a84180e78d075927240c4))
* merge components, add verification tests ([f0ecf1e](https://gitlab.com/hoppr/hoppr/commit/f0ecf1e7f7471b17dbbf42e6aa80e8dedc46f76b))
* merge components, add verification tests ([ce6e4d1](https://gitlab.com/hoppr/hoppr/commit/ce6e4d17cd8ce4d1127956d1f72b2f346ee7402a))
* Merge dev into next ([765d25a](https://gitlab.com/hoppr/hoppr/commit/765d25a29cc9d3fed6794a01d25892118408e177))
* Merge main into branch ([e2fd972](https://gitlab.com/hoppr/hoppr/commit/e2fd97246baadff00a84b1fecb0b8a79412e70ee))
* **Minor:** Image build cleanup ([ba4e6c3](https://gitlab.com/hoppr/hoppr/commit/ba4e6c3bc68fe469b3ad68ed636f43b55c84f425))
* missing `rev` command in skopeo image ([cb38a3e](https://gitlab.com/hoppr/hoppr/commit/cb38a3e8ae5dc2afcac4bdbb87f110d6af58997e))
* missing collector plugin CLI tools ([b1284ba](https://gitlab.com/hoppr/hoppr/commit/b1284ba6c1df27487e36279c5af5478b27450322))
* move insertion of required stages to Transfer object creation ([fa0d31b](https://gitlab.com/hoppr/hoppr/commit/fa0d31b99fcc55411cc2fd32bcfb7f050b14fba9))
* move insertion of required stages to Transfer object creation ([a7da1d5](https://gitlab.com/hoppr/hoppr/commit/a7da1d53253de835a764f3d2ab68f052faa03aa7))
* move insertion of required stages to Transfer object creation ([57604d5](https://gitlab.com/hoppr/hoppr/commit/57604d5e724fadcd71694b466a82b19a9696d624))
* move insertion of required stages to Transfer object creation ([53243ab](https://gitlab.com/hoppr/hoppr/commit/53243abcb44c5c031af5c1cc625b8cf81e2b3954))
* moved test_main.py under test/unit/ directory ([90df7ad](https://gitlab.com/hoppr/hoppr/commit/90df7adb7fd293b7b3da96ac70ca159fe0d8f480))
* mypy errors ([b08426c](https://gitlab.com/hoppr/hoppr/commit/b08426ca5b7af32c8d192d2309381b19d03d136d))
* mypy errors ([d94d090](https://gitlab.com/hoppr/hoppr/commit/d94d090f231edaa21de806bc8ada8743326c57c7))
* mypy errors ([d612474](https://gitlab.com/hoppr/hoppr/commit/d612474d5f71b79683280781ca3cda18ee05d240))
* New branch old issue ([a906ca3](https://gitlab.com/hoppr/hoppr/commit/a906ca3cc6afc01f97483e136f9b3370312bb5bb))
* Only attempt PyPI source collect if whl not collected ([31de8ae](https://gitlab.com/hoppr/hoppr/commit/31de8ae07d414c09295db59662442ee7a15f92cc))
* only load purl-type-specific plugins when components of that type are being processed (Issue [#77](https://gitlab.com/hoppr/hoppr/issues/77)) ([1789f21](https://gitlab.com/hoppr/hoppr/commit/1789f21df784738c58b8ea3dac19e50240a2b13b))
* Only publish if there's a new release from semver dryrun ([da88e9b](https://gitlab.com/hoppr/hoppr/commit/da88e9b4a2e6449ea5550f8adcffceb975756af4))
* only rename maven file on successful collection ([3455e74](https://gitlab.com/hoppr/hoppr/commit/3455e743e92fa74ed1d8f82e66921dd41c837c63))
* Only run semantic release publish on develop and main ([0ecc904](https://gitlab.com/hoppr/hoppr/commit/0ecc904fb1583a9f530d4a469a331391007e70d1))
* parameter name typo ([9d7c3b1](https://gitlab.com/hoppr/hoppr/commit/9d7c3b1c0751816f5c108f3fb83eaec10f47a490))
* pip arguments ([0cc47f1](https://gitlab.com/hoppr/hoppr/commit/0cc47f17f3eefcd4a650d213d7d8701c59485c8b))
* platform check ([de541db](https://gitlab.com/hoppr/hoppr/commit/de541db0bf4a810a158ca175b1c38a573a1ab9e7))
* **plugin:** add type hint to auth ([0bc2e29](https://gitlab.com/hoppr/hoppr/commit/0bc2e29c0e36d0b596cd1d5f493a14566e9bd9dc))
* **plugin:** properly authenticate nexus requests ([be82ed4](https://gitlab.com/hoppr/hoppr/commit/be82ed43660ca25ed2f105a43dbd1603deb4df38))
* prepend stage if needed for deltas ([a0dbfcb](https://gitlab.com/hoppr/hoppr/commit/a0dbfcbb40649182f1fef9ce8cdb1ae5bf151c73))
* prepend stage if needed for deltas ([7bbb2b5](https://gitlab.com/hoppr/hoppr/commit/7bbb2b5d76d6245430e5e587e8e15b64f448c2d5))
* prepend stage if needed for deltas ([e93d3a0](https://gitlab.com/hoppr/hoppr/commit/e93d3a0ff654fe4aae50a96003682dcd6a7d4f74))
* prepend stage if needed for deltas ([2467c6b](https://gitlab.com/hoppr/hoppr/commit/2467c6b693832775d4739d0de41d7fc2059b5296))
* prevent loading plugins that aren't needed ([17722f3](https://gitlab.com/hoppr/hoppr/commit/17722f3238b9c5436549e3c1fbd9e7fb84f49dc3))
* prevent loading plugins that aren't needed ([b00db2b](https://gitlab.com/hoppr/hoppr/commit/b00db2b5925915fa950e14ea565e4496c4bf4aa9))
* processor relative file handling ([59636aa](https://gitlab.com/hoppr/hoppr/commit/59636aaeb06ff4bddd167fdc302b914ca3342cd5))
* processor relative file handling ([5c3cdb1](https://gitlab.com/hoppr/hoppr/commit/5c3cdb18abede20e59e52afdcaeec1de69a8298c))
* proper attestations for nexus-search- and composite- collectors ([a1a9651](https://gitlab.com/hoppr/hoppr/commit/a1a96517eebc432d83153cf4808cc17d40f17f81))
* proper attestations for nexus-search- and composite- collectors ([7ae7d7a](https://gitlab.com/hoppr/hoppr/commit/7ae7d7ae82558a5586740a4dd50a700a6a6e82b6))
* Protect keys ([9001a21](https://gitlab.com/hoppr/hoppr/commit/9001a219a93817d064a5cce207c267347b112549))
* provide better error message on empty config file content ([e87fff8](https://gitlab.com/hoppr/hoppr/commit/e87fff885842320adf85e7c86abb319e8dfa4f2d))
* pylint error ([6982109](https://gitlab.com/hoppr/hoppr/commit/69821095b2319075c65433a1f76e5d4e47fd361b))
* pylint error ([e36b242](https://gitlab.com/hoppr/hoppr/commit/e36b2424739e0dc4814855fb304dca1edd24e448))
* pylint issues fix ([7c7810c](https://gitlab.com/hoppr/hoppr/commit/7c7810c9ac63268472c2faf50e43694bdcd2cccb))
* raw collector stripping purl namespace ([1aca256](https://gitlab.com/hoppr/hoppr/commit/1aca256351c854eaa1c7f02e209961a20ece99aa))
* raw collector stripping purl namespace ([ee20ebe](https://gitlab.com/hoppr/hoppr/commit/ee20ebe02b6c1c7f1b1be9a44b82087565a5a5af))
* raw collector stripping purl namespace ([bba65cb](https://gitlab.com/hoppr/hoppr/commit/bba65cb7805b78a329ee59df1c10b9ad54959f8d))
* raw collector stripping purl namespace ([2c9309d](https://gitlab.com/hoppr/hoppr/commit/2c9309d09150511489185b455cf38d631611820b))
* README feedback; broken link fixes; try PyPI banner fix ([932121d](https://gitlab.com/hoppr/hoppr/commit/932121d087bbd2019007adb576b6c9122d4881fa))
* Rebase dev ([9399451](https://gitlab.com/hoppr/hoppr/commit/9399451c5f23b599d6d54c02538ebe4017a95620))
* redeclare Component attrs with hashable types ([a2034fa](https://gitlab.com/hoppr/hoppr/commit/a2034fa8ffd64f58b832199d0964d43893401962))
* redeclare Component attrs with hashable types ([26bbfc0](https://gitlab.com/hoppr/hoppr/commit/26bbfc064c252ce8b7e65205fe7d0c98987a5a41))
* Reference main so that develop can become default branch ([76f707b](https://gitlab.com/hoppr/hoppr/commit/76f707b6c3435bdc68d73b3717c451c6a6c95369))
* Release dev channel ([42ee862](https://gitlab.com/hoppr/hoppr/commit/42ee86236810072b2d303ea255f2d2556fd53749))
* Release dev channel ([15b4ae7](https://gitlab.com/hoppr/hoppr/commit/15b4ae76944accd1b4f0e6ae379b02c5efabb923))
* Remove blank lines ([196218c](https://gitlab.com/hoppr/hoppr/commit/196218c39d7dbb50c74722922190f61905f46053))
* remove construct method call ([2cf4963](https://gitlab.com/hoppr/hoppr/commit/2cf49637902c87dd786f37485521e8c55617685f))
* Remove gitlab semantic release comment on MRs ([4af82e6](https://gitlab.com/hoppr/hoppr/commit/4af82e6023f842a8587ec5183c5b3a212dc9e5f5))
* Remove licensing scanning and replace with policy ([e852316](https://gitlab.com/hoppr/hoppr/commit/e85231642c2f75a022d3872049b26e349821cc13))
* remove need for boolean from fail-open logic ([23bfa03](https://gitlab.com/hoppr/hoppr/commit/23bfa0397d12d39fcbca526eaadd651805e97c57))
* Remove node engine reference from package.json ([3c26b4e](https://gitlab.com/hoppr/hoppr/commit/3c26b4e768f23e9746db9f4b654a4799cb95e00b))
* Remove pack, and idx from check ([6e24462](https://gitlab.com/hoppr/hoppr/commit/6e244629bf39228cee030550da47863b11c6aec5))
* Remove package.json ([e13de8d](https://gitlab.com/hoppr/hoppr/commit/e13de8d885045b1e750fceeace1e60d4d9d032b6))
* remove prerelease ([1e038d0](https://gitlab.com/hoppr/hoppr/commit/1e038d0b73fe23da781bdea4cb02b9241e19aa8e))
* remove prerelease ([6818939](https://gitlab.com/hoppr/hoppr/commit/6818939d8237876f7dfabdce72a0dab1270db9b3))
* remove problematic git files from tar toc comparison ([b883b23](https://gitlab.com/hoppr/hoppr/commit/b883b23fbf230857a6244b7b8207144e3cc3a920))
* remove problematic git files from tar toc comparison ([89f1621](https://gitlab.com/hoppr/hoppr/commit/89f16211e602d92820cad9719257a57d2bfbcb39))
* Remove quotations around build ([3848f46](https://gitlab.com/hoppr/hoppr/commit/3848f463bb8640e4acd524bfdd04838291e9222d))
* Remove quotations from parallel matrix ([28ee2b7](https://gitlab.com/hoppr/hoppr/commit/28ee2b707fa0f36992f131e7bbb548f164b23e14))
* Remove quotes ([7323a8b](https://gitlab.com/hoppr/hoppr/commit/7323a8b16abc50d284a7d67bf7a346fe0190369c))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([0ac8adc](https://gitlab.com/hoppr/hoppr/commit/0ac8adc4c70bf79e5068daf8f8d5501c045af29a))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([87ae120](https://gitlab.com/hoppr/hoppr/commit/87ae1209adced6c7004143d33e314df719740da1))
* remove references to deleted job ([6d9890e](https://gitlab.com/hoppr/hoppr/commit/6d9890e99f43b439f20505142e2706c1556d5abf))
* remove references to deleted job ([7d1ef14](https://gitlab.com/hoppr/hoppr/commit/7d1ef14d1bb86f478ad35b5c08038b08b1fc916c))
* Remove skip ci and attempt to let workflow rules handle pipelines ([0e325a5](https://gitlab.com/hoppr/hoppr/commit/0e325a5e7a2caea831856e0ba6566f0a5ecaf2c7))
* restore Dockerfile FROM directive ([e7e0622](https://gitlab.com/hoppr/hoppr/commit/e7e06222a9768bfeaab9f10e13a9b213ccd45be7))
* Result constructor param typo ([09290d2](https://gitlab.com/hoppr/hoppr/commit/09290d25bd2be5f07635c718c7691dad6e00e652))
* Revert Changes ([f6797ea](https://gitlab.com/hoppr/hoppr/commit/f6797ea323e79a9bcd8b67b00c4939004e0dafcc))
* revert to older version of npm-check-updates ([9fb3ebf](https://gitlab.com/hoppr/hoppr/commit/9fb3ebf011be353fe119759514868151e0ddf3c2))
* Roll back chore rules ([e40fbfb](https://gitlab.com/hoppr/hoppr/commit/e40fbfbcb463ccc704c351b1acad04f3ea1fb0ea))
* Roll base image back from 9.0 to 8.6 ([31f8166](https://gitlab.com/hoppr/hoppr/commit/31f81666e3bb135ad7ad7b05d28cbc96c77859bc))
* Run if commit message doesn't start with chore ([daa5d11](https://gitlab.com/hoppr/hoppr/commit/daa5d119686ca58488cc04c62d48b73bb9c49319))
* Run pre-commit ([7b740ec](https://gitlab.com/hoppr/hoppr/commit/7b740eca0a6f3bd92d99e7c8f29c3fd435f0df4d))
* Run the entire dockerfile as one large image ([47ec5a5](https://gitlab.com/hoppr/hoppr/commit/47ec5a57dece0be112dca8c068a2807edc811bf8))
* sbom metadata ([eb5225f](https://gitlab.com/hoppr/hoppr/commit/eb5225f8f0729e44701d6b75ce7f74870f7eecbb))
* sbom metadata ([f97e7a7](https://gitlab.com/hoppr/hoppr/commit/f97e7a7acb00e9d361887dfad55c7bc2f03b10f0))
* SBOM spec selection logic ([d12bd8b](https://gitlab.com/hoppr/hoppr/commit/d12bd8ba19c0b5b8ec879d1f8190b8afb999d80b))
* SBOM spec selection logic ([2cd7286](https://gitlab.com/hoppr/hoppr/commit/2cd72866b1ae7a1fda1b580a4f7dbc2942a0da49))
* semantic-release version bump ([d45efb7](https://gitlab.com/hoppr/hoppr/commit/d45efb73d70625bda73ca4c4dfec33698c62957f))
* set file name correctly for collect_maven_plugin ([07e4eb5](https://gitlab.com/hoppr/hoppr/commit/07e4eb55388b25f46fdd2ed7a0af04fa86a8e427))
* set prerelease back to true ([49b130e](https://gitlab.com/hoppr/hoppr/commit/49b130e916dc7b3f9ec9fdaead06ce29e78f32c8))
* Set the correct version of oras ([d8357a4](https://gitlab.com/hoppr/hoppr/commit/d8357a431e33ec0fb45e0fc51537eae603e7cb6a))
* set timeout for run_command to reduce hanging ([9be8afe](https://gitlab.com/hoppr/hoppr/commit/9be8afefc62f1868d908cf8ecb8e9bd211662c37))
* set timetag to utc timezone ([12aa368](https://gitlab.com/hoppr/hoppr/commit/12aa368689a4225fc9662f59624ee90049fb8c4f))
* Setup codequality analytics and build attestation ([c5c4514](https://gitlab.com/hoppr/hoppr/commit/c5c4514e153becdf4a39713259b289c4469a24eb))
* Setup dependencies on test image ([6fabada](https://gitlab.com/hoppr/hoppr/commit/6fabada652d546cd89f1cd16eb32bae2cd0cb08a))
* shared RLock for Docker unit tests ([b2f3c9c](https://gitlab.com/hoppr/hoppr/commit/b2f3c9c5eb31bd31e46940fdf453ff56ae0f54f5))
* Simlink python ([0009483](https://gitlab.com/hoppr/hoppr/commit/0009483d56965e8770ae67383ecc69d4e7a53697))
* Simplify some of the configuration options ([a52594a](https://gitlab.com/hoppr/hoppr/commit/a52594a9e0873392934d6bfdb442ee3633f58a00))
* speed up unit tests ([32f8715](https://gitlab.com/hoppr/hoppr/commit/32f87159f4fc125a338e61fadefb80f5cb306ccb))
* Split out ci config per !184#note_1170995317 ([f42e582](https://gitlab.com/hoppr/hoppr/commit/f42e5825d7a3ebeab60ae107b10624d5567c7173)), closes [184#note_1170995317](https://gitlab.com/hoppr/184/issues/note_1170995317)
* stage name regex field type ([3d4c133](https://gitlab.com/hoppr/hoppr/commit/3d4c13381a43ddbdcbe4629a22ef0f75b7019035))
* strip quotes to compare found URL ([396c31a](https://gitlab.com/hoppr/hoppr/commit/396c31a90846673f991867a2c2057959457222f0))
* syntax ([88b8961](https://gitlab.com/hoppr/hoppr/commit/88b8961dcc41c6a21e02da417a3c059cf0036641))
* syntax ([4e8fc18](https://gitlab.com/hoppr/hoppr/commit/4e8fc18f3bc21f00d60e9a190e94fc7a4a9d9078))
* syntax ([84c2c38](https://gitlab.com/hoppr/hoppr/commit/84c2c3888485f188af9202b9fc8c1d20fc243331))
* syntax ([762a702](https://gitlab.com/hoppr/hoppr/commit/762a702417b358c3fd0b568c631bbd0c9a5a5aef))
* temporary peg of securesystemslib to address in-toto bug ([8261f41](https://gitlab.com/hoppr/hoppr/commit/8261f414b9951ede1e5a7efc44dfa429d6cf5790))
* test image tags ([0180488](https://gitlab.com/hoppr/hoppr/commit/01804886f7a709e105e432d53d96c7ca19c67a80))
* test pattern matching ([c6e7c51](https://gitlab.com/hoppr/hoppr/commit/c6e7c5197847e13f54a14cf112ca58d5cad86602))
* **test:** credentials unit test coverage ([74b981f](https://gitlab.com/hoppr/hoppr/commit/74b981f51e58adbd5581e815f239f97e54003ed0))
* transfer file default value ([ac1bf1a](https://gitlab.com/hoppr/hoppr/commit/ac1bf1a775d534bd89217443c1711bebd49e957b))
* turn off allow-failure on semantic-release:dry-run job to avoid breaking changes from renovate ([3a32a65](https://gitlab.com/hoppr/hoppr/commit/3a32a656abdb3c2f7cb006547a353f3fdae615b5))
* type check errors ([02b3d27](https://gitlab.com/hoppr/hoppr/commit/02b3d2713ccd8169643121abfb5ec206d01bbe4b))
* type error ([25aea9c](https://gitlab.com/hoppr/hoppr/commit/25aea9cb031b72a620c04029fce89872e0a9f3cd))
* type errors ([30861b6](https://gitlab.com/hoppr/hoppr/commit/30861b6abc6810f234c85a9de1d6db64c826430b))
* unit test context missing delivered sbom ([0c3d27a](https://gitlab.com/hoppr/hoppr/commit/0c3d27ae52d28492b3080249430099a6cd3cd93f))
* unit test message ([81415ed](https://gitlab.com/hoppr/hoppr/commit/81415edb08a9a6f925a911939095a5c001107531))
* unit test message ([dc95a02](https://gitlab.com/hoppr/hoppr/commit/dc95a025e8d295c4b0572ba105ede3b69f8a4738))
* unit test message ([69a164f](https://gitlab.com/hoppr/hoppr/commit/69a164f500097c97b29cf408a9ca695d6a3505e3))
* unit test message ([5846f61](https://gitlab.com/hoppr/hoppr/commit/5846f6109f3e5adf55ec6bfa4596e95f002e1ead))
* Update artifact name ([4f64b03](https://gitlab.com/hoppr/hoppr/commit/4f64b03b8a36590887e8430aa5caf3a48e25cd7a))
* update assert ([eba7ec9](https://gitlab.com/hoppr/hoppr/commit/eba7ec90d1927369be2bb91115388ec3fab0ec58))
* update CODEOWNERS for repo move ([6541b0c](https://gitlab.com/hoppr/hoppr/commit/6541b0c6ce79ed1dc0e4156d2e39a2620ee4b0c4))
* update delivered_sbom with process return objects ([46da6f3](https://gitlab.com/hoppr/hoppr/commit/46da6f324eca0ff4bda4077f385ff2ad20e87177))
* Update expected toc (since it's changed), and correct the regression boms ([fe79087](https://gitlab.com/hoppr/hoppr/commit/fe79087aeef00eab3792de8c5d3ea343ab9ce4d3))
* update expected toc's with delta stage intermediate delivered bom ([f016526](https://gitlab.com/hoppr/hoppr/commit/f0165265077c35e578b62d252c384594732e0d3c))
* update expected toc's with delta stage intermediate delivered bom ([3291773](https://gitlab.com/hoppr/hoppr/commit/32917735d1e84afa74aa584c83f899206bb3cfea))
* update expected_tar_toc files for changed maven file names ([b1ef013](https://gitlab.com/hoppr/hoppr/commit/b1ef01389841407c0723a2ced6bc397709c2a24d))
* Update git collector depth defaults per MR suggestion ([43a3dbb](https://gitlab.com/hoppr/hoppr/commit/43a3dbb296db4a43f2a70f9eb87a6d56ad3ff510))
* Update maven write to disk ([82d42b6](https://gitlab.com/hoppr/hoppr/commit/82d42b6dbce5eec0868199da0c1f4c32c9a99d61))
* update Result object to include optional return object.  Check that return object matches plugin Bom Access value ([b346872](https://gitlab.com/hoppr/hoppr/commit/b3468720b276af24d95c0e748ecb3701a4fd0764))
* Update variables to contain artifact name ([0400e28](https://gitlab.com/hoppr/hoppr/commit/0400e280e42b9eec5282bb7d59fab3ae100a1872))
* updated expected tar for pypi ([7356d6b](https://gitlab.com/hoppr/hoppr/commit/7356d6b6c82ee1d54dcd22158451664dd18c7779))
* Updated unit tests for docker collection ([7dbd2af](https://gitlab.com/hoppr/hoppr/commit/7dbd2af98c871d8cfdc20b48f0164fc8cb861ff0))
* updates to ensure pipeline works for forks. ([5fa6fe9](https://gitlab.com/hoppr/hoppr/commit/5fa6fe98921248797dc75375d500be9ab2f37381))
* urljoin stripping path components ([4c6ad9f](https://gitlab.com/hoppr/hoppr/commit/4c6ad9f3de6d2351b4eb274b884062d1b0c6e841))
* use enums for constants, per code review ([40cc853](https://gitlab.com/hoppr/hoppr/commit/40cc8538d5921846202167e7f0089a18d1719137))
* Use list of options and check for troublesome values ([fb39372](https://gitlab.com/hoppr/hoppr/commit/fb39372b37ee1bc5eec7ac49e618a521d38b5be3))
* Use long switches for readability. ([fdbfec2](https://gitlab.com/hoppr/hoppr/commit/fdbfec26f6f528ce1c79bff518b4f61f2708db5d))
* use more stable package for apt testing ([f365df7](https://gitlab.com/hoppr/hoppr/commit/f365df765c2bd162a9cf6c8120c720a6897fbc23))
* use more stable package for apt testing ([1aca9d7](https://gitlab.com/hoppr/hoppr/commit/1aca9d7c5d751785f8aba52bbd2cb63319fd8583))
* Use python3 for virtual environment creation in CI Dockerfile ([e39ea91](https://gitlab.com/hoppr/hoppr/commit/e39ea91f9456b7f006ef6dea5f57579376829776))
* Use semantic release to publish package ([27e8eb0](https://gitlab.com/hoppr/hoppr/commit/27e8eb02b8bd3405cec90b5d3112662dc2675c17))
* Using copy from Maven-Dependency-Plugin instead of get ([7ab2fe1](https://gitlab.com/hoppr/hoppr/commit/7ab2fe15234b1bda27f91bb0ea6b025cde2c22d2))
* windows ANSI processing ([eba2cb4](https://gitlab.com/hoppr/hoppr/commit/eba2cb4a18d7bb375e9984c838772506d6c40bb7))
* wrapper for run cmd ([9d5844b](https://gitlab.com/hoppr/hoppr/commit/9d5844b8f0661b3be8ef63d120bf5d96023fe844))


### Reverts

* files not relevant to this branch ([fe61dea](https://gitlab.com/hoppr/hoppr/commit/fe61deaf45ff0fd0e0e5252ac783f751ee6ddedf))
* files not relevant to this branch ([c66595a](https://gitlab.com/hoppr/hoppr/commit/c66595a47a8d089b98566940eb4ae565d094c5da))
* generic types ([96721e4](https://gitlab.com/hoppr/hoppr/commit/96721e42511934c36bbb9014da20d24a1b2b9546))
* generic types ([9915c1a](https://gitlab.com/hoppr/hoppr/commit/9915c1aa2bb39bbccdc959b2584f56954698d84c))
* method_name condition logic ([6d9a16b](https://gitlab.com/hoppr/hoppr/commit/6d9a16bc87de3e8cce5a835093076ea76242b627))
* method_name condition logic ([6445f18](https://gitlab.com/hoppr/hoppr/commit/6445f18cb62e61bb2b8738bd46dbd75df766e961))
* model integration changes ([e27eb91](https://gitlab.com/hoppr/hoppr/commit/e27eb91fcc8a8b5cc2e0c6f110c6de1e37010f82))
* shared memory manager changes ([09a6ee3](https://gitlab.com/hoppr/hoppr/commit/09a6ee348e8820fc91d5ac5ad2016f8e96aa041e))
* shared memory manager changes ([4dbd8c0](https://gitlab.com/hoppr/hoppr/commit/4dbd8c0fc99b7e2d52b717bc555b326962a27467))

## [1.8.0](https://gitlab.com/hoppr/hoppr/compare/v1.7.1...v1.8.0) (2023-02-09)


### Features

* add delta_sbom capability ([c73e081](https://gitlab.com/hoppr/hoppr/commit/c73e0816444cefa4547917da54dc650e7777d7da))
* add delta_sbom capability ([5bb5a59](https://gitlab.com/hoppr/hoppr/commit/5bb5a599d0c111399101f5e676f20e19c1954022))


### Bug Fixes


* add command line option to override previous collection location in delta_sbom plugin ([b3eb0bd](https://gitlab.com/hoppr/hoppr/commit/b3eb0bd1b1b40c936d34c7075fcf525ddf2e76c7))
* add command line option to override previous collection location in delta_sbom plugin ([d818730](https://gitlab.com/hoppr/hoppr/commit/d818730a0b6c4d930cdb220f31c2555a21aff42a))
* add expected-tar-toc to delta integration test ([40fdb97](https://gitlab.com/hoppr/hoppr/commit/40fdb9778b2b20c0b07aa475e5de931a6e4cf227))
* add integration test for deltas ([6789f9c](https://gitlab.com/hoppr/hoppr/commit/6789f9c240a110f10e717dc42c3cff40ba4352e6))
* applying MR suggestion ([c3adeee](https://gitlab.com/hoppr/hoppr/commit/c3adeee1813eba8dd308fec358d27af9f25c1f1d))
* applying MR suggestion ([4e61f84](https://gitlab.com/hoppr/hoppr/commit/4e61f84272b14180e16f6a4baa424f5d60eb2752))
* clean up repository_url handling ([c867a3b](https://gitlab.com/hoppr/hoppr/commit/c867a3bda17c70e59e1d50672543077b7daae8cb))
* clean up repository_url handling ([95a0288](https://gitlab.com/hoppr/hoppr/commit/95a0288eeecd7c0baf03af5b67dd0d50fb549708))
* Correct deployment teir ([d4282c8](https://gitlab.com/hoppr/hoppr/commit/d4282c8c1e54fe188acfde63a9307c33474c9f8b))
* Cut release from next branch ([17d0d03](https://gitlab.com/hoppr/hoppr/commit/17d0d03ac243fea51d9ea98738d0e8293c60803d))
* fail on no change, improved status messages ([801dea2](https://gitlab.com/hoppr/hoppr/commit/801dea20d82167b303ebbd0a339cb8e408c9b873))
* fail on no change, improved status messages ([fa610d0](https://gitlab.com/hoppr/hoppr/commit/fa610d09a73bf6736e9792096614e08a84a6c8aa))
* move insertion of required stages to Transfer object creation ([fa0d31b](https://gitlab.com/hoppr/hoppr/commit/fa0d31b99fcc55411cc2fd32bcfb7f050b14fba9))
* move insertion of required stages to Transfer object creation ([57604d5](https://gitlab.com/hoppr/hoppr/commit/57604d5e724fadcd71694b466a82b19a9696d624))
* prepend stage if needed for deltas ([a0dbfcb](https://gitlab.com/hoppr/hoppr/commit/a0dbfcbb40649182f1fef9ce8cdb1ae5bf151c73))
* prepend stage if needed for deltas ([e93d3a0](https://gitlab.com/hoppr/hoppr/commit/e93d3a0ff654fe4aae50a96003682dcd6a7d4f74))
* unit test message ([81415ed](https://gitlab.com/hoppr/hoppr/commit/81415edb08a9a6f925a911939095a5c001107531))
* unit test message ([69a164f](https://gitlab.com/hoppr/hoppr/commit/69a164f500097c97b29cf408a9ca695d6a3505e3))
* update expected toc's with delta stage intermediate delivered bom ([f016526](https://gitlab.com/hoppr/hoppr/commit/f0165265077c35e578b62d252c384594732e0d3c))

## [1.7.2](https://gitlab.com/hoppr/hoppr/compare/v1.7.1...v1.7.2) (2023-02-20)


### Bug Fixes

* add purl-type/repo-type mappings to nexus_search collector ([07d421d](https://gitlab.com/hoppr/hoppr/commit/07d421da5d03256ee0689840b54ebd03475b0a24))
* add repo to purl type list ([addf4cf](https://gitlab.com/hoppr/hoppr/commit/addf4cf531ad250c81c01294553593f4a95f0533))
* expected-tar-toc sort order changed ([2094c14](https://gitlab.com/hoppr/hoppr/commit/2094c14eacaba8891c35c325ea11ec321c5948fe))
* manual version revs because lock file conflicts spam ([3ac4810](https://gitlab.com/hoppr/hoppr/commit/3ac4810b2731e8afa15c98d54fb58ea987dae985))
* only rename maven file on successful collection ([cf2263e](https://gitlab.com/hoppr/hoppr/commit/cf2263e4446f04d9fe495cc1efca6efa68eefa43))
* set file name correctly for collect_maven_plugin ([bd3194b](https://gitlab.com/hoppr/hoppr/commit/bd3194b2a5bf12ce06a05a6f4fea2700b3756757))
* set timeout for run_command to reduce hanging ([c513c6c](https://gitlab.com/hoppr/hoppr/commit/c513c6cf19cdf4fca93be302320cd9a29ffa203a))
* update expected_tar_toc files for changed maven file names ([399a8d3](https://gitlab.com/hoppr/hoppr/commit/399a8d38f762c8f1cf704c0204c9e5a79a789a40))


### Bug Fixes

* add purl-type/repo-type mappings to nexus_search collector ([07d421d](https://gitlab.com/hoppr/hoppr/commit/07d421da5d03256ee0689840b54ebd03475b0a24))
* add repo to purl type list ([addf4cf](https://gitlab.com/hoppr/hoppr/commit/addf4cf531ad250c81c01294553593f4a95f0533))
* expected-tar-toc sort order changed ([2094c14](https://gitlab.com/hoppr/hoppr/commit/2094c14eacaba8891c35c325ea11ec321c5948fe))
* manual version revs because lock file conflicts spam ([3ac4810](https://gitlab.com/hoppr/hoppr/commit/3ac4810b2731e8afa15c98d54fb58ea987dae985))
* only rename maven file on successful collection ([cf2263e](https://gitlab.com/hoppr/hoppr/commit/cf2263e4446f04d9fe495cc1efca6efa68eefa43))
* set file name correctly for collect_maven_plugin ([bd3194b](https://gitlab.com/hoppr/hoppr/commit/bd3194b2a5bf12ce06a05a6f4fea2700b3756757))
* set timeout for run_command to reduce hanging ([c513c6c](https://gitlab.com/hoppr/hoppr/commit/c513c6cf19cdf4fca93be302320cd9a29ffa203a))
* update expected_tar_toc files for changed maven file names ([399a8d3](https://gitlab.com/hoppr/hoppr/commit/399a8d38f762c8f1cf704c0204c9e5a79a789a40))

## [1.7.1](https://gitlab.com/hoppr/hoppr/compare/v1.7.0...v1.7.1) (2023-02-08)

### Bug Fixes

* add expected-tar-toc for remaining integration tests ([de0bc7d](https://gitlab.com/hoppr/hoppr/commit/de0bc7d95c4115edb6d7fa66f6212cbd7c02b440))
* add expected-tar-toc for remaining integration tests ([eef848e](https://gitlab.com/hoppr/hoppr/commit/eef848e894065d338abdfe11089dbbdd33776fa9))
* Added user_env support to find_credentials ([5319664](https://gitlab.com/hoppr/hoppr/commit/531966423d0ef3409d224a720256327955a4d18b))
* Branch isolation testing ([ed09760](https://gitlab.com/hoppr/hoppr/commit/ed0976099be406f793c88035f65bab2bbd35125b))
* check for exception not thrown in pypi success ([d8df078](https://gitlab.com/hoppr/hoppr/commit/d8df0785d51115a7564382699bdf7d626e006c3a))
* check tar toc on integration tests ([85444e7](https://gitlab.com/hoppr/hoppr/commit/85444e7e4dfc8591bd849e40b24578984200f1a6))
* check tar toc on integration tests ([9e612a4](https://gitlab.com/hoppr/hoppr/commit/9e612a408e8e0ec631f7f399d1dffe03df44143c))
* Cleaned up maven command ([0042ccd](https://gitlab.com/hoppr/hoppr/commit/0042ccdc9658e46c98e706a9d6392777a289193f))
* Corrected Maven-Dependency-Plugin arguments so that maven artifacts would be bundled ([1c86a07](https://gitlab.com/hoppr/hoppr/commit/1c86a07a117539263a90242b5215b9fb7605b340))
* expected metadata source location ([1ffb443](https://gitlab.com/hoppr/hoppr/commit/1ffb4437a4cc1d74f560bd708759d92cdf0b4361))
* expected metadata source location ([efe120a](https://gitlab.com/hoppr/hoppr/commit/efe120a6d3319b6e1f97b672ed5beddb4b2e8147))
* Fixed Docker repo:tag information being lost in collection ([a8fc4a9](https://gitlab.com/hoppr/hoppr/commit/a8fc4a9333f410b98f63d561bd6abe091e77acb5))
* Fixed Docker repo:tag information being lost in collection ([75c283c](https://gitlab.com/hoppr/hoppr/commit/75c283cd5c6042eba8bdf9f60ba1520d444c037c))
* Fixed type-check findings ([66de3b8](https://gitlab.com/hoppr/hoppr/commit/66de3b8092a76a0185ff96535dbd0107a840f358))
* Fixed unit tests ([00207a8](https://gitlab.com/hoppr/hoppr/commit/00207a80d1164c1755d2b75eaa86fff96a8ad519))
* Fixing Merge Conflicts ([5feada8](https://gitlab.com/hoppr/hoppr/commit/5feada8398bdae635a4ae3c31fe4da1ee5cac7da))
* increase minimum unit test coverage to 100% ([abb04ee](https://gitlab.com/hoppr/hoppr/commit/abb04ee20070bf0b449cd42e6fa5924e2db57e6d))
* Only attempt PyPI source collect if whl not collected ([33e1571](https://gitlab.com/hoppr/hoppr/commit/33e1571fdf33424fde1c2b474556e2ce4cca799e))
* Rebase dev ([2bfb341](https://gitlab.com/hoppr/hoppr/commit/2bfb34185986c900d5d37b6a1c905773ff9247af))
* Remove gitlab semantic release comment on MRs ([37c5a6e](https://gitlab.com/hoppr/hoppr/commit/37c5a6ef48f53d979dd72dc5586a7c76eb1a4b57))
* remove need for boolean from fail-open logic ([20c8d5c](https://gitlab.com/hoppr/hoppr/commit/20c8d5c7fac3a68bc9c778c56d9a62afaedb7310))
* Remove node engine reference from package.json ([e1a9da1](https://gitlab.com/hoppr/hoppr/commit/e1a9da10853905befc2ffed1eae9c989a7c13cb2))
* Remove package.json ([4b162fd](https://gitlab.com/hoppr/hoppr/commit/4b162fd56970273c6c9cbbe045ca0ab17276302d))
* remove problematic git files from tar toc comparison ([7a4c0a1](https://gitlab.com/hoppr/hoppr/commit/7a4c0a15c4a6c72b5f95d5b9f4ab89e26018bf83))
* remove problematic git files from tar toc comparison ([b55df15](https://gitlab.com/hoppr/hoppr/commit/b55df1582c57b01f7b724c3bc3d259ae7f85d7a1))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([eae151b](https://gitlab.com/hoppr/hoppr/commit/eae151bfa1c51b55af55cbd8dd2a410856f628f8))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([0cb11ab](https://gitlab.com/hoppr/hoppr/commit/0cb11abd4804d8ba57a4d313c18c7b10dde205e2))
* remove references to deleted job ([7237e6a](https://gitlab.com/hoppr/hoppr/commit/7237e6a0c4bf6221c7ecce6af9b7fd9bc608a025))
* remove references to deleted job ([a79ceed](https://gitlab.com/hoppr/hoppr/commit/a79ceed9730d1a4620b036537f30c095f3d2ec7d))
* syntax ([47a47bc](https://gitlab.com/hoppr/hoppr/commit/47a47bc513a138c4ab2608785ddb2c9e10fb4161))
* syntax ([b1b4dc7](https://gitlab.com/hoppr/hoppr/commit/b1b4dc796ca585f62cd8cba7895a774e0f3b2c64))
* syntax ([7f7570d](https://gitlab.com/hoppr/hoppr/commit/7f7570d97f1fe2103fafa90e9d1d4125d9fe1091))
* syntax ([bbf66dc](https://gitlab.com/hoppr/hoppr/commit/bbf66dc613bc827e0053e13ca2883139a2d46b80))
* **test:** credentials unit test coverage ([e78aed7](https://gitlab.com/hoppr/hoppr/commit/e78aed70cc1abb061b4b21845f66a80cb4efa947))
* updated expected tar for pypi ([3827e5f](https://gitlab.com/hoppr/hoppr/commit/3827e5f998989d9b79177bc7043197da39e32a49))
* Updated unit tests for docker collection ([35bd32b](https://gitlab.com/hoppr/hoppr/commit/35bd32b07c1087f1027bc89208f0e657c744b304))
* Using copy from Maven-Dependency-Plugin instead of get ([eafc0bc](https://gitlab.com/hoppr/hoppr/commit/eafc0bc5d7379e0c04d259c54e28f05e7b5fa787))
* wrapper for run cmd ([a5945fc](https://gitlab.com/hoppr/hoppr/commit/a5945fc0ce4b47d39d330f6798e8886467caab94))

## [1.7.1-dev.7](https://gitlab.com/hoppr/hoppr/compare/v1.7.1-dev.6...v1.7.1-dev.7) (2023-02-20)


### Bug Fixes

* manual version revs because lock file conflicts spam ([3ac4810](https://gitlab.com/hoppr/hoppr/commit/3ac4810b2731e8afa15c98d54fb58ea987dae985))

## [1.7.1-dev.6](https://gitlab.com/hoppr/hoppr/compare/v1.7.1-dev.5...v1.7.1-dev.6) (2023-02-20)


### Bug Fixes

* add repo to purl type list ([addf4cf](https://gitlab.com/hoppr/hoppr/commit/addf4cf531ad250c81c01294553593f4a95f0533))

## [1.7.1-dev.5](https://gitlab.com/hoppr/hoppr/compare/v1.7.1-dev.4...v1.7.1-dev.5) (2023-02-16)


### Bug Fixes

* expected-tar-toc sort order changed ([2094c14](https://gitlab.com/hoppr/hoppr/commit/2094c14eacaba8891c35c325ea11ec321c5948fe))
* only rename maven file on successful collection ([cf2263e](https://gitlab.com/hoppr/hoppr/commit/cf2263e4446f04d9fe495cc1efca6efa68eefa43))
* set file name correctly for collect_maven_plugin ([bd3194b](https://gitlab.com/hoppr/hoppr/commit/bd3194b2a5bf12ce06a05a6f4fea2700b3756757))
* set timeout for run_command to reduce hanging ([c513c6c](https://gitlab.com/hoppr/hoppr/commit/c513c6cf19cdf4fca93be302320cd9a29ffa203a))
* update expected_tar_toc files for changed maven file names ([399a8d3](https://gitlab.com/hoppr/hoppr/commit/399a8d38f762c8f1cf704c0204c9e5a79a789a40))

## [1.7.1-dev.4](https://gitlab.com/hoppr/hoppr/compare/v1.7.1-dev.3...v1.7.1-dev.4) (2023-02-15)


### Bug Fixes

* add purl-type/repo-type mappings to nexus_search collector ([07d421d](https://gitlab.com/hoppr/hoppr/commit/07d421da5d03256ee0689840b54ebd03475b0a24))

## [1.7.1-dev.3](https://gitlab.com/hoppr/hoppr/compare/v1.7.1-dev.2...v1.7.1-dev.3) (2023-02-08)


### Bug Fixes

* Branch isolation testing ([ed09760](https://gitlab.com/hoppr/hoppr/commit/ed0976099be406f793c88035f65bab2bbd35125b))
* check for exception not thrown in pypi success ([d8df078](https://gitlab.com/hoppr/hoppr/commit/d8df0785d51115a7564382699bdf7d626e006c3a))
* Only attempt PyPI source collect if whl not collected ([33e1571](https://gitlab.com/hoppr/hoppr/commit/33e1571fdf33424fde1c2b474556e2ce4cca799e))
* remove need for boolean from fail-open logic ([20c8d5c](https://gitlab.com/hoppr/hoppr/commit/20c8d5c7fac3a68bc9c778c56d9a62afaedb7310))
* updated expected tar for pypi ([3827e5f](https://gitlab.com/hoppr/hoppr/commit/3827e5f998989d9b79177bc7043197da39e32a49))
* wrapper for run cmd ([a5945fc](https://gitlab.com/hoppr/hoppr/commit/a5945fc0ce4b47d39d330f6798e8886467caab94))

## [1.7.1-dev.2](https://gitlab.com/hoppr/hoppr/compare/v1.7.1-dev.1...v1.7.1-dev.2) (2023-02-08)


### Bug Fixes

* increase minimum unit test coverage to 100% ([abb04ee](https://gitlab.com/hoppr/hoppr/commit/abb04ee20070bf0b449cd42e6fa5924e2db57e6d))
* **test:** credentials unit test coverage ([e78aed7](https://gitlab.com/hoppr/hoppr/commit/e78aed70cc1abb061b4b21845f66a80cb4efa947))

## [1.7.1-dev.1](https://gitlab.com/hoppr/hoppr/compare/v1.7.0...v1.7.1-dev.1) (2023-02-08)


### Bug Fixes

* add expected-tar-toc for remaining integration tests ([de0bc7d](https://gitlab.com/hoppr/hoppr/commit/de0bc7d95c4115edb6d7fa66f6212cbd7c02b440))
* add expected-tar-toc for remaining integration tests ([eef848e](https://gitlab.com/hoppr/hoppr/commit/eef848e894065d338abdfe11089dbbdd33776fa9))
* Added user_env support to find_credentials ([5319664](https://gitlab.com/hoppr/hoppr/commit/531966423d0ef3409d224a720256327955a4d18b))
* check tar toc on integration tests ([85444e7](https://gitlab.com/hoppr/hoppr/commit/85444e7e4dfc8591bd849e40b24578984200f1a6))
* check tar toc on integration tests ([9e612a4](https://gitlab.com/hoppr/hoppr/commit/9e612a408e8e0ec631f7f399d1dffe03df44143c))
* Cleaned up maven command ([0042ccd](https://gitlab.com/hoppr/hoppr/commit/0042ccdc9658e46c98e706a9d6392777a289193f))
* Corrected Maven-Dependency-Plugin arguments so that maven artifacts would be bundled ([1c86a07](https://gitlab.com/hoppr/hoppr/commit/1c86a07a117539263a90242b5215b9fb7605b340))
* expected metadata source location ([1ffb443](https://gitlab.com/hoppr/hoppr/commit/1ffb4437a4cc1d74f560bd708759d92cdf0b4361))
* expected metadata source location ([efe120a](https://gitlab.com/hoppr/hoppr/commit/efe120a6d3319b6e1f97b672ed5beddb4b2e8147))
* Fixed Docker repo:tag information being lost in collection ([a8fc4a9](https://gitlab.com/hoppr/hoppr/commit/a8fc4a9333f410b98f63d561bd6abe091e77acb5))
* Fixed Docker repo:tag information being lost in collection ([75c283c](https://gitlab.com/hoppr/hoppr/commit/75c283cd5c6042eba8bdf9f60ba1520d444c037c))
* Fixed type-check findings ([66de3b8](https://gitlab.com/hoppr/hoppr/commit/66de3b8092a76a0185ff96535dbd0107a840f358))
* Fixed unit tests ([00207a8](https://gitlab.com/hoppr/hoppr/commit/00207a80d1164c1755d2b75eaa86fff96a8ad519))
* Fixing Merge Conflicts ([5feada8](https://gitlab.com/hoppr/hoppr/commit/5feada8398bdae635a4ae3c31fe4da1ee5cac7da))
* Rebase dev ([2bfb341](https://gitlab.com/hoppr/hoppr/commit/2bfb34185986c900d5d37b6a1c905773ff9247af))
* Remove gitlab semantic release comment on MRs ([37c5a6e](https://gitlab.com/hoppr/hoppr/commit/37c5a6ef48f53d979dd72dc5586a7c76eb1a4b57))
* Remove node engine reference from package.json ([e1a9da1](https://gitlab.com/hoppr/hoppr/commit/e1a9da10853905befc2ffed1eae9c989a7c13cb2))
* Remove package.json ([4b162fd](https://gitlab.com/hoppr/hoppr/commit/4b162fd56970273c6c9cbbe045ca0ab17276302d))
* remove problematic git files from tar toc comparison ([7a4c0a1](https://gitlab.com/hoppr/hoppr/commit/7a4c0a15c4a6c72b5f95d5b9f4ab89e26018bf83))
* remove problematic git files from tar toc comparison ([b55df15](https://gitlab.com/hoppr/hoppr/commit/b55df1582c57b01f7b724c3bc3d259ae7f85d7a1))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([eae151b](https://gitlab.com/hoppr/hoppr/commit/eae151bfa1c51b55af55cbd8dd2a410856f628f8))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([0cb11ab](https://gitlab.com/hoppr/hoppr/commit/0cb11abd4804d8ba57a4d313c18c7b10dde205e2))
* remove references to deleted job ([7237e6a](https://gitlab.com/hoppr/hoppr/commit/7237e6a0c4bf6221c7ecce6af9b7fd9bc608a025))
* remove references to deleted job ([a79ceed](https://gitlab.com/hoppr/hoppr/commit/a79ceed9730d1a4620b036537f30c095f3d2ec7d))
* syntax ([47a47bc](https://gitlab.com/hoppr/hoppr/commit/47a47bc513a138c4ab2608785ddb2c9e10fb4161))
* syntax ([b1b4dc7](https://gitlab.com/hoppr/hoppr/commit/b1b4dc796ca585f62cd8cba7895a774e0f3b2c64))
* syntax ([7f7570d](https://gitlab.com/hoppr/hoppr/commit/7f7570d97f1fe2103fafa90e9d1d4125d9fe1091))
* syntax ([bbf66dc](https://gitlab.com/hoppr/hoppr/commit/bbf66dc613bc827e0053e13ca2883139a2d46b80))
* Updated unit tests for docker collection ([35bd32b](https://gitlab.com/hoppr/hoppr/commit/35bd32b07c1087f1027bc89208f0e657c744b304))
* Using copy from Maven-Dependency-Plugin instead of get ([eafc0bc](https://gitlab.com/hoppr/hoppr/commit/eafc0bc5d7379e0c04d259c54e28f05e7b5fa787))

## [1.7.0-dev.29](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.28...v1.7.0-dev.29) (2023-02-08)


### Bug Fixes

* add expected-tar-toc for remaining integration tests ([eef848e](https://gitlab.com/hoppr/hoppr/commit/eef848e894065d338abdfe11089dbbdd33776fa9))
* check tar toc on integration tests ([9e612a4](https://gitlab.com/hoppr/hoppr/commit/9e612a408e8e0ec631f7f399d1dffe03df44143c))
* expected metadata source location ([efe120a](https://gitlab.com/hoppr/hoppr/commit/efe120a6d3319b6e1f97b672ed5beddb4b2e8147))
* remove problematic git files from tar toc comparison ([b55df15](https://gitlab.com/hoppr/hoppr/commit/b55df1582c57b01f7b724c3bc3d259ae7f85d7a1))
* remove redundant integration tests, reduce integration test sizes, run integration tests on every pipeline ([0cb11ab](https://gitlab.com/hoppr/hoppr/commit/0cb11abd4804d8ba57a4d313c18c7b10dde205e2))
* remove references to deleted job ([a79ceed](https://gitlab.com/hoppr/hoppr/commit/a79ceed9730d1a4620b036537f30c095f3d2ec7d))
* syntax ([7f7570d](https://gitlab.com/hoppr/hoppr/commit/7f7570d97f1fe2103fafa90e9d1d4125d9fe1091))
* syntax ([bbf66dc](https://gitlab.com/hoppr/hoppr/commit/bbf66dc613bc827e0053e13ca2883139a2d46b80))

## [1.7.0-dev.28](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.27...v1.7.0-dev.28) (2023-02-06)


### Bug Fixes

* Remove node engine reference from package.json ([e1a9da1](https://gitlab.com/hoppr/hoppr/commit/e1a9da10853905befc2ffed1eae9c989a7c13cb2))
* Remove package.json ([4b162fd](https://gitlab.com/hoppr/hoppr/commit/4b162fd56970273c6c9cbbe045ca0ab17276302d))

## [1.7.0-dev.27](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.26...v1.7.0-dev.27) (2023-02-06)


### Bug Fixes

* Added user_env support to find_credentials ([5319664](https://gitlab.com/hoppr/hoppr/commit/531966423d0ef3409d224a720256327955a4d18b))
* Cleaned up maven command ([0042ccd](https://gitlab.com/hoppr/hoppr/commit/0042ccdc9658e46c98e706a9d6392777a289193f))
* Corrected Maven-Dependency-Plugin arguments so that maven artifacts would be bundled ([1c86a07](https://gitlab.com/hoppr/hoppr/commit/1c86a07a117539263a90242b5215b9fb7605b340))
* Fixed type-check findings ([66de3b8](https://gitlab.com/hoppr/hoppr/commit/66de3b8092a76a0185ff96535dbd0107a840f358))
* Using copy from Maven-Dependency-Plugin instead of get ([eafc0bc](https://gitlab.com/hoppr/hoppr/commit/eafc0bc5d7379e0c04d259c54e28f05e7b5fa787))

## [1.7.0-dev.26](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.25...v1.7.0-dev.26) (2023-02-05)


### Bug Fixes

* Remove gitlab semantic release comment on MRs ([37c5a6e](https://gitlab.com/hoppr/hoppr/commit/37c5a6ef48f53d979dd72dc5586a7c76eb1a4b57))

## [1.7.0-dev.25](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.24...v1.7.0-dev.25) (2023-01-30)

## [1.7.0-dev.24](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.23...v1.7.0-dev.24) (2023-01-25)

## [1.7.0-dev.23](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.22...v1.7.0-dev.23) (2023-01-25)


### Features

* allow docker collector to use docker.io when --no-strict option is set ([368f513](https://gitlab.com/hoppr/hoppr/commit/368f513f762f838700f0c20a3b47a8cbc31e7d63))


### Bug Fixes

* linting union for type hints ([d274ba7](https://gitlab.com/hoppr/hoppr/commit/d274ba75714f972ca884ee4209b249954b974e22))
* main module exits using sys instead of typer ([47e7872](https://gitlab.com/hoppr/hoppr/commit/47e7872d237fb20745728e201ae22eb737264aff))
* moved test_main.py under test/unit/ directory ([75f7776](https://gitlab.com/hoppr/hoppr/commit/75f777641c5dd338d963df1da034a0c8231a1462))
* pylint error ([3d35a87](https://gitlab.com/hoppr/hoppr/commit/3d35a8745ec8ace5363dc3cf884306f3aab6013d))

## [1.7.0-dev.22](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.21...v1.7.0-dev.22) (2023-01-24)


### Bug Fixes

* enum base type ([414970f](https://gitlab.com/hoppr/hoppr/commit/414970f5242e75a5aa9a404702a800e2831168a5))
* import source type error ([3b6e973](https://gitlab.com/hoppr/hoppr/commit/3b6e973168a568f91f0eb5d2c95cf4e6b9195854))
* mypy errors ([5cdacff](https://gitlab.com/hoppr/hoppr/commit/5cdacffb58a7e33e9c4df38a03c2ae3dd04c68ca))
* stage name regex field type ([6dd8de2](https://gitlab.com/hoppr/hoppr/commit/6dd8de27a259141c9c2017d7590793439d5e5c69))
* type check errors ([1eb8f40](https://gitlab.com/hoppr/hoppr/commit/1eb8f4082d5481b3866a528baffe8d8ad279eb3d))


### Reverts

* model integration changes ([655c408](https://gitlab.com/hoppr/hoppr/commit/655c408becc567e145e19b9f468b1ddbdead7d8b))

## [1.7.0-dev.21](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.20...v1.7.0-dev.21) (2023-01-24)


### Bug Fixes

* pylint error ([8f86960](https://gitlab.com/hoppr/hoppr/commit/8f86960607f89ca8b23d18c94d01c5fc6c3c7977))
* urljoin stripping path components ([6cb3d61](https://gitlab.com/hoppr/hoppr/commit/6cb3d613349334339ff7ff4fdbf1f95d556a7680))

## [1.7.0-dev.20](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.19...v1.7.0-dev.20) (2023-01-18)


### Bug Fixes

* exclude nulls from output sboms ([2a4af6e](https://gitlab.com/hoppr/hoppr/commit/2a4af6e433cbd2ad7ee784c882cbd747a1449533))

## [1.7.0-dev.19](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.18...v1.7.0-dev.19) (2023-01-17)


### Bug Fixes

* Fixes [#150](https://gitlab.com/hoppr/hoppr/issues/150) and corrected when consolidated and deliveried SBOMs are written. It also address additional unit testing ([48d14ef](https://gitlab.com/hoppr/hoppr/commit/48d14efe5a1a2c7146c7687d374731be9670654b))

## [1.7.0-dev.18](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.17...v1.7.0-dev.18) (2023-01-17)


### Features

* creation of in-toto attestations ([69cc89d](https://gitlab.com/hoppr/hoppr/commit/69cc89d0c769c86590f1f984b80c26a832f270b7))


### Bug Fixes

* address issue where in-toto was looking maven files ([10c9016](https://gitlab.com/hoppr/hoppr/commit/10c9016f0ce04031572ea78b2091e9d44fac66e1))
* bug with with attestations created from GitLab CI Runners ([8f09f0e](https://gitlab.com/hoppr/hoppr/commit/8f09f0e2670ce6dec3a3f5e0fa5159ebb5cb36ad))
* fixed issue with bundle options for functionary_key ([1315a80](https://gitlab.com/hoppr/hoppr/commit/1315a80ffc68d140f16a0614cd766f51d6fed4e9))

## [1.7.0-dev.17](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.16...v1.7.0-dev.17) (2023-01-17)


### Bug Fixes

* Clean up artifact name ([fcbb383](https://gitlab.com/hoppr/hoppr/commit/fcbb383be6e388a6bc7ea138a073bfd1020d5069))
* Correct build artifacts ([7c59ff7](https://gitlab.com/hoppr/hoppr/commit/7c59ff7dc840a6b55a939e46308b2b4a2009e774))
* Remove blank lines ([9fec5a2](https://gitlab.com/hoppr/hoppr/commit/9fec5a2df888f298c6a858c7484dd76c5847c6ad))
* Remove quotes ([3b34729](https://gitlab.com/hoppr/hoppr/commit/3b34729a1be0fc2accd271484c1e3e67ab6a7077))
* Setup codequality analytics and build attestation ([bc295a0](https://gitlab.com/hoppr/hoppr/commit/bc295a026933cb5e3abe3acd51b4d5faf0113c8d))
* Update artifact name ([3d957d4](https://gitlab.com/hoppr/hoppr/commit/3d957d4c3e2198dc7223988712c3527f78ed2bf8))
* Update variables to contain artifact name ([2d64ebd](https://gitlab.com/hoppr/hoppr/commit/2d64ebd7fa280ef6e1b5fde1ec9823bab9f4d066))

## [1.7.0-dev.16](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.15...v1.7.0-dev.16) (2023-01-16)


### Bug Fixes

* add collection metadata to sbom for apt components ([d4304ab](https://gitlab.com/hoppr/hoppr/commit/d4304abd7919af4771939709080ec01bf27c9c39))
* add collections params for docker ([580ecef](https://gitlab.com/hoppr/hoppr/commit/580ecefade0334617f57cbbbfc98e5c0471ad952))
* complete unit test coverage for collect_nexus_search ([f8928fb](https://gitlab.com/hoppr/hoppr/commit/f8928fbf4bbd64d199acc7b0ba2f86b5bef20456))
* set timetag to utc timezone ([cf2d3e8](https://gitlab.com/hoppr/hoppr/commit/cf2d3e871e297d9fd95397bab1273b009929cab2))
* use enums for constants, per code review ([1cf99d8](https://gitlab.com/hoppr/hoppr/commit/1cf99d8e5c90b0e9f744d5ef5051fdb6a20ac24c))

## [1.7.0-dev.15](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.14...v1.7.0-dev.15) (2023-01-12)


### Bug Fixes

* pylint issues fix ([1404090](https://gitlab.com/hoppr/hoppr/commit/1404090f52acf05080fc6703e6a98b509752491c))

## [1.7.0-dev.14](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.13...v1.7.0-dev.14) (2023-01-12)


### Bug Fixes

* Update maven write to disk ([35a8739](https://gitlab.com/hoppr/hoppr/commit/35a8739f4046e8cbea33113d0c11356f40deb75e))

## [1.7.0-dev.13](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.12...v1.7.0-dev.13) (2023-01-10)


### Features

* add composite collector ([72b9d56](https://gitlab.com/hoppr/hoppr/commit/72b9d563cec9ef8ac03bc338fab6c6ae3a2913e9))


### Bug Fixes

* have nexus collector respect user-specified purl types ([5f93e73](https://gitlab.com/hoppr/hoppr/commit/5f93e73223545d8664f2bf2a6f572a1b735c28f9))

## [1.7.0-dev.12](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.11...v1.7.0-dev.12) (2023-01-09)

## [1.7.0-dev.11](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.10...v1.7.0-dev.11) (2023-01-09)


### Bug Fixes

* **plugin:** add type hint to auth ([a5ca981](https://gitlab.com/hoppr/hoppr/commit/a5ca9815e113cd8762b8a17fe03c3e023084bead))
* **plugin:** properly authenticate nexus requests ([bf4834f](https://gitlab.com/hoppr/hoppr/commit/bf4834f51e52fe8c9e2d26b7441280c22af5bd25))
* updates to ensure pipeline works for forks. ([b18cb53](https://gitlab.com/hoppr/hoppr/commit/b18cb534d41e4da71fac977a103e53fd2844e2e1))

## [1.7.0-dev.10](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.9...v1.7.0-dev.10) (2023-01-04)


### Bug Fixes

* add mock for os.path.exist ([0942eb3](https://gitlab.com/hoppr/hoppr/commit/0942eb3e2aabc1891ba2f4898cdfd524dd65111a))
* fast forward branch ([7c18ce2](https://gitlab.com/hoppr/hoppr/commit/7c18ce283f95f94800705e29b2fa26a16337f969))

## [1.7.0-dev.9](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.8...v1.7.0-dev.9) (2022-12-20)


### Bug Fixes

* pip arguments ([5b88e2e](https://gitlab.com/hoppr/hoppr/commit/5b88e2e281f83a203c6612771294d565515f93c9))


### Reverts

* files not relevant to this branch ([a8ed375](https://gitlab.com/hoppr/hoppr/commit/a8ed375f00e9c7998ee1ebe621ab6ea6213c5106))
* files not relevant to this branch ([7bba214](https://gitlab.com/hoppr/hoppr/commit/7bba214a305c84682ec225dc9f4252c84e570cea))

## [1.7.0-dev.8](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.7...v1.7.0-dev.8) (2022-12-19)


### Bug Fixes

* update assert ([92a143d](https://gitlab.com/hoppr/hoppr/commit/92a143dcbaf59078bcdee54001bc28cda3ee7115))

## [1.7.0-dev.7](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.6...v1.7.0-dev.7) (2022-12-15)


### Bug Fixes

* provide better error message on empty config file content ([21f16d3](https://gitlab.com/hoppr/hoppr/commit/21f16d33ed912d8e00dec88be07f54794cd4aff4))

## [1.7.0-dev.6](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.5...v1.7.0-dev.6) (2022-12-15)


### Features

* --no-strict CLI flag ([b57cfe0](https://gitlab.com/hoppr/hoppr/commit/b57cfe00438a528b316caa6136fdb8305b297df4))


### Bug Fixes

* all source distros from manifest repos ([f3808c1](https://gitlab.com/hoppr/hoppr/commit/f3808c16054b6488c226fd424d034b59b5e0aed8))
* clear loaded manifests ([7638c3f](https://gitlab.com/hoppr/hoppr/commit/7638c3f0cd5f14824abad741cd9ff3de8d15cdc4))
* strip quotes to compare found URL ([d476ca9](https://gitlab.com/hoppr/hoppr/commit/d476ca9b04d3049bae16020f1770f6f99ad5db63))
* type error ([344f18c](https://gitlab.com/hoppr/hoppr/commit/344f18c1d95d390488240cb41249ea0e228b0523))

## [1.7.0-dev.5](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.4...v1.7.0-dev.5) (2022-12-15)


### Bug Fixes

* Add Bot label to renovate MRs ([d8a894c](https://gitlab.com/hoppr/hoppr/commit/d8a894c717ef3b67c5c37d6ad247a36f025c3c4a))

## [1.7.0-dev.4](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.3...v1.7.0-dev.4) (2022-12-14)


### Features

* add nexus search collector ([a20f58d](https://gitlab.com/hoppr/hoppr/commit/a20f58d497b6a5cd9ec70248d1c07d02c6afa726))

## [1.7.0-dev.3](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.2...v1.7.0-dev.3) (2022-12-12)

## [1.7.0-dev.2](https://gitlab.com/hoppr/hoppr/compare/v1.7.0-dev.1...v1.7.0-dev.2) (2022-12-08)


### Bug Fixes

* add missing import ([5b9ba89](https://gitlab.com/hoppr/hoppr/commit/5b9ba8928268b2e4aa9d309d6283246e536879d5))
* add missing strict_repos typer argument ([5514d75](https://gitlab.com/hoppr/hoppr/commit/5514d7502eaabf01ee5abc503a767ae2eda6b011))
* add pytest-cov package ([32c4e4b](https://gitlab.com/hoppr/hoppr/commit/32c4e4b181f2ce559ecafa9ea7862c26828a6051))
* main process logfile lock ([f4536c0](https://gitlab.com/hoppr/hoppr/commit/f4536c0ba3f65edd92adce114289ca86a0e31f4f))
* shared RLock for Docker unit tests ([7f85953](https://gitlab.com/hoppr/hoppr/commit/7f859539aa9de0c22ca75a7d05aeedfbe1fa3187))

## [1.7.0-dev.1](https://gitlab.com/hoppr/hoppr/compare/v1.6.3-dev.6...v1.7.0-dev.1) (2022-12-07)


### Features

* --no-strict CLI flag ([affc1a5](https://gitlab.com/hoppr/hoppr/commit/affc1a55c2efe188ddf4d782d3b64a1410056be8))


### Bug Fixes

* transfer file default value ([067b653](https://gitlab.com/hoppr/hoppr/commit/067b6537eefb54240b79d66aea798d2ccde4f0a5))

## [1.6.3-dev.6](https://gitlab.com/hoppr/hoppr/compare/v1.6.3-dev.5...v1.6.3-dev.6) (2022-12-05)


### Bug Fixes

* hoppr group ([6bc6f83](https://gitlab.com/hoppr/hoppr/commit/6bc6f834307b2517ec0efa773f4c6b7995bdb395))
* update CODEOWNERS for repo move ([c930691](https://gitlab.com/hoppr/hoppr/commit/c9306914d7a45a4c7063fed0b3fe98c729020d20))

## [1.6.3-dev.5](https://gitlab.com/hoppr/hoppr/compare/v1.6.3-dev.4...v1.6.3-dev.5) (2022-12-05)


### Bug Fixes

* gitattributes ([0e94ef6](https://gitlab.com/hoppr/hoppr/commit/0e94ef64d1dab04eaddec631828c158cc37a565c))

## [1.6.3-dev.4](https://gitlab.com/hoppr/hoppr/compare/v1.6.3-dev.3...v1.6.3-dev.4) (2022-12-01)


### Bug Fixes

* dev branch test; update README ([d983df9](https://gitlab.com/hoppr/hoppr/commit/d983df925a4a169801546d097f251303140fce56))

## [1.6.3-dev.3](https://gitlab.com/hoppr/hoppr/compare/v1.6.3-dev.2...v1.6.3-dev.3) (2022-11-30)

## [1.6.3-dev.2](https://gitlab.com/hoppr/hoppr/compare/v1.6.3-dev.1...v1.6.3-dev.2) (2022-11-29)


### Bug Fixes

* include resources folder in poetry build ([8945c70](https://gitlab.com/hoppr/hoppr/commit/8945c7074824669b680a349568c587477ffe00f3))
* platform check ([b62677b](https://gitlab.com/hoppr/hoppr/commit/b62677b6ffdfb108c24e5bf5caff8d9b384eafac))
* test pattern matching ([6772858](https://gitlab.com/hoppr/hoppr/commit/6772858b38609937830e9c6b373c1dab8c6db6db))
* windows ANSI processing ([45b3aa6](https://gitlab.com/hoppr/hoppr/commit/45b3aa68d5d3799d976f588df6c018f2900c756d))

## [1.6.3-dev.1](https://gitlab.com/hoppr/hoppr/compare/v1.6.2...v1.6.3-dev.1) (2022-11-23)


### Bug Fixes

* append dev instead of current branch name ([d221aa3](https://gitlab.com/hoppr/hoppr/commit/d221aa3e4d86b7678ba7e0a258ceee9de3ccd0d3))
* generate dev version if not on main/dev ([0a13943](https://gitlab.com/hoppr/hoppr/commit/0a13943b6fb8a8b1434f96415f82c0f9aedcde0f))
* missing `rev` command in skopeo image ([054c37b](https://gitlab.com/hoppr/hoppr/commit/054c37bfdbc555364ade81170efa115aa566c60f))
* missing collector plugin CLI tools ([c7a2fd6](https://gitlab.com/hoppr/hoppr/commit/c7a2fd61fedff78e6179c52cf07e39b52ff286f9))
* remove prerelease ([80f2663](https://gitlab.com/hoppr/hoppr/commit/80f26631a3773ed744306a7a439b4c3a74964021))
* remove prerelease ([229b35e](https://gitlab.com/hoppr/hoppr/commit/229b35e08863a95e98d6129b5e84420308efeab4))
* restore Dockerfile FROM directive ([83f6728](https://gitlab.com/hoppr/hoppr/commit/83f6728fc80a38e9d4ba951fe144d7906dea36b3))
* semantic-release version bump ([f7b9077](https://gitlab.com/hoppr/hoppr/commit/f7b90776cdf393ba558e37fb26e39e4f6d2be841))
* set prerelease back to true ([5dd88c4](https://gitlab.com/hoppr/hoppr/commit/5dd88c43bbcb3cb97acdd93520ab3e74250c4ef0))
* test image tags ([25e035d](https://gitlab.com/hoppr/hoppr/commit/25e035dd15603e2f1717073f990ed9635e78a033))

## [1.6.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.6.1...v1.6.2) (2022-11-19)


### Bug Fixes

* Add build arg to dockerfile ([da86fb6](https://gitlab.com/lmco/hoppr/hoppr/commit/da86fb609bf130f6e2529e268d315b38d4bd22d9))
* Add entrypoint to hoppr ([c43f0d0](https://gitlab.com/lmco/hoppr/hoppr/commit/c43f0d094b2b51f7b6fa742c5b7dca91caba6a9a))
* Add hopctl docker image ([fbb8e66](https://gitlab.com/lmco/hoppr/hoppr/commit/fbb8e661b977d1489fc30d50a10e46436f9fe5df))
* Add hopctl docker image ([0e05700](https://gitlab.com/lmco/hoppr/hoppr/commit/0e05700cbdc8c72d7106ca7d30453418b1344b61))
* Add hopctl docker image ([28bd132](https://gitlab.com/lmco/hoppr/hoppr/commit/28bd132948ff9d3511e666c51e0b34be0c4db3bf))
* Clean up other variables in ci ([fe10719](https://gitlab.com/lmco/hoppr/hoppr/commit/fe107198929ce90071ad7281202cda5e61e412e4))
* Clean up rules and workflow ([cc82d02](https://gitlab.com/lmco/hoppr/hoppr/commit/cc82d02a3eddab2ea921a653677a34ddb45f30ff))
* Cleanup notes in config ([1721020](https://gitlab.com/lmco/hoppr/hoppr/commit/1721020cdc0dc27b4834ef97ff99d639220a41d5))
* Cleanup rules ([00192a5](https://gitlab.com/lmco/hoppr/hoppr/commit/00192a56a647cb1bbd9c041719c96db2080e9812))
* Cleanup rules ([4d3bd31](https://gitlab.com/lmco/hoppr/hoppr/commit/4d3bd3129c374788d842fd1955db5dbee7ecb23a))
* Correct dockerfile ([8a0134b](https://gitlab.com/lmco/hoppr/hoppr/commit/8a0134b63fe70d339d9c929e970c794551bd5921))
* Correct release yaml file ([1cad0a6](https://gitlab.com/lmco/hoppr/hoppr/commit/1cad0a6ce6e17b2fd53d7319bdf60d2abee78200))
* Correct releaserc file ([bf1648b](https://gitlab.com/lmco/hoppr/hoppr/commit/bf1648becf34d7a2ba4e47276e94138fce023816))
* Correct releaserc.yml ([93147c1](https://gitlab.com/lmco/hoppr/hoppr/commit/93147c161651acb5d26f225102183a4d7a0d4667))
* Correct requirements ([cf24fa3](https://gitlab.com/lmco/hoppr/hoppr/commit/cf24fa3947576332159f0382b1352d4a4cd760f3))
* Correct simlinks in ci docker ([001e0b6](https://gitlab.com/lmco/hoppr/hoppr/commit/001e0b6ea9432ef6fd689b8f46d34b91719ca9e7))
* Correct syntax in ci docker ([2cbf4ce](https://gitlab.com/lmco/hoppr/hoppr/commit/2cbf4ce3b720c3f3ab889e62a98b2792d8cbb6b3))
* Correct the wheel name ([60e269c](https://gitlab.com/lmco/hoppr/hoppr/commit/60e269c5c91c8c70ab9340af547d356f9325f6bc))
* Don't run pipelines on merge event ([e0f0e12](https://gitlab.com/lmco/hoppr/hoppr/commit/e0f0e12a19667669724eeba85c51539541254cb1))
* Ensure python is on the path ([f18abb6](https://gitlab.com/lmco/hoppr/hoppr/commit/f18abb65eb2b5b9f6fd62dbc4880ad717a69e2ee))
* Fixed the releaseing issue ([9295e2e](https://gitlab.com/lmco/hoppr/hoppr/commit/9295e2e0be565b4d3afb4d744fe8c15f3b8141f9))
* image tag reset after rebase ([7f6255f](https://gitlab.com/lmco/hoppr/hoppr/commit/7f6255f648bace84e45ac27d460d99e38986b031))
* Make a quicklink script for linking python in dockerfile ([5226159](https://gitlab.com/lmco/hoppr/hoppr/commit/5226159cbde6ca3b2fe202fcf4abd40a10681d39))
* Make semantic release pass all jobs ([022cf11](https://gitlab.com/lmco/hoppr/hoppr/commit/022cf1147d08326671c4cb2b40fa6650976aba14))
* Merge main into branch ([08bc351](https://gitlab.com/lmco/hoppr/hoppr/commit/08bc3517cfaff1c9095dbc8a664536211af98c3e))
* **Minor:** Image build cleanup ([715381f](https://gitlab.com/lmco/hoppr/hoppr/commit/715381f016da2633a8bc58e01240b86411fca457))
* Only publish if there's a new release from semver dryrun ([74e8805](https://gitlab.com/lmco/hoppr/hoppr/commit/74e8805d1b5630ebfb43221d7e20d70eb94d6a14))
* Only run semantic release publish on develop and main ([c4fbdf3](https://gitlab.com/lmco/hoppr/hoppr/commit/c4fbdf347c8fefecd92c607aff532da988adaace))
* Protect keys ([d760f33](https://gitlab.com/lmco/hoppr/hoppr/commit/d760f336033541176a8308c0c1142b331123b5c6))
* Reference main so that develop can become default branch ([083fe3b](https://gitlab.com/lmco/hoppr/hoppr/commit/083fe3b513ff21a116bf025033ff9ada5e28ecad))
* Release dev channel ([f42fe8f](https://gitlab.com/lmco/hoppr/hoppr/commit/f42fe8f94fedfd8e3962484c90a63a50b1f4b9bf))
* Release dev channel ([4b9207b](https://gitlab.com/lmco/hoppr/hoppr/commit/4b9207be063b2b94cd6330280f8cc577a01cb93c))
* Remove quotations around build ([6365fe7](https://gitlab.com/lmco/hoppr/hoppr/commit/6365fe7551f3038d3d2fd7e8731cc5893dc591bf))
* Remove quotations from parallel matrix ([02d25d2](https://gitlab.com/lmco/hoppr/hoppr/commit/02d25d295b8cf75465dfe5f81d366ecf2cf58209))
* Remove skip ci and attempt to let workflow rules handle pipelines ([47dbc44](https://gitlab.com/lmco/hoppr/hoppr/commit/47dbc44636032733147c52c3470f64f467923fb3))
* Revert Changes ([7d3c1eb](https://gitlab.com/lmco/hoppr/hoppr/commit/7d3c1eb88c2588d0b89d35e33654776398a83578))
* Roll back chore rules ([03a53d2](https://gitlab.com/lmco/hoppr/hoppr/commit/03a53d20e81d0e487d03d3ed8445ea97848eb68b))
* Roll base image back from 9.0 to 8.6 ([916dce2](https://gitlab.com/lmco/hoppr/hoppr/commit/916dce27be7ff7207d6c7506fb53c41a8797c6bf))
* Run if commit message doesn't start with chore ([349e791](https://gitlab.com/lmco/hoppr/hoppr/commit/349e791e92f3dd62b5a1c00c9cd80a3327472c2e))
* Run the entire dockerfile as one large image ([57cf073](https://gitlab.com/lmco/hoppr/hoppr/commit/57cf0730e2eb866d65c05b61b82158444a4b5b97))
* Setup dependencies on test image ([26a1805](https://gitlab.com/lmco/hoppr/hoppr/commit/26a18051d09488213337f6c9861a693f2b8aec43))
* Simlink python ([fbb9a9b](https://gitlab.com/lmco/hoppr/hoppr/commit/fbb9a9bb4346511450e21fb7162fd27303466e85))
* Split out ci config per !184#note_1170995317 ([aa1250f](https://gitlab.com/lmco/hoppr/hoppr/commit/aa1250ffaa613c9a2319ccae137f79152ca937df)), closes [184#note_1170995317](https://gitlab.com/lmco/184/issues/note_1170995317)
* unit test context missing delivered sbom ([4e40c45](https://gitlab.com/lmco/hoppr/hoppr/commit/4e40c45405907550a2c9fa31300636b6c33bc510))
* Use long switches for readability. ([a6cae38](https://gitlab.com/lmco/hoppr/hoppr/commit/a6cae38adf724c998f549bcfd5604f065f4af1da))
* Use python3 for virtual environment creation in CI Dockerfile ([26c5651](https://gitlab.com/lmco/hoppr/hoppr/commit/26c5651cb5daa2ce4bd950b4833b211c9ed2ee50))
* Use semantic release to publish package ([f844308](https://gitlab.com/lmco/hoppr/hoppr/commit/f844308641b09803840083640e359b104e0fefe9))

## [1.6.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.6.0...v1.6.1) (2022-11-16)


### Bug Fixes

* processor relative file handling ([20b3952](https://gitlab.com/lmco/hoppr/hoppr/commit/20b3952f87de3e09dc4273015860cea3912ec857))
* processor relative file handling ([9a08142](https://gitlab.com/lmco/hoppr/hoppr/commit/9a081420cef6c4a63c98b4d4e8b87d3afd326933))
* type errors ([86b357e](https://gitlab.com/lmco/hoppr/hoppr/commit/86b357e795c27e36cdb1088453546474e655b193))

## [1.6.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.5.1...v1.6.0) (2022-11-16)


### Features

* Added ability to retrieve BOM files from an OCI registry ([64b7a0c](https://gitlab.com/lmco/hoppr/hoppr/commit/64b7a0c9ed4117e1ffaf84ed74d007e1004697c3))


### Bug Fixes

* add delivered_sbom to context, work from that variable ([eac1cf8](https://gitlab.com/lmco/hoppr/hoppr/commit/eac1cf828968069f1c99e2c36dee2e38ab5a9a0c))
* add initial checks for BOM access ([fc7605f](https://gitlab.com/lmco/hoppr/hoppr/commit/fc7605f3a15f67262a9478a625afc25c3d37f635))
* catch exception from _get_required_coverage, check for empty/missing boms ([d0fe601](https://gitlab.com/lmco/hoppr/hoppr/commit/d0fe601795fff168ce2074b738aa91e505e83ba5))
* code review comments ([f3abc34](https://gitlab.com/lmco/hoppr/hoppr/commit/f3abc34c2806d72d4ba5aa93d1c68e322debb08f))
* Code review comments ([d842a42](https://gitlab.com/lmco/hoppr/hoppr/commit/d842a420766610bd380aedaa387b41cf5a0d442c))
* only load purl-type-specific plugins when components of that type are being processed (Issue [#77](https://gitlab.com/lmco/hoppr/hoppr/issues/77)) ([cf26fa5](https://gitlab.com/lmco/hoppr/hoppr/commit/cf26fa59e791c7e7fd81f4e12a0d266e270b64b4))
* remove docs tasks ([e7967ce](https://gitlab.com/lmco/hoppr/hoppr/commit/e7967cebef744fec64af2c2a8de306ae2100ae01))
* revert to older version of npm-check-updates ([03b765b](https://gitlab.com/lmco/hoppr/hoppr/commit/03b765bc0fe6493f88b8e46ccce055d888ef4cdd))
* turn off allow-failure on semantic-release:dry-run job to avoid breaking changes from renovate ([eed1ab0](https://gitlab.com/lmco/hoppr/hoppr/commit/eed1ab09281be308e06902fc21e682cca69b81f8))
* **deps:** update dependency hoppr-cyclonedx-models to v0.2.10 ([bb0a2cc](https://gitlab.com/lmco/hoppr/hoppr/commit/bb0a2cc9cfffd99cb919515d74f1db51e3523f36))
* update delivered_sbom with process return objects ([c7448a1](https://gitlab.com/lmco/hoppr/hoppr/commit/c7448a142ef997eb218964e2a544910015978720))
* update Result object to include optional return object.  Check that return object matches plugin Bom Access value ([cd87958](https://gitlab.com/lmco/hoppr/hoppr/commit/cd879581329dd969951f3c1f8abe005b59a2d0d0))

## [1.5.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.5.0...v1.5.1) (2022-11-10)


### Bug Fixes

* **deps:** update dependency typer to ^0.7.0 ([660a39c](https://gitlab.com/lmco/hoppr/hoppr/commit/660a39cc5819b919fe77fbdfeb812a6225beaa84))

## [1.5.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.4.2...v1.5.0) (2022-11-09)


### Features

* verbose flag ([fe3c8fa](https://gitlab.com/lmco/hoppr/hoppr/commit/fe3c8fafb983c6dad483284f37774ff74ca01444))


### Bug Fixes

* bom helm chart version ([d3690fb](https://gitlab.com/lmco/hoppr/hoppr/commit/d3690fb6ac4816674e254978a5e59c1c121a19ee))
* default log_level value ([878cae8](https://gitlab.com/lmco/hoppr/hoppr/commit/878cae89aba2dbafbe78125fe7c5adb573182e63))
* helm collector append purl name ([4faa7cd](https://gitlab.com/lmco/hoppr/hoppr/commit/4faa7cd0b877ee290ad68d4e7a99773ac4cd08d7))
* manifest helm repo URL ([1dd0eae](https://gitlab.com/lmco/hoppr/hoppr/commit/1dd0eaeda88152d8ab17bc4390424fa8bb3fe65a))
* parameter name typo ([f567f59](https://gitlab.com/lmco/hoppr/hoppr/commit/f567f5972de70b141e40d4d80d41ade6dfb1f2e1))
* Result constructor param typo ([45283dd](https://gitlab.com/lmco/hoppr/hoppr/commit/45283ddfec7e04eb76963cf8c7d0960365b837a3))
* test_logger escape sequences ([6c6555e](https://gitlab.com/lmco/hoppr/hoppr/commit/6c6555e2684b30eef0602a0952988f3143d78e3f))

## [1.4.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.4.1...v1.4.2) (2022-11-07)


### Bug Fixes

* purl repository_url without a port specified will now match either http or https ([0b61af2](https://gitlab.com/lmco/hoppr/hoppr/commit/0b61af2fe6b7b8a9c560ad82e68d69e7e0d90ca7))
* update repository_url comparison to handle default ports ([77e7620](https://gitlab.com/lmco/hoppr/hoppr/commit/77e7620e8bd24521a1b490d69e9a83c4dc6316af))
* Use socket.getservbyname to determine default port ([4573599](https://gitlab.com/lmco/hoppr/hoppr/commit/4573599345ad1bb32c1a3d90fe9a00b80e95b15b))

## [1.4.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.4.0...v1.4.1) (2022-11-03)


### Bug Fixes

* Add the changelog back in ([f4b146e](https://gitlab.com/lmco/hoppr/hoppr/commit/f4b146ef4cc14ed22a622e58661a1dd3eab8ddea))

## [1.4.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.3.1...v1.4.0) (2022-11-01)


### Features

* add ComponentCoverage class, set defaults for base plug-ins ([3473fa3](https://gitlab.com/lmco/hoppr/hoppr/commit/3473fa3036f4a4f881bfcee9d1c4762876f4c1c1))
* evaluate component coverage ([70c7815](https://gitlab.com/lmco/hoppr/hoppr/commit/70c78153708be6bf2bfdcf01b99bd662359b8027))


### Bug Fixes

* allow integration tests to be run manually ([d4136e3](https://gitlab.com/lmco/hoppr/hoppr/commit/d4136e3eaf555f71a30ecef8c7d8d52db6a53516))
* Bug fixes in hoppr/core_plugins/collect_raw_plugin.py, added logging ([b9a91c4](https://gitlab.com/lmco/hoppr/hoppr/commit/b9a91c463e6d9b58cea1a12547707dc54a983ae6))
* change 'file://' repos to 'file:' ([a5c4472](https://gitlab.com/lmco/hoppr/hoppr/commit/a5c447212a6608bc823438171ecb78c8e1b9840d))
* move default_component_coverate from instance variable to class variable ([8a39129](https://gitlab.com/lmco/hoppr/hoppr/commit/8a39129d923a5dbefaa56434da0933fa996c9611))
* remove trailing colon ([1f494ca](https://gitlab.com/lmco/hoppr/hoppr/commit/1f494ca447078389406b6b4a3734066d26e2ef70))
* remove unused dependency cyclonedx-python-lib ([9262bb7](https://gitlab.com/lmco/hoppr/hoppr/commit/9262bb7b7b6f710eb7b4fbc9caab29d6330a3edf))
* shorten error messages from Result.from_http_response ([6e64885](https://gitlab.com/lmco/hoppr/hoppr/commit/6e6488563e78adf2dc5a9f388d7ed4e1849d0b58))
* update all transfer config files to include 'plugins' key ([2840dac](https://gitlab.com/lmco/hoppr/hoppr/commit/2840dac46ea100ba6e58ed073a38502b67d0f4da))
* update another transfer config file to include 'plugins' key ([a36ecd3](https://gitlab.com/lmco/hoppr/hoppr/commit/a36ecd3f04c5401eaa270bdcfcbb4a6d72c364e2))

## [1.3.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.3.0...v1.3.1) (2022-10-27)


### Bug Fixes

* add config for debugging python directly ([ac393f1](https://gitlab.com/lmco/hoppr/hoppr/commit/ac393f1adf74320d8d5e6df3df883781f8210384))

## [1.3.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.2.2...v1.3.0) (2022-10-26)


### Features

* recognize and respect 'repository_url' qualifier in purls ([80f4eb5](https://gitlab.com/lmco/hoppr/hoppr/commit/80f4eb5e1c9aa3d2396ba2d3899a2f783eaebecc))


### Bug Fixes

* add ssh:// to schemes to be trimed when comparing urls ([28ef1fd](https://gitlab.com/lmco/hoppr/hoppr/commit/28ef1fddc119d6e245d1947f02652fff5c1e2596))
* linting/typing issues ([28a494c](https://gitlab.com/lmco/hoppr/hoppr/commit/28a494c9f88705798dfca08054cc5ae783fa554a))

## [1.2.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.2.1...v1.2.2) (2022-10-18)


### Bug Fixes

* Add codeowners file ([06ebb1c](https://gitlab.com/lmco/hoppr/hoppr/commit/06ebb1c2076e9d8c09b21914173e07232ba09450))
* Add resource group to semantic release dry-run ([35d9718](https://gitlab.com/lmco/hoppr/hoppr/commit/35d97180945d62cb920465917a2f232bd5c245b3))
* Clean up artifacts ([6ab03b9](https://gitlab.com/lmco/hoppr/hoppr/commit/6ab03b9dc5f3f4c2b9abed2af1e62f187d963406))
* Correct yaml syntax ([13f153d](https://gitlab.com/lmco/hoppr/hoppr/commit/13f153d49c018fa197373c373989c425ec835b5e))
* Don't show skipped pipelines ([c836b05](https://gitlab.com/lmco/hoppr/hoppr/commit/c836b056e259b25e1af30e7e2e33bd2d336ebb00))
* hide sidenav link ([d1e4d56](https://gitlab.com/lmco/hoppr/hoppr/commit/d1e4d560880d9622ccf73edb24b19c879915a73d))
* Interrupt false ([52d9ec3](https://gitlab.com/lmco/hoppr/hoppr/commit/52d9ec3f15197d1beb5c5cca53aef98055b6ca65))
* Make ci build wait for semantic release ([3cf022b](https://gitlab.com/lmco/hoppr/hoppr/commit/3cf022bac651c1224601e3aaadf2703eca47eb91))
* Parent Child ([284351f](https://gitlab.com/lmco/hoppr/hoppr/commit/284351ff8151d837457881fd44638da2393ef835))
* Set environment ([00f2f7d](https://gitlab.com/lmco/hoppr/hoppr/commit/00f2f7d5dc400c4c8b47688bab029f8116805107))
* Specify environment for dry-run ([3021cbe](https://gitlab.com/lmco/hoppr/hoppr/commit/3021cbe652f921687933c34e88a0946e642bdc98))
* test ([cf7342a](https://gitlab.com/lmco/hoppr/hoppr/commit/cf7342aef4b58ae8d778333643a8a70683f0d3db))
* test concurrency ([8e671a7](https://gitlab.com/lmco/hoppr/hoppr/commit/8e671a72be83dd6c3d7f373000d8828607c0953b))

## [1.2.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.2.0...v1.2.1) (2022-10-18)


### Bug Fixes

* cleaned up a purl reference ([e04d824](https://gitlab.com/lmco/hoppr/hoppr/commit/e04d824aad89dc548512fa3f51b34c0358b041c8))
* removed unneeded variable, combined if statements ([8a222e5](https://gitlab.com/lmco/hoppr/hoppr/commit/8a222e5a025ef45aa823a69ee1c3792de8b3fbac))

## [1.2.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.1.0...v1.2.0) (2022-10-18)


### Features

* docker compose build ([d226402](https://gitlab.com/lmco/hoppr/hoppr/commit/d226402491bf0fa7881cd4756ecd190f7d2a6eba))


### Reverts

* pyproject.toml, .releaserc.yml ([0ba0e35](https://gitlab.com/lmco/hoppr/hoppr/commit/0ba0e351a7d94e7bdc342dacb3ed172471351dc9))

## [1.1.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.0.4...v1.1.0) (2022-10-17)


### Features

* BatchCollectorPlugin base class (name TBD) ([f65ac20](https://gitlab.com/lmco/hoppr/hoppr/commit/f65ac20abade163394539bc9a2220fe982977254))


### Bug Fixes

* class variable type ([3f671e2](https://gitlab.com/lmco/hoppr/hoppr/commit/3f671e2d0a1dd6713f51bbc289dbb0dd8fc70fc4))
* class variable type ([2ecd3b5](https://gitlab.com/lmco/hoppr/hoppr/commit/2ecd3b54baecc5f2b5fa6bc0a366b6ac4a721ba1))
* flexibility for modular packages ([ffff23c](https://gitlab.com/lmco/hoppr/hoppr/commit/ffff23cb437ee5c2658f88c206a670a6ab63ded6))
* get component subdir correctly ([60eb7d1](https://gitlab.com/lmco/hoppr/hoppr/commit/60eb7d126ef8bf0e68099f884e5f2c590f1adc48))
* import path ([7650ecc](https://gitlab.com/lmco/hoppr/hoppr/commit/7650ecc43d1871060a0b076d4a93b6f8bdec890d))
* lint fixes ([648587b](https://gitlab.com/lmco/hoppr/hoppr/commit/648587bee573dab38797bfd1b07c87f01738e866))
* lint suggestions and tests ([fad8fa3](https://gitlab.com/lmco/hoppr/hoppr/commit/fad8fa388bf3aaf56085a980c92e9e56652afc44))
* logic to set proxy for repo ([942e8c4](https://gitlab.com/lmco/hoppr/hoppr/commit/942e8c48b905f006d8706ce704c69e1fb914867a))
* pages-preview rules ([3f428e2](https://gitlab.com/lmco/hoppr/hoppr/commit/3f428e27be16a0f1fec5181f972669296e458878))
* return fail result ([8a2093c](https://gitlab.com/lmco/hoppr/hoppr/commit/8a2093ca052b0cf6f83138a149c27e3359d163af))
* type errors ([6af8ffc](https://gitlab.com/lmco/hoppr/hoppr/commit/6af8ffcdbe8e2ac7d515ed2f96f0394ed9074cb6))
* type errors, directory_for() return type ([a035f0a](https://gitlab.com/lmco/hoppr/hoppr/commit/a035f0a9be1109fc33a6cd6d59d690649a2dc5ff))


### Reverts

* supports_purl_type to allow PurlType ([fa34d3a](https://gitlab.com/lmco/hoppr/hoppr/commit/fa34d3a8a8ed87038b17a9a78dc38cf9543cd2da))
* unrelated pipeline changes ([5e3d303](https://gitlab.com/lmco/hoppr/hoppr/commit/5e3d303b1c69816505436714160217234aca41c0))

## [1.0.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.0.3...v1.0.4) (2022-10-13)


### Bug Fixes

* pages-preview rules ([1749eaf](https://gitlab.com/lmco/hoppr/hoppr/commit/1749eaf13052c3a1923cacd8a9f6525fe8fdc7bf))
* semantic-release-replace regex ([9e07a4e](https://gitlab.com/lmco/hoppr/hoppr/commit/9e07a4e2c571fb73c7429ada87f777865436c951))


### Reverts

* pyproject.toml formatting ([724f9f1](https://gitlab.com/lmco/hoppr/hoppr/commit/724f9f1ca96dff61fa09a9139e0ece5c5768f0ca))

## [1.0.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.0.2...v1.0.3) (2022-10-11)


### Bug Fixes

* black wanted another space ([76f6e60](https://gitlab.com/lmco/hoppr/hoppr/commit/76f6e6039e2d67f4d44dfe151e538f584273beb7))
* fix release, fix logged purl ([49d2511](https://gitlab.com/lmco/hoppr/hoppr/commit/49d251101395d3b5772e5fba7822598d391b9415))
* lint issue ([24fc461](https://gitlab.com/lmco/hoppr/hoppr/commit/24fc4613044ce9f22d9fc31e3f1d488d5794e6ec))

## [1.0.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.0.1...v1.0.2) (2022-10-10)


### Bug Fixes

* documentation changes for v1.0 ([738128b](https://gitlab.com/lmco/hoppr/hoppr/commit/738128b1ab14c9a2b542001caea859ad9b7695db))
* patch yum collector to enable collection of module rpms ([5e90de5](https://gitlab.com/lmco/hoppr/hoppr/commit/5e90de56a7528c18098cd434ce6d68ca7783c117))

## [1.0.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v1.0.0...v1.0.1) (2022-10-05)


### Bug Fixes

* remove 'Starting' messages for skip processes from log ([ec5c874](https://gitlab.com/lmco/hoppr/hoppr/commit/ec5c874a21adff6e77e881dd908bbbedcabc2a5a))

## [1.0.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.4...v1.0.0) (2022-09-29)


### ⚠ BREAKING CHANGES

* Declare v1.0.0

### Features

* Declare v1.0.0 ([31efe26](https://gitlab.com/lmco/hoppr/hoppr/commit/31efe2693ca7060882425d1da9f2a5271b371999))

## [0.20.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.3...v0.20.4) (2022-09-29)


### Bug Fixes

* pass credentials properly from CollectRawPlugin ([6dec417](https://gitlab.com/lmco/hoppr/hoppr/commit/6dec417910e15d7c6e3c865adbca575b26508580))

## [0.20.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.2...v0.20.3) (2022-09-28)


### Bug Fixes

* revert collectyumplugin to 0.20.1 state ([0d7169b](https://gitlab.com/lmco/hoppr/hoppr/commit/0d7169b98607e9ac52516834aefeab7f2de297ee))

## [0.20.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.1...v0.20.2) (2022-09-22)


### Bug Fixes

* creds object None handling ([bdf60f3](https://gitlab.com/lmco/hoppr/hoppr/commit/bdf60f33570528eb1f717fe77d83a69e4e35bb41))
* decode command output ([8ff96a6](https://gitlab.com/lmco/hoppr/hoppr/commit/8ff96a60e7ea0050192304dca42e5b3f569be385))
* fail if stdout empty ([cac2383](https://gitlab.com/lmco/hoppr/hoppr/commit/cac238347fc3681aa059a5a245d33d61900c87ba))
* lint suggestions ([a6c50a6](https://gitlab.com/lmco/hoppr/hoppr/commit/a6c50a6287f4addefc68bc5419c822d034c570ea))
* missing import ([0462ad5](https://gitlab.com/lmco/hoppr/hoppr/commit/0462ad56a1a5f92606260e8e4ec4e0898cdeb221))

## [0.20.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.20.0...v0.20.1) (2022-09-22)


### Bug Fixes

* always keep test-bundle log as artifact ([4ebb67e](https://gitlab.com/lmco/hoppr/hoppr/commit/4ebb67e40a88b49a8d6a7b0e832557c316f2202b))
* helm collector tries twice, with and without directory in --repo ([e7963cc](https://gitlab.com/lmco/hoppr/hoppr/commit/e7963cc99f189b38d859267cf8757d3c924cf15f))

## [0.20.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.9...v0.20.0) (2022-09-20)


### Features

* move logging to file, give limited feedback to stdout ([bd6a180](https://gitlab.com/lmco/hoppr/hoppr/commit/bd6a180f4d2f47319e36374ba25862dd36842c12))


### Bug Fixes

* add bundle command line option for log file location ([5e11a59](https://gitlab.com/lmco/hoppr/hoppr/commit/5e11a596f53308034e4bb517ee47663df86cabc2))
* add display message on stage failure ([ccc1643](https://gitlab.com/lmco/hoppr/hoppr/commit/ccc164368383941a6fa50e1b06a352eb71e47e82))
* linting again ([d701e38](https://gitlab.com/lmco/hoppr/hoppr/commit/d701e38ed4aea5bff047b3cf2013110bba9e3a8d))
* linting issue ([2d10ae8](https://gitlab.com/lmco/hoppr/hoppr/commit/2d10ae858270ba52586bb8778907a27efe3bdace))
* linting/comments ([afcce3c](https://gitlab.com/lmco/hoppr/hoppr/commit/afcce3cf539e4328c0141757cb8210c178f79c92))
* rename log_fn to logfile_location ([940dfa7](https://gitlab.com/lmco/hoppr/hoppr/commit/940dfa746b20505e847b19cc3f99fbc493fcd86d))

## [0.19.9](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.8...v0.19.9) (2022-09-20)


### Bug Fixes

* font and search bar fix ([f4768ac](https://gitlab.com/lmco/hoppr/hoppr/commit/f4768aca7fa3901abfaf9181cd62cc366ffff991))

## [0.19.8](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.7...v0.19.8) (2022-09-15)


### Bug Fixes

* hoppr docs bug fixes ([b391610](https://gitlab.com/lmco/hoppr/hoppr/commit/b391610af572b79ca0d8229336ebfbced6ac29d6))

## [0.19.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.6...v0.19.7) (2022-09-08)


### Bug Fixes

* add gcr.io as docker repo ([d91e927](https://gitlab.com/lmco/hoppr/hoppr/commit/d91e927d946a9e35d02761c0a9cd781ebcb583fd))
* add integration test manifests, fix gitignore ([e816a0f](https://gitlab.com/lmco/hoppr/hoppr/commit/e816a0fed8d1777bc0e670185788fd81a81645d0))
* add registry.gitlab.com repo for docker ([50d4c34](https://gitlab.com/lmco/hoppr/hoppr/commit/50d4c34c9919e7e05cee9e7078b74a781769680a))
* add test cases, see what happens ([e91d1c4](https://gitlab.com/lmco/hoppr/hoppr/commit/e91d1c42621b183a4d2dabe5046055f5a6a4d4a9))
* always build images ([86f8767](https://gitlab.com/lmco/hoppr/hoppr/commit/86f8767097f50284bb2681a64d3e477dc8e13de0))
* bom locations in manifests ([f611c03](https://gitlab.com/lmco/hoppr/hoppr/commit/f611c03d764c554ab5454758907915af01cdbf3b))
* Correct paths ([b5ada75](https://gitlab.com/lmco/hoppr/hoppr/commit/b5ada756684fccd74c02762a8d4d6e80d12a52c5))
* Create docker specific Boms ([e678f72](https://gitlab.com/lmco/hoppr/hoppr/commit/e678f721fe70050ec71067be4834ed7a551ede6f))
* debug ([e1e9afa](https://gitlab.com/lmco/hoppr/hoppr/commit/e1e9afa798e76b594c09dbd70f560f7565249105))
* debug ([de82165](https://gitlab.com/lmco/hoppr/hoppr/commit/de8216540de830fb25ba12d05382fcc9e3ebbdd3))
* debug looking for tar ([4122825](https://gitlab.com/lmco/hoppr/hoppr/commit/4122825ce2313947973a7e0fd4d1e946752f9ba6))
* debug python tar download ([00fdee8](https://gitlab.com/lmco/hoppr/hoppr/commit/00fdee8715f85d9c7d499e7c0e52834d00d2414b))
* debug python tar download ([f6fce33](https://gitlab.com/lmco/hoppr/hoppr/commit/f6fce3394400ccec616efd960a64130a118b840e))
* First steps on int tests ([bda2934](https://gitlab.com/lmco/hoppr/hoppr/commit/bda293443e879ad9454e54cba8f48d08db414053))
* helm tarfile location, git projects ([d4c3f2f](https://gitlab.com/lmco/hoppr/hoppr/commit/d4c3f2fa0f4fc3f8be3f1192b0fe288596f8dae9))
* hoppr install ([bd8d317](https://gitlab.com/lmco/hoppr/hoppr/commit/bd8d317b8f07e6ef4e98114ad783bf5619add201))
* install tar ([c68ebdf](https://gitlab.com/lmco/hoppr/hoppr/commit/c68ebdf9b29d5d1be992d8558061f036aa5216cc))
* install xz ([ba9c4b6](https://gitlab.com/lmco/hoppr/hoppr/commit/ba9c4b6ab5b6adc0ff3707ee450f51effb1bc71b))
* mod dockerfile to get docker build to run ([d69d3b3](https://gitlab.com/lmco/hoppr/hoppr/commit/d69d3b3e1d8b311cc34aa0dca932d34a58eaaa5a))
* registry ([e2d0a98](https://gitlab.com/lmco/hoppr/hoppr/commit/e2d0a98055edee5c48f1afea468556c1614abb26))
* registry reference ([3846682](https://gitlab.com/lmco/hoppr/hoppr/commit/38466824931368bff7a20b36b8422865786880db))
* remove docker-builder tag ([00fa050](https://gitlab.com/lmco/hoppr/hoppr/commit/00fa05052136aacbc544884b75672ab88c12fcc1))
* Remove monster Renovate Image from test ([e59009d](https://gitlab.com/lmco/hoppr/hoppr/commit/e59009d709d95df551ffebc572423a8c3599d82c))
* rules syntax ([8ccb5c9](https://gitlab.com/lmco/hoppr/hoppr/commit/8ccb5c9b7406cef5345f17fa237b8af5998f8efb))
* run integration tests only on main, cleanup ([6d43357](https://gitlab.com/lmco/hoppr/hoppr/commit/6d43357ccc42a36db7ed24659d8e8c257b061ed2))
* Test GRC ([326a028](https://gitlab.com/lmco/hoppr/hoppr/commit/326a02876bc23ea348dadc2b1d0fa6e4c2af0b5b))
* touch dockerfile ([f8806f3](https://gitlab.com/lmco/hoppr/hoppr/commit/f8806f39ce0f981d5c6453b8cc2744b3fb9d5f42))
* try rules again ([1b66fe5](https://gitlab.com/lmco/hoppr/hoppr/commit/1b66fe5dd14851f6695cac344b1937551596235d))
* try rules changes ([28896f6](https://gitlab.com/lmco/hoppr/hoppr/commit/28896f6fc43771385a2c768f7f53358b75231be3))
* turn off rules for docker build ([76a164c](https://gitlab.com/lmco/hoppr/hoppr/commit/76a164c74ae8371d40f08a4f63f5635fefbc2460))
* typo ([9edb4e3](https://gitlab.com/lmco/hoppr/hoppr/commit/9edb4e37b54eec8581fbde2fa1f29eec9f634369))
* typo in dockerfile name ([7169c3a](https://gitlab.com/lmco/hoppr/hoppr/commit/7169c3a28e7727d2e5fd54c4db93d144026cc8e1))
* update gitlab-ci to run docker build ([b9c2e06](https://gitlab.com/lmco/hoppr/hoppr/commit/b9c2e06a97f1fd19ee5bc19e353d60140560eaa9))

## [0.19.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.5...v0.19.6) (2022-09-01)


### Bug Fixes

* add second append of maven_opts ([4485266](https://gitlab.com/lmco/hoppr/hoppr/commit/4485266dba3a9d39e2aa4e1d6c7069b0cce2d9c0))

## [0.19.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.4...v0.19.5) (2022-09-01)


### Bug Fixes

* add maven options to collect_maven config ([bdbdfbe](https://gitlab.com/lmco/hoppr/hoppr/commit/bdbdfbe6a82a7dde5c366efe99a50690db779f87))

## [0.19.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.3...v0.19.4) (2022-08-31)


### Bug Fixes

* docs updates for short switches ([9aec204](https://gitlab.com/lmco/hoppr/hoppr/commit/9aec204c3fde79baec24f953ebec2dc2413d4a40))

## [0.19.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.2...v0.19.3) (2022-08-31)


### Bug Fixes

* deal with empty namespace in purl ([a382277](https://gitlab.com/lmco/hoppr/hoppr/commit/a38227751b2aeda72a45168fafc984f16e405a24))

## [0.19.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.1...v0.19.2) (2022-08-29)


### Bug Fixes

* Add basic tests for generator ([b37009b](https://gitlab.com/lmco/hoppr/hoppr/commit/b37009b663697d3b118d5b042b4de9dff9ef0cb4))
* Add basic tests for generator ([4b95f30](https://gitlab.com/lmco/hoppr/hoppr/commit/4b95f304c80336a5760ae666fd4fb156bca9a017))
* Add changes per MR 119 ([6e5a126](https://gitlab.com/lmco/hoppr/hoppr/commit/6e5a126363498d2d11b55b07c45bed4e627ebcf9))
* Add components ([9391c43](https://gitlab.com/lmco/hoppr/hoppr/commit/9391c437a291cfda8bbb0e4671089bd62048d503))
* Add in generation code ([76cb41e](https://gitlab.com/lmco/hoppr/hoppr/commit/76cb41e2b510c11c527fbf6df3d5c36ab7026e4e))
* Add sboms switch for feedback and updates per MR comments ([75b1059](https://gitlab.com/lmco/hoppr/hoppr/commit/75b10590a9b2e621df70b7b8637524a2ce21e64d))
* Bump hoppr cdx version ([96ec59b](https://gitlab.com/lmco/hoppr/hoppr/commit/96ec59bc74f033282614cd7f71179898849b38c4))
* Bump poetry version ([57a434e](https://gitlab.com/lmco/hoppr/hoppr/commit/57a434e10cb1061e6c524fe0e66e1fd8f75857f0))
* Bump poetry version ([535b773](https://gitlab.com/lmco/hoppr/hoppr/commit/535b7734ce6c4c4edeef36066229dbebe5be3da6))
* Bump the model version ([4cf9d82](https://gitlab.com/lmco/hoppr/hoppr/commit/4cf9d82b4936390887c8b7124b765a0762d4faa5))
* Clean up comments ([f36cd54](https://gitlab.com/lmco/hoppr/hoppr/commit/f36cd547e9fcaf04384bd92274e0261cf8b2aefc))
* Clean up comments ([d1b0020](https://gitlab.com/lmco/hoppr/hoppr/commit/d1b0020ba70cf5dacd6f64498e29ffa3de5bf91f))
* Clean up per mr review ([450cddc](https://gitlab.com/lmco/hoppr/hoppr/commit/450cddc92db28d7d52f6b64b83749faedbaa0805))
* Clean up print statement ([f12b817](https://gitlab.com/lmco/hoppr/hoppr/commit/f12b8179ec19d9f5901a17fd931aa92a8950c832))
* code review comment, use cpu_count for default max_processes ([bd3a9a3](https://gitlab.com/lmco/hoppr/hoppr/commit/bd3a9a3aa768e8010501e21479f65d933463d6e9))
* Correct bom typeerror ([7fa342c](https://gitlab.com/lmco/hoppr/hoppr/commit/7fa342cbf28d759c3a2eaa662a7a2c1f0a7eae39))
* Correct errors ([08253c5](https://gitlab.com/lmco/hoppr/hoppr/commit/08253c5fe6ccae181f3a8c7622106a6d6b92c5ea))
* Correct invalid escape sequence ([73f4e2d](https://gitlab.com/lmco/hoppr/hoppr/commit/73f4e2d9a4064d7deeec1e906a82cc7699998749))
* Correct lockfile ([4062829](https://gitlab.com/lmco/hoppr/hoppr/commit/4062829d014ec15e4d0954a3f7afef7651fa664e))
* Correct poetry lock file ([a199a90](https://gitlab.com/lmco/hoppr/hoppr/commit/a199a9003d736f05bfc6f7f3e0564b4691a1cc32))
* Correct the test errors ([ed967b8](https://gitlab.com/lmco/hoppr/hoppr/commit/ed967b8ec5b30ee88d11291b5be2f7bd77067081))
* Correct the test errors ([71b82cd](https://gitlab.com/lmco/hoppr/hoppr/commit/71b82cd08eac31af790fe103b6fc76144646f797))
* Correct the test errors ([20e9d8e](https://gitlab.com/lmco/hoppr/hoppr/commit/20e9d8edc4b69da7e2e500cb1b2c75ec26b38d5c))
* Correct variable names ([41b093a](https://gitlab.com/lmco/hoppr/hoppr/commit/41b093a0fd4281e1724d38f7249309fdb0c02d29))
* Handle schema tag ([0ee39fa](https://gitlab.com/lmco/hoppr/hoppr/commit/0ee39fae889f887ab4e2d4305649586cbc6c0202))
* Handle schema tag ([0562d39](https://gitlab.com/lmco/hoppr/hoppr/commit/0562d39dbc7c0a4818b5bb9163ab7cfb909971e5))
* Merge ([b278fff](https://gitlab.com/lmco/hoppr/hoppr/commit/b278fffb71b42f52eb80319194c7dded2f1bc026))
* Merge ([0ea5af4](https://gitlab.com/lmco/hoppr/hoppr/commit/0ea5af46b42ef657e31223f1f7630b31dd729a12))
* Merge main into branch ([26bb6fa](https://gitlab.com/lmco/hoppr/hoppr/commit/26bb6fa52259616dab6823302ddffc7ebdc447b3))
* Merge main into branch ([c32b43e](https://gitlab.com/lmco/hoppr/hoppr/commit/c32b43ebacec8cebaa8a2c0bdb65a819939cbd60))
* put max_processes in Context, use max_processes from config file, fix bug in remove_empty utility ([ab9e09b](https://gitlab.com/lmco/hoppr/hoppr/commit/ab9e09b9d4137f70ff56bde043252d5930cd2533))
* Remove credentials default file ([7fccf29](https://gitlab.com/lmco/hoppr/hoppr/commit/7fccf29aa220b8aa52621dedf92ae3c4f8174130))
* Remove exclude ([ea07ca3](https://gitlab.com/lmco/hoppr/hoppr/commit/ea07ca3dd5ef81485040e636bdf020028c222739))
* Remove values.yml and update main.py ([27a97ee](https://gitlab.com/lmco/hoppr/hoppr/commit/27a97ee31efe5cc74a6b4d70a784763c6822119b))
* typo ([63fc7c2](https://gitlab.com/lmco/hoppr/hoppr/commit/63fc7c2ad682f05461548e0f8b2d9b3d74d476a9))
* Update credentials variable to HOPPR_CREDS_CONFIG to match validate env ([d402167](https://gitlab.com/lmco/hoppr/hoppr/commit/d402167250ad10d36ddec9a9a1cff5f16ca165ec))
* Update git collector to support gitlab, github, and golang purl types ([0f3b258](https://gitlab.com/lmco/hoppr/hoppr/commit/0f3b25871b57467a65594d69aa2b72fcca3cfe7f))
* Update per MR comments ([75cd885](https://gitlab.com/lmco/hoppr/hoppr/commit/75cd885037c2c02e65dde279fb6ccb589920320f))
* Update poetry lock ([ad57383](https://gitlab.com/lmco/hoppr/hoppr/commit/ad57383d8a6307187709a9f33e56ff9fec9362f7))
* Update poetry lock file ([ee79895](https://gitlab.com/lmco/hoppr/hoppr/commit/ee79895933ab14ac1078f2ed02c2477013044288))
* Update purl_type.py to include new types ([f208be0](https://gitlab.com/lmco/hoppr/hoppr/commit/f208be0deec909d9aa2a22d98d6aaed18f43b815))
* Updates per mr review ([1f2e6d9](https://gitlab.com/lmco/hoppr/hoppr/commit/1f2e6d9b10e05077ea1a0e65d1e3e73cf1048451))
* Write yaml test ([0b21526](https://gitlab.com/lmco/hoppr/hoppr/commit/0b21526ce81d6d06cc7d17349f1fa05f21415ecd))

## [0.19.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.19.0...v0.19.1) (2022-08-29)


### Bug Fixes

* bug when deleting docker file for retry ([a4b1140](https://gitlab.com/lmco/hoppr/hoppr/commit/a4b1140812a68db7c49f6962dd1d0312442d7f28))

## [0.19.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.19...v0.19.0) (2022-08-25)


### Features

* add vscode tasks for hoppr docs ([0af74df](https://gitlab.com/lmco/hoppr/hoppr/commit/0af74df27097cabcc5ded7d0eb626abe8634cd5f))


### Bug Fixes

* add clarifying note yum/dnf ([f2cb08b](https://gitlab.com/lmco/hoppr/hoppr/commit/f2cb08baccf847af6a71de4e99fc896d8ba6b244))
* definitions ([afaead7](https://gitlab.com/lmco/hoppr/hoppr/commit/afaead72c215de5b288a541855cf246fea17ae58))
* MR updates ([9709376](https://gitlab.com/lmco/hoppr/hoppr/commit/9709376b3d275200f4dfb9ae88a0f4d0f24b342f))
* Updating docs for MVP alignment ([c6ecdf0](https://gitlab.com/lmco/hoppr/hoppr/commit/c6ecdf0d7f10d85157fb3824022d0d968643fdcf))

## [0.18.19](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.18...v0.18.19) (2022-08-16)


### Bug Fixes

* allow cyclonedx models to update ([857659d](https://gitlab.com/lmco/hoppr/hoppr/commit/857659d1c3271b05c835fa656d6d952f58d083bb))

## [0.18.18](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.17...v0.18.18) (2022-08-15)


### Bug Fixes

* minor bug resulting in repeated failure messages in summary ([1b0c68d](https://gitlab.com/lmco/hoppr/hoppr/commit/1b0c68dc1ba5d11d568211730619d48221170c82))

## [0.18.17](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.16...v0.18.17) (2022-08-11)


### Bug Fixes

* confusing test purl ([a9937ed](https://gitlab.com/lmco/hoppr/hoppr/commit/a9937ed4a4918195ee1c1bb48d722d21db13c0e4))
* Yum collector directories ([6127730](https://gitlab.com/lmco/hoppr/hoppr/commit/6127730f27aeb4d847e913b08c1199ae307f2b95))

## [0.18.16](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.15...v0.18.16) (2022-08-04)


### Bug Fixes

* Changed hoppr_types to utilize hoppr-cyclonedx-models ([9648618](https://gitlab.com/lmco/hoppr/hoppr/commit/9648618fba1e87b6f396f5b6093e9aab2872020b))
* Changed imports that used hoppr_types to use oppr_cyclonedx_models module ([24f7f73](https://gitlab.com/lmco/hoppr/hoppr/commit/24f7f73860abdfa8153c65f76959bdb71a447afa))
* Changed types to hoppr_types, poetry add hoppr_cyclonedx ([6ebba64](https://gitlab.com/lmco/hoppr/hoppr/commit/6ebba645a4090b42c3af90c54a6289b095237463))
* deleted excess cyclonedx files ([f28fcae](https://gitlab.com/lmco/hoppr/hoppr/commit/f28fcae7c27332b65379edd8803b97b1479c92b2))
* modified files using old 'types' directory ([5a87731](https://gitlab.com/lmco/hoppr/hoppr/commit/5a8773131378e6e4fc484010b9697e6790c3707c))
* modify 'type' in pylintrc ([f6c40d2](https://gitlab.com/lmco/hoppr/hoppr/commit/f6c40d24f89844362436deff415b800a6807743f))
* modify files with old type naming ([3541522](https://gitlab.com/lmco/hoppr/hoppr/commit/3541522c9b6539b3c746890c459497e519df7255))

## [0.18.15](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.14...v0.18.15) (2022-08-02)


### Bug Fixes

* typo ([2bf4ec1](https://gitlab.com/lmco/hoppr/hoppr/commit/2bf4ec10a6228753c71b5143c962df64921ca4e1))
* Update git collector to support gitlab, github, and golang purl types ([b244739](https://gitlab.com/lmco/hoppr/hoppr/commit/b24473991b6d82e99045121a8b0796033956d173))
* Update purl_type.py to include new types ([f874516](https://gitlab.com/lmco/hoppr/hoppr/commit/f8745168900155da42551ce70d052db6256af42b))

## [0.18.14](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.13...v0.18.14) (2022-07-27)


### Bug Fixes

* code review comment, use cpu_count for default max_processes ([d347282](https://gitlab.com/lmco/hoppr/hoppr/commit/d3472828ab8d697820f5843420fc5da46e7b017f))
* put max_processes in Context, use max_processes from config file, fix bug in remove_empty utility ([4b47397](https://gitlab.com/lmco/hoppr/hoppr/commit/4b473979bb927d650e392506c33590d8576dbb95))

## [0.18.13](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.12...v0.18.13) (2022-07-26)


### Bug Fixes

* Hotfix pages urls ([6bb1083](https://gitlab.com/lmco/hoppr/hoppr/commit/6bb10838e86a6c3f64f52441f26aed4b72fd995e))

## [0.18.12](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.11...v0.18.12) (2022-07-26)


### Bug Fixes

* Clean up darkmode ([823c8e0](https://gitlab.com/lmco/hoppr/hoppr/commit/823c8e096cbccbb6a792542e5f9a2924c08c8361))
* Correct overview path ([3a62b12](https://gitlab.com/lmco/hoppr/hoppr/commit/3a62b120e59a660a082ae617adf7c9f614ed3422))
* Delete bad image ([9686c62](https://gitlab.com/lmco/hoppr/hoppr/commit/9686c62e074ceb01cfb128b4ca6ffe3358eab796))
* Test out darkmode ([a70f355](https://gitlab.com/lmco/hoppr/hoppr/commit/a70f355da766d21b083587e9898afc93a1e1531a))

## [0.18.11](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.10...v0.18.11) (2022-07-21)


### Bug Fixes

* move logging to new class, needed for reuse in plug-ins ([6a49e33](https://gitlab.com/lmco/hoppr/hoppr/commit/6a49e33a3e512247f056b112e1167c53d7e473a8))

## [0.18.10](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.9...v0.18.10) (2022-07-19)


### Bug Fixes

* Add missing init to test module ([5e981d8](https://gitlab.com/lmco/hoppr/hoppr/commit/5e981d84a9720ef9be92d70c94161ee83d215bac))
* Add vscode debug support; tasks/launch.json ([3aa3830](https://gitlab.com/lmco/hoppr/hoppr/commit/3aa3830cc0701e3c24747a8ba7e0f6d760370246))
* Allow untrusted. ([bca89f9](https://gitlab.com/lmco/hoppr/hoppr/commit/bca89f9968149db4f83f40c2beeda1b3276df78e))
* Put --allow-untrusted in the add line. ([13b977f](https://gitlab.com/lmco/hoppr/hoppr/commit/13b977f9ddffb6a5909a775f8edf4e15f1069cd3))
* Try using plus equals ([512109f](https://gitlab.com/lmco/hoppr/hoppr/commit/512109fdbcc28066d42e27d0c45e57539ae0e359))
* trying another different syntax ([127eaef](https://gitlab.com/lmco/hoppr/hoppr/commit/127eaefb27bb6b561e4971ba10f72930bdbc91d3))
* trying different syntax ([4d8675c](https://gitlab.com/lmco/hoppr/hoppr/commit/4d8675c7894b554fbd67491b98337d942d446b44))
* trying yet another different syntax ([57292be](https://gitlab.com/lmco/hoppr/hoppr/commit/57292bebdb7e0e5c811e321690f6883638ce3c4a))
* Use correct yq version ([c89061a](https://gitlab.com/lmco/hoppr/hoppr/commit/c89061aeb9decb67a29e62e7c99242a6665b08c9))
* yq issue, added reason to an error log message ([2434066](https://gitlab.com/lmco/hoppr/hoppr/commit/2434066cd476dc45ecd4f4c55ce310f0c62b05ac))
* YQ version ([cb4b6cf](https://gitlab.com/lmco/hoppr/hoppr/commit/cb4b6cfb0a1cd976b0818e92045cc4a6c638bf70))

## [0.18.9](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.8...v0.18.9) (2022-07-11)


### Bug Fixes

* Add artifact attestation ([f5d9133](https://gitlab.com/lmco/hoppr/hoppr/commit/f5d9133d8eb41a1bc108cddc00ea6c098e21f55c))
* Add clean images ([6ff6017](https://gitlab.com/lmco/hoppr/hoppr/commit/6ff601745e42f3bdc1b9bef4aedd9619731c56b5))
* Add in the correct images ([109bf4c](https://gitlab.com/lmco/hoppr/hoppr/commit/109bf4ca55a093297c5392808a19766c58718dbf))
* Added lfs ([1f23c73](https://gitlab.com/lmco/hoppr/hoppr/commit/1f23c73902fd7443006bf2d609770ec6670a0b4f))
* Compress images and add lfs ([6d92cb1](https://gitlab.com/lmco/hoppr/hoppr/commit/6d92cb193eeb50325ebdd507fae32960cebf039c))
* Correct image resolutions ([3c2cfe3](https://gitlab.com/lmco/hoppr/hoppr/commit/3c2cfe38d06b6247deed91e5d30804e5c2bbfc6d))
* Flatten out the svg rgb color ([1b23474](https://gitlab.com/lmco/hoppr/hoppr/commit/1b234749343adbee81dc66e92179a6daef7c04d0))
* Push changes ([f4a5b36](https://gitlab.com/lmco/hoppr/hoppr/commit/f4a5b3634c7d5c9d549ff63ca9e59338cca024fa))
* Remove LFS ([fcc0225](https://gitlab.com/lmco/hoppr/hoppr/commit/fcc0225eeceab7388196d00cfc04472f0c300305))
* Revert ([5eb4c2f](https://gitlab.com/lmco/hoppr/hoppr/commit/5eb4c2fca850f31649249ad2a0feec09b982b70e))
* Rework the images ([847b1a3](https://gitlab.com/lmco/hoppr/hoppr/commit/847b1a3b1830f297332f829539e1e74a3518eeab))
* Speed up the ground scroll a little ([443005b](https://gitlab.com/lmco/hoppr/hoppr/commit/443005bfdc8b7c27b38351f687a565e361d9f35a))
* Test generating metadata ([c5e8b1a](https://gitlab.com/lmco/hoppr/hoppr/commit/c5e8b1ac28c809228195a1d2f0534e5fb4829c48))
* Update names, update more icon ([5054584](https://gitlab.com/lmco/hoppr/hoppr/commit/5054584f3c66bc2aebc1caabf002eeacdba84f78))
* Update package.json ([decfe10](https://gitlab.com/lmco/hoppr/hoppr/commit/decfe10200d38cd3db9b2df66a6657cebb09bd29))

## [0.18.8](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.7...v0.18.8) (2022-07-01)


### Bug Fixes

* Manual merge ([8af13b0](https://gitlab.com/lmco/hoppr/hoppr/commit/8af13b0ba9bd7be79c744d74f9bcc6dbeb5fe7f8))
* MR Cleanup and improved net utils coverage ([29d16fd](https://gitlab.com/lmco/hoppr/hoppr/commit/29d16fdaf9fc9f842d0b718a3c4583285def0004))
* poetry.lock ([79ce411](https://gitlab.com/lmco/hoppr/hoppr/commit/79ce411ec7b6ba9511b56c303e44ed75c0af1b6c))
* Pylint multi-return + test_process ([44fd966](https://gitlab.com/lmco/hoppr/hoppr/commit/44fd96637cb1c8ec23d9d6b34d05a27b59e0dfc7))
* yum test ([2809153](https://gitlab.com/lmco/hoppr/hoppr/commit/28091536787f75f31687369ac5155fd5f432d3e3))

## [0.18.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.6...v0.18.7) (2022-06-29)


### Features

* implement collect_yum_plugin. ([57db6d7](https://gitlab.com/lmco/hoppr/hoppr/commit/57db6d71f1bdc178dd887fdabc7935c2419eef4b))


### Bug Fixes

* address first wave of MR comments. ([134e958](https://gitlab.com/lmco/hoppr/hoppr/commit/134e9586ebc3f9cc5029b307915d4b5ab5ab76f8))
* formatting update from black. ([0cbb2bd](https://gitlab.com/lmco/hoppr/hoppr/commit/0cbb2bdb008f04785a16091bd6ebe4f1a7c81502))
* log whole purl object instead of just name. ([9a315ac](https://gitlab.com/lmco/hoppr/hoppr/commit/9a315ac50d24260848fa8ca8a124befa2d2c473b))

## [0.18.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.5...v0.18.6) (2022-06-28)


### Bug Fixes

* Hotfix the overview reference ([65e9c35](https://gitlab.com/lmco/hoppr/hoppr/commit/65e9c350f3bdf9318deaae03a2f53cb0b2b7d3a9))

## [0.18.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.4...v0.18.5) (2022-06-28)


### Bug Fixes

* Add apt cache and install yarn ([2dccbb1](https://gitlab.com/lmco/hoppr/hoppr/commit/2dccbb101103637f2e4fde2e215774c2435abeef))
* Add core build tools ([be681c2](https://gitlab.com/lmco/hoppr/hoppr/commit/be681c2a286a1ecfa5556e17e4ca49e4231a83de))
* Add in templating for env ([b7b82a6](https://gitlab.com/lmco/hoppr/hoppr/commit/b7b82a632d752415eee59edf21b402cec47cf0e2))
* Add in webp images ([e3532bf](https://gitlab.com/lmco/hoppr/hoppr/commit/e3532bf1e54253e405c322fc65920a41f0c810d7))
* Add longer image to see if it cleans up mobile ([400f449](https://gitlab.com/lmco/hoppr/hoppr/commit/400f4499f8ca570e284d8e3fbe7fe926ea9a4186))
* Add md_in_html for dynamic tags ([0ed4754](https://gitlab.com/lmco/hoppr/hoppr/commit/0ed4754b0593848f3e4651b70107f8a5484e5d18))
* Add semantic dependencies back in ([4b0d5cb](https://gitlab.com/lmco/hoppr/hoppr/commit/4b0d5cb19da42dc95b1ac93dffb92eb7387e00e6))
* Clean up docs ([a22c742](https://gitlab.com/lmco/hoppr/hoppr/commit/a22c7422764b1a426843a24c1a89b84014d15981))
* Clean up headers ([554e02b](https://gitlab.com/lmco/hoppr/hoppr/commit/554e02be75de8902f38cee361fa7f32434eb1ddf))
* Clean up YQ syntax ([1e3b631](https://gitlab.com/lmco/hoppr/hoppr/commit/1e3b631168357ca4fcab4d3da1ec3cddbd4809a9))
* Cleanup hoppr ([b58d63b](https://gitlab.com/lmco/hoppr/hoppr/commit/b58d63bac2a14535bfb3a63a206124034cbd069d))
* Correct install syntax ([6c374dd](https://gitlab.com/lmco/hoppr/hoppr/commit/6c374ddc8937a5133f05c50cb8d35293205b36ab))
* Correct mkdocs ([229e790](https://gitlab.com/lmco/hoppr/hoppr/commit/229e790caa08f37b1e5653b4801e0635a127917d))
* Correct mkdocs ([c50d135](https://gitlab.com/lmco/hoppr/hoppr/commit/c50d13594634fc6c20aff0ff8db91b484717b3e3))
* Correct pages job ([4d6898c](https://gitlab.com/lmco/hoppr/hoppr/commit/4d6898ccc74d06d1ab919ea3e0010b992fc9d79a))
* Correct the link and ref ([124077b](https://gitlab.com/lmco/hoppr/hoppr/commit/124077b3d3dc46675fbf23441e995d00ed141528))
* Correct the link text ([93f80cc](https://gitlab.com/lmco/hoppr/hoppr/commit/93f80cc733c44bf767507f0b5935c9945c66a296))
* Corrected image layout ([32b59d4](https://gitlab.com/lmco/hoppr/hoppr/commit/32b59d406a732cef1bf637429e6617c018a8afc8))
* Expose Docs on MR Request ([0a2149d](https://gitlab.com/lmco/hoppr/hoppr/commit/0a2149df20ea1cf3d28a070e878607e11332915d))
* Merge in ([b0f8330](https://gitlab.com/lmco/hoppr/hoppr/commit/b0f833098c3e5070c4d89d61d70c39cf320c6cea))
* Pass at parallax ([2bd038c](https://gitlab.com/lmco/hoppr/hoppr/commit/2bd038c9f336e6e89c35e94982e08bf0512364d5))
* Pass at parallax ([073ff88](https://gitlab.com/lmco/hoppr/hoppr/commit/073ff889f9ede8906057f20145bcfcdffb766db5))
* Pre-Commit fixes ([d464c79](https://gitlab.com/lmco/hoppr/hoppr/commit/d464c7955768c66822d173585ef78856422ac0bb))
* Pre-Commit fixes ([5ccb1fb](https://gitlab.com/lmco/hoppr/hoppr/commit/5ccb1fb3ed9999231b5ab09d4475008ba8492863))
* Pre-Commit fixes ([ae518a5](https://gitlab.com/lmco/hoppr/hoppr/commit/ae518a57368797527f09a0bc08d2f8b8ec76aaf5))
* Remove yarn.lock ([9a9d3a8](https://gitlab.com/lmco/hoppr/hoppr/commit/9a9d3a8f2f7cb3f0f804c8fc4a7db175578494b6))
* Set default hoppr.version field in mkdocs to pass linting ([2af642a](https://gitlab.com/lmco/hoppr/hoppr/commit/2af642a72362c1312c0bd7552ff4cbc1fce2a991))
* Set hoppr version in docs ([9e27fd1](https://gitlab.com/lmco/hoppr/hoppr/commit/9e27fd1159b59d3ce18a314bb4b7d47a3f88f24a))
* Update gant ([ede4b5d](https://gitlab.com/lmco/hoppr/hoppr/commit/ede4b5d63aec5e6ee68e1f2208e5d5857715d23b))
* Update roadmap to reflect appropriate dates ([309ed33](https://gitlab.com/lmco/hoppr/hoppr/commit/309ed335d004ee6931d55245b0c6d64b45f1ed93))
* We don't have any containers yet ([9a79f46](https://gitlab.com/lmco/hoppr/hoppr/commit/9a79f468845e79e76d84f4fd917c842332abe609))

## [0.18.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.3...v0.18.4) (2022-06-27)


### Bug Fixes

* **deps:** update dependency packageurl-python to ^0.10.0 ([0bdaddf](https://gitlab.com/lmco/hoppr/hoppr/commit/0bdaddf8bcc284e5eb7ab407e067253e32cc47fd))

## [0.18.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.2...v0.18.3) (2022-06-23)

## [0.18.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.1...v0.18.2) (2022-06-22)


### Bug Fixes

* setup versioning to be updated before build, and for publish to only publish (instead of build too). ([c907149](https://gitlab.com/lmco/hoppr/hoppr/commit/c907149d18275a1031c03279f54d1246d49b6782))
* version in pyproject.toml is updated with semantic version ([4347611](https://gitlab.com/lmco/hoppr/hoppr/commit/4347611b752fc336660c7e59d71c0b4074551752))

## [0.18.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.18.0...v0.18.1) (2022-06-14)


### Bug Fixes

* add message when manifest is not re-loaded ([116b550](https://gitlab.com/lmco/hoppr/hoppr/commit/116b5508bac8c873a1b7847f9f5a143302aa7439))
* potential infinite recursion in manifest load process ([bbe0ac7](https://gitlab.com/lmco/hoppr/hoppr/commit/bbe0ac7fbc938d70036f1fcb7b540ed34215d4e9))
* use resolved path for file checks of loaded_manifests ([20af8f8](https://gitlab.com/lmco/hoppr/hoppr/commit/20af8f8b8d7e675a75194270171c1ee3390137e3))

## [0.18.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.17.1...v0.18.0) (2022-06-13)


### Features

* add metadata to bundles ([778640a](https://gitlab.com/lmco/hoppr/hoppr/commit/778640aa025cf4690a8fdc935aa5c04f56a1c1b1))


### Bug Fixes

* Move consolidated_sbom to context, use Transfer object for transfer config ([6b35ee2](https://gitlab.com/lmco/hoppr/hoppr/commit/6b35ee23a49b4e8442c82ba76db3ce2745e379b3))

## [0.17.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.17.0...v0.17.1) (2022-06-09)


### Bug Fixes

* **deps:** update dependency semantic-release to v19.0.3 ([b5b9bbc](https://gitlab.com/lmco/hoppr/hoppr/commit/b5b9bbc49f773b37f961234a14473b34937d5c31))

## [0.17.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.16.0...v0.17.0) (2022-06-06)


### Features

* add maven copier ([062af40](https://gitlab.com/lmco/hoppr/hoppr/commit/062af405b3ee815377d5191557a4d903d2e51717))


### Bug Fixes

* changed name of test component from 'Bob' to 'TestComponent', for Eric ([1ad4190](https://gitlab.com/lmco/hoppr/hoppr/commit/1ad41900a4bb5a644b21f5dc3f52159047d032e5))
* linting issues ([f041c16](https://gitlab.com/lmco/hoppr/hoppr/commit/f041c16fcf3088aedae09d714fd32e2f12e43145))
* merge from main, update helm collector for run_command ([af6051c](https://gitlab.com/lmco/hoppr/hoppr/commit/af6051cd63e8f21696697e19ebbb3624d367a720))
* use credentials (when appropriate) in maven-plugin, add test files ([28ebe3f](https://gitlab.com/lmco/hoppr/hoppr/commit/28ebe3fcd80b5614bd1904ea09cae9f0a3f70f18))

## [0.16.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.6...v0.16.0) (2022-06-02)


### Features

* helm collector plugin ([ea0a8f7](https://gitlab.com/lmco/hoppr/hoppr/commit/ea0a8f7e843539229b8d4785cf241dadaa0a2324))


### Bug Fixes

* resolving merge request threads ([2851815](https://gitlab.com/lmco/hoppr/hoppr/commit/2851815dd48d76d17ea9c1e6871fa9e602d37e56))
* updated pylint version ([e090888](https://gitlab.com/lmco/hoppr/hoppr/commit/e090888ebfe39e8b35c86bacd78b65689f69891a))

## [0.15.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.5...v0.15.6) (2022-06-01)

## [0.15.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.4...v0.15.5) (2022-06-01)


### Bug Fixes

* Update taxonomy reference in navigation layout ([5fe0399](https://gitlab.com/lmco/hoppr/hoppr/commit/5fe03992158049a7a9377de4711196c1d656764c))

## [0.15.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.3...v0.15.4) (2022-05-31)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.3.2 ([1f8f020](https://gitlab.com/lmco/hoppr/hoppr/commit/1f8f0201372b712105060d59d46adccf9bd4fb13))

## [0.15.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.2...v0.15.3) (2022-05-29)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.3.1 ([0a3e506](https://gitlab.com/lmco/hoppr/hoppr/commit/0a3e506bf6831ab18a809cb624034b28641cf22a))

## [0.15.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.1...v0.15.2) (2022-05-27)


### Bug Fixes

* **deps:** update dependency conventional-changelog-conventionalcommits to v5 ([96de6d3](https://gitlab.com/lmco/hoppr/hoppr/commit/96de6d34134c95b35c79116fd30a6a20bd865bd7))

### [0.15.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.15.0...v0.15.1) (2022-05-26)


### Bug Fixes

* created a Credentials Object for ease of use ([662d434](https://gitlab.com/lmco/hoppr/hoppr/commit/662d434601beafcff187b0db41bd6f65a240cb33))
* merge from main ([401d93d](https://gitlab.com/lmco/hoppr/hoppr/commit/401d93d551fea1caa2ef0bf67bd897a85b6710e9))
* small edits on pypi collector for CredObject ([85929e9](https://gitlab.com/lmco/hoppr/hoppr/commit/85929e91c873e3ceb71aaf33cadc15c4b6910f76))

## [0.15.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.14.0...v0.15.0) (2022-05-26)


### Features

* add pypi collector, bug fixes, cleanup ([09c7fd8](https://gitlab.com/lmco/hoppr/hoppr/commit/09c7fd8ff85e33fe39a8e28b0ef4e946d7d491f6))


### Bug Fixes

* merge with main, clean-up purl check in git ([da27f11](https://gitlab.com/lmco/hoppr/hoppr/commit/da27f115f653c88dd05e15beb049532b901e527a))
* remove setuptoos installation -- not needed ([b9f3e72](https://gitlab.com/lmco/hoppr/hoppr/commit/b9f3e72097a47e5a1980bccd78aa72c7d44e3d8e))
* Return RETRY rather than FAIL on docker failure ([3634361](https://gitlab.com/lmco/hoppr/hoppr/commit/363436165e1f797008e5d31ec47da3cd00814cfc))

## [0.14.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.6...v0.14.0) (2022-05-25)


### Features

* git collector plugin ([30532c8](https://gitlab.com/lmco/hoppr/hoppr/commit/30532c841488ef81c483062b542c11981f448c26))


### Bug Fixes

* git collector bundling ([a3cf4e2](https://gitlab.com/lmco/hoppr/hoppr/commit/a3cf4e25bbedc4bc3fe5d067d6bea522b5ebb8d7))

### [0.13.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.5...v0.13.6) (2022-05-25)


### Bug Fixes

* Correct syntax ([ea67974](https://gitlab.com/lmco/hoppr/hoppr/commit/ea679741b2faaabd909cb1e71a8e0fd822cfae64))
* Correct the artifacts Report section ([c3890c6](https://gitlab.com/lmco/hoppr/hoppr/commit/c3890c685ab06fe119094897dcf9a8476fea330b))

### [0.13.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.4...v0.13.5) (2022-05-23)


### Bug Fixes

* improved unit test & code coverage ([1e3aca1](https://gitlab.com/lmco/hoppr/hoppr/commit/1e3aca15488e16d2e0eb354be9a3b78e4e924a5e))

### [0.13.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.3...v0.13.4) (2022-05-23)


### Bug Fixes

* initial integration tests, minor fixes ([da26260](https://gitlab.com/lmco/hoppr/hoppr/commit/da26260c817722738e9442cbe45804370c4c2197))
* Merge branch 'man' into eliminate-prototype-code ([e0887c8](https://gitlab.com/lmco/hoppr/hoppr/commit/e0887c8cc39e45171215d4042d218e70971451e4))
* remove print statements ([dd1d85d](https://gitlab.com/lmco/hoppr/hoppr/commit/dd1d85de3d047fc5df07ddca3b57e7e2656a12c7))
* start using credentials from credentials.config, rather than kludges ([541d906](https://gitlab.com/lmco/hoppr/hoppr/commit/541d90623e1af7a218e151cb77ee830174b470dd))
* use actual manifest, flattened sbom ([62c4552](https://gitlab.com/lmco/hoppr/hoppr/commit/62c455234583d2b6b53d84331599378e6f565eff))
* use typer.echo instead of print ([5bf306c](https://gitlab.com/lmco/hoppr/hoppr/commit/5bf306c5a719dca13e531f40135107595c453d5b))

### [0.13.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.2...v0.13.3) (2022-05-18)


### Bug Fixes

* added back generic-manifest-child, fixed tests to reflect it ([d64a524](https://gitlab.com/lmco/hoppr/hoppr/commit/d64a524a18c241191a6426df438996324a36964c))
* code clean-up, test repo property creation ([fd027ab](https://gitlab.com/lmco/hoppr/hoppr/commit/fd027abda4787a30374b055ffd791d48d29ba458))
* comment-out a couple of asserts until generic-child is checked in ([576ac9c](https://gitlab.com/lmco/hoppr/hoppr/commit/576ac9c5164a508edd30063e83c677d995a04560))
* dedup input repositories ([22e639d](https://gitlab.com/lmco/hoppr/hoppr/commit/22e639d1a71145c749787b0a335f0c842b505a19))
* fixed unit tests for flattening ([3f07472](https://gitlab.com/lmco/hoppr/hoppr/commit/3f07472598236864ebfc54ea12663b518c2df177))
* Implement ADR 22, refactoring manifest to use dict of purl_types for repositories ([5f01e6b](https://gitlab.com/lmco/hoppr/hoppr/commit/5f01e6b4e62524b38939b009c8edf2962e6cba06))
* merge from main ([beb5260](https://gitlab.com/lmco/hoppr/hoppr/commit/beb52600dcde6e1bce3c33fcae9802b2ad71902a))
* merge with main ([b82a8e8](https://gitlab.com/lmco/hoppr/hoppr/commit/b82a8e81dde0acc716049f0213a2222a6e003670))
* MR cleanup ([ad00b06](https://gitlab.com/lmco/hoppr/hoppr/commit/ad00b062c39cc2c0e73dd9c6efae99e6d594281b))
* mypy typing issue, reused variable name kept old type ([96134a2](https://gitlab.com/lmco/hoppr/hoppr/commit/96134a287e8aff528f7851159ce4ce16bbab4f56))
* update collector.py to use new manifest_file_content repositories structure ([23f557d](https://gitlab.com/lmco/hoppr/hoppr/commit/23f557d5b32ebb22bc18e01f1ed05bf2e3de4e76))
* update components to current BOM version ([3f1299f](https://gitlab.com/lmco/hoppr/hoppr/commit/3f1299f572a908ffb47edc757da81bf69f8a258b))

### [0.13.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.1...v0.13.2) (2022-05-16)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.3.0 ([1084a72](https://gitlab.com/lmco/hoppr/hoppr/commit/1084a72aba822d5a731f9d6431c041d66736fde7))

### [0.13.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.13.0...v0.13.1) (2022-05-12)


### Bug Fixes

* merge from main ([3bea9c1](https://gitlab.com/lmco/hoppr/hoppr/commit/3bea9c1599ef2622782a197eef76a4ac5be430d9))

## [0.13.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.12.2...v0.13.0) (2022-05-11)


### Features

* add docker collector ([470e002](https://gitlab.com/lmco/hoppr/hoppr/commit/470e00299f070ff13f646e838ddc4c72eb152b84))

### [0.12.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.12.1...v0.12.2) (2022-05-11)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.2.1 ([2316121](https://gitlab.com/lmco/hoppr/hoppr/commit/23161215794d83423679290151be4e32e6068776))

### [0.12.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.12.0...v0.12.1) (2022-05-11)


### Bug Fixes

* **deps:** update dependency semantic-release-slack-bot to v3.5.3 ([2879571](https://gitlab.com/lmco/hoppr/hoppr/commit/2879571609b5d585c4fb2462ffe2cdf8cda2e706))

## [0.12.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.11.1...v0.12.0) (2022-05-09)


### Features

* add tar bundler plug-in ([dc0025f](https://gitlab.com/lmco/hoppr/hoppr/commit/dc0025fe0ffd441a52881e1a8f3c728a121c2e20))


### Bug Fixes

* add timetag to bundle file name if it already exists ([97ea4ff](https://gitlab.com/lmco/hoppr/hoppr/commit/97ea4ffaa0e6c911085ca689debe1566e1bdfa79))
* merged from main ([48a597a](https://gitlab.com/lmco/hoppr/hoppr/commit/48a597a3959e6dd703187dedb993a27c14c7c1e1))
* remove 'tryit' app from hopctl ([21a01e9](https://gitlab.com/lmco/hoppr/hoppr/commit/21a01e95f207b9e2094aef072f89d228b2e1a5b1))
* remove base_test_plugin and all dependencies ([3dc715a](https://gitlab.com/lmco/hoppr/hoppr/commit/3dc715a7f0599f221a49a04fa836696d77fa062e))
* update syntax for cobertura report for updated gitlab ([d335aef](https://gitlab.com/lmco/hoppr/hoppr/commit/d335aefb011ea6837f3a4a524ec7bbf377ea7791))

### [0.11.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.11.0...v0.11.1) (2022-05-03)


### Bug Fixes

* add sboms to manifests. ([63bd76a](https://gitlab.com/lmco/hoppr/hoppr/commit/63bd76af34e30e65f0d81bc7ddb4964f7d496e0e))
* added Bom versions 1.3 and 1.4 to manifest.py ([2390ed4](https://gitlab.com/lmco/hoppr/hoppr/commit/2390ed421b392b03fce3181b3eabcfe687b5cfc1))
* load_sbom for local and urls; basic auth only ([4c215f7](https://gitlab.com/lmco/hoppr/hoppr/commit/4c215f779ba195dde5790f41681ba98b328c52c4))
* pre-commit errors ([2eed063](https://gitlab.com/lmco/hoppr/hoppr/commit/2eed063cdf644bd3e083f2d622b1a55625fdab3e))
* recursive child Manifests ([89b19a5](https://gitlab.com/lmco/hoppr/hoppr/commit/89b19a5f5c6202f5fb0221397f54eacdef15fc33))

## [0.11.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.10.3...v0.11.0) (2022-05-03)


### Features

* adding pydantic modesl for cyclone dx ([6972248](https://gitlab.com/lmco/hoppr/hoppr/commit/69722484e7fce78b43fc28956a4ba8b8f6b2d0e4))
* initial processor classes ([2e10f42](https://gitlab.com/lmco/hoppr/hoppr/commit/2e10f4288c2184fcb6d4ebba1b8a730e20a46932))


### Bug Fixes

* Add summary output, comments, result merge method ([95cee10](https://gitlab.com/lmco/hoppr/hoppr/commit/95cee10be3b72c750a6d6860ed003523a46b0ace))
* add unit tests for processor classes ([22610f8](https://gitlab.com/lmco/hoppr/hoppr/commit/22610f8d606481a49dbcd1c9b9d2ba8ea8800c71))
* cleanup ([0481b13](https://gitlab.com/lmco/hoppr/hoppr/commit/0481b13eba15dc3133f1b36926fc4cddf4c28733))
* code review comments, also fixed unit tests to ignore cyclonedx files ([65d5a1a](https://gitlab.com/lmco/hoppr/hoppr/commit/65d5a1a62a06ba12a1ea402678d84d02be57fcaa))
* Correct regex for semantic-release ([d8cae8a](https://gitlab.com/lmco/hoppr/hoppr/commit/d8cae8aaea43225694ced137db33bcd088c26d9f))
* Correct the init py regex ([954b891](https://gitlab.com/lmco/hoppr/hoppr/commit/954b8918e7a08f015b449aa1b2d76a1db93672b2))
* linter issues ([a97d707](https://gitlab.com/lmco/hoppr/hoppr/commit/a97d707ee2effe892ddfc5f3635b0a4ef7c9ebf4))
* linting ([a879623](https://gitlab.com/lmco/hoppr/hoppr/commit/a879623bcafad6b318a2bd546d1d3e832f9ca75b))
* Merge conflicts ([3dbc9a2](https://gitlab.com/lmco/hoppr/hoppr/commit/3dbc9a219aff0533c0b1ad6905844554eaf1948f))
* merge from main ([ab9afe9](https://gitlab.com/lmco/hoppr/hoppr/commit/ab9afe929e453bfb41a383db608c0454e32a961f))
* missed merge conflict, unit test update ([cfcea50](https://gitlab.com/lmco/hoppr/hoppr/commit/cfcea50be2b6f8653e6ca404beab26309ba42fb6))
* renamed sub-stage methods ([f778db5](https://gitlab.com/lmco/hoppr/hoppr/commit/f778db52f28b502723e8653b8ba4c82193b9626b))
* resolve conflict, merge branch 'main' into 'create-hopctl-bundle-command' ([f314995](https://gitlab.com/lmco/hoppr/hoppr/commit/f3149952836f060f18d54854a8aea4494b66d625))
* revert to f-string formatting for log messages, to support future logger wrapping ([4f6335e](https://gitlab.com/lmco/hoppr/hoppr/commit/4f6335e128eabfcbffd70ff0796e5781ec1893d9))
* switch to concurrent.futures for multi-processing ([92e773b](https://gitlab.com/lmco/hoppr/hoppr/commit/92e773b5b3c02070a50c72c9088acb470974f823))
* trailing whitespace ([6c346bd](https://gitlab.com/lmco/hoppr/hoppr/commit/6c346bdf665f1120ab23a964c1e2300b84dd7fb9))
* use pydantic sbom definitions rather than cyclonedx ([bf18e06](https://gitlab.com/lmco/hoppr/hoppr/commit/bf18e066857556901fd191ad3358bb795d04a960))

### [0.10.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.10.2...v0.10.3) (2022-04-28)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.2.0 ([7c487ec](https://gitlab.com/lmco/hoppr/hoppr/commit/7c487ec6ea5d5a9f9b35a12de422322569c82045))

### [0.10.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.10.1...v0.10.2) (2022-04-28)


### Bug Fixes

* **deps:** update dependency click to v8.1.3 ([3d021df](https://gitlab.com/lmco/hoppr/hoppr/commit/3d021dfb1d2d1feec9163abf7f718724cc53b514))

### [0.10.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.10.0...v0.10.1) (2022-04-26)

## [0.10.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.9.2...v0.10.0) (2022-04-26)


### Features

* create collect_raw_plugin ([467fa79](https://gitlab.com/lmco/hoppr/hoppr/commit/467fa79bee5e957e52ee36be2a33324690ffa4eb))


### Bug Fixes

* add exception handling to hoppr_plugin decorator ([d272eba](https://gitlab.com/lmco/hoppr/hoppr/commit/d272ebac2cea6b15488f1eb1264fe49693f85606))
* code review comments ([fbdf1f8](https://gitlab.com/lmco/hoppr/hoppr/commit/fbdf1f89101913341e2c954e85d1f7c085739f3a))

### [0.9.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.9.1...v0.9.2) (2022-04-20)


### Bug Fixes

* **deps:** pin dependency semantic-release-conventional-commits to 3.0.0 ([9c64953](https://gitlab.com/lmco/hoppr/hoppr/commit/9c6495392ecf05392bbd72bf2b0fef27cf0c7065))

### [0.9.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.9.0...v0.9.1) (2022-04-20)


### Bug Fixes

* **docs:** added JSON Schema locations ([a8d7902](https://gitlab.com/lmco/hoppr/hoppr/commit/a8d79027b983d41d4bd6873cdf4d8f522c281276))
* **docs:** Documenting definitions and input files ([94d4e62](https://gitlab.com/lmco/hoppr/hoppr/commit/94d4e6265a7b545c889d5478ad3a3d297b2144c7))
* Add documentation to specify MVP purl support ([8d7b822](https://gitlab.com/lmco/hoppr/hoppr/commit/8d7b822128f1332ff6ad564d701154c35aa43884))
* Correct semantic versioning ([a28dfb1](https://gitlab.com/lmco/hoppr/hoppr/commit/a28dfb18277c6aec0ddaca9d19dd3ec4436b59f9))
* Tweak docs ([01d497c](https://gitlab.com/lmco/hoppr/hoppr/commit/01d497c5525516d08ba75e81bb4993f7226076b9))
* Update mkdocs ([a53c5e4](https://gitlab.com/lmco/hoppr/hoppr/commit/a53c5e4937efed771c425e1b0dad6645c5117416))
* Update schema doc ([1b49040](https://gitlab.com/lmco/hoppr/hoppr/commit/1b49040eb6c857bf6b6acc82bb3f88b45e60d1ba))

## [0.9.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.8.0...v0.9.0) (2022-04-20)


### Features

* test_transfer.py unit tests ([0c19aab](https://gitlab.com/lmco/hoppr/hoppr/commit/0c19aababaeed3b9d08594049a87063f667f8103))
* transfer_file_content and transfer classes ([10ff023](https://gitlab.com/lmco/hoppr/hoppr/commit/10ff0231a32b41266eeb6c8f8b3235861c98f0c7))


### Bug Fixes

* transfer_file_content class and description update ([6d30e76](https://gitlab.com/lmco/hoppr/hoppr/commit/6d30e761a0294fe2169c8032746e137e8dfc7f36))

## [0.8.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.4...v0.8.0) (2022-04-18)


### Features

* add initial manifest config. ([40f955d](https://gitlab.com/lmco/hoppr/hoppr/commit/40f955dfe96fd4f6d60df2cb0d2d114b4810c5f9))
* complete first part of manifest config and add command in main. ([efe3c99](https://gitlab.com/lmco/hoppr/hoppr/commit/efe3c9991f71ebbebcdf1e6076f346ad00ad333d))
* updates to manifest config. ([4866441](https://gitlab.com/lmco/hoppr/hoppr/commit/4866441b77e8b51e93eef141262c472d008eff22))


### Bug Fixes

* Add dependencies ([b7e2a00](https://gitlab.com/lmco/hoppr/hoppr/commit/b7e2a001fe5f7f6d8d2b95b44bffef05d433624b))
* Add dependencies ([b2cd88e](https://gitlab.com/lmco/hoppr/hoppr/commit/b2cd88e021d0f14a4a6573ec6bd0e06f680cee1d))
* Add in bom type and demo ([8ae7603](https://gitlab.com/lmco/hoppr/hoppr/commit/8ae7603e9b628ac847071be2d5ba1a1963c83dfc))
* Add py.typed ([bb25e49](https://gitlab.com/lmco/hoppr/hoppr/commit/bb25e49e08b2c3753e0edf8f644970202a9f3d36))
* Add types-PyYAML to dependencies ([5ee67ff](https://gitlab.com/lmco/hoppr/hoppr/commit/5ee67ff1f9a6910f8d04e461512e148ae3095225))
* cleared pre-commit errors/warnings ([6608b5c](https://gitlab.com/lmco/hoppr/hoppr/commit/6608b5cd71212c40010eb1315f6ef0170ad936e2))
* Correct poetry black command ([cdb218e](https://gitlab.com/lmco/hoppr/hoppr/commit/cdb218e480fd2d316c74abb0b4789fd2f2b21629))
* Create manifest type and validate schema ([614e097](https://gitlab.com/lmco/hoppr/hoppr/commit/614e09766074a7106a70cf5e5ce04e44af7bb6df))
* fix poetry.lock merge. ([dae0fe7](https://gitlab.com/lmco/hoppr/hoppr/commit/dae0fe7c651f631da4bab8c05ff2cb77b189e891))
* manifest can now be loaded ([52d5a4e](https://gitlab.com/lmco/hoppr/hoppr/commit/52d5a4e00053dd3bcf72ff6821c18374f823b7d9))
* pipeline errors ([0d699d0](https://gitlab.com/lmco/hoppr/hoppr/commit/0d699d0b4941197dc9c2a820ee0e0c615914293f))
* pre-commit errors (except hoppr/hoppr_types/bom_type.py) ([392dc36](https://gitlab.com/lmco/hoppr/hoppr/commit/392dc36cd58bb4bb9007de733842bbab908f3071))
* Remove utils ([114a16e](https://gitlab.com/lmco/hoppr/hoppr/commit/114a16e15597973fead5b6491b96f560ee439956))
* repaired unit tests ([776c181](https://gitlab.com/lmco/hoppr/hoppr/commit/776c1814213c183906977b7158e6d9b6babcafd9))
* Represent possible looping concept ([7cc3130](https://gitlab.com/lmco/hoppr/hoppr/commit/7cc313024ac6c722b1aefbb012d5de365f7b911f))
* Run black on hoppr ([75e418d](https://gitlab.com/lmco/hoppr/hoppr/commit/75e418dae2836fd9210217ea9c64ace384922097))
* Run poetry black and reformat ([adaebf8](https://gitlab.com/lmco/hoppr/hoppr/commit/adaebf8a38b7c7820c71ef86c151cefa3233e78b))
* Update components ([fc50d55](https://gitlab.com/lmco/hoppr/hoppr/commit/fc50d5524786272d15ce53437fd16203f2562e76))
* Update poetry ([1c5993a](https://gitlab.com/lmco/hoppr/hoppr/commit/1c5993a76982f0c97cc79cd3a136127930c290c2))
* update poetry.lock ([9e00c64](https://gitlab.com/lmco/hoppr/hoppr/commit/9e00c64cdda704356087bff98878585a789c1413))
* updated CredentialRequiredService to match credential schema ([3aba796](https://gitlab.com/lmco/hoppr/hoppr/commit/3aba7969179d8cbbbb41fe13741d57d7b5e533d1))
* Use decorator ([dbdebb0](https://gitlab.com/lmco/hoppr/hoppr/commit/dbdebb0774e6fd6bc6f6020badc635a0de13732d))

### [0.7.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.3...v0.7.4) (2022-04-18)


### Bug Fixes

* Add py.typed file ([d09ce03](https://gitlab.com/lmco/hoppr/hoppr/commit/d09ce036b00fca195cf7ebdf011116994721675a))

### [0.7.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.2...v0.7.3) (2022-04-15)


### Bug Fixes

* Update pyproject.tml from 3.9 to 3.10 ([6d1e712](https://gitlab.com/lmco/hoppr/hoppr/commit/6d1e712b3b6af0d1a0016e5a46f7ac3194026afa))

### [0.7.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.1...v0.7.2) (2022-04-14)


### Bug Fixes

* **deps:** update dependency click to v8.1.2 ([9239be0](https://gitlab.com/lmco/hoppr/hoppr/commit/9239be0c999e1ceeada5cd1b78bf29fa9b3bf818))

### [0.7.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.7.0...v0.7.1) (2022-04-14)


### Bug Fixes

* Add in __init__.py and correct linter findings ([fd3361d](https://gitlab.com/lmco/hoppr/hoppr/commit/fd3361d324bd5981890d19f7a18ccaf0366c88a2))
* Add json sbom parser ([6999259](https://gitlab.com/lmco/hoppr/hoppr/commit/69992593b03049fdb1586403609b2b9b3e5c0557))
* Add parser ([0d9be9b](https://gitlab.com/lmco/hoppr/hoppr/commit/0d9be9b85d995ca66ccc9dc259ae966d7531ae90))
* Remove pandas and use dateutil ([a8fc215](https://gitlab.com/lmco/hoppr/hoppr/commit/a8fc215aaaab2be8adf79592f7bf3aaa13da0d29))

## [0.7.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.6.2...v0.7.0) (2022-04-14)


### Features

* add base class for collectors ([f66ef26](https://gitlab.com/lmco/hoppr/hoppr/commit/f66ef26ec89fa9e7a5db39504de8cfa2cf810ee3))


### Bug Fixes

* missed uploading renamed files ([18405ac](https://gitlab.com/lmco/hoppr/hoppr/commit/18405acd7f8919990ffe90764998e891c1d45d3a))
* removed debug statement ([e448ecb](https://gitlab.com/lmco/hoppr/hoppr/commit/e448ecb0083ef68e5058d4780173f887d02080ac))
* rename plugin base class modules to remove redundant '_plugin' ([44d612a](https://gitlab.com/lmco/hoppr/hoppr/commit/44d612a45496ed658010aa8648a2f88896c3ffc1))

### [0.6.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.6.1...v0.6.2) (2022-04-14)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.1.2 ([898f0d2](https://gitlab.com/lmco/hoppr/hoppr/commit/898f0d208b503ced05b3f48729f1b895686aab25))

### [0.6.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.6.0...v0.6.1) (2022-04-13)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.1.1 ([2c90a63](https://gitlab.com/lmco/hoppr/hoppr/commit/2c90a6385b95a5a3d80fce095d6c83ecd163e80e))

## [0.6.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.13...v0.6.0) (2022-04-12)


### Features

* add class for tracking state between plug-ins/stages ([6987ac6](https://gitlab.com/lmco/hoppr/hoppr/commit/6987ac6651419e93349f701c6e8adbfb55fbb128))
* merged initialize method into __init__, replaced if with match/case, removed thread-saftey form base class ([c194648](https://gitlab.com/lmco/hoppr/hoppr/commit/c194648e95b008ba12e0268dac90ece127e1b829))
* upgrade base classes to support staged architecture ([4fae3de](https://gitlab.com/lmco/hoppr/hoppr/commit/4fae3de78cbccbe357636280ac0a5def50b35941))


### Bug Fixes

* add retry logic to base hoppr_plugin ([547e6f7](https://gitlab.com/lmco/hoppr/hoppr/commit/547e6f7ade6d6ab138cd7b1fd247c628ff572c49))
* comments on plug-in base classes ([e38e781](https://gitlab.com/lmco/hoppr/hoppr/commit/e38e781cadebfe41fd6f3ba56366d9079d69cb75))
* convert _pre_ and _post_operation to a decorator (hoppr_process) ([415dfd0](https://gitlab.com/lmco/hoppr/hoppr/commit/415dfd0dd14a0fc3cf271ae0f4d7efdb3428aa43))
* finished updating decorators ([29cf43f](https://gitlab.com/lmco/hoppr/hoppr/commit/29cf43fb81977c4e969307c76025e7610d1baed3))
* include state class ([2455473](https://gitlab.com/lmco/hoppr/hoppr/commit/2455473f6c4962744ba211b14c11ae4e0e3a74df))
* moved methods less likely to be overridden to the bottom of the file ([ae1dd41](https://gitlab.com/lmco/hoppr/hoppr/commit/ae1dd41d2418e96f2e410cc9668609f63f9331df))
* pass state into execute/reverse methods in base classes ([c99f70d](https://gitlab.com/lmco/hoppr/hoppr/commit/c99f70d1790839b5c77a1557e2d0ef576cdf3239))
* re-ran black ([790e655](https://gitlab.com/lmco/hoppr/hoppr/commit/790e65536b337c47cc32e78727775a8d6f3a0d47))
* refactor base classes, replace state with context ([8718169](https://gitlab.com/lmco/hoppr/hoppr/commit/8718169fff65ae54aa64cf5b316797bc310e59e2))
* refactor base classes, replace state with context ([ea6aeef](https://gitlab.com/lmco/hoppr/hoppr/commit/ea6aeef9eb521ac7f8bf4aa03c55301fe9774f18))
* Remove unused exception type ([7b79064](https://gitlab.com/lmco/hoppr/hoppr/commit/7b790649028ae72486c98fbbe1bbd5905157a615))
* unit test ([c2a8038](https://gitlab.com/lmco/hoppr/hoppr/commit/c2a8038203892048651399901c2996095e391cce))

### [0.5.13](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.12...v0.5.13) (2022-04-12)


### Bug Fixes

* Add label ([83e2b82](https://gitlab.com/lmco/hoppr/hoppr/commit/83e2b8227364949d94659c4279a49775a39c93e0))

### [0.5.12](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.11...v0.5.12) (2022-04-11)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.1.0 ([0ee37c0](https://gitlab.com/lmco/hoppr/hoppr/commit/0ee37c04fd99a29f3a3d4c9cdcf8e90e84a02cbb))

### [0.5.11](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.10...v0.5.11) (2022-04-09)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9 ([854c7a4](https://gitlab.com/lmco/hoppr/hoppr/commit/854c7a443ea2e22e80af1433584a7b47e9e63f2f))

### [0.5.10](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.9...v0.5.10) (2022-04-05)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v8.1.0 ([c664047](https://gitlab.com/lmco/hoppr/hoppr/commit/c664047dd9235d8392fc3d1ddef279999c9dd77b))

### [0.5.9](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.8...v0.5.9) (2022-03-31)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v8.0.2 ([8d088e0](https://gitlab.com/lmco/hoppr/hoppr/commit/8d088e07682b238d184afd57751c896d09babcde))

### [0.5.8](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.7...v0.5.8) (2022-03-30)


### Bug Fixes

* versions not reflected in __init__.py or pyporject.toml ([142cd0d](https://gitlab.com/lmco/hoppr/hoppr/commit/142cd0dffe4e76ab0e105a0c4da152f0d39bda3d))

### [0.5.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.6...v0.5.7) (2022-03-29)


### Bug Fixes

* pre-commit updated to use local poetry ([ea76c18](https://gitlab.com/lmco/hoppr/hoppr/commit/ea76c18f20a9aa1c1dced62398663f5a2ebcee58))

### [0.5.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.5...v0.5.6) (2022-03-29)


### Bug Fixes

* **dep:** forced click to downgrade to resolve dependency issue with typer ([a26a8ec](https://gitlab.com/lmco/hoppr/hoppr/commit/a26a8ec91f77a793822278ce03476800f68c2682))
* **docs:** Adding details about semantic versioning and where pypi packages are generated ([02f6028](https://gitlab.com/lmco/hoppr/hoppr/commit/02f6028a7021dd948f2b6b5946e69da781e62616))

### [0.5.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.4...v0.5.5) (2022-03-24)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v8.0.1 ([3580dd2](https://gitlab.com/lmco/hoppr/hoppr/commit/3580dd20b2403d9a989d4e0d6dd3074a80f2efd0))
* add initial schemas (until we have a permanent place for them) ([cf193eb](https://gitlab.com/lmco/hoppr/hoppr/commit/cf193eb05cd3b0ad513961ef504881949f516504))
* add utilities for reading json/yml config files ([3dded41](https://gitlab.com/lmco/hoppr/hoppr/commit/3dded41a438c74f54e45704d1f8342f14fd743ab))

### [0.5.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.3...v0.5.4) (2022-03-24)


### Bug Fixes

* **deps:** update dependency @semantic-release-plus/docker to v3.1.2 ([19ca2a9](https://gitlab.com/lmco/hoppr/hoppr/commit/19ca2a9f085dab7fb1bb1c5f51c492a5f869053d))

### [0.5.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.2...v0.5.3) (2022-03-22)


### Bug Fixes

* add extends to publish-whl ([2f50905](https://gitlab.com/lmco/hoppr/hoppr/commit/2f50905116be925c5a66153cb5fbe15086b99c82))
* get correct RELEASE_VERSION ([0a3dc2d](https://gitlab.com/lmco/hoppr/hoppr/commit/0a3dc2d7d473a6e17643153fc68cdc25a6f102e4))
* remove changes for testing ([34f729f](https://gitlab.com/lmco/hoppr/hoppr/commit/34f729f4b714accb9e9132ac4d58721ce688c913))

### [0.5.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.1...v0.5.2) (2022-03-22)


### Bug Fixes

* **dev:** updates to MR comments to be understandable sentences ([ceedf1f](https://gitlab.com/lmco/hoppr/hoppr/commit/ceedf1fb7d247aedd14fd4237dd2a46af3123521))

### [0.5.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.5.0...v0.5.1) (2022-03-22)


### Bug Fixes

* **dev:** Updated Issue and MR templates ([0fd8df8](https://gitlab.com/lmco/hoppr/hoppr/commit/0fd8df82fda0156528b852c3a5a7e0be710f82ce))

## [0.5.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.7...v0.5.0) (2022-03-22)


### Features

* Add loggers to plugins, add base class for all plugins ([2bf2d5f](https://gitlab.com/lmco/hoppr/hoppr/commit/2bf2d5f3d269cca7cf4504b5a4f12f9d65891c92))
* define bundler base class ([4f72003](https://gitlab.com/lmco/hoppr/hoppr/commit/4f720032b3b2939eef77f9a671d34a1f510016bc))
* Rename hoppr directory to hopctl ([42bb930](https://gitlab.com/lmco/hoppr/hoppr/commit/42bb93084adbc19ae03177bdf24db04cbd14a113))


### Bug Fixes

* Add methods to plugin_utils to check for required system commands ([0e869ee](https://gitlab.com/lmco/hoppr/hoppr/commit/0e869ee8f6056fbe3e116da08f2a92f80b548135))
* Added factory method to Result object to build from requests.Result object ([fc54269](https://gitlab.com/lmco/hoppr/hoppr/commit/fc5426907feab64d8e23bd9f247b0df6eb7d234b))
* added rules to publish-whl job to match semantic-release ([ff9000d](https://gitlab.com/lmco/hoppr/hoppr/commit/ff9000dac1ff89ad1eee94754a9f23f8cc735bf8))
* Install mypy types in pipeline ([136fc16](https://gitlab.com/lmco/hoppr/hoppr/commit/136fc16f17b50db39834c6b54523de069e39b1b9))
* Merge branch 'main' into plugin-base-class-refinement ([4728edb](https://gitlab.com/lmco/hoppr/hoppr/commit/4728edbed59c1e5a9f9cd946d495b4a4f02ad618))
* need separate stage for publish-whl job ([7c048d9](https://gitlab.com/lmco/hoppr/hoppr/commit/7c048d9199884c52c3c2b324b424b2b13844b842))
* poetry.lock file error ([056d255](https://gitlab.com/lmco/hoppr/hoppr/commit/056d25550cb6c085e18f01c784281901660f466c))
* poetry.lock syntax ([2dfc865](https://gitlab.com/lmco/hoppr/hoppr/commit/2dfc8656f86d41b3f55551291e2e09622af97ff8))
* publish whl file to gitlab registry ([dd17a90](https://gitlab.com/lmco/hoppr/hoppr/commit/dd17a9059ab8105ce80affeff5c519ed6c0c1f2b))
* reverted directory structure to hoppr/hoppr/hoppr ([39e9678](https://gitlab.com/lmco/hoppr/hoppr/commit/39e9678f0aa1e9223aab4c5ad830dc815515a32d))
* specify file for mypy --install-types ([d65a9ca](https://gitlab.com/lmco/hoppr/hoppr/commit/d65a9ca9e90fbb9e4677478ab2c2b3f449553892))
* **test:** made a unit test error message more enterprise-y ([2b128fe](https://gitlab.com/lmco/hoppr/hoppr/commit/2b128fee6090adcc757c82ae735a0adfd0d0ee92))
* **test:** replaced unused plugin name with "plugin_stub_name" ([7d8826d](https://gitlab.com/lmco/hoppr/hoppr/commit/7d8826d6198c2a6cef25cad3e80d2fc753bf397f))
* **test:** replaced unused test plugin name with plugin_stub_name ([1291408](https://gitlab.com/lmco/hoppr/hoppr/commit/1291408210561da0911b7070f2bcf873c4644136))
* **utils:** Split out plugin_utils to a separate module ([9e97dd1](https://gitlab.com/lmco/hoppr/hoppr/commit/9e97dd1ed7a214abbcb0ea76f6dea6fc2234fc2e))
* update pyproject.tml to correctly handle hopctl directory ([82871d3](https://gitlab.com/lmco/hoppr/hoppr/commit/82871d3189ffc600547288bc62e90c1f8ca66b74))

### [0.4.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.6...v0.4.7) (2022-03-21)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v8 ([152ba68](https://gitlab.com/lmco/hoppr/hoppr/commit/152ba68808494a1facee207b080c950b8babc62a))

### [0.4.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.5...v0.4.6) (2022-03-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v7.2.1 ([7516cd4](https://gitlab.com/lmco/hoppr/hoppr/commit/7516cd4e0428a058347b0323b27b049d43a0855f))

### [0.4.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.4...v0.4.5) (2022-03-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v7.2.0 ([01946c5](https://gitlab.com/lmco/hoppr/hoppr/commit/01946c5811819d978ee3de09453aceb1be016e90))

### [0.4.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.3...v0.4.4) (2022-03-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v7.1.1 ([9e3bce5](https://gitlab.com/lmco/hoppr/hoppr/commit/9e3bce5f2e684627f1e84ce0b7c8373fe1c83db2))

### [0.4.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.2...v0.4.3) (2022-03-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v7.1.0 ([2c53c00](https://gitlab.com/lmco/hoppr/hoppr/commit/2c53c007c120b99f661e87ef61b4a7f77b5592bd))

### [0.4.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.4.1...v0.4.2) (2022-03-17)


### Bug Fixes

* **dev:** added link to pre commit config file ([d611e26](https://gitlab.com/lmco/hoppr/hoppr/commit/d611e26dfe34245263654e7c38d6980a88c2ba99))
* **dev:** moved pre-commit to be listed under dev-dependencies ([8db57ee](https://gitlab.com/lmco/hoppr/hoppr/commit/8db57eec6bbdf9e6f9277950119ed179342c2690))

### [0.4.1](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/lmco/hoppr/hoppr/compare/v0.4.0...v0.4.1) (2022-03-15)


### Bug Fixes

* Corrected error when abstract methods not implemented ([b6b487d](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/lmco/hoppr/hoppr/commit/b6b487d57f75c68677f783a34b955abba5d6ced8))

## [0.4.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.7...v0.4.0) (2022-03-14)


### Features

* Initial 'hello-world' plugin POC ([546ecbe](https://gitlab.com/lmco/hoppr/hoppr/commit/546ecbeec464d66afb401101f44c007224deb7de))


### Bug Fixes

* Add mypy type checker to pipeline ([ce677e2](https://gitlab.com/lmco/hoppr/hoppr/commit/ce677e25bd686e83f9fb67f24abbc1741202ae52))
* Correct class naming convention ([237433d](https://gitlab.com/lmco/hoppr/hoppr/commit/237433d31e088685bed26a6df8af601e369b4982))
* Make CollectorPluginBase.get_version an abstractmethod ([18daf8a](https://gitlab.com/lmco/hoppr/hoppr/commit/18daf8a35972088f40c7168bca89da3ab53b97fb))
* Merge branch 'main' into initial-plugin-structure ([c9ba5d6](https://gitlab.com/lmco/hoppr/hoppr/commit/c9ba5d62051eceb1a70741c043000e7f15ed1b0e))
* move coverage to dev-dependencies ([14f6667](https://gitlab.com/lmco/hoppr/hoppr/commit/14f6667e757ae7052eb5fc8df00ebde9af953e65))
* Move publish to semantic-release ([e8960f6](https://gitlab.com/lmco/hoppr/hoppr/commit/e8960f6280f15c2ad128758f78f61110b25bfd09))
* Move pylint, pytest, coverage, and pylint to dev-dependencies ([b77b2da](https://gitlab.com/lmco/hoppr/hoppr/commit/b77b2daa0b1c2872474440ded8c928dc031d8378))
* refacored utils for clarity, removed comments ([5a05098](https://gitlab.com/lmco/hoppr/hoppr/commit/5a050983f3fb80d704b43e09e71ea30d18379167))
* Remove check for RELEASE_VERSION environment variable from version command ([6c18027](https://gitlab.com/lmco/hoppr/hoppr/commit/6c180270b2532a533b7652920495e9edbc4a2781))
* removed schemas.py ([0598272](https://gitlab.com/lmco/hoppr/hoppr/commit/0598272811cbbec9aa0938d6d9445c49a53af79d))
* update collector.py to fix mypy errors ([26935ac](https://gitlab.com/lmco/hoppr/hoppr/commit/26935acf5bae5bc8e2e829ac1798e443639e5ea2))
* update collectory.py for black ([3bfd10e](https://gitlab.com/lmco/hoppr/hoppr/commit/3bfd10ee066c2f4f1209bbf9833230407ae4e5d6))
* Update formatting with black ([eb29b3f](https://gitlab.com/lmco/hoppr/hoppr/commit/eb29b3f891d0034dc1a18fa30d99f4017d2875b9))
* update unit tests for new class names ([1d6c3ab](https://gitlab.com/lmco/hoppr/hoppr/commit/1d6c3abc7cc0f02f0a606146bebf1a2c05a07336))
* version reporting, add build to pipeline, include tests ([abd32bf](https://gitlab.com/lmco/hoppr/hoppr/commit/abd32bfd1f22f56d672c4a07e7887c30518c043c))

### [0.3.7](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.6...v0.3.7) (2022-03-09)


### Bug Fixes

* **docs:** adding docs for gitpod to development ([46e9479](https://gitlab.com/lmco/hoppr/hoppr/commit/46e9479c70c99289e656d06a58495a3dd4ce8c21))
* **docs:** adding review app environments for docs on branches ([140fc8b](https://gitlab.com/lmco/hoppr/hoppr/commit/140fc8beb7213cac95df73c3715c93358da9d8ec))
* **docs:** Updating contributing docs to reference Conventional Commits ([5bf4539](https://gitlab.com/lmco/hoppr/hoppr/commit/5bf45396f923fd3fbebf0db18c6013ce90926952))

### [0.3.6](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.5...v0.3.6) (2022-03-08)


### Bug Fixes

* **dev:** adding quick gitpods config. ([380c1b3](https://gitlab.com/lmco/hoppr/hoppr/commit/380c1b3e873f984c9ed14cc4db2056e731cd3d94))
* **dev:** automatically start mkdocs in gitpods ([818e666](https://gitlab.com/lmco/hoppr/hoppr/commit/818e666f8792eb85c49e6a37e5bd639e4698d371))

### [0.3.5](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.4...v0.3.5) (2022-03-03)


### Bug Fixes

* Extract Renovate into new project ([375b5ab](https://gitlab.com/lmco/hoppr/hoppr/commit/375b5ab8243c789ea6cddaf867071f0c217162ed))

### [0.3.4](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.3...v0.3.4) (2022-03-01)


### Bug Fixes

* **docs:** adding issue/mr templates, contributing guidelines, and development docs ([18f59ca](https://gitlab.com/lmco/hoppr/hoppr/commit/18f59cae8b851e036f51b4f2a315567ace7cd4a0))

### [0.3.3](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.2...v0.3.3) (2022-03-01)


### Bug Fixes

* Don't run semantic release and other jobs on schedule ([9e82e21](https://gitlab.com/lmco/hoppr/hoppr/commit/9e82e217bc9e0608bddbba72f444b55df299f30a))
* Hotfix for pages-test ([e6b5d3a](https://gitlab.com/lmco/hoppr/hoppr/commit/e6b5d3a6833a490ab299def667992be6b4ff292b))

### [0.3.2](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.1...v0.3.2) (2022-02-28)


### Bug Fixes

* **deps:** update semantic-release monorepo ([52127f8](https://gitlab.com/lmco/hoppr/hoppr/commit/52127f8f618d4424e1f04e7b801e077cb2529356))

### [0.3.1](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.3.0...v0.3.1) (2022-02-28)


### Bug Fixes

* **deps:** update dependency @semantic-release-plus/docker to v3 ([f413b61](https://gitlab.com/lmco/hoppr/hoppr/commit/f413b6159f669e34b0edd43341a62e3472162037))
* **deps:** update dependency ansi-regex to v6 ([ab2553b](https://gitlab.com/lmco/hoppr/hoppr/commit/ab2553b898ba8387b38a6327265fc27ff0ebc9ee))
* **deps:** update dependency semantic-release-slack-bot to v3 ([ddc4ac3](https://gitlab.com/lmco/hoppr/hoppr/commit/ddc4ac38c8a0985eb824f4a85ed96770a63f226c))

## [0.3.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.2.0...v0.3.0) (2022-02-28)


### Features

* **docs:** adding intro description ([4f5ecb2](https://gitlab.com/lmco/hoppr/hoppr/commit/4f5ecb26d8b911854ba875992c7f0ca1c070b653))


### Bug Fixes

* **deps:** pin dependencies ([bb3a738](https://gitlab.com/lmco/hoppr/hoppr/commit/bb3a738fca593785770e132725585d9912c3d853))
* Add in workflow to handle MR's ([7fef364](https://gitlab.com/lmco/hoppr/hoppr/commit/7fef3642937d5ef8a840bbb1ef8a5719461a484e))
* Update renovate ([6b847df](https://gitlab.com/lmco/hoppr/hoppr/commit/6b847dfdfd46f92bb617cc157f0f1bb4e95b2843))

## [0.2.0](https://gitlab.com/lmco/hoppr/hoppr/compare/v0.1.1...v0.2.0) (2022-02-28)


### Features

* **docs:** adding mkdocs structure and gitlab pages CI ([399975c](https://gitlab.com/lmco/hoppr/hoppr/commit/399975ca4c154980fe479d447f78b236999b9437))


### Bug Fixes

* Add renovate ([90cc5e3](https://gitlab.com/lmco/hoppr/hoppr/commit/90cc5e3c9f1c1a7c1c7607bced322b92f8e97a03))
* Add semantic release ([cbf6717](https://gitlab.com/lmco/hoppr/hoppr/commit/cbf6717a9957cf281d32bf6610eb26df433cebb8))
* Clean up and add templates ([d360ba5](https://gitlab.com/lmco/hoppr/hoppr/commit/d360ba51f9081f12647229e1348affad6755fe03))
* Correct the echo, use handle ([73199aa](https://gitlab.com/lmco/hoppr/hoppr/commit/73199aaea24b308aa39c7f2822595373a884990d))
* Hotfix for semantic release publish ([2e8353e](https://gitlab.com/lmco/hoppr/hoppr/commit/2e8353eef2e4cf47c9a5ed3bc21917985a097136))
* Hotfix the image entrypoint ([36427b1](https://gitlab.com/lmco/hoppr/hoppr/commit/36427b18a1284a34331246e6f8d685f03cfeed6b))
* Release semantic release and setup renovate schedule ([08e7364](https://gitlab.com/lmco/hoppr/hoppr/commit/08e7364c782e924ced1dbdb8ef438b33db327554))
* Remove errant echo command ([4d98743](https://gitlab.com/lmco/hoppr/hoppr/commit/4d987437c96aa18a93aa2c396513e397e77e019c))
* Stages ([7fdaf0a](https://gitlab.com/lmco/hoppr/hoppr/commit/7fdaf0a31f51c983088e47df45263cd2e3376654))
