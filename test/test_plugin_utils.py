import unittest

from unittest import mock

import hoppr.plugin_utils


class TestPluginUtils(unittest.TestCase):
    def test_dir_name_from_repo_url(self):
        repo = "https://www.something.com/morestuff/"
        dir = hoppr.plugin_utils.dir_name_from_repo_url(repo)

        assert dir == "https%3A%2F%2Fwww.something.com%2Fmorestuff"

    def test_repo_url_from_dir_name(self):
        dir = "https%3A%2F%2Fwww.something.com%2Fmorestuff"
        repo = hoppr.plugin_utils.repo_url_from_dir_name(dir)

        assert repo == "https://www.something.com/morestuff"

    @mock.patch("hoppr.plugin_utils.which", side_effect=("alpha", None, "gamma", None))
    def test_get_missing_cmds(self, mock_which):
        test_cmds = ["alpha", "beta", ["gamma", "delta"]]
        expected_missing = ["beta", "delta"]
        found_missing = hoppr.plugin_utils.get_missing_commands(test_cmds)
        assert found_missing == expected_missing

    @mock.patch("hoppr.plugin_utils.which", side_effect=("alpha", None, "gamma", None))
    def test_check_for_missing_cmds(self, mock_which):
        test_cmds = ["alpha", "beta", "gamma", "delta"]

        result = hoppr.plugin_utils.check_for_missing_commands(test_cmds)
        assert result.is_fail()

    @mock.patch("hoppr.plugin_utils.which", side_effect=("alpha", "beta", "gamma", "delta"))
    def test_check_for_missing_cmds_none(self, mock_which):
        test_cmds = ["alpha", "beta", "gamma", "delta"]

        # This should not raise an exception
        hoppr.plugin_utils.check_for_missing_commands(test_cmds)
