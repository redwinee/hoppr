"""
Test module for HopprProcessor and StageProcessor classes
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name

from __future__ import annotations

import multiprocessing
import os
import test

from pathlib import Path
from typing import Literal

import pytest

from pytest import CaptureFixture, FixtureRequest, MonkeyPatch

import hoppr.main
import hoppr.processor

from hoppr.base_plugins.hoppr import HopprPlugin, hoppr_process
from hoppr.exceptions import HopprPluginError
from hoppr.models import HopprContext
from hoppr.models.manifest import Component, LocalFile, Manifest, OciFile, Sbom, UrlFile
from hoppr.models.transfer import ComponentCoverage, Plugin, StageName, StageRef
from hoppr.models.types import BomAccess
from hoppr.processor import Future  # type: ignore[attr-defined]
from hoppr.processor import HopprProcessor, StageProcessor
from hoppr.result import Result

# Import on Unix systems only
if os.name == "posix":
    import pwd


@pytest.mark.usefixtures("component")
class UnitTestPlugin(HopprPlugin):
    """
    HopprPlugin class for unit testing
    """

    test_return_obj: Component | None = None

    def get_version(self) -> str:
        return "0.1.2"

    @hoppr_process
    def pre_stage_process(self):
        return Result.success(return_obj=self.test_return_obj)

    @hoppr_process
    def post_stage_process(self):
        return Result.retry()


test_component_list: list[Component] = [
    Component.parse_obj(
        {
            "name": "README.md",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:generic/README.md",
        }
    ),
    Component.parse_obj(
        {
            "name": "CHANGELOG.md",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:generic/docs/CHANGELOG.md",
        }
    ),
]

test_bom = Sbom.parse_obj(dict(specVersion="1.4", version=1, bomFormat="CycloneDX", components=test_component_list))
test_empty_bom = Sbom.parse_obj(dict(specVersion="1.4", version=1, bomFormat="CycloneDX", components=[]))


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """
    return Component.parse_obj(
        dict(name="TestComponent", version="1.0.0", purl="pkg:deb/test-artifact@1:1.0.0?arch=all", type="application")
    )


@pytest.fixture(scope="function")
def context_fixture(context_fixture: HopprContext) -> HopprContext:
    """
    Fixture to return a HopprContext object for testing
    """
    return context_fixture


@pytest.fixture(name="processor", scope="function")
def hoppr_processor_fixture(monkeypatch: MonkeyPatch) -> HopprProcessor:
    """
    Fixture to return HopprProcessor object
    """
    credentials_file = Path(test.__file__).parent / "resources" / "credential" / "cred-test.yml"
    manifest_file = Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"
    transfer_file = Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml"

    # Environment variables required for credentials file validation
    monkeypatch.setenv(name="SOME_USERNAME", value="test_user")
    monkeypatch.setenv(name="SOME_TOKEN", value="SoMe_ToKeN1234!@#$")
    monkeypatch.setenv(name="DOCKER_PW", value="DoCKeR_PW1234!@#$")
    monkeypatch.setenv(name="DOCKER_ADMIN_PW", value="AdminDockerPW")
    monkeypatch.setenv(name="GITLAB_PW", value="GiTLaB_PW1234!@#$")

    processor = HopprProcessor(transfer_file, manifest_file, credentials_file)
    processor.metadata_files = [transfer_file, manifest_file, credentials_file]

    return processor


@pytest.fixture(scope="function", params=[dict(plugin_class=UnitTestPlugin)])
def plugin_fixture(plugin_fixture: UnitTestPlugin) -> UnitTestPlugin:
    """
    Fixture to return a plugin object for testing
    """
    return plugin_fixture


@pytest.fixture(scope="function")
def processor_plugin_fixture(
    request: FixtureRequest, context_fixture: HopprContext, config_fixture: dict[str, str]
) -> UnitTestPlugin:
    """
    Fixture to return a plugin object for testing
    """
    param_dict = dict(getattr(request, "param", {}))

    plugin = UnitTestPlugin(context=context_fixture, config=config_fixture)

    plugin.bom_access = param_dict.get("bom_access", BomAccess.NO_ACCESS)
    plugin.test_return_obj = param_dict.get("return_obj", None)

    return plugin


@pytest.mark.parametrize(
    argnames=["bom_access", "return_obj", "method_name", "expected_result"],
    argvalues=[
        (BomAccess.NO_ACCESS, None, "pre_stage_process", "SUCCESS"),
        (BomAccess.NO_ACCESS, None, "process_component", "SKIP"),
        (BomAccess.NO_ACCESS, None, "post_stage_process", "RETRY"),
        (BomAccess.NO_ACCESS, None, "not_a_real_process", "FAIL"),
        (BomAccess.COMPONENT_ACCESS, "String is not good", "pre_stage_process", "FAIL"),
        (BomAccess.FULL_ACCESS, "String is not good", "pre_stage_process", "FAIL"),
        (BomAccess.NO_ACCESS, "String is not good", "pre_stage_process", "FAIL"),
    ],
)
def test__run_plugin(  # pylint: disable=too-many-arguments
    bom_access: BomAccess,
    return_obj: str | None,
    context_fixture: HopprContext,
    method_name: str,
    expected_result: Literal["SUCCESS", "FAIL", "RETRY", "SKIP", "EXCLUDED"],
    monkeypatch: MonkeyPatch,
):
    """
    Test hoppr.processor._run_plugin function
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=UnitTestPlugin, name="bom_access", value=bom_access)
        patch.setattr(target=UnitTestPlugin, name="test_return_obj", value=return_obj)

        result = hoppr.processor._run_plugin(
            plugin_name="test.unit.test_processor",
            context=context_fixture,
            config=None,
            method_name=method_name,
            component=None,
        )

        result_check = {
            "SUCCESS": result.is_success,
            "FAIL": result.is_fail,
            "RETRY": result.is_retry,
            "SKIP": result.is_skip,
            "EXCLUDED": result.is_excluded,
        }[expected_result]

        assert result_check(), f"Expected {expected_result} result, got {result}"

        # Check result message if `test_return_obj` specified as a string
        if result.return_obj is not None:
            assert result.message == (
                f"Plugin UnitTestPlugin has BOM access level {bom_access.name}, but returned an object of type str"
            )


def test_hoppr_processor__collect_consolidated_bom(processor: HopprProcessor, context_fixture: HopprContext):
    """
    Test HopprProcess._collect_consolidated_bom method
    """
    processor.context = context_fixture

    processor._collect_consolidated_bom()
    consolidated_bom = processor.context.collect_root_dir / "generic" / "_metadata_" / "_consolidated_bom.json"
    content = consolidated_bom.read_text(encoding="utf-8")

    assert processor.context.consolidated_sbom.json(exclude_none=True, by_alias=True, indent=2) == content


def test_hoppr_processor__collect_manifest_metadata(
    processor: HopprProcessor,
    manifest_fixture: Manifest,
    context_fixture: HopprContext,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """
    Test HopprProcessor._collect_manifest_metadata method
    """
    processor.context = context_fixture

    Manifest.loaded_manifests[LocalFile(local=tmp_path / "manifest.yml")] = manifest_fixture
    Manifest.loaded_manifests[UrlFile(url="https://test.hoppr.com/manifest.yml")] = manifest_fixture
    Sbom.loaded_sboms[LocalFile(local=tmp_path / "sbom.json")] = processor.context.sboms[0]
    Sbom.loaded_sboms[OciFile(oci="oci://test.hoppr.com/sbom.json")] = processor.context.sboms[0]
    Sbom.loaded_sboms[UrlFile(url="https://test.hoppr.com/sbom.json")] = processor.context.sboms[0]

    with monkeypatch.context() as patch:
        patch.setattr(target=manifest_fixture, name="includes", value=list(Manifest.loaded_manifests.keys()))
        patch.setattr(target=manifest_fixture, name="sboms", value=list(Sbom.loaded_sboms.keys()))

        processor._collect_manifest_metadata(manifest=manifest_fixture, target_dir=tmp_path)


@pytest.mark.skipif(os.name == "nt", reason="The `pwd` library is not available for the NT (Windows) platform")
def test_hoppr_processor__collect_metadata_posix(
    processor: HopprProcessor, context_fixture: HopprContext, monkeypatch: MonkeyPatch
):
    """
    Test HopprProcessor._collect_metadata method
    """
    test_user = type("test_user_struct", (object,), dict(pw_name="test_user"))
    monkeypatch.setattr(target=pwd, name="getpwuid", value=lambda uid: test_user)
    monkeypatch.setattr(target=processor, name="_collect_file", value=lambda file_name, target_dir: None)
    monkeypatch.setattr(target=processor, name="_collect_manifest_metadata", value=lambda manifest, target_dir: None)

    processor.context = context_fixture
    processor._collect_metadata()


@pytest.mark.skipif(os.name == "posix", reason="The `os.getlogin` method is not available on Linux/Darwin platforms")
def test_hoppr_processor__collect_metadata_nt(
    processor: HopprProcessor, context_fixture: HopprContext, monkeypatch: MonkeyPatch
):
    """
    Test HopprProcessor._collect_metadata method
    """
    monkeypatch.setattr(target=os, name="getlogin", value=lambda: "test_user")
    monkeypatch.setattr(target=processor, name="_collect_file", value=lambda file_name, target_dir: None)
    monkeypatch.setattr(target=processor, name="_collect_manifest_metadata", value=lambda manifest, target_dir: None)

    processor.context = context_fixture

    processor._collect_metadata()


def test_hoppr_processor__summarize_results_success(processor: HopprProcessor, context_fixture: HopprContext):
    """
    Test HopprProcessor._summarize_results method
    """
    stage_ref = StageRef(name=StageName("test_stage"), plugins=[])
    stage = StageProcessor(stage_ref, context_fixture)
    processor.context = context_fixture

    stage.results["pre_stage_process"] = [
        ("plugin-a", None, Result.success()),
        ("plugin-a", None, Result.fail()),
        ("plugin-a", None, Result.retry()),
    ]

    stage.results["process_component"] = [
        ("plugin-a", "my-component", Result.success()),
        ("plugin-a", "my-component", Result.fail()),
        ("plugin-a", "my-component", Result.retry()),
    ]

    processor.stage_processor_map[stage_ref] = stage
    failure_count = processor._summarize_results()
    assert failure_count == 4


def test_hoppr_processor_get_logger(processor: HopprProcessor):
    """
    Test HopprProcessor.get_logger method
    """
    with multiprocessing.Manager() as manager:
        processor.get_logger(lock=manager.RLock(), log_name="HopprProcessorLogger")


def test_hoppr_processor_run(processor: HopprProcessor, monkeypatch: MonkeyPatch):
    """
    Test HopprProcessor.run method
    """
    monkeypatch.setattr(target=StageProcessor, name="run", value=Result.success)

    result = processor.run()
    assert result.is_success()


def test_hoppr_processor_run_fail(processor: HopprProcessor, monkeypatch: MonkeyPatch):
    """
    Test HopprProcessor.run method fails with no SBOMs in manifest
    """
    monkeypatch.setattr(target=processor, name="_summarize_results", value=lambda: 42)
    monkeypatch.setattr(target=StageProcessor, name="run", value=lambda self: Result.success())

    result = processor.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"

    assert processor.get_logger() is not None


def test_hoppr_processor_run_no_bom_fail(processor: HopprProcessor, monkeypatch: MonkeyPatch):
    """
    Test HopprProcessor.run method fails with no SBOMs in manifest
    """
    processor.manifest.sboms = []

    with monkeypatch.context() as patch:
        patch.setattr(target=processor, name="_collect_file", value=lambda *args, **kwargs: None)
        patch.setattr(target=StageProcessor, name="run", value=Result.success)
        patch.setattr(target=Sbom.consolidated_sbom, name="components", value=[])
        patch.setattr(target=Sbom.consolidated_sbom, name="externalReferences", value=None)
        processor.metadata_files = [Path("dummy_metadata")]

        result = processor.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_hoppr_processor_run_stage_fail(processor: HopprProcessor, monkeypatch: MonkeyPatch):
    """
    Test HopprProcessor.run method with stage run failure
    """
    monkeypatch.setattr(target=processor, name="_summarize_results", value=lambda: 42)
    monkeypatch.setattr(target=StageProcessor, name="run", value=lambda self: Result.fail())

    result = processor.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == "42 failed during this execution"


def test_stage_processor__check_bom_access_component(context_fixture: HopprContext, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor._check_bom_access method with COMPONENT access
    """
    stage_ref = StageRef(name=StageName("test_stage"), plugins=[])
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [
        Plugin(name="test.unit.test_processor", config=None),
        Plugin(name="test.unit.test_processor", config=None),
    ]

    with monkeypatch.context() as patch:
        patch.setattr(target=UnitTestPlugin, name="bom_access", value=BomAccess.COMPONENT_ACCESS)
        patch.setattr(target=hoppr.processor, name="plugin_class", value=lambda plugin_name: UnitTestPlugin)

        result = stage._check_bom_access()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == (
            "Stage test_stage has one or more plugins with COMPONENT_ACCESS: "
            "UnitTestPlugin, UnitTestPlugin, and required component coverage for the stage of OPTIONAL.\n    "
            "If any plugins have COMPONENT access, the stage required coverage "
            "must be EXACTLY_ONCE or NO_MORE_THAN_ONCE."
        )


def test_stage_processor__check_bom_access_full(context_fixture: HopprContext, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor._check_bom_access method with FULL access
    """
    stage_ref = StageRef(name=StageName("test_stage"), plugins=[])
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [
        Plugin(name="test.unit.test_processor", config=None),
        Plugin(name="test.unit.test_processor", config=None),
    ]

    with monkeypatch.context() as patch:
        patch.setattr(target=UnitTestPlugin, name="bom_access", value=BomAccess.FULL_ACCESS)
        patch.setattr(target=hoppr.processor, name="plugin_class", value=lambda plugin_name: UnitTestPlugin)

        result = stage._check_bom_access()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == (
            "Stage test_stage has one or more plugins with FULL_ACCESS: "
            "UnitTestPlugin, UnitTestPlugin, and multiple plugins defined for the stage.\n    "
            "Any plugin with FULL BOM access must be the only plugin in the stage"
        )


def test_stage_processor__check_component_coverage(context_fixture: HopprContext):
    """
    Test StageProcessor._check_component_coverage method
    """
    stage_ref = StageRef(name=StageName("test_stage"), plugins=[], component_coverage="EXACTLY_ONCE")
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    stage.required_coverage = stage._get_required_coverage()

    stage._save_result("process_component", "TestPlugin", Result.success(), stage.context.delivered_sbom.components[0])

    assert stage._check_component_coverage("process_component") == 1
    assert len(stage.results["process_component"]) == 2


def test_stage_processor__get_required_coverage(context_fixture: HopprContext, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor._get_required_coverage method
    """

    class _OptionalCoveragePlugin(UnitTestPlugin):
        default_component_coverage = ComponentCoverage.OPTIONAL

    class _NoMoreThanOnceCoveragePlugin(UnitTestPlugin):
        default_component_coverage = ComponentCoverage.NO_MORE_THAN_ONCE

    def _mock_plugin_class(plugin_name: str):
        match plugin_name:
            case "OptionalCoveragePlugin":
                return _OptionalCoveragePlugin
            case "NoMoreThanOnceCoveragePlugin":
                return _NoMoreThanOnceCoveragePlugin

    stage_ref = StageRef(name=StageName("test_stage"), plugins=[])
    stage = StageProcessor(stage_ref, context_fixture)
    assert stage._get_required_coverage() == ComponentCoverage.OPTIONAL

    stage.plugin_ref_list = [
        Plugin(name="OptionalCoveragePlugin", config=None),
        Plugin(name="NoMoreThanOnceCoveragePlugin", config=None),
    ]

    monkeypatch.setattr(target=hoppr.processor, name="plugin_class", value=_mock_plugin_class)

    with pytest.raises(HopprPluginError):
        stage._get_required_coverage()

    stage_ref.component_coverage = "AT_LEAST_ONCE"
    stage = StageProcessor(stage_ref, context_fixture)
    assert stage._get_required_coverage() == ComponentCoverage.AT_LEAST_ONCE


@pytest.mark.parametrize(
    argnames=["failures", "retries", "expected_result"],
    argvalues=[
        (0, 0, Result.success()),
        (0, 1, Result.fail("1 'process_component' processes returned 'retry'")),
        (2, 0, Result.fail("2 'process_component' processes failed")),
        (4, 8, Result.fail("4 'process_component' processes failed, and 8 returned 'retry'")),
    ],
)
def test_stage_processor__get_stage_result(
    context_fixture: HopprContext, failures: int, retries: int, expected_result: Result
):
    """
    Test StageProcessor._get_required_coverage method
    """
    stage_ref = StageRef(name=StageName("test_stage"), plugins=[])
    stage = StageProcessor(stage_ref, context_fixture)

    result = stage._get_stage_result("process_component", failures=failures, retries=retries)
    assert result.status == expected_result.status
    assert result.message == expected_result.message


def test_stage_processor__run_all_fail(context_fixture: HopprContext, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor._run_all method returns fail result
    """
    monkeypatch.setattr(target=Future, name="result", value=lambda *args, **kwargs: Result.fail())
    monkeypatch.setattr(
        target=hoppr.processor,
        name="plugin_instance",
        value=lambda *args, **kwargs: UnitTestPlugin(context_fixture),
    )

    stage_ref = StageRef(name=StageName("test_stage"), plugins=[])
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [
        Plugin(name="test.unit.test_processor", config=None),
        Plugin(name="test.unit.test_processor", config=None),
    ]

    result = stage._run_all("pre_stage_process")
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_stage_processor__report_result(context_fixture: HopprContext, capsys: CaptureFixture):
    """
    Test StageProcessor._update_bom method
    """
    stage_ref = StageRef(name=StageName("test_stage"), plugins=[], component_coverage="EXACTLY_ONCE")
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    comp = Component.parse_obj(
        {
            "name": "transfer-test",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:generic/MODIFIED_README.md",
        }
    )

    stage._report_result("test_stage", comp, Result.success())
    captured = capsys.readouterr()
    assert captured.out == "      test_stage SUCCESS for pkg:generic/MODIFIED_README.md\n"

    stage._report_result("test_stage", comp, Result.excluded())
    captured = capsys.readouterr()
    assert captured.out == "      test_stage EXCLUDED for pkg:generic/MODIFIED_README.md\n"

    stage._report_result("test_stage", comp, Result.fail())
    captured = capsys.readouterr()
    assert captured.out == "      test_stage FAIL for pkg:generic/MODIFIED_README.md\n"


def test_stage_processor__update_bom(context_fixture: HopprContext):
    """
    Test StageProcessor._update_bom method
    """
    stage_ref = StageRef(name=StageName("test_stage"), plugins=[], component_coverage="EXACTLY_ONCE")
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    new_bom = Sbom.parse_obj({"specVersion": "1.4", "version": 1, "bomFormat": "CycloneDX", "components": []})

    new_comp = Component.parse_obj(
        {
            "name": "transfer-test",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:generic/MODIFIED_README.md",
        }
    )

    new_bom.components.append(new_comp)
    stage._update_bom(new_bom, None)

    assert len(stage.context.delivered_sbom.components) == 1
    assert stage.context.delivered_sbom.components[0].purl == "pkg:generic/MODIFIED_README.md"


def test_stage_processor__update_bom_component(context_fixture: HopprContext):
    """
    Test StageProcessor._update_bom method
    """
    stage_ref = StageRef(name=StageName("test_stage"), plugins=[], component_coverage="EXACTLY_ONCE")
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    new_comp = Component.parse_obj(
        {
            "name": "transfer-test",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:generic/MODIFIED_README.md",
        }
    )

    stage._update_bom(new_comp, test_component_list[0])

    assert stage.context.delivered_sbom.components[0].purl == "pkg:generic/MODIFIED_README.md"


def test_stage_processor_run_check_bom_access_fail(context_fixture: HopprContext, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor.run method returns _check_bom_access fail result
    """
    monkeypatch.setattr(target=StageProcessor, name="_check_bom_access", value=lambda self: Result.fail())
    monkeypatch.setattr(
        target=hoppr.processor,
        name="plugin_instance",
        value=lambda *args, **kwargs: UnitTestPlugin(context_fixture),
    )

    stage_ref = StageRef(name=StageName("test_stage"), plugins=[])
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [Plugin(name="test.unit.test_processor", config=None)]

    result = stage.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_stage_processor_run_module_not_found(context_fixture: HopprContext):
    """
    Test StageProcessor.run method raises ModuleNotFoundError
    """
    stage_ref = StageRef(name=StageName("test_stage"), plugins=[])
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [Plugin(name="test.unit.test_processor.NotFoundPlugin", config=None)]

    result = stage.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_stage_processor_run_retry(context_fixture: HopprContext, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor.run method returns retry result
    """
    monkeypatch.setattr(
        target=hoppr.processor,
        name="plugin_instance",
        value=lambda *args, **kwargs: UnitTestPlugin(context_fixture),
    )

    stage_ref = StageRef(name=StageName("test_stage"), plugins=[])
    stage = StageProcessor(stage_ref, context_fixture)
    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [Plugin(name="test.unit.test_processor", config=None)]

    result = stage.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == "1 'post_stage_process' processes returned 'retry'"


def test_stage_processor_run_success(
    processor: HopprProcessor, context_fixture: HopprContext, monkeypatch: MonkeyPatch
):
    """
    Test StageProcessor.run method returns success result
    """
    stage_ref = processor.transfer.stages[0]
    stage_processor = StageProcessor(stage_ref, context_fixture)

    monkeypatch.setattr(target=stage_processor, name="_run_all", value=Result.success)

    result = stage_processor.run()
    assert result.is_success()
