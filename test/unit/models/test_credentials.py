"""
Test module for credentials pydantic models
"""
import test

from pathlib import Path

import pytest

from pytest import MonkeyPatch

from hoppr.models.credentials import Credentials

credentials_dict = {
    "kind": "Credentials",
    "metadata": {"name": "Registry Credentials", "version": "v1", "description": "Sample credentials file"},
    "schemaVersion": "v1",
    "credential_required_services": [
        {"url": "some-site.com", "user": "pipeline-bot", "user_env": None, "pass_env": "SOME_TOKEN"},
        {"url": "registry-test.com", "user": "cis-gitlab", "user_env": None, "pass_env": "DOCKER_PW"},
        {"url": "gitlab.com", "user": "infra-pipeline-auto", "user_env": None, "pass_env": "GITLAB_PW"},
    ],
}

credentials_file = Path(test.__file__).parent / "resources" / "credential" / "cred-test.yml"


def test_load_credentials_dict(monkeypatch: MonkeyPatch):
    """
    Test Credentials.load method with dict input
    """
    with monkeypatch.context() as patch:
        patch_dict = credentials_dict
        patch_dict["credential_required_services"] = [
            {"url": "gitlab.com", "user_env": "GITLAB_USER", "pass_env": "GITLAB_PW"}
        ]

        patch.setenv(name="GITLAB_USER", value="gitlab_test_user")
        patch.setenv(name="GITLAB_PW", value="GiTLaB_PW1234!@#$")

        credentials = Credentials.load(credentials_dict)
        assert credentials is not None

        found_creds = Credentials.find("gitlab.com")
        assert found_creds is not None
        assert found_creds.username == "gitlab_test_user"
        assert found_creds.password.get_secret_value() == "GiTLaB_PW1234!@#$"


def test_load_credentials_file(monkeypatch: MonkeyPatch):
    """
    Test Credentials.load method with Path input
    """
    with monkeypatch.context() as patch:
        patch.setenv(name="SOME_USERNAME", value="pipeline-bot")
        patch.setenv(name="SOME_TOKEN", value="SoMe_ToKeN1234!@#$")
        patch.setenv(name="DOCKER_PW", value="DoCKeR_PW1234!@#$")
        patch.setenv(name="DOCKER_ADMIN_PW", value="AdminDockerPW")
        patch.setenv(name="GITLAB_PW", value="GiTLaB_PW1234!@#$")

        credentials = Credentials.load(credentials_file)

        # Test __getitem__ method (indexed lookup on Credentials instance)
        assert credentials is not None
        assert credentials["some-site.com"].username == "pipeline-bot"
        assert credentials["some-site.com"].password.get_secret_value() == "SoMe_ToKeN1234!@#$"
        assert credentials["registry-test.com"].password.get_secret_value() == "DoCKeR_PW1234!@#$"
        assert credentials["registry-test.com/admin"].password.get_secret_value() == "AdminDockerPW"
        assert credentials["gitlab.com"].password.get_secret_value() == "GiTLaB_PW1234!@#$"

        # Test Credentials.find class method
        found_creds = Credentials.find("some-site.com")
        assert found_creds is not None
        assert found_creds.password.get_secret_value() == "SoMe_ToKeN1234!@#$"

        found_creds = Credentials.find("registry-test.com")
        assert found_creds is not None
        assert found_creds.password.get_secret_value() == "DoCKeR_PW1234!@#$"

        found_creds = Credentials.find("gitlab.com")
        assert found_creds is not None
        assert found_creds.password.get_secret_value() == "GiTLaB_PW1234!@#$"

        found_creds = Credentials.find("gitlab")
        assert found_creds is None

        found_creds = Credentials.find("registry-test.com/something")
        assert found_creds is not None
        assert found_creds.password.get_secret_value() == "DoCKeR_PW1234!@#$"

        found_creds = Credentials.find("registry-test.com/admin/stuff")
        assert found_creds is not None
        assert found_creds.password.get_secret_value() == "AdminDockerPW"


def test_load_credentials_none():
    """
    Test Credentials.load method with None input
    """
    credentials = Credentials.load(None)
    assert credentials is None


def test_load_credentials_wrong_type():
    """
    Test Credentials.load method with wrong input type
    """
    with pytest.raises(expected_exception=TypeError):
        Credentials.load(0)
