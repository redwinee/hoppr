"""
Test module for manifest pydantic models
"""
from __future__ import annotations

import test
import uuid

from copy import deepcopy
from pathlib import Path
from typing import TYPE_CHECKING, Callable, Literal, Type, TypeVar

import jmespath
import pytest

from pytest import FixtureRequest, MonkeyPatch
from requests import HTTPError
from ruamel.yaml import YAML

import hoppr
import hoppr.net
import hoppr.oci_artifacts
import hoppr.utils

from hoppr.constants import BomProps
from hoppr.exceptions import HopprLoadDataError
from hoppr.models.manifest import (
    Component,
    ExternalReference,
    LocalFile,
    Manifest,
    ManifestFile,
    OciFile,
    Property,
    Repository,
    Sbom,
    Sboms,
    UrlFile,
)
from hoppr.models.types import RepositoryUrl

if TYPE_CHECKING:
    from pydantic.typing import DictStrAny

# pylint: disable=protected-access,unused-argument

ExpectedException = TypeVar("ExpectedException", bound=BaseException)


@pytest.fixture(scope="function", autouse=True)
def clear_loaded_data():
    """
    Fixture to automatically reset loaded SBOMs and includes before each test
    """
    Manifest.loaded_manifests.clear()
    Sbom.loaded_sboms.clear()

    if hasattr(Sbom, "consolidated_sbom"):
        del Sbom.consolidated_sbom


@pytest.fixture(name="includes", scope="function")
def includes_fixture(request: FixtureRequest) -> list[DictStrAny]:
    """
    Fixture to return `includes` object
    """
    if hasattr(request, "param"):
        return request.param

    return [
        {"local": "test/resources/manifest/manifest.yml"},
        {"local": "test/resources/manifest/unit/manifest.yml"},
        {"url": "https://test.hoppr.com/helm/manifest.yml"},
        {"url": "https://test.hoppr.com/pypi/manifest.yml"},
    ]


@pytest.fixture(name="load_url_fixture", scope="function")
def _load_url_fixture(request: FixtureRequest) -> Callable[..., object]:
    """
    Fixture to patch hoppr.net.load_url
    """

    def _load_url(url: str):
        if request.param == "HTTPError":
            raise HTTPError

        if request.param == "HopprLoadDataError":
            raise HopprLoadDataError

        return request.param

    return _load_url


@pytest.fixture(name="manifest_dict", scope="function")
def manifest_dict_fixture(includes: list[DictStrAny], sboms: list[DictStrAny]):
    """
    Fixture to return dict representation of a ManifestFile object
    """
    return {
        "kind": "Manifest",
        "metadata": {
            "name": "Unit Test Manifest",
            "version": "0.1.0",
            "description": "Manifest for unit tests",
        },
        "schemaVersion": "v1",
        "repositories": {
            "deb": [
                {"url": "https://apt.repo.alpha.com"},
                {"url": "https://apt.repo.beta.com"},
            ],
            "generic": [{"url": "file://"}],
            "pypi": [{"url": "https://pypi.hoppr.com/repositories/pypi"}],
            "rpm": [
                {"url": "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"},
                {"url": "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os"},
                {"url": "https://rpm.hoppr.com/dl/8/PowerTools/x86_64/os"},
            ],
        },
        "includes": includes or [],
        "sboms": sboms or [],
    }


@pytest.fixture(name="manifest", scope="function")
def manifest_fixture(manifest_dict: DictStrAny, tmp_path: Path) -> Manifest:
    """
    Fixture to return Manifest object
    """
    manifest_path = tmp_path / "manifest.yml"
    yaml = YAML(typ="safe", pure=True)

    with manifest_path.open(mode="w+", encoding="utf-8") as stream:
        yaml.dump(data=manifest_dict, stream=stream)

    return Manifest.load(Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml")


@pytest.fixture(name="sboms", scope="function")
def sboms_fixture(request: FixtureRequest) -> list[DictStrAny]:
    """
    Fixture to return `sboms` object
    """
    if hasattr(request, "param"):
        return request.param

    return [
        {"local": "../../bom/unit_bom1_mini.json"},
        {"local": "../../bom/unit_bom2_mini.json"},
        {"url": "http://test.hoppr.com/sbom_1.json"},
        {"url": "http://test.hoppr.com/sbom_2.json"},
    ]


def test_external_reference_hash():
    """
    Test ExternalReference.__hash__ method
    """
    ext_ref = ExternalReference.parse_obj(
        {
            "url": "https://test.hoppr.com/sbom.json",
            "comment": "urn:uuid:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
            "type": "bom",
            "hashes": [{"alg": "SHA-256", "content": "0123456789abcdef0123456789abcdef"}],
        }
    )

    assert isinstance(hash(ext_ref), int)


def test_manifest_file_parse_file():
    """
    Test ManifestFile.parse_file method
    """
    manifest_path = Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"
    ManifestFile.parse_file(manifest_path)


def test_manifest_file_parse_file_fail(monkeypatch: MonkeyPatch):
    """
    Test ManifestFile.parse_file method fail to load file
    """
    manifest_path = Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"

    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda file: None)

    with pytest.raises(expected_exception=TypeError) as pytest_exception:
        ManifestFile.parse_file(manifest_path)

    assert pytest_exception.value.args[0] == "Local file content was not loaded as dictionary"


@pytest.mark.parametrize(argnames=["includes", "sboms"], argvalues=[([], [])], indirect=True)
def test_manifest_file_parse_obj(manifest: ManifestFile):
    """
    Test ManifestFile.parse_obj method
    """
    manifest_dict = manifest.dict(by_alias=True)

    manifest_dict["includes"] = [
        {"local": "test/resources/manifest/manifest.yml"},
        {"local": "test/resources/manifest/unit/manifest.yml"},
    ]

    manifest_dict["sboms"] = [
        {"local": "../../bom/unit_bom1_mini.json"},
        {"local": "../../bom/unit_bom2_mini.json"},
    ]

    manifest_obj = ManifestFile.parse_obj(manifest_dict)

    # Test that local refs were removed
    assert len(manifest_obj.includes) == 0
    assert len(manifest_obj.sboms) == 0


def test_manifest__add_component():
    """
    Test Manifest._add_component method
    """
    component = Component(
        name="something/else",
        version="1.2.3",
        purl="pkg:good1/something/else@1.2.3",
        type="file",
        properties=[],
    )  # pyright: ignore[reportGeneralTypeIssues]

    # pylint: disable=duplicate-code
    Sbom.consolidated_sbom = Sbom(
        specVersion="1.4",
        version=1,
        bomFormat="CycloneDX",  # type: ignore[arg-type]
        serialNumber=uuid.uuid4().urn,
        components=[],
        externalReferences=[],
        metadata=None,
        services=[],
        dependencies=[],
        compositions=[],
        vulnerabilities=[],
        signature=None,
    )

    assert component.properties is not None

    # Test adding component without property
    Manifest._add_component(component)

    # Test adding same component with property merges the property with existing component
    component.properties.append(Property(name="hoppr:test:name", value="hoppr:test:value"))
    search_expr = "components[?purl=='pkg:good1/something/else@1.2.3'] | [].properties[?name=='hoppr:test:name']"

    Manifest._add_component(component)
    props = jmespath.search(expression=search_expr, data=Sbom.consolidated_sbom.dict(by_alias=True))
    assert len(props) == 1

    # Test idempotency
    Manifest._add_component(component)
    props = jmespath.search(expression=search_expr, data=Sbom.consolidated_sbom.dict(by_alias=True))
    assert len(props) == 1


@pytest.mark.parametrize(
    argnames=["ref_type", "location"],
    argvalues=[
        ("local", Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"),
        ("url", "https://test.hoppr.com/helm/manifest.yml"),
    ],
)
def test_manifest_find(
    ref_type: Literal["local", "url"], location: str | Path, manifest: Manifest, monkeypatch: MonkeyPatch
):
    """
    Test Manifest.find method
    """
    Manifest.loaded_manifests[LocalFile(local=Path(location))] = manifest
    Manifest.loaded_manifests[UrlFile(url=str(location))] = manifest

    manifest_obj = Manifest.find(ref_type, location)
    assert manifest_obj == manifest

    # Test that None is returned for invalid include type
    assert Manifest.find(ref_type="invalid", location="nonexistent/path") is None  # type: ignore[arg-type]


@pytest.mark.parametrize(
    argnames="includes_refs",
    argvalues=[
        [OciFile(oci="https://registry.hoppr.com"), UrlFile(url="https://test.hoppr.com/manifest.yml")],
    ],
)
def test_manifest_includes_validation_error(
    manifest: ManifestFile, includes_refs: list[LocalFile | UrlFile], monkeypatch: MonkeyPatch
):
    """
    Test pydantic validator for Manifest.includes attribute raises TypeError
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.utils, name="load_file", value=lambda file: None)
        patch.setattr(target=hoppr.net, name="load_url", value=lambda file: None)
        patch.setattr(target=manifest, name="sboms", value=[])
        patch.setattr(target=Manifest, name="loaded_manifests", value=[])

        print(manifest.repositories)

        with pytest.raises(expected_exception=TypeError) as pytest_exception:
            Manifest.populate_loaded_manifests(includes_refs, values={"repositories": manifest.repositories.dict()})

        assert pytest_exception.value.args[0] == "URL manifest include was not loaded as dictionary"


def test_manifest_includes_validator(manifest: ManifestFile, monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.includes attribute
    """
    monkeypatch.setattr(target=hoppr.net, name="load_url", value=lambda url: manifest.dict(by_alias=True))
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda path: manifest.dict(by_alias=True))

    Manifest.populate_loaded_manifests(
        includes=[
            LocalFile(local=Path("../manifest/manifest_1.yml")),
            UrlFile(url="https://test.hoppr.com/manifest_1.yml"),
        ],
        values={"repositories": manifest.repositories.dict()},
    )


@pytest.mark.parametrize(argnames=["includes", "sboms"], argvalues=[([], [])], indirect=True)
def test_manifest_load_dict(manifest_dict: DictStrAny):
    """
    Test Manifest.load method with dict input
    """
    Manifest.load(manifest_dict)


def test_manifest_load_file():
    """
    Test Manifest.load method with Path input
    """
    manifest_path = Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"
    manifest = Manifest.load(manifest_path)

    assert manifest.repositories["deb"] == [
        Repository.parse_obj({"url": "https://apt.repo.alpha.com"}),
        Repository.parse_obj({"url": "https://apt.repo.beta.com"}),
    ]
    assert manifest.repositories["generic"] == [Repository.parse_obj({"url": "file://"})]
    assert manifest.repositories["pypi"] == [Repository.parse_obj({"url": "https://pypi.hoppr.com/repositories/pypi"})]
    assert manifest.repositories["rpm"] == [
        Repository.parse_obj({"url": "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"}),
        Repository.parse_obj({"url": "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os"}),
        Repository.parse_obj({"url": "https://rpm.hoppr.com/dl/8/PowerTools/x86_64/os"}),
    ]

    include = Manifest.find(ref_type="local", location=manifest_path.with_name("child-a.yml"))
    assert include is not None

    # Test Repositories.__setitem__ method by adding repositories expected in included manifest
    expected_include_repos = deepcopy(manifest.repositories)
    expected_include_repos["deb"].append(Repository.parse_obj({"url": "https://apt.repo.gamma.com"}))
    expected_include_repos["pypi"].append(Repository.parse_obj({"url": "https://another.pypy.repo.com/"}))
    expected_include_repos["pypi"].append(Repository.parse_obj({"url": "https://pypi.org"}))

    assert include.repositories == expected_include_repos

    # Test Repositories.__iter__ method
    for purl_type_repo_list in manifest.repositories:
        assert isinstance(purl_type_repo_list, tuple)

    # Test consolidated_sbom expected values
    assert len(Sbom.loaded_sboms) == 3

    num_components = 0
    for _, sbom in Sbom.loaded_sboms.items():
        num_components += len(sbom.components)

    assert num_components == 8
    assert len(Sbom.loaded_sboms) == 3
    assert len(Sbom.consolidated_sbom.components) == 5  # (8 total) - (2 duplicates) - (1 missing purl)

    assert Sbom.consolidated_sbom.components[0].externalReferences
    assert len(Sbom.consolidated_sbom.components[0].externalReferences) == 1

    assert Sbom.consolidated_sbom.components[1].externalReferences
    assert len(Sbom.consolidated_sbom.components[1].externalReferences) == 2

    assert Sbom.consolidated_sbom.components[2].externalReferences
    assert len(Sbom.consolidated_sbom.components[2].externalReferences) == 2

    assert Sbom.consolidated_sbom.components[3].externalReferences
    assert len(Sbom.consolidated_sbom.components[3].externalReferences) == 1

    assert Sbom.consolidated_sbom.components[4].externalReferences
    assert len(Sbom.consolidated_sbom.components[4].externalReferences) == 1


@pytest.mark.parametrize(
    argnames=["load_url_fixture", "expected_exception", "expected_message"],
    argvalues=[
        ({}, None, None),
        ([], TypeError, "URL manifest include was not loaded as dictionary"),
        ("HopprLoadDataError", HopprLoadDataError, ""),
        ("HTTPError", HopprLoadDataError, ""),
    ],
    indirect=["load_url_fixture"],
)
def test_manifest_load_url(  # pylint: disable=too-many-arguments
    manifest: ManifestFile,
    manifest_dict: DictStrAny,
    load_url_fixture: Callable[..., object],
    expected_exception: Type[ExpectedException] | None,
    expected_message: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test Manifest.load method with URL input
    """
    manifest_url = "https://test.hoppr.com/manifest.yml"

    with monkeypatch.context() as patch:
        patch.setattr(target=ManifestFile, name="parse_obj", value=lambda obj: manifest)
        patch.setattr(target=hoppr.net, name="load_url", value=load_url_fixture)

        if expected_exception is not None:
            with pytest.raises(expected_exception, match=expected_message) as ex:
                Manifest.load(manifest_url)

            assert ex.type == expected_exception
        else:
            Manifest.load(manifest_url)


@pytest.mark.parametrize(
    argnames="sbom_refs",
    argvalues=[
        [OciFile(oci="oci://test.hoppr.com/oci-sbom.json")],
        [UrlFile(url="https://test.hoppr.com/url-sbom.json")],
    ],
)
def test_manifest_sboms_validation_error(sbom_refs: Sboms, monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.sboms attribute raises TypeError
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.net, name="load_url", value=lambda url: None)
        patch.setattr(target=hoppr.oci_artifacts, name="pull_artifact", value=lambda artifact: None)
        patch.setattr(target=hoppr.utils, name="load_file", value=lambda path: None)

        with pytest.raises(expected_exception=TypeError):
            Manifest.populate_loaded_sboms(sbom_refs, values={})


@pytest.mark.parametrize(
    argnames="sbom_refs",
    argvalues=[
        [LocalFile(local=Path("/hoppr/test/local-sbom.json"))],
        [OciFile(oci="oci://test.hoppr.com/oci-sbom.json")],
        [UrlFile(url="https://test.hoppr.com/url-sbom.json")],
        [None],
    ],
)
def test_manifest_sboms_validator(sbom_refs: Sboms, manifest: ManifestFile, monkeypatch: MonkeyPatch):
    """
    Test pydantic validator for Manifest.sboms attribute
    """
    sbom = Sbom.parse_file(Path(test.__file__).parent / "resources" / "bom" / "unit_bom1_mini.json")
    sbom_dict = sbom.dict(by_alias=True)

    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.net, name="load_url", value=lambda url: sbom_dict)
        patch.setattr(target=hoppr.oci_artifacts, name="pull_artifact", value=lambda path: sbom_dict)
        patch.setattr(target=Sbom, name="parse_file", value=lambda path: sbom)
        patch.setattr(target=Sbom, name="loaded_sboms", value={})

        Manifest.populate_loaded_sboms(sbom_refs, values={"repositories": manifest.repositories.dict()})


def test_property_hash():
    """
    Test Property.__hash__ method
    """
    prop = Property.parse_obj(
        {
            "name": BomProps.COMPONENT_SEARCH_SEQUENCE,
            "value": '{"version": "v1", "repositories": ["alpha", "beta", "gamma"]}',
        }
    )

    assert isinstance(hash(prop), int)


def test_repository_url_validator():
    """
    Test pydantic validator for Repository.url attribute
    """
    assert Repository.validate_url(url="file:") == RepositoryUrl(url="file://")
    assert Repository.validate_url(url="docker.io") == RepositoryUrl(url="http://docker.io")


def test_sbom_find():
    """
    Test Sbom.find method
    """
    sbom_path = Path(test.__file__).parent / "resources" / "bom" / "unit_bom1_mini.json"
    sbom = Sbom.parse_file(sbom_path)

    Sbom.loaded_sboms.clear()
    Sbom.loaded_sboms[LocalFile(local=Path("local-sbom.json"))] = sbom
    Sbom.loaded_sboms[OciFile(oci="oci://test.hoppr.com/oci-sbom.json")] = sbom
    Sbom.loaded_sboms[UrlFile(url="https://test.hoppr.com/url-sbom.json")] = sbom

    assert Sbom.find(ref_type="local", location=Path("local-sbom.json")) == sbom
    assert Sbom.find(ref_type="oci", location="oci://test.hoppr.com/oci-sbom.json") == sbom
    assert Sbom.find(ref_type="url", location="https://test.hoppr.com/url-sbom.json") == sbom
    assert Sbom.find(ref_type="invalid", location=Path("local-sbom.json")) is None  # type: ignore[arg-type]


def test_sbom_hash():
    """
    Test Sbom.__hash__ method
    """
    sbom = Sbom.parse_file(Path(test.__file__).parent / "resources" / "bom" / "unit_bom1_mini.json")
    assert isinstance(hash(sbom), int)

    assert sbom.components is not None
    for component in sbom.components:
        hashable = Component.parse_obj(component.dict(by_alias=True))
        assert isinstance(hash(hashable), int)
