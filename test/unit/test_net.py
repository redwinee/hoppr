"""
Test module for net class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import json

from test.mock_objects import MockHttpResponse
from typing import Callable

import pytest

from pytest import FixtureRequest, MonkeyPatch

import hoppr.net
import hoppr.plugin_utils
import hoppr.utils

from hoppr.exceptions import HopprLoadDataError
from hoppr.models.credentials import CredentialRequiredService
from hoppr.net import Credentials, requests  # type: ignore[attr-defined]

test_input_data = {
    "alpha": [1, 2, 3],
    "beta": ["dog", "cat"],
    "gamma": {"x": 42, "y": "why not", "z": ["mixed", 7, "array"]},
}


@pytest.fixture(scope="function")
def response_fixture(request: FixtureRequest) -> Callable[[str], MockHttpResponse]:
    """
    Mock Http fixture
    """

    def _find_response(*args, **kwargs) -> MockHttpResponse:
        """
        Implemented function to be referenced for response_fixture to return a Callable
        """
        param_dict = dict(getattr(request, "param", {}))

        param_dict["status_code"] = param_dict.get("status_code", 200)
        param_dict["content"] = param_dict.get("content", json.dumps(test_input_data))

        return MockHttpResponse(**param_dict)

    return _find_response


def test_load_url(
    find_credentials_fixture: CredentialRequiredService, response_fixture: MockHttpResponse, monkeypatch: MonkeyPatch
):
    """
    Test hoppr.net.load_url function
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    content = hoppr.net.load_url(url="url_goes_here")
    assert content == test_input_data


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": test_input_data}], indirect=True)
def test_load_url_unsupported_input_type(monkeypatch: MonkeyPatch, response_fixture: MockHttpResponse):
    """
    Test hoppr.net.load_url function failure due to unsupported input
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    with pytest.raises(expected_exception=HopprLoadDataError):
        hoppr.net.load_url(url="url_goes_here")


@pytest.mark.parametrize(
    argnames="response_fixture", argvalues=[{"content": json.dumps(test_input_data).encode("utf-8")}], indirect=True
)
def test_load_url_bytes(monkeypatch: MonkeyPatch, response_fixture: MockHttpResponse):
    """
    Test hoppr.net.load_url function with byte data
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    content = hoppr.net.load_url(url="url_goes_here")
    assert content == test_input_data


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": "BAD DATA"}], indirect=True)
def test_load_url_bad_data(monkeypatch: MonkeyPatch, response_fixture: MockHttpResponse):
    """
    Test hoppr.net.load_url function raises exception given bad data
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    with pytest.raises(expected_exception=HopprLoadDataError):
        hoppr.net.load_url(url="url_goes_here")


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": b"stuff"}], indirect=True)
def test_download_file(
    monkeypatch: MonkeyPatch, find_credentials_fixture: CredentialRequiredService, response_fixture: MockHttpResponse
):
    """
    Test hoppr.net.download_file function
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setenv(name="MOCK_PASS_ENV", value="testPassword")

    resp = hoppr.net.download_file(
        url="http://dummy.com",
        dest="whereever",
        creds=CredentialRequiredService.parse_obj(
            {"url": "http://dummy.com", "user": "testUser", "pass_env": "MOCK_PASS_ENV"}
        ),
    )
    assert resp.status_code == 200


@pytest.mark.parametrize(argnames="response_fixture", argvalues=[{"content": b"stuff"}], indirect=True)
def test_download_file_without_credentials(monkeypatch: MonkeyPatch, response_fixture: MockHttpResponse):
    """
    Test hoppr.net.download_file function without credentials
    """
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)
    resp = hoppr.net.download_file(url="http://dummy.com", dest="whereever")
    assert resp.status_code == 200
