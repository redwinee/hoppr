"""
Test module for MemoryLogger class
"""
import logging
import multiprocessing
import os
import re

from pathlib import Path

import pytest

from pytest import FixtureRequest

from hoppr.mem_logger import MemoryLogger

LOG_PATTERNS = [
    r".*\[INFO\].*test info message.*",
    r".*\[WARNING\].*test warning message.*",
    r".*\[ERROR\].*test error message.*",
    r".*\[CRITICAL\].*test fatal message.*",
    r".*\[CRITICAL\].*test critical message.*",
]


@pytest.fixture(name="mem_logger", scope="function")
def mem_logger_fixture(request: FixtureRequest) -> MemoryLogger:
    """
    Fixture to return MemoryLogger instance
    """

    class HopprTestLogger:  # pylint: disable=too-few-public-methods
        """Class to test log name generation"""

        def get_logger(self) -> MemoryLogger:
            """Return a MemoryLogger instance"""
            return MemoryLogger(**param_dict)

    param_dict = dict(getattr(request, "param", {}))

    # Set default params for MemoryLogger
    param_dict["file_name"] = param_dict.get("file_name", "hoppr1.log")
    param_dict["lock"] = param_dict.get("lock", None)
    param_dict["log_name"] = param_dict.get("log_name", None)
    param_dict["log_level"] = param_dict.get("log_level", logging.INFO)
    param_dict["flush_immed"] = param_dict.get("flush_immed", False)

    mem_logger = HopprTestLogger().get_logger()
    mem_logger.debug("test debug message")
    mem_logger.info("test info message")
    mem_logger.warning("test warning message")
    mem_logger.error("test error message")
    mem_logger.fatal("test fatal message")
    mem_logger.critical("test critical message")

    return mem_logger


@pytest.mark.parametrize(
    argnames="mem_logger", argvalues=[dict(file_name="hoppr1.log", lock=multiprocessing.RLock())], indirect=True
)
def test_logging(mem_logger: MemoryLogger):
    """
    Test log methods at INFO log level
    """
    mem_logger.flush()
    mem_logger.close()

    captured = Path("hoppr1.log").read_text(encoding="utf-8")
    assert re.match(pattern="\n".join(LOG_PATTERNS), string=captured)

    os.remove("hoppr1.log")


@pytest.mark.parametrize(
    argnames="mem_logger",
    argvalues=[dict(file_name="hoppr1.log", lock=multiprocessing.RLock(), log_level=logging.DEBUG)],
    indirect=True,
)
def test_verbose_logging(mem_logger: MemoryLogger):
    """
    Test log methods at DEBUG log level
    """
    mem_logger.flush()
    mem_logger.close()

    captured = Path("hoppr1.log").read_text(encoding="utf-8")
    assert re.match(pattern="\n".join([r".*\[DEBUG\].*test debug message.*", *LOG_PATTERNS]), string=captured)

    os.remove("hoppr1.log")


@pytest.mark.parametrize(
    argnames="mem_logger", argvalues=[dict(file_name="hoppr2.log", lock=multiprocessing.RLock())], indirect=True
)
def test_clear_log(mem_logger: MemoryLogger):
    """
    Test MemoryLogger.clear_targets method
    """
    mem_logger.clear_targets()
    mem_logger.close()

    captured = Path("hoppr2.log").read_text(encoding="utf-8")
    assert re.match(pattern=r"\s*", string=captured)

    os.remove("hoppr2.log")


@pytest.mark.parametrize(argnames="mem_logger", argvalues=[dict(file_name="hoppr1.log", lock=None)], indirect=True)
def test_logging_nolock(mem_logger: MemoryLogger):
    """
    Test log methods with no lock
    """
    mem_logger.flush()
    mem_logger.close()

    captured = Path("hoppr1.log").read_text(encoding="utf-8")
    assert re.match(pattern="\n".join(LOG_PATTERNS), string=captured)

    os.remove("hoppr1.log")


@pytest.mark.parametrize(
    argnames="mem_logger",
    argvalues=[
        dict(file_name="hoppr2.log", lock=None, flush_immed=True),
    ],
    indirect=True,
)
def test_clear_log_nolock(mem_logger: MemoryLogger):
    """
    Test MemoryLogger.clear_targets method with no lock
    """
    mem_logger.clear_targets()
    mem_logger.close()

    captured = Path("hoppr2.log").read_text(encoding="utf-8")
    assert re.match(pattern=r"\s*", string=captured)

    os.remove("hoppr2.log")
