"""
Test module for CollectGitPlugin class
"""
# pylint: disable=redefined-outer-name,too-many-arguments,unused-argument,duplicate-code

from subprocess import CompletedProcess
from typing import Dict, List

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Scope
from pytest import MonkeyPatch

import hoppr.plugin_utils

from hoppr.core_plugins.collect_git_plugin import CollectGitPlugin
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Component
from hoppr.result import Result, ResultStatus


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectGitPlugin)])
def plugin_fixture(plugin_fixture: CollectGitPlugin) -> CollectGitPlugin:
    """
    Override and parametrize plugin_fixture to return CollectGitPlugin
    """

    return plugin_fixture


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """

    return Component(name="TestComponent", purl="pkg:git/something/else@1.2.3", type="file")  # type: ignore


@pytest.fixture(name="excluded_component")
def excluded_component_fixture() -> Component:
    """
    Test Component with Scope Excluded fixture
    """
    return Component(
        name="TestExcludedComponent",
        purl="pkg:git/something/not-needed@4.5.6",
        type="file",  # type: ignore
        scope=Scope.excluded,
    )


def get_repos(*args, **kwargs) -> List[str]:
    """
    Mock _get_repos method
    """

    return ["http://somewhere.com", "https://somewhere.com"]


def get_ssh_repo(*args, **kwargs) -> List[str]:
    """
    Mock _get_repos_method but instead return with an ssh scheme
    """

    return ["ssh://somewhere.com"]


@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_fixture"],
    argvalues=[(dict(git_command="git"), dict(returncode=0))],
    indirect=True,
)
def test_collect_git_success(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: Dict[str, str],
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of the Git Collector
    """

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collect_git_fail(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
):
    """
    Test a failing run of the Git Collector
    """

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to clone")


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collector_git_update_fail(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    find_credentials_fixture: CredentialRequiredService,
    run_command_fixture: CompletedProcess,
):
    """
    Test git collector is able to clone but not able to commit changes
    """

    def mock_clone(*args, **kwargs):
        return Result(status=ResultStatus.SUCCESS)

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_ssh_repo)
    monkeypatch.setattr(target=plugin_fixture, name="git_clone", value=mock_clone)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith(
        "Failure after 3 attempts, final message Failed to make the clone usable as a remote"
    )


def test_get_version(plugin_fixture: CollectGitPlugin):
    """
    Test git collector get version method
    """
    assert len(plugin_fixture.get_version()) > 0


def test_collector_git_command_not_found(
    plugin_fixture: CollectGitPlugin, component: Component, monkeypatch: MonkeyPatch
):
    """
    Test git collector with missing required command
    """

    def mock_bad_result(*args, **kwargs):
        return Result.fail("[mock] command not found")

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=mock_bad_result)
    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "[mock] command not found"


@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_fixture"],
    argvalues=[(dict(git_command="git"), dict(returncode=0))],
    indirect=True,
)
def test_collect_git_ssh_success(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: Dict[str, str],
    find_credentials_fixture: CredentialRequiredService,
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of the Git Collector with ssh
    """

    # pylint: disable=too-many-arguments
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_ssh_repo)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_fixture"],
    argvalues=[(dict(git_command="git", custom_git_clone_options="--depth 2"), dict(returncode=0))],
    indirect=True,
)
def test_collect_git_custom_config_success(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: Dict[str, str],
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of the Git Collector
    """

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_fixture"],
    argvalues=[(dict(git_command="git", depth="50"), dict(returncode=0))],
    indirect=True,
)
def test_collect_git_set_depth_success(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: Dict[str, str],
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of the Git Collector
    """

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


def test_collect_git_no_config_success(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of no config
    """
    plugin_fixture.config = None
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


def test_collector_excluded_git(
    plugin_fixture: CollectGitPlugin, excluded_component: Component, monkeypatch: MonkeyPatch
):
    """
    Test git collector skip run given a purl with a scope of excluded
    """
    collect_result = plugin_fixture.process_component(excluded_component)
    assert collect_result.is_excluded(), f"Expected EXCLUDED result, got {collect_result}"


@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_fixture"],
    argvalues=[(dict(git_command="git", depth="50"), dict(returncode=0))],
    indirect=True,
)
def test_collect_git_pass_version_success(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: Dict[str, str],
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of the Git Collector
    """
    component.version = 'v1.2.3'
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
