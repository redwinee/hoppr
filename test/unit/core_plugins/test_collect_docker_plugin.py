"""
Test module for CollectDockerPlugin class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

import os

from subprocess import CompletedProcess

import pytest

from pytest import MonkeyPatch
from pytest_mock import MockerFixture

import hoppr.plugin_utils

from hoppr.core_plugins.collect_docker_plugin import CollectDockerPlugin
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Component
from hoppr.result import Result


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectDockerPlugin)])
def plugin_fixture(plugin_fixture: CollectDockerPlugin) -> CollectDockerPlugin:
    """
    Override and parametrize plugin_fixture to return CollectDockerPlugin
    """

    return plugin_fixture


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """

    return Component(name="TestComponent", purl="pkg:docker/something/else@1.2.3", type="file")  # type: ignore


def mock_get_repos(comp: Component) -> list[str]:
    """
    Mock _get_repos method
    """

    return ["http://somewhere.com", "https://somewhere.com"]


@pytest.mark.parametrize(
    argnames="completed_process_fixture",
    argvalues=[{"returncode": 0}],
    indirect=True,
)
def test_collect_docker_success(
    plugin_fixture: CollectDockerPlugin,
    component: Component,
    completed_process_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
    mocker: MockerFixture,
):
    """
    Test a successful run of the Docker Collector
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=mock_get_repos)

    mock_run_command = mocker.patch.object(
        target=plugin_fixture, attribute="run_command", return_value=completed_process_fixture
    )

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"

    mock_run_command.assert_called_once_with(
        [
            "skopeo",
            "copy",
            "--src-tls-verify=false",
            "docker://somewhere.com/something/else:1.2.3",
            (
                f"docker-archive:{plugin_fixture.context.collect_root_dir}/"
                "docker/http%3A%2F%2Fsomewhere.com/something/else_1.2.3:somewhere.com/"
                "something/else:1.2.3"
            ),
        ],
        [],
    )


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collect_docker_fail(
    plugin_fixture: CollectDockerPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
):
    """
    Test a failing run of the Docker Collector
    """

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=mock_get_repos)
    monkeypatch.setattr(target=os.path, name="exists", value=lambda path: True)
    monkeypatch.setattr(target=os, name="remove", value=lambda path: None)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith(
        "Failure after 3 attempts, final message Skopeo failed to copy docker image"
    )


def test_get_version(plugin_fixture: CollectDockerPlugin):
    """
    Test get_version method
    """

    assert len(plugin_fixture.get_version()) > 0


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(skopeo_command="skopeo")], indirect=True)
def test_collect_docker_command_not_found(
    plugin_fixture: CollectDockerPlugin,
    config_fixture: dict[str, str],
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
):
    """
    Test if the required command is not found
    """
    # pylint: disable=duplicate-code
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=mock_get_repos)
    monkeypatch.setattr(
        target=hoppr.plugin_utils,
        name="check_for_missing_commands",
        value=lambda message: Result.fail("[mock] command not found"),
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "[mock] command not found"
