"""
Test module for CollectPypiPlugin class
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

import importlib.util

from pathlib import Path
from subprocess import CompletedProcess

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Scope
from pytest import MonkeyPatch
from pytest_mock import MockerFixture

import hoppr.plugin_utils

from hoppr.core_plugins.collect_pypi_plugin import CollectPypiPlugin
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Component, Repository
from hoppr.models.types import PurlType
from hoppr.result import Result


@pytest.fixture(name="component")
def component_fixture():
    """
    Test Component fixture
    """
    return Component(name="TestComponent", purl="pkg:pypi/hoppr/hippo@1.2.3", type="file")  # type: ignore


@pytest.fixture(name="excluded_component")
def excluded_component_fixture() -> Component:
    """
    Test Component with Scope Excluded fixture
    """
    return Component(
        name="TestExcludedComponent",
        purl="pkg:pypi/something/not-needed@4.5.6",
        type="file",  # type: ignore
        scope=Scope.excluded,
    )


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectPypiPlugin)])
def plugin_fixture(plugin_fixture: CollectPypiPlugin, monkeypatch: MonkeyPatch, tmp_path: Path) -> CollectPypiPlugin:
    """
    Override and parametrize plugin_fixture to return CollectPypiPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(
        target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://pypi.hoppr.com/hoppr/pypi"]
    )

    plugin_fixture.context.collect_root_dir = tmp_path
    plugin_fixture.context.repositories[PurlType.RPM] = [
        Repository.parse_obj(dict(url="https://somewhere.com", description=""))
    ]

    return plugin_fixture


def test_collect_pypi_repository_url_fail(
    plugin_fixture: CollectPypiPlugin,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: fail
    """
    monkeypatch.setattr(target=plugin_fixture, name="check_purl_specified_url", value=Result.fail)
    monkeypatch.setattr(target=plugin_fixture, name="_run_cmd_wrapper", value=lambda: False)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"


def test_collect_pypi_success(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: success
    """
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


def test_collect_pypi_only_binary(plugin_fixture: CollectPypiPlugin, component: Component, mocker: MockerFixture):
    """
    Test ensure binary is only downloaded and source is not
    """
    collect_binary = mocker.patch.object(plugin_fixture, plugin_fixture.collect_binary_only.__name__, return_value=True)
    # Note that collect_src `return_value` is set to "True" here to verify that call_count is later still 0
    collect_src = mocker.patch.object(plugin_fixture, plugin_fixture.collect_source.__name__, return_value=True)

    collect_result = plugin_fixture.process_component(component)

    assert collect_binary.call_count == 1
    assert collect_binary.return_value is True
    assert collect_src.call_count == 0
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


def test_collect_pypi_binary_fail_src_success(
    plugin_fixture: CollectPypiPlugin, component: Component, mocker: MockerFixture
):
    """
    Test ensure source is downloaded if binary download fails
    """
    collect_binary = mocker.patch.object(
        plugin_fixture, plugin_fixture.collect_binary_only.__name__, return_value=False
    )
    collect_src = mocker.patch.object(plugin_fixture, plugin_fixture.collect_source.__name__, return_value=True)

    collect_result = plugin_fixture.process_component(component)

    assert collect_binary.call_count == 1
    assert collect_binary.return_value is False
    assert collect_src.call_count == 1
    assert collect_src.return_value is True
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[dict(returncode=1)], indirect=True)
def test_collect_pypi_fail(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: fail
    """
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to download")


def test_collect_pypi_pip_not_found(plugin_fixture: CollectPypiPlugin, component: Component, monkeypatch: MonkeyPatch):
    """
    Test collect method: pip not found
    """
    monkeypatch.setattr(target=importlib.util, name="find_spec", value=lambda *args, **kwargs: None)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail()
    assert collect_result.message == "The pip package was not found. Please install and try again."


def test_collector_excluded_pypi(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    excluded_component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test raw collector skip run given a purl with a scope of excluded
    """
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    collect_result = plugin_fixture.process_component(excluded_component)
    assert collect_result.is_excluded(), f"Expected EXCLUDED result, got {collect_result}"


def test_get_version(plugin_fixture: CollectPypiPlugin):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0
