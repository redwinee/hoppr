"""
Test module for CollectHelmPlugin class
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import test

from pathlib import Path
from subprocess import CompletedProcess
from typing import Callable

import pytest

from pytest import FixtureRequest, MonkeyPatch

import hoppr.core_plugins.collect_helm_plugin
import hoppr.plugin_utils
import hoppr.utils

from hoppr.core_plugins.collect_helm_plugin import CollectHelmPlugin
from hoppr.models import HopprContext
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Component, Repository
from hoppr.models.types import PurlType
from hoppr.result import Result

sbom_path = Path(test.__file__).parent / "resources" / "bom" / "int_helm_bom.json"


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl = request.param if hasattr(request, "param") else "pkg:helm/something/else@1.2.3"
    return Component(name="TestHelmComponent", purl=purl, type="file")  # type: ignore


@pytest.fixture(scope="function")
def load_file_fixture(request: FixtureRequest) -> Callable:
    """
    Test yaml.safe_load fixture
    """

    def _load_file(input_file_path: Path) -> list | dict:
        data: list[dict[str, str]] = [
            dict(url="https://charts.hoppr.com/hoppr", name="hoppr"),
            dict(url="https://charts.hoppr.com/stable", name="stable"),
        ]

        if request.param == "raise":
            return data

        return dict(repositories=data)

    return _load_file


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectHelmPlugin)])
def plugin_fixture(plugin_fixture: CollectHelmPlugin, monkeypatch: MonkeyPatch) -> CollectHelmPlugin:
    """
    Override and parametrize plugin_fixture to return CollectHelmPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere.com"])

    plugin_fixture.context.repositories[PurlType.HELM] = [
        Repository.parse_obj(dict(url="https://somewhere.com", description=""))
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "config_fixture"],
    argvalues=[(dict(returncode=0), dict(helm_command="helm")), (dict(returncode=1), dict(helm_command="helm"))],
    indirect=True,
)
# pylint: disable=too-many-arguments
def test_collect_helm(
    plugin_fixture: CollectHelmPlugin,
    config_fixture: dict,
    completed_process_fixture: CompletedProcess,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    if completed_process_fixture.returncode == 0:
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
    else:
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message.startswith(
            "Failure after 3 attempts, final message Failed to download else version 1.2.3 helm chart"
        )


@pytest.mark.parametrize(
    argnames=["context_fixture", "load_file_fixture"],
    argvalues=[(dict(strict_repos=False), "load"), (dict(strict_repos=False), "raise")],
    indirect=True,
)
def test_helm_no_strict(
    context_fixture: HopprContext,
    load_file_fixture: list[dict[str, str]],
    monkeypatch: MonkeyPatch,
):
    """
    Test plugin creation with --no-strict flag
    """
    monkeypatch.setattr(target=context_fixture, name="strict_repos", value=False)
    monkeypatch.setattr(target=Path, name="exists", value=lambda self: True)
    monkeypatch.setattr(target=hoppr.core_plugins.collect_helm_plugin.utils, name="load_file", value=load_file_fixture)

    plugin = CollectHelmPlugin(context=context_fixture, config=None)
    assert plugin.system_repositories == ["https://charts.hoppr.com/hoppr", "https://charts.hoppr.com/stable"]


def test_get_version(plugin_fixture: CollectHelmPlugin):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0
