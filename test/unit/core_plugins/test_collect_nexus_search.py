"""
Unit tests for the Nexus Search Collector
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument


from pathlib import Path
from test.mock_objects import MockHttpResponse

import pytest

from packageurl import PackageURL
from pytest import FixtureRequest, MonkeyPatch
from pytest_mock import MockerFixture
from requests.auth import HTTPBasicAuth

from hoppr.core_plugins.collect_nexus_search import CollectNexusSearch
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Component


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl_string = getattr(request, "param", "pkg:rpm/test_component@0.1.2")
    purl = PackageURL.from_string(purl_string)
    return Component.parse_obj(dict(name=purl.name, version=purl.version, purl=purl_string, type="file"))


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectNexusSearch)])
def plugin_fixture(plugin_fixture: CollectNexusSearch, config_fixture: dict[str, str]) -> CollectNexusSearch:
    """
    Override and parametrize plugin_fixture to return CollectNexusSearch
    """
    return plugin_fixture


def test_get_version(plugin_fixture: CollectNexusSearch):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(purl_types=["rpm", "pip"])], indirect=True)
def test_collect_nexus_success(
    plugin_fixture: CollectNexusSearch,
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    mocker: MockerFixture,
    monkeypatch: MonkeyPatch,
):
    """
    Test a successful call to collect_nexus
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)

    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=True)
    mocker.patch.object(
        CollectNexusSearch, "get_download_urls", return_value=["https://mock_nexus.com/repository/test/testobj.txt"]
    )
    mocker.patch(
        "hoppr.core_plugins.collect_nexus_search.download_file",
        return_value=MockHttpResponse(200, content="mocked content"),
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


def test_collect_nexus_not_nexus(plugin_fixture: CollectNexusSearch, component: Component, mocker: MockerFixture):
    """
    Test calling collect_nexus on a non-nexus repository
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=False)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "https://mock_nexus.com is not a Nexus instance"


@pytest.mark.parametrize(argnames="component", argvalues=["pkg:git/something/else@1.2.3"], indirect=True)
def test_collect_nexus_git_not_supported(
    plugin_fixture: CollectNexusSearch, component: Component, mocker: MockerFixture
):
    """
    Test calling collect_nexus for a git purl
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=True)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_skip(), f"Expected SKIP result, got {collect_result}"
    assert collect_result.message == "Class CollectNexusSearch does not support purl type git"


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(purl_types=["rpm", "pip"])], indirect=True)
def test_collect_nexus_nothing_found(plugin_fixture: CollectNexusSearch, component: Component, mocker: MockerFixture):
    """
    Test calling collect_nexus with no artifact found
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=True)
    mocker.patch.object(CollectNexusSearch, "get_download_urls", return_value=[])
    mocker.patch(
        "hoppr.core_plugins.collect_nexus_search.download_file",
        return_value=MockHttpResponse(200, content="mocked content"),
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == (
        "No artifacts found in Nexus instance https://mock_nexus.com for purl pkg:rpm/test_component@0.1.2"
    )


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(purl_types=["rpm", "pip"])], indirect=True)
def test_collect_nexus_download_fail(plugin_fixture: CollectNexusSearch, component: Component, mocker: MockerFixture):
    """
    Test calling collect_nexus with a failure on download
    """
    mocker.patch.object(CollectNexusSearch, "_get_repos", return_value=["https://mock_nexus.com"])
    mocker.patch.object(CollectNexusSearch, "is_nexus_instance", return_value=True)
    mocker.patch.object(
        CollectNexusSearch, "get_download_urls", return_value=["https://mock_nexus.com/repository/test/testobj.txt"]
    )
    mocker.patch(
        "hoppr.core_plugins.collect_nexus_search.download_file", return_value=MockHttpResponse(404, content="not found")
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "HTTP Status Code: 404; "


@pytest.mark.parametrize(
    argnames=["purl_string", "expected_directory"],
    argvalues=[
        (
            "pkg:docker/namespace/image@1.2.3",
            Path("docker/https%3A%2F%2Fmy-nexus.com%2Frepository%2Fmy_repo/the_path"),
        ),
        (
            "pkg:pypi/namespace/module.py@1.2.3",
            Path("pypi/https%3A%2F%2Fmy-nexus.com%2Frepository%2Fmy_repo/module.py_1.2.3"),
        ),
    ],
)
def test_directory_for_nexus(plugin_fixture: CollectNexusSearch, purl_string: str, expected_directory: Path):
    """
    Test the directory_for_nexus method
    """
    repo_url = "https://my-nexus.com/repository/my_repo/the_path/file.txt"

    purl: PackageURL = PackageURL.from_string(purl_string)  # pyright: ignore[reportGeneralTypeIssues]
    assert plugin_fixture._directory_for_nexus(purl, repo_url) == (
        plugin_fixture.context.collect_root_dir / expected_directory
    )


def test_is_nexus(mocker: MockerFixture, cred_object_fixture: CredentialRequiredService):
    """
    Test the is_nexus method on success
    """
    mocker.patch("hoppr.core_plugins.collect_nexus_search.sleep")
    mock_request_get = mocker.patch("requests.get")
    mock_request_get.side_effect = [MockHttpResponse(500, content="network issue"), MockHttpResponse(200)]

    auth = HTTPBasicAuth(
        username=cred_object_fixture.username, password=cred_object_fixture.password.get_secret_value()
    )
    assert CollectNexusSearch.is_nexus_instance("http://my-nexus.com", auth)


def test_is_not_nexus(mocker: MockerFixture, cred_object_fixture: CredentialRequiredService):
    """
    Test the is_nexus method on failure
    """
    mocker.patch("hoppr.core_plugins.collect_nexus_search.sleep")
    mocker.patch("requests.get", return_value=MockHttpResponse(404, content="not found"))

    auth = HTTPBasicAuth(
        username=cred_object_fixture.username, password=cred_object_fixture.password.get_secret_value()
    )
    assert not CollectNexusSearch.is_nexus_instance("http://my-nexus.com", auth)


def test_is_nexus_multifail(mocker: MockerFixture, cred_object_fixture: CredentialRequiredService):
    """
    Test the is_nexus method with network failures
    """
    mocker.patch("hoppr.core_plugins.collect_nexus_search.sleep")
    mocker.patch("requests.get", return_value=MockHttpResponse(500, content="network issue"))

    auth = HTTPBasicAuth(
        username=cred_object_fixture.username, password=cred_object_fixture.password.get_secret_value()
    )
    assert not CollectNexusSearch.is_nexus_instance("http://my-nexus.com", auth)


@pytest.mark.parametrize(
    argnames=["purl_string", "expected_urls"],
    argvalues=[
        ("pkg:deb/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:gems/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:golang/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:generic/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:rpm/namespace/file.txt@1.2.3", ["http://my-nexus.com/repository/my-repo/file.ext"]),
        (
            "pkg:maven/namespace/file.txt@1.2.3",
            [
                "http://my-nexus.com/repository/my-repo/file.ext",
                "http://my-nexus.com/repository/my-repo/file.ext",
                "http://my-nexus.com/repository/my-repo/file.ext",
            ],
        ),
    ],
)
def test_get_download_urls(purl_string: str, expected_urls: list[str], mocker: MockerFixture):
    """
    Test the get_download_urls method
    """
    search_result = '{"items": [{ "downloadUrl": "http://my-nexus.com/repository/my-repo/file.ext"}]}'
    mocker.patch("requests.get", return_value=MockHttpResponse(200, content=search_result))

    purl: PackageURL = PackageURL.from_string(purl_string)  # pyright: ignore[reportGeneralTypeIssues]
    assert CollectNexusSearch.get_download_urls(purl, "http://my-nexus.com/repository/my-repo") == expected_urls


@pytest.mark.parametrize(
    argnames=["purl_string", "expected_urls"],
    argvalues=[
        ("pkg:deb/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:gems/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:golang/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:generic/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        ("pkg:rpm/namespace/file.txt@1.2.3", ["https://my-nexus.com/repository/my-repo/file.ext"]),
        (
            "pkg:maven/namespace/file.txt@1.2.3",
            [
                "https://my-nexus.com/repository/my-repo/file.ext",
                "https://my-nexus.com/repository/my-repo/file.ext",
                "https://my-nexus.com/repository/my-repo/file.ext",
            ],
        ),
    ],
)
def test_get_download_urls_https(purl_string: str, expected_urls: list[str], mocker: MockerFixture):
    """
    Test the get_download_urls method
    """
    search_result = '{"items": [{ "downloadUrl": "https://my-nexus.com/repository/my-repo/file.ext"}]}'
    mocker.patch("requests.get", return_value=MockHttpResponse(200, content=search_result))

    purl: PackageURL = PackageURL.from_string(purl_string)  # pyright: ignore[reportGeneralTypeIssues]
    assert CollectNexusSearch.get_download_urls(purl, "https://my-nexus.com/repository/my-repo") == expected_urls


def test_get_attestation_prods_config():
    """
    Test getting attestation products as configured by the user
    """
    config = {"purl_types": ["alpha", "bravo"]}

    prods = CollectNexusSearch.get_attestation_products(config)

    assert prods == ["alpha/*", "bravo/*"]


def test_get_attestation_prods_no_config():
    """
    Test getting default attestation products
    """
    prods = CollectNexusSearch.get_attestation_products()

    assert prods == [
        "deb/*",
        "docker/*",
        "generic/*",
        "golang/*",
        "helm/*",
        "maven/*",
        "npm/*",
        "pypi/*",
        "repo/*",
        "rpm/*",
    ]
