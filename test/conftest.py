"""
Shareable fixtures for unit tests
"""
from __future__ import annotations

import json
import multiprocessing
import time

from copy import deepcopy
from pathlib import Path
from subprocess import CompletedProcess
from typing import Callable

import pytest

from pytest import FixtureRequest, MonkeyPatch, TempPathFactory

from hoppr.base_plugins.hoppr import HopprPlugin
from hoppr.models import HopprContext
from hoppr.models.credentials import CredentialRequiredService
from hoppr.models.manifest import Manifest, Sbom

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument


@pytest.fixture(autouse=True)
def print_test_name(request: FixtureRequest):
    """
    Print name of test
    """
    print(f"-------- Starting {request.node.name} --------")


@pytest.fixture(autouse=True)
def sleepless(monkeypatch: MonkeyPatch):
    """
    Spend less time in sleep
    """
    monkeypatch.setattr(target=time, name="sleep", value=lambda secs: None)
    
@pytest.fixture(scope="function")
def collect_root_dir_fixture(request: FixtureRequest, tmp_path_factory: TempPathFactory) -> Path:
    """
    Parametrizable temp dir fixture
    """
    basename = getattr(request, "param", "hoppr_unit_test")
    return tmp_path_factory.mktemp(basename)


@pytest.fixture
def completed_process_fixture(request: FixtureRequest) -> CompletedProcess:
    """
    CompletedProcess fixture for subprocess commands

    Intended for use with `pytest` test modules. For `unittest.TestCase` based
    classes, use the `test.mock_objects.MockSubprocessRun` mock object.
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["args"] = param_dict.get("args", ["mock", "command"])
    param_dict["returncode"] = param_dict.get("returncode", 0)
    param_dict["stdout"] = param_dict.get("stdout", b"Mock stdout")
    param_dict["stderr"] = param_dict.get("stderr", b"Mock stderr")

    return CompletedProcess(**param_dict)


@pytest.fixture
def config_fixture(request: FixtureRequest) -> dict[str, str]:
    """
    Test plugin config fixture
    """
    param_dict = dict(getattr(request, "param", {}))
    return param_dict


@pytest.fixture
def context_fixture(
    request: FixtureRequest, manifest_fixture: Manifest, collect_root_dir_fixture: Path
) -> HopprContext:
    """
    Test Context fixture
    """
    sbom_path = Path(__file__).parent / "resources" / "bom" / "unit_bom1_mini.json"
    strict_repos = True

    if hasattr(request, "cls") and request.cls is not None:
        sbom_path = getattr(request.cls, "sbom_path", sbom_path)
        strict_repos = getattr(request.cls, "strict_repos", strict_repos)
    else:
        param_dict = dict(getattr(request, "param", {}))
        sbom_path = param_dict.get("sbom_path", sbom_path)
        strict_repos = param_dict.get("strict_repos", strict_repos)

    with sbom_path.open(mode="r", encoding="utf-8") as bom_file:
        bom_dict = json.load(fp=bom_file)

    sbom = Sbom(**bom_dict)

    return HopprContext(
        collect_root_dir=collect_root_dir_fixture,
        consolidated_sbom=sbom,
        credential_required_services=None,
        delivered_sbom=deepcopy(sbom),
        logfile_lock=multiprocessing.Manager().RLock(),
        max_processes=3,
        repositories=manifest_fixture.repositories,
        sboms=list(Sbom.loaded_sboms.values()),
        stages=[],
    )


@pytest.fixture
def cred_object_fixture(request: FixtureRequest, monkeypatch: MonkeyPatch) -> CredentialRequiredService:
    """
    Test CredObject fixture
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["url"] = param_dict.get("url", "https://test.hoppr.com")
    param_dict["user"] = param_dict.get("user", "mock_user")
    param_dict["user_env"] = param_dict.get("pass_env", "MONKEYPATCH_USER_ENV")
    param_dict["pass_env"] = param_dict.get("pass_env", "MONKEYPATCH_PASS_ENV")
    param_dict["password"] = param_dict.get("password", "mock_password")

    monkeypatch.setenv(name=param_dict["user_env"], value=param_dict["user"])
    monkeypatch.setenv(name=param_dict["pass_env"], value=param_dict["password"])

    return CredentialRequiredService.parse_obj(param_dict)


@pytest.fixture
def find_credentials_fixture(
    cred_object_fixture: CredentialRequiredService,
) -> Callable[[str], CredentialRequiredService]:
    """
    Fixture to use when monkeypatching `Credentials.find` method
    """

    def _find_credentials(url: str) -> CredentialRequiredService:
        return cred_object_fixture

    return _find_credentials


@pytest.fixture
def manifest_fixture(request: FixtureRequest) -> Manifest:
    """
    Test Manifest fixture
    """
    manifest_path = Path(__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"

    if hasattr(request, "cls") and request.cls is not None:
        manifest_path = getattr(request.cls, "manifest_path", manifest_path)
    else:
        param_dict = dict(getattr(request, "param", {}))
        manifest_path = param_dict.get("manifest_path", manifest_path)

    return Manifest.load(manifest_path)


@pytest.fixture
def plugin_fixture(
    request: FixtureRequest, config_fixture: dict[str, str], context_fixture: HopprContext
) -> HopprPlugin | None:
    """
    # Test collector plugin fixture

    ## Usage

    ### pytest

    To use this fixture in a pytest module, override this fixture in the test module,
    specifying the type of Hoppr plugin to return in the fixture params as a dict of
    the form `{"plugin_class": <Hoppr plugin class type>}`. For example:

    ```python
    # test_collect_raw.py

    @pytest.fixture(scope="function", params=[dict(plugin_class=CollectRawPlugin)])
    def plugin_fixture(plugin_fixture: CollectRawPlugin, monkeypatch: MonkeyPatch) -> CollectRawPlugin:
        monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
        return plugin_fixture
    ```

    In this overridden fixture, you can monkeypatch attributes that are common to all
    functions in the test module.

    ### unittest

    For unittest.TestCase based classes, declare the following class attributes:

    - `PluginClass = <Hoppr plugin class type>`
    - `plugin_fixture: PluginClass`

    and decorate the test class with `@pytest.mark.usefixtures("plugin_fixture")`. For example:

    ```python
    @pytest.mark.usefixtures("plugin_fixture")
    class TestCollectRawPlugin(unittest.TestCase):
        PluginClass = CollectRawPlugin
        plugin_fixture: PluginClass

        def test_collect_raw(self):
            self.plugin.config = dict(config_option="config value")
            ...
    ```
    """
    # Fixture requested by a class
    if hasattr(request, "cls") and request.cls is not None:
        plugin_class = getattr(request.cls, "PluginClass", None)
        if plugin_class is None:
            raise pytest.UsageError(f"The PluginClass attribute was not found in class {request.cls}")
    # Fixture requested by a function decorated with `@pytest.mark.parametrize`
    else:
        param_dict = dict(getattr(request, "param", {}))
        plugin_class = param_dict.get("plugin_class")
        if plugin_class is None:
            raise pytest.UsageError("The plugin_class fixture param was not specified")

    if not issubclass(plugin_class, HopprPlugin):
        raise pytest.UsageError(f"Class {plugin_class} is not a subclass of HopprPlugin")

    plugin_obj = plugin_class(context=context_fixture, config=config_fixture)

    # Set the `plugin_fixture` class attribute if fixture requested from a unittest-based class
    if hasattr(request, "cls") and request.cls is not None:
        request.cls.plugin_fixture = plugin_obj

    return plugin_obj


@pytest.fixture
def run_command_fixture(
    completed_process_fixture: CompletedProcess,
) -> Callable[[list[str], list[str] | None, str | None], CompletedProcess]:
    """
    Override completed_process_fixture to return Callable
    """

    def _run_command(
        command: list[str], password_list: list[str] | None = None, cwd: str | None = None, timeout: float | None = 60
    ) -> CompletedProcess:
        return completed_process_fixture

    return _run_command
