"""
init file for the 'hoppr.core_plugins.report_generator' module
"""

from hoppr.core_plugins.report_generator.report_generator import Report, ReportGenerator

__all__ = ["Report", "ReportGenerator"]
