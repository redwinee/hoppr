"""
Entrypoint if executed as a module (`python -m hoppr`)
"""
from hoppr.cli.hopctl import app  # pragma: no cover

app(prog_name="hopctl")  # pragma: no cover
